module io.gitlab.jfronny.commons.flow.backend.unsafe {
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.unsafe;
    requires io.gitlab.jfronny.commons.flow;
    provides io.gitlab.jfronny.commons.flow.spi.FlowBackend with io.gitlab.jfronny.commons.flow.unsafe.UnsafeBackend;
}
