package io.gitlab.jfronny.commons.flow.unsafe;

import io.gitlab.jfronny.commons.flow.Continuation;
import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.commons.unsafe.reflect.Reflect;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A class intended to behave like a JVM Continuation but without the need to use the internal API.
 * The exact implementation may need to change in the future to adapt to changes in the internal API or how we access it.
 * For example, if our encapsulation bypass stops working, we may need to rely on threads to simulate continuations like <a href="https://github.com/forax/loom-fiber/blob/master/src/main/java/fr/umlv/loom/continuation/Continuation.java">here</a>.
 */
public class UnsafeContinuation implements Continuation {
    protected static final Class DELEGATE = Try.orThrow(() -> Class.forName("jdk.internal.vm.Continuation"));
    private static final BiFunction<Object, Runnable, Object> CONSTRUCTOR = Try.orThrow(() -> Reflect.constructor(DELEGATE, UnsafeContinuationScope.DELEGATE, Runnable.class));
    private static final Function<Object, Object> GET_CURRENT = Try.orThrow(() -> Reflect.staticFunction(DELEGATE, "getCurrentContinuation", DELEGATE, UnsafeContinuationScope.DELEGATE));
    private static final Consumer<Object> RUN = Try.orThrow(() -> Reflect.instanceProcedure(DELEGATE, "run"));
    private static final Function<Object, Boolean> YIELD = Try.orThrow(() -> Reflect.staticFunction(DELEGATE, "yield", boolean.class, UnsafeContinuationScope.DELEGATE));
    private static final Function<Object, Boolean> IS_DONE = Try.orThrow(() -> Reflect.instanceFunction(DELEGATE, "isDone", boolean.class));
    private static final Function<Object, Boolean> IS_PREEMPTED = Try.orThrow(() -> Reflect.instanceFunction(DELEGATE, "isPreempted", boolean.class));

    private final Object delegate;
    private final UnsafeContinuationScope scope;

    public UnsafeContinuation(UnsafeContinuationScope scope, Runnable target) {
        this(scope, CONSTRUCTOR.apply(scope.delegate, target));
    }

    private UnsafeContinuation(UnsafeContinuationScope scope, Object delegate) {
        this.scope = scope;
        this.delegate = delegate;
    }

    public UnsafeContinuationScope getScope() {
        return scope;
    }

    public static UnsafeContinuation getCurrentContinuation(UnsafeContinuationScope scope) {
        Object delegate = GET_CURRENT.apply(scope.delegate);
        return delegate == null ? null : new UnsafeContinuation(scope, delegate);
    }

    public void run() {
        RUN.accept(delegate);
    }

    public static boolean yield(UnsafeContinuationScope scope) {
        return YIELD.apply(scope.delegate);
    }

    public boolean isDone() {
        return IS_DONE.apply(delegate);
    }

    public boolean isPreempted() {
        return IS_PREEMPTED.apply(delegate);
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof UnsafeContinuation cnt &&  delegate.equals(cnt.delegate);
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }
}
