package io.gitlab.jfronny.commons.flow.unsafe;

import io.gitlab.jfronny.commons.flow.ContinuationScope;
import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.commons.unsafe.reflect.Reflect;

import java.util.Objects;
import java.util.function.Function;

/**
 * A class intended to behave like a JVM ContinuationScope but without the need to use the internal API.
 * The exact implementation may need to change in the future to adapt to changes in the internal API or how we access it.
 */
public class UnsafeContinuationScope implements ContinuationScope {
    protected static final Class DELEGATE = Try.orThrow(() -> Class.forName("jdk.internal.vm.ContinuationScope"));
    private static final Function<String, Object> CONSTRUCTOR = Try.orThrow(() -> Reflect.constructor(DELEGATE, String.class));

    private final String name;
    protected final Object delegate;

    public UnsafeContinuationScope(String name) {
        this.name = Objects.requireNonNull(name);
        this.delegate = CONSTRUCTOR.apply(name);
    }

    public UnsafeContinuationScope() {
        this("io.gitlab.jfronny.commons.flow.ContinuationScope");
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
