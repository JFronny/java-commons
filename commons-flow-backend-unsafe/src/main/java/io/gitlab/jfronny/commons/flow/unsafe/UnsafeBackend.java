package io.gitlab.jfronny.commons.flow.unsafe;

import io.gitlab.jfronny.commons.flow.spi.FlowBackend;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

public class UnsafeBackend implements FlowBackend<UnsafeContinuation, UnsafeContinuationScope> {
    @NotNull
    @Override
    public UnsafeContinuationScope createScope(@NotNull String name) {
        return new UnsafeContinuationScope(name);
    }

    @NotNull
    @Override
    public UnsafeContinuation createContinuation(@NotNull UnsafeContinuationScope scope, @NotNull Runnable target) {
        return new UnsafeContinuation(scope, target);
    }

    @Override
    public UnsafeContinuation getCurrentContinuation(@NotNull UnsafeContinuationScope scope) {
        return UnsafeContinuation.getCurrentContinuation(scope);
    }

    @Override
    public boolean yield(UnsafeContinuationScope scope) {
        return UnsafeContinuation.yield(scope);
    }

    @Override
    public @NotNull Thread newVirtualThread(@Nullable Executor scheduler, @Nullable String name, int characteristics, @NotNull Runnable task) {
        return UnsafeScheduledVirtualThreadBuilder.newVirtualThread(scheduler, name, characteristics, task);
    }

    @Override
    public Thread.Builder.@NotNull OfVirtual ofVirtual(@Nullable Executor scheduler) {
        return UnsafeScheduledVirtualThreadBuilder.ofVirtual(scheduler);
    }

    @Override
    public @NotNull ForkJoinWorkerThread createCarrierThread(@NotNull ForkJoinPool pool) {
        return UnsafeScheduledVirtualThreadBuilder.createCarrierThread(pool);
    }

    @Override
    public void setVirtualThreadPriority(@NotNull Thread virtualThread, int priority) {
        UnsafeScheduledVirtualThreadBuilder.setPriority(virtualThread, priority);
    }

    @Override
    public int getVirtualThreadPriority(@NotNull Thread virtualThread) {
        return UnsafeScheduledVirtualThreadBuilder.getPriority(virtualThread);
    }
}
