package io.gitlab.jfronny.commons.flow.unsafe;

import io.gitlab.jfronny.commons.flow.PrioritizedRunnable;
import io.gitlab.jfronny.commons.flow.RemovableExecutor;
import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.commons.unsafe.Unsafe;
import io.gitlab.jfronny.commons.unsafe.reflect.LambdaFactory;
import io.gitlab.jfronny.commons.unsafe.reflect.Reflect;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.function.Function;

public class UnsafeScheduledVirtualThreadBuilder {
    private static final NewVirtualThreadFnc NEW_VIRTUAL_THREAD_FNC;
    private static final Function<Executor, Thread.Builder.OfVirtual> NEW_VIRTUAL_THREAD_BUILDER;
    private static final Function<ForkJoinPool, ForkJoinWorkerThread> NEW_CARRIER_THREAD;
    private static final long VIRTUAL_THREAD_CONTINUATION;
    private static final long VIRTUAL_THREAD_SCHEDULER;

    static {
//        java.lang.VirtualThread.startVirtualThread();
//        new java.lang.VirtualThread.VirtualThreadFactory().newThread();
//        new java.lang.VirtualThread.VirtualThreadBuilder().unstarted()

        NEW_VIRTUAL_THREAD_FNC = Try.orThrow(() -> {
            Class<?> THREAD_BUILDERS = Class.forName("java.lang.ThreadBuilders");
            MethodHandles.Lookup lookup = LambdaFactory.lookup(THREAD_BUILDERS);
            MethodHandle unreflected = lookup.unreflect(THREAD_BUILDERS.getDeclaredMethod("newVirtualThread", Executor.class, String.class, int.class, Runnable.class));
            // Can't use LambdaMetafactory since our signature interface is not in java.base
            return (scheduler, name, characteristics, task) -> Try.orThrow(() -> (Thread) unreflected.invoke(scheduler, name, characteristics, task));
//            MethodType signature = MethodType.methodType(Thread.class, Executor.class, String.class, int.class, Runnable.class);
//            return (NewVirtualThreadFnc) LambdaMetafactory.metafactory(
//                    LambdaFactory.getLookupRoot(),
//                    "newVirtualThread",
//                    MethodType.methodType(NewVirtualThreadFnc.class),
//                    signature,
//                    unreflected,
//                    signature
//            ).getTarget().invoke();
        });
        NEW_VIRTUAL_THREAD_BUILDER = Try.orThrow(() -> Reflect.constructor("java.lang.ThreadBuilders$VirtualThreadBuilder", Executor.class));
        NEW_CARRIER_THREAD = Try.orThrow(() -> Reflect.constructor("jdk.internal.misc.CarrierThread", ForkJoinPool.class));
        Class<?> vtClass = Try.orThrow(() -> Class.forName("java.lang.VirtualThread"));
        Field cntField = Try.orThrow(() -> Reflect.getDeclaredField(vtClass, "runContinuation"));
        VIRTUAL_THREAD_CONTINUATION = Try.orThrow(() -> Unsafe.objectFieldOffset(cntField));
        Field shdField = Try.orThrow(() -> Reflect.getDeclaredField(vtClass, "scheduler"));
        VIRTUAL_THREAD_SCHEDULER = Try.orThrow(() -> Unsafe.objectFieldOffset(shdField));
    }

    @FunctionalInterface
    private interface NewVirtualThreadFnc {
        Thread newVirtualThread(Executor scheduler, String name, int characteristics, Runnable task);
    }

    public static @NotNull Thread newVirtualThread(@Nullable Executor scheduler, @Nullable String name, int characteristics, @NotNull Runnable task) {
        Objects.requireNonNull(task);
        Thread thread = NEW_VIRTUAL_THREAD_FNC.newVirtualThread(scheduler, name, characteristics, task);
        if (task instanceof PrioritizedRunnable pr) setPriority(thread, pr.getPriority());
        return thread;
    }

    public static Thread.Builder.OfVirtual ofVirtual(@Nullable Executor scheduler) {
        return NEW_VIRTUAL_THREAD_BUILDER.apply(scheduler);
    }

    public static ForkJoinWorkerThread createCarrierThread(@NotNull ForkJoinPool pool) {
        return NEW_CARRIER_THREAD.apply(pool);
    }

    public static void setPriority(Thread thread, int priority) {
        Runnable threadContinuation = Unsafe.getObject(thread, VIRTUAL_THREAD_CONTINUATION);
        PrioritizedRunnable prioritizedThreadContinuation = PrioritizedRunnable.of(threadContinuation);
        if (prioritizedThreadContinuation.getPriority() == priority) return;
        PrioritizedRunnable newThreadContinuation = PrioritizedRunnable.of(prioritizedThreadContinuation, priority);
        Unsafe.putObject(thread, VIRTUAL_THREAD_CONTINUATION, newThreadContinuation);
        Executor scheduler = Unsafe.getObject(thread, VIRTUAL_THREAD_SCHEDULER);
        RemovableExecutor removableScheduler = RemovableExecutor.of(scheduler);
        if (removableScheduler != null) {
            if (removableScheduler.remove(threadContinuation) || removableScheduler.remove(prioritizedThreadContinuation)) {
                removableScheduler.execute(newThreadContinuation);
            }
        }
    }

    public static int getPriority(Thread thread) {
        return PrioritizedRunnable.of(Unsafe.getObject(thread, VIRTUAL_THREAD_CONTINUATION)).getPriority();
    }
}
