package io.gitlab.jfronny.commons.flow.unsafe.test.perf;

import org.microhttp.LogEntry;
import org.microhttp.Logger;

public class NoOpLogger implements Logger {
    @Override
    public boolean enabled() {
        return false;
    }

    @Override
    public void log(LogEntry... entries) {

    }

    @Override
    public void log(Exception e, LogEntry... entries) {

    }
}
