# Performance testing for PriorityScheduler
This testing suite is based on [this codebase](https://github.com/ebarlas/project-loom-comparison) (which also contains details on how to use it)
It is used to ensure that the performance loss of this scheduler is within expected limits.
Since it uses a completely different backing technology, there are bound to be use cases where it is behaves differently,
and (most likely) performs significantly worse than the default scheduler (or outright stalls, since creating threads in case of congestion with blocking tasks is not supported).
Nonetheless, some unscientific testing seams to suggest that the performance loss incurred by this does not affect mean performance too much (although the measured lows are noticeably worse).

Long story short: If you don't need priority-based scheduling, don't use it!