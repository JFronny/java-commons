package io.gitlab.jfronny.commons.flow.unsafe.test.perf;

import io.gitlab.jfronny.commons.flow.DefaultSchedulers;
import io.gitlab.jfronny.commons.flow.ScheduledVirtualThreadBuilder;
import org.microhttp.*;

import java.io.IOException;
import java.net.http.HttpClient;
import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Frontend {
    public static void main(String[] args) throws IOException {
        Args a = Args.parse(args);
        System.out.println(a);
        Options options = Options.builder()
                .withHost(a.host)
                .withPort(a.port)
                .withAcceptLength(a.acceptLength)
                .withReuseAddr(true)
                .withReusePort(true)
                .build();
        Logger logger = a.debug ? new DebugLogger() : new NoOpLogger();
        EventLoop eventLoop = new EventLoop(options, logger, handler(a));
        eventLoop.start();
    }

    static Handler handler(Args args) {
        return args.type.equals(Type.ASYNC)
                ? new AsyncHandler(httpClient(args.type), args.backend)
                : new SyncHandler(executorService(args), httpClient(args.type), args.backend);
    }

    static ExecutorService executorService(Args args) {
        return switch(args.type) {
            case VTHREAD -> Executors.newVirtualThreadPerTaskExecutor();
            case DVTHREAD -> ScheduledVirtualThreadBuilder.newVirtualThreadPerTaskExecutor(DefaultSchedulers.createPriorityScheduler());
            case ASYNC, THREAD -> Executors.newFixedThreadPool(args.threads);
        };
    }

    static HttpClient httpClient(Type type) {
        var builder = HttpClient.newBuilder();
        return (switch (type) {
            case VTHREAD -> builder.executor(Executors.newVirtualThreadPerTaskExecutor());
            case DVTHREAD -> builder.executor(ScheduledVirtualThreadBuilder.newVirtualThreadPerTaskExecutor(DefaultSchedulers.createPriorityScheduler()));
            case ASYNC, THREAD -> builder;
        }).connectTimeout(Duration.ofSeconds(5)).build();
    }

    record Args(String host, int port, Type type, int threads, String backend, int acceptLength, boolean debug) {
        static Args parse(String[] args) {
            return new Args(
                    args.length >= 1 ? args[0] : "localhost",
                    args.length >= 2 ? Integer.parseInt(args[1]) : 8081,
                    args.length >= 3 ? Type.valueOf(args[2].toUpperCase()) : Type.VTHREAD,
                    args.length >= 4 ? Integer.parseInt(args[3]) : 10,
                    args.length >= 5 ? args[4] : "localhost:8080",
                    args.length >= 6 ? Integer.parseInt(args[5]) : 0,
                    args.length >= 7 ? Boolean.parseBoolean(args[6]) : true);
        }
    }

    enum Type {
        VTHREAD, THREAD, ASYNC, DVTHREAD
    }
}
