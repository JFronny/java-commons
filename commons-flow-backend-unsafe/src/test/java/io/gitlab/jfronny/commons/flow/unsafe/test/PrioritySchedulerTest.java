package io.gitlab.jfronny.commons.flow.unsafe.test;

import io.gitlab.jfronny.commons.flow.DefaultSchedulers;
import io.gitlab.jfronny.commons.flow.ScheduledVirtualThreadBuilder;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executor;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrioritySchedulerTest {
    public static class Ctx {
        String data;
    }
    Runnable simpleTask(String data, Ctx ctx) {
        return () -> {
            sleep(50);
            ctx.data = data;
            System.out.println(data);
        };
    }
    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    DefaultSchedulers.Props props = new DefaultSchedulers.Props(3, 1, 0);

    @Test
    void ensureThreadsStillWork() {
        Ctx ctx = new Ctx();
        Executor scheduler = DefaultSchedulers.createPriorityScheduler(props);
        ScheduledVirtualThreadBuilder.startVirtualThread(scheduler, simpleTask("Hello", ctx));
        sleep(100);
        assertEquals("Hello", ctx.data);
    }
}
