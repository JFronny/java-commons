package io.gitlab.jfronny.commons.flow.unsafe.test.perf;

public record Meeting(String time, String subject) {
}
