package io.gitlab.jfronny.commons.flow.unsafe.test;

import io.gitlab.jfronny.commons.flow.Continuation;
import io.gitlab.jfronny.commons.flow.ContinuationScope;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

public class ContinuationTest {
    @Test
    void simpleTest() {
        ContinuationScope scope = ContinuationScope.create();
        assertNull(Continuation.getCurrentContinuation(scope));
        AtomicReference<String> result = new AtomicReference<>();
        Continuation cnt = Continuation.create(scope, () -> {
            result.set("Hello " + Thread.currentThread().getName());
            assertTrue(Continuation.yield(scope));
            result.set("World" + Thread.currentThread().getName());
        });
        assertEquals(scope, cnt.getScope());
        assertNull(result.get());

        String mainThread = Thread.currentThread().getName();
        assertFalse(cnt.isDone());
        assertFalse(cnt.isPreempted());
        cnt.run();
        assertEquals("Hello " + mainThread, result.get());
        assertFalse(cnt.isDone());
        assertFalse(cnt.isPreempted());
        cnt.run();
        assertEquals("World" + mainThread, result.get());
        assertTrue(cnt.isDone());
        assertFalse(cnt.isPreempted());
    }
}
