package io.gitlab.jfronny.commons.flow.unsafe.test;

import io.gitlab.jfronny.commons.flow.Enumerable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnumeratorTest {
    @Test
    void simpleTest() {
        String mainThread = Thread.currentThread().getName();
        String sd = mainThread;
        for (String s : sampleFunction()) {
            sd += s;
        }
        assertEquals(mainThread + "HelloWorld" + mainThread + "Test", sd);
    }

    Enumerable<String> sampleFunction() {
        return new Enumerable<>(items -> {
            items.accept("Hello");
            items.accept("World");
            items.accept(Thread.currentThread().getName());
            items.accept("Test");
        });
    }
}
