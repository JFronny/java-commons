package io.gitlab.jfronny.commons.flow.unsafe.test.perf;

import java.util.List;

public record Authorization(List<String> authorities) {
}
