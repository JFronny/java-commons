plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    implementation(projects.commonsFlow)
    implementation(projects.commonsUnsafe)

    testImplementation(libs.junit.jupiter.api)
    testImplementation(libs.bundles.flow.perf.test)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-flow-backend-unsafe"

            from(components["java"])
        }
    }
}

