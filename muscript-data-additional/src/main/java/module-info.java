module io.gitlab.jfronny.commons.muscript.data.additional {
    uses io.gitlab.jfronny.muscript.data.additional.DataExprMapper.Mapping;
    uses io.gitlab.jfronny.muscript.data.additional.context.IExprBinder;
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.data;
    requires io.gitlab.jfronny.commons.muscript.ast;
    requires io.gitlab.jfronny.commons.muscript.core;
    exports io.gitlab.jfronny.muscript.data.additional;
    exports io.gitlab.jfronny.muscript.data.additional.impl;
    exports io.gitlab.jfronny.muscript.data.additional.context;
    exports io.gitlab.jfronny.muscript.data.additional.libs;
    provides io.gitlab.jfronny.muscript.data.dynamic.context.ISimpleDynamic with io.gitlab.jfronny.muscript.data.additional.impl.SimpleDynamicImpl;
}