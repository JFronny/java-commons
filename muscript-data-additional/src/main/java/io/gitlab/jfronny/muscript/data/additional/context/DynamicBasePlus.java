package io.gitlab.jfronny.muscript.data.additional.context;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.data.dynamic.DynamicBase;

public interface DynamicBasePlus extends DynamicBase {
    Expr toExpr();
}
