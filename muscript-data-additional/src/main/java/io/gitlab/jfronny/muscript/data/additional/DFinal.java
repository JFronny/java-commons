package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.commons.LazySupplier;
import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.commons.data.ImmCollection;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.IExprParser;
import io.gitlab.jfronny.muscript.ast.dynamic.Variable;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.data.additional.context.DynamicBasePlus;
import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
import io.gitlab.jfronny.muscript.data.dynamic.type.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

public class DFinal {
    public static DBool of(boolean b) {
        return new FBool(b);
    }

    public static DNumber of(double b) {
        return new FNumber(b);
    }

    public static DString of(String b) {
        return new FString(b);
    }

    public static DObject of(Map<String, ? extends Dynamic> b) {
        return of(new DTypeObject(null), b);
    }

    public static DObject of(DTypeObject signature, Map<String, ? extends Dynamic> b) {
        return new FObject(ImmCollection.copyOf((Map<String, Dynamic>) b), Objects.requireNonNull(signature));
    }

    public static DList of(Dynamic... b) {
        return of(new DTypeList(null), b);
    }

    public static DList of(DTypeList signature, Dynamic... b) {
        return new FList(List.of(b), Objects.requireNonNull(signature));
    }

    public static DList of(List<? extends Dynamic> b) {
        return of(new DTypeList(null), b);
    }

    public static DList of(DTypeList signature, List<? extends Dynamic> b) {
        return new FList(ImmCollection.copyOf((List<Dynamic>) b), Objects.requireNonNull(signature));
    }

    public static DCallable of(Function<DList, ? extends Dynamic> b, Supplier<String> serialized) {
        return of(new DTypeCallable(null, null), b, Objects.requireNonNull(serialized));
    }

    public static DCallable of(DType signature, Function<DList, ? extends Dynamic> b, Supplier<String> serialized) {
        return of(Objects.requireNonNull(signature), b, serialized, null);
    }

    public static DCallable of(Function<DList, ? extends Dynamic> b, String name) {
        return of(new DTypeCallable(null, null), b, name);
    }

    public static DCallable of(DType signature, Function<DList, ? extends Dynamic> b, String name) {
        return of(Objects.requireNonNull(signature), name, b, () -> new Variable(CodeLocation.NONE, name));
    }

    public static DCallable of(Function<DList, ? extends Dynamic> b, Supplier<String> serialized, String name) {
        return of(new DTypeCallable(null, null), b, serialized, name);
    }

    public static DCallable of(DType signature, Function<DList, ? extends Dynamic> b, Supplier<String> serialized, String name) {
        return of(Objects.requireNonNull(signature), name, b, () -> IExprParser.INSTANCE.parse(serialized.get()));
    }

    public static DCallable of(String name, Function<DList, ? extends Dynamic> b, Supplier<Expr> serialized) {
        return of(new DTypeCallable(null, null), name, b, serialized);
    }

    public static DCallable of(DType signature, String name, Function<DList, ? extends Dynamic> b, Supplier<Expr> serialized) {
        return new FCallable((Function<DList, Dynamic>) b, new LazySupplier<>(serialized), name, Objects.requireNonNull(signature));
    }

    /**
     * Marks all DFinal implementation classes. Use this in instanceof checks
     */
    public sealed interface FImpl {
    }

    private record FBool(boolean value) implements DBool, FImpl {
        @Override
        public Boolean getValue() {
            return value;
        }

        @Override
        public String toString() {
            return StringFormatter.toString(value);
        }

        @Override
        public DType getSignature() {
            return DTypePrimitive.BOOL;
        }

        @Override
        public boolean isDirect() {
            return true;
        }
    }

    private record FNumber(double value) implements DNumber, FImpl {
        @Override
        public Double getValue() {
            return value;
        }

        @Override
        public String toString() {
            return StringFormatter.toStringPrecise(value);
        }

        @Override
        public DType getSignature() {
            return DTypePrimitive.NUMBER;
        }

        @Override
        public boolean isDirect() {
            return true;
        }
    }

    private record FString(String value) implements DString, FImpl {
        @Override
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return DynamicSerializer.INSTANCE.serialize(this);
        }

        @Override
        public DType getSignature() {
            return DTypePrimitive.STRING;
        }

        @Override
        public boolean isDirect() {
            return true;
        }
    }

    private record FObject(Map<String, Dynamic> value, DTypeObject signature) implements DObject, FImpl {
        @Override
        public Map<String, Dynamic> getValue() {
            return value;
        }

        @Override
        public String toString() {
            return DynamicSerializer.INSTANCE.serialize(this);
        }

        @Override
        public DType getSignature() {
            return signature;
        }

        @Override
        public boolean isDirect() {
            return value.values().stream().allMatch(Dynamic::isDirect);
        }
    }

    private record FList(List<Dynamic> value, DTypeList signature) implements DList, FImpl {
        @Override
        public List<Dynamic> getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value.toString();
        }

        @Override
        public DType getSignature() {
            return signature;
        }

        @Override
        public boolean isDirect() {
            return value.stream().allMatch(Dynamic::isDirect);
        }
    }

    private record FCallable(Function<DList, Dynamic> value, Supplier<Expr> gen, String name, DType signature) implements DCallable, FImpl, DynamicBasePlus {
        @Override
        public Expr toExpr() {
            return gen.get();
        }

        @Override
        public Function<DList, Dynamic> getValue() {
            return value;
        }

        @Override
        public String toString() {
            return toExpr().toString();
        }

        @Override
        public String getName() {
            return name == null ? DCallable.super.getName() : name;
        }

        @Override
        public DCallable named(String name) {
            return new FCallable(value, gen, name, signature);
        }

        @Override
        public DType getSignature() {
            return signature;
        }
    }
}
