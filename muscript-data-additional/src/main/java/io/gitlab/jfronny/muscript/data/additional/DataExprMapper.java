package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.data.additional.impl.DataExprMappingDefault;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Stream;

public class DataExprMapper {
    public static @Nullable Expr map(Dynamic dynamic) {
        for (Mapping mapping : Mapping.MAPPINGS) {
            Expr expr = mapping.map(dynamic);
            if (expr != null) {
                return expr;
            }
        }
        return null;
    }

    public interface Mapping {
        List<Mapping> MAPPINGS = Stream.concat(
                ServiceLoader.load(Mapping.class).stream().map(ServiceLoader.Provider::get),
                Stream.of(new DataExprMappingDefault())
        ).toList();

        @Nullable Expr map(Dynamic dynamic);
    }
}
