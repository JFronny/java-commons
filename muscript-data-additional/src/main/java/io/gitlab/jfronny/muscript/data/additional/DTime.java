package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.data.dynamic.DNumber;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DNumberLens;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DStringLens;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;

import java.time.LocalTime;
import java.util.Map;
import java.util.function.Supplier;

import static io.gitlab.jfronny.muscript.data.dynamic.type.DSL.*;

public record DTime(Supplier<LocalTime> time) implements DObject {
    public static final DType SIGNATURE = object(NUMBER).or(STRING).or(NUMBER);

    @Override
    public Map<String, Dynamic> getValue() {
        return Map.of(
                "hour", DFinal.of(time.get().getHour()),
                "minute", DFinal.of(time.get().getMinute()),
                "second", DFinal.of(time.get().getSecond())
        );
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    public DString asString() {
        return new DStringLens(this, this::toString);
    }

    @Override
    public boolean isNumber() {
        return true;
    }

    @Override
    public DNumber asNumber() {
        return new DNumberLens(this, () -> time.get().toSecondOfDay());
    }

    @Override
    public DType getSignature() {
        return SIGNATURE;
    }

    @Override
    public String toString() {
        return time.get().toString();
    }
}
