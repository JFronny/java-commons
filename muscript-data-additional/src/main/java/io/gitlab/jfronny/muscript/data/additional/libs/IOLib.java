package io.gitlab.jfronny.muscript.data.additional.libs;

import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.additional.context.IExprBinder;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.List;

import static io.gitlab.jfronny.muscript.data.dynamic.type.DSL.*;

public class IOLib {
    public static Scope addTo(MuScriptVersion version, Scope scope, IOWrapper io) {
        if (version.contains(MuScriptVersion.V3)) {
            scope.set("readString", callable(STRING, arg("fileName", STRING)), args -> {
                if (args.size() != 1) throw new SignatureDesyncException("readString");
                return DFinal.of(io.readString(args.get(0).asString().getValue()));
            }).set("runScript", callable(null, arg("fileName", STRING), arg("args", null, true)), args -> {
                if (args.isEmpty()) throw new SignatureDesyncException("runScript");
                Script script = io.parseScript(args.get(0).asString().getValue());
                List<? extends Dynamic> l = args.getValue();
                DList innerArgs = DFinal.of(l.subList(1, l.size()));
                return IExprBinder.INSTANCE.bind(script, scope.fork()).call(innerArgs);
            });
        }
        return scope;
    }
}
