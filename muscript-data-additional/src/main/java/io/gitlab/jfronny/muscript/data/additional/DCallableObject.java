package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.data.dynamic.DCallable;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DCallableLens;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeAnd;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeObject;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

public record DCallableObject(Map<String, ? extends Dynamic> value, DTypeObject objectSignature, DCallable callable) implements DObject {
    public DCallableObject(Map<String, ? extends Dynamic> value, DCallable callable) {
        this(value, new DTypeObject(null), callable);
    }

    public DCallableObject {
        Objects.requireNonNull(value);
        Objects.requireNonNull(objectSignature);
        Objects.requireNonNull(callable);
    }

    @Override
    public Map<String, ? extends Dynamic> getValue() {
        return value;
    }

    @Override
    public boolean isCallable() {
        return true;
    }

    @Override
    public DCallable asCallable() {
        return new DCallableLens(this, callable::getValue);
    }

    @Override
    public DType getSignature() {
        return new DTypeAnd(Set.of(objectSignature, callable.getSignature()));
    }

    @Override
    public String toString() {
        return DynamicSerializer.INSTANCE.serialize(this);
    }
}
