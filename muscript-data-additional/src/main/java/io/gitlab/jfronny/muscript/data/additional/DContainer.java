package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.data.dynamic.DynamicBase;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;

public abstract class DContainer<T> implements DynamicBase {
    private T value;

    @Override
    public T getValue() {
        return value;
    }

    public T setValue(T value) {
        if (value != null)
            this.value = value;
        return this.value;
    }

    @Override
    public String toString() {
        return DynamicSerializer.INSTANCE.serialize(this);
    }
}
