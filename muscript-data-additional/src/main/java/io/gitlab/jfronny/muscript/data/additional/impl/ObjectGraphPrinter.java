package io.gitlab.jfronny.muscript.data.additional.impl;

import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;

public class ObjectGraphPrinter {
    public static String printGraph(Object o) throws IllegalAccessException {
        if (o == null) return "null";
        StringBuilder builder = new StringBuilder();
        IndentingWriter writer = new IndentingWriter(builder, "");
        writer.writeLine("[" + o.getClass().getSimpleName() + "]");
        printGraph(writer.level(), o, o.getClass());
        return builder.toString();
    }

    private static void printGraph(IndentingWriter writer, Object o, Class<?> klazz) throws IllegalAccessException {
        for (Field field : klazz.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers())) continue;
            field.setAccessible(true);
            Object fo = field.get(o);
            if (fo == null) {
                writer.writeLine(field.getName() + " [" + field.getType().getSimpleName() + "]: null");
                continue;
            }
            Class<?> kz = fo.getClass();
            String name = field.getName() + " [" + kz.getSimpleName() + "]";
            if (kz.isEnum()) {
                writer.writeLine(name + " = " + fo);
            } else if (kz.isAssignableFrom(String.class)) {
                writer.writeLine(name + " = \"" + fo + "\"");
            } else if (kz.isAssignableFrom(Double.class)) {
                writer.writeLine(name + " = " + fo);
            } else if (kz.isAssignableFrom(Dynamic.class)) {
                writer.writeLine(name + " = " + fo);
            } else if (kz.isAssignableFrom(Collection.class)) {
                writer.writeLine(name + ":");
                for (Object element : (Collection<?>) fo) {
                    printGraph(writer.level(), element, element.getClass());
                }
            } else {
                writer.writeLine(name + ":");
                printGraph(writer.level(), fo, fo.getClass());
            }
        }
        klazz = klazz.getSuperclass();
        if (klazz != null) printGraph(writer, o, klazz);
    }

    private record IndentingWriter(StringBuilder out, String indentation) {
        public void writeLine(String text) {
            out.append(indentation).append(text.replace("\n", "\\n")).append('\n');
        }

        public IndentingWriter level() {
            return new IndentingWriter(out, indentation + "  ");
        }
    }
}
