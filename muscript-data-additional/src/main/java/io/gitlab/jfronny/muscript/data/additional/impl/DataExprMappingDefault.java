package io.gitlab.jfronny.muscript.data.additional.impl;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.NullLiteral;
import io.gitlab.jfronny.muscript.ast.bool.BoolLiteral;
import io.gitlab.jfronny.muscript.ast.dynamic.Bind;
import io.gitlab.jfronny.muscript.ast.dynamic.Call;
import io.gitlab.jfronny.muscript.ast.dynamic.ObjectLiteral;
import io.gitlab.jfronny.muscript.ast.dynamic.Variable;
import io.gitlab.jfronny.muscript.ast.number.NumberLiteral;
import io.gitlab.jfronny.muscript.ast.string.StringLiteral;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.data.additional.*;
import io.gitlab.jfronny.muscript.data.additional.context.DynamicBasePlus;
import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DLens;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;
import static io.gitlab.jfronny.muscript.data.additional.DataExprMapper.*;

public class DataExprMappingDefault implements Mapping {
    @Override
    public @Nullable Expr map(Dynamic dynamic) {
        return switch (dynamic) {
            case DynamicBasePlus base -> base.toExpr();
            case DLens lens -> DataExprMapper.map(lens.getSource());
            case DCallableObject dco -> new Call(CodeLocation.NONE, new Bind(CodeLocation.NONE, new Variable(CodeLocation.NONE, "callableObject"), convertObjectSimple(dco)), List.of(new Call.Argument(asDynamic(DataExprMapper.map(dco.callable())), false)));
            case DDate date -> new Call(CodeLocation.NONE, new Variable(CodeLocation.NONE, "date"), List.of(
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.date().get().getYear())), false),
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.date().get().getMonthValue())), false),
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.date().get().getDayOfMonth())), false)
            ));
            case DTime time -> new Call(CodeLocation.NONE, new Variable(CodeLocation.NONE, "time"), List.of(
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, time.time().get().getHour())), false),
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, time.time().get().getMinute())), false),
                    new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, time.time().get().getSecond())), false)
            ));
            case DEnum enm -> convertEnum(enm);
            case NamedDCallable(var inner, var name) -> DataExprMapper.map(inner);
            case DelegateDynamic delegate -> DataExprMapper.map(delegate.getDelegate());
            case DBool bool -> new BoolLiteral(CodeLocation.NONE, bool.getValue());
            case DList list -> new Call(CodeLocation.NONE, new Variable(CodeLocation.NONE, "listOf"), list.getValue().stream().map(s -> new Call.Argument(asDynamic(DataExprMapper.map(s)), false)).toList());
            case DNull nul -> new NullLiteral(CodeLocation.NONE);
            case DNumber number -> new NumberLiteral(CodeLocation.NONE, number.getValue());
            case DObject object -> convertObjectSimple(object);
            case DString string -> new StringLiteral(CodeLocation.NONE, string.getValue());
            default -> null;
        };
    }

    private static DynamicExpr convertObjectSimple(DObject object) {
        return new ObjectLiteral(CodeLocation.NONE, object.getValue().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, s -> asDynamic(DataExprMapper.map(s.getValue())))));
    }

    private static DynamicExpr convertEnum(DEnum enm) {
        List<Call.Argument> args = new LinkedList<>();
        args.add(new Call.Argument(convertObjectSimple(enm), false));
        if (enm.value() != null) args.add(new Call.Argument(asDynamic(new StringLiteral(CodeLocation.NONE, enm.value().value())), false));
        return new Call(CodeLocation.NONE, new Variable(CodeLocation.NONE, "enum"), args);
    }
}
