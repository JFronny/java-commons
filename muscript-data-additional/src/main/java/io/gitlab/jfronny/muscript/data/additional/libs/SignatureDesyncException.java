package io.gitlab.jfronny.muscript.data.additional.libs;

public class SignatureDesyncException extends IllegalArgumentException {
    public SignatureDesyncException(String method) {
        super("Signature desync detected for method: " + method + " (invoked with illegal arguments but passed automatic check)");
    }
}
