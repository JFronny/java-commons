package io.gitlab.jfronny.muscript.data.additional.impl;

import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.context.ISimpleDynamic;

public class SimpleDynamicImpl implements ISimpleDynamic {
    @Override
    public DString of(String value) {
        return DFinal.of(value);
    }

    @Override
    public DList of(Dynamic... values) {
        return DFinal.of(values);
    }
}
