package io.gitlab.jfronny.muscript.data.additional.context;

import io.gitlab.jfronny.commons.data.ImmCollection;
import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeList;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeObject;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Scope implements DObject, IScope {
    private final @Nullable Scope source;
    private final Map<String, Dynamic> override = new HashMap<>();

    public Scope() {
        this(null);
    }

    public Scope(@Nullable DObject source) {
        if (source == null) {
            this.source = null;
        } else if (source instanceof Scope src) {
            this.source = src;
        } else {
            this.source = new Scope();
            source.getValue().forEach(this::set);
        }
    }

    public Scope(@Nullable Scope source) {
        this.source = source;
    }

    public static Scope of(DObject source) {
        return source instanceof Scope scope ? scope: new Scope(source);
    }

    @Override
    public Map<String, Dynamic> getValue() {
        if (source == null) return ImmCollection.of(override);
        var map = new HashMap<>(source.getValue());
        map.putAll(override);
        return ImmCollection.of(map);
    }

    /**
     * @deprecated Using the convenience methods is recommended wherever possible.
     */
    @Deprecated(forRemoval = false)
    public Scope set(String key, Dynamic value) {
        if (key.startsWith("$")) {
            if (!setGlobal(key, value)) override.put(key, value);
        } else {
            override.put(key, value);
        }
        return this;
    }

    private boolean setGlobal(String key, Dynamic value) {
        if (override.containsKey(key)) {
            override.put(key, value);
            return true;
        } else if (source != null) {
            return source.setGlobal(key, value);
        } else {
            return false;
        }
    }

    public Scope set(String key, boolean value) {
        return set(key, DFinal.of(value));
    }

    public Scope set(String key, double value) {
        return set(key, DFinal.of(value));
    }

    public Scope set(String key, String value) {
        return set(key, DFinal.of(value));
    }

    public Scope set(String key, Map<String, ? extends Dynamic> value) {
        return set(key, DFinal.of(value));
    }

    public Scope set(String key, DTypeObject signature, Map<String, ? extends Dynamic> value) {
        return set(key, DFinal.of(signature, value));
    }

    public Scope set(String key, List<? extends Dynamic> value) {
        return set(key, DFinal.of(value));
    }

    public Scope set(String key, DTypeList signature, List<? extends Dynamic> value) {
        return set(key, DFinal.of(signature, value));
    }

    public Scope set(String key, Function<DList, ? extends Dynamic> value) {
        return set(key, DFinal.of(value, () -> key, key));
    }

    public Scope set(String key, DType signature, Function<DList, ? extends Dynamic> value) {
        return set(key, DFinal.of(signature, value, () -> key, key));
    }

    public Scope fork() {
        return new Scope(this);
    }

    public DObject getOverrides() {
        return DFinal.of(override);
    }

    @Override
    public String toString() {
        return DynamicSerializer.INSTANCE.serialize(this);
    }
}
