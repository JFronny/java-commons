package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DListLens;

import java.util.List;
import java.util.Map;

public enum DEmpty implements DObject {
    INSTANCE;

    @Override
    public String toString() {
        return "[]";
    }

    @Override
    public Map<String, ? extends Dynamic> getValue() {
        return Map.of();
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public DList asList() {
        return new DListLens(this, List::of);
    }
}
