package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.dynamic.Call;
import io.gitlab.jfronny.muscript.ast.dynamic.Variable;
import io.gitlab.jfronny.muscript.ast.number.NumberLiteral;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.data.dynamic.DNumber;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DNumberLens;
import io.gitlab.jfronny.muscript.data.dynamic.lens.DStringLens;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;
import static io.gitlab.jfronny.muscript.data.dynamic.type.DSL.*;

public record DDate(Supplier<LocalDate> date) implements DObject {
    public static final DType SIGNATURE = object(NUMBER).or(STRING).or(NUMBER);

    @Override
    public Map<String, Dynamic> getValue() {
        return Map.of(
                "year", DFinal.of(date.get().getYear()),
                "month", DFinal.of(date.get().getMonthValue()),
                "day", DFinal.of(date.get().getDayOfMonth())
        );
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    public DString asString() {
        return new DStringLens(this, this::toString);
    }

    @Override
    public boolean isNumber() {
        return true;
    }

    @Override
    public DNumber asNumber() {
        return new DNumberLens(this, () -> date.get().toEpochDay());
    }

    public DynamicExpr toExpr() {
        return new Call(CodeLocation.NONE, new Variable(CodeLocation.NONE, "date"), List.of(
                new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.get().getYear())), false),
                new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.get().getMonthValue())), false),
                new Call.Argument(asDynamic(new NumberLiteral(CodeLocation.NONE, date.get().getDayOfMonth())), false)
        ));
    }

    @Override
    public DType getSignature() {
        return SIGNATURE;
    }

    @Override
    public String toString() {
        return date.get().toString();
    }
}
