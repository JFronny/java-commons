package io.gitlab.jfronny.muscript.data.additional.context;

import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.data.dynamic.DCallable;

import java.util.ServiceLoader;

public interface IExprBinder {
    IExprBinder INSTANCE = ServiceLoader.load(IExprBinder.class)
            .findFirst()
            .orElseGet(() -> new IExprBinder() {
                @Override
                public DCallable bind(Script script, Scope scope) {
                    throw new UnsupportedOperationException();
                }
            });

    DCallable bind(Script script, Scope scope);
}
