package io.gitlab.jfronny.muscript.data.additional.libs;

public class IntentionalException extends RuntimeException {
    public IntentionalException(String message) {
        super(message);
    }
}
