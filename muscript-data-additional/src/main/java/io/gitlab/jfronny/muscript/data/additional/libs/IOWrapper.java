package io.gitlab.jfronny.muscript.data.additional.libs;

import io.gitlab.jfronny.muscript.ast.context.IExprParser;
import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.core.SourceFS;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface IOWrapper {
    String readString(String path);

    Script parseScript(String path);

    class Caching implements IOWrapper {
        private final Map<String, String> cachedStrings = new HashMap<>();
        private final Map<String, Script> cachedScripts = new HashMap<>();
        private final IOWrapper inner;

        public Caching(IOWrapper inner) {
            this.inner = Objects.requireNonNull(inner);
        }

        @Override
        public String readString(String path) {
            return cachedStrings.computeIfAbsent(path, inner::readString);
        }

        @Override
        public Script parseScript(String path) {
            return cachedScripts.computeIfAbsent(path, inner::parseScript);
        }
    }

    class SourceFSWrapper implements IOWrapper {
        private final SourceFS inner;
        private final MuScriptVersion version;

        public SourceFSWrapper(MuScriptVersion version, SourceFS inner) {
            this.inner = Objects.requireNonNull(inner);
            this.version = Objects.requireNonNull(version);
        }

        @Override
        public String readString(String path) {
            return inner.read(path);
        }

        @Override
        public Script parseScript(String path) {
            return IExprParser.INSTANCE.parseMultiScript(version, path, inner);
        }
    }
}
