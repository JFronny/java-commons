package io.gitlab.jfronny.muscript.data.additional;

import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;

public interface DelegateDynamic extends DynamicBase {
    Dynamic getDelegate();

    @Override
    default Object getValue() {
        return getDelegate().getValue();
    }

    @Override
    default boolean isBool() {
        return getDelegate().isBool();
    }

    @Override
    default DBool asBool() {
        return getDelegate().asBool();
    }

    @Override
    default boolean isNumber() {
        return getDelegate().isNumber();
    }

    @Override
    default DNumber asNumber() {
        return getDelegate().asNumber();
    }

    @Override
    default boolean isString() {
        return getDelegate().isString();
    }

    @Override
    default DString asString() {
        return getDelegate().asString();
    }

    @Override
    default boolean isObject() {
        return getDelegate().isObject();
    }

    @Override
    default DObject asObject() {
        return getDelegate().asObject();
    }

    @Override
    default boolean isList() {
        return getDelegate().isList();
    }

    @Override
    default DList asList() {
        return getDelegate().asList();
    }

    @Override
    default boolean isCallable() {
        return getDelegate().isCallable();
    }

    @Override
    default DCallable asCallable() {
        return getDelegate().asCallable();
    }

    @Override
    default DType getSignature() {
        return getDelegate().getSignature();
    }
}
