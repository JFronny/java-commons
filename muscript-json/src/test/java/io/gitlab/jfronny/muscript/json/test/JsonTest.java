package io.gitlab.jfronny.muscript.json.test;

import io.gitlab.jfronny.commons.serialize.databind.DatabindSerializer;
import io.gitlab.jfronny.commons.serialize.databind.ObjectMapper;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.json.JsonLib;
import io.gitlab.jfronny.muscript.parser.Parser;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;
import static io.gitlab.jfronny.muscript.data.additional.DFinal.of;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonTest {
    @Test
    void simpleSerialize() throws IOException {
        assertEquals("\"Yes\"", serialize(of("Yes")));
        assertEquals("{\"key\":3}", serialize(of(Map.of("key", of(3)))));
        assertEquals("[true,12,7]", serialize(of(of(true), of(12), of(7).asString()))); // string lens ignored
    }

    @Test
    void simpleDeserialize() throws IOException {
        assertEquals(of("Yes"), deserialize("\"Yes\"", Dynamic.class));
        assertEquals(of(Map.of("key", of(3))), deserialize("{\"key\":3}", Dynamic.class));
        assertEquals(of(of(true), of(12), of("7")), deserialize("[true,12,\"7\"]", Dynamic.class));
    }

    @Test
    void muscriptDSL() {
        assertEquals("{\"key\":\"One\",\"key2\":3}", execute("{key = 'One', key2 = 3}::toJson()").asString().getValue());
        assertEquals(of(Map.of("key", of(3), "value", of(false))), execute("args[0]::fromJson()", "{\"key\": 3, \"value\": false}"));
    }

    @Test
    void matrix() {
        assertEquals("[[1,0,0],[0,6.123233995736766E-17,-1],[0,1,6.123233995736766E-17]]", evaluate(Parser.parseScript(MuScriptVersion.DEFAULT, """
                getRotation = { yaw, pitch, roll ->
                  toJson(
                    listOf(
                      listOf(cos(pitch) * cos(roll), sin(yaw) * sin(pitch) * cos(roll) - cos(yaw) * sin(roll), cos(yaw) * sin(pitch) * cos(roll) + sin(yaw) * sin(roll)),
                      listOf(cos(pitch) * sin(roll), sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll), cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll)),
                      listOf(-sin(pitch), sin(yaw) * cos(pitch), cos(yaw) * cos(pitch))
                    )
                  )
                }
                
                // the result of the last statement will be the result of running the script
                // the following allows using the script in conditions as follows: runScript("path/to/script.mu").getRotation(yaw, pitch, roll)
                {
                  getRotation = getRotation
                }
                
                getRotation(PI / 2, 0, 0)
                """).content(), createScope()).asString().getValue());
    }

    private Scope createScope() {
        return JsonLib.addTo(StandardLib.createScope(MuScriptVersion.DEFAULT));
    }

    private Dynamic execute(String source, String... args) {
        return evaluate(
                asDynamic(Parser.parse(MuScriptVersion.DEFAULT, source)),
                createScope().set("args", of(Arrays.stream(args).map(DFinal::of).toList()))
        );
    }

    private DatabindSerializer serializer = new DatabindSerializer(new JsonTransport(), new ObjectMapper());

    private String serialize(Object object) throws IOException {
        return serializer.serialize(object);
    }

    private <T> T deserialize(String source, Class<T> klazz) throws IOException {
        return serializer.deserialize(source, klazz);
    }
}
