module io.gitlab.jfronny.commons.muscript.json {
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.muscript.data.additional;
    requires io.gitlab.jfronny.commons.muscript.data;
    requires io.gitlab.jfronny.commons.serialize.json;
    requires io.gitlab.jfronny.commons.serialize;
    requires io.gitlab.jfronny.commons.serialize.databind.api;
    exports io.gitlab.jfronny.muscript.json;
    provides io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter with
            io.gitlab.jfronny.muscript.json.impl.typed.additional.DDateTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.additional.DEnumTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.additional.DTimeTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DBoolTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DCallableTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DListTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DNullTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DNumberTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DObjectTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DStringTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.typed.DynamicBaseTypeAdapter,
            io.gitlab.jfronny.muscript.json.impl.DynamicTypeAdapter;
}