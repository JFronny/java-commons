package io.gitlab.jfronny.muscript.json.impl.typed.additional;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DEnum;

@SerializerFor(targets = DEnum.class, hierarchical = true)
public class DEnumTypeAdapter extends TypeAdapter<DEnum> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DEnum value, Writer writer) throws TEx, MalformedDataException {
        if (value.value() == null) {
            writer.beginArray();
            for (String s : value.values().keySet()) {
                writer.value(s);
            }
            writer.endArray();
        } else writer.value(value.value().value());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DEnum deserialize(Reader reader) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException("Deserializing DEnum is unsupported");
    }
}
