package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.json.impl.DynamicTypeAdapterHolder;

@SerializerFor(targets = DString.class, hierarchical = true)
public class DStringTypeAdapter extends TypeAdapter<DString> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DString value, Writer writer) throws TEx, MalformedDataException {
        writer.value(value.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DString deserialize(Reader reader) throws TEx, MalformedDataException {
        if (reader.peek() == Token.STRING) return DFinal.of(reader.nextString());
        else return DynamicTypeAdapterHolder.getInstance().deserialize(reader).asString();
    }
}
