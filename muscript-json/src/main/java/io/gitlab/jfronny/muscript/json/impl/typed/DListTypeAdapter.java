package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.json.impl.DynamicTypeAdapterHolder;

import java.util.LinkedList;
import java.util.List;

@SerializerFor(targets = DList.class, hierarchical = true)
public class DListTypeAdapter extends TypeAdapter<DList> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DList value, Writer writer) throws TEx, MalformedDataException {
        writer.beginArray();
        for (Dynamic v : value.getValue()) {
            DynamicTypeAdapterHolder.getInstance().serialize(v, writer);
        }
        writer.endArray();
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DList deserialize(Reader reader) throws TEx, MalformedDataException {
        List<Dynamic> elements = new LinkedList<>();
        reader.beginArray();
        while (reader.peek() != Token.END_ARRAY) {
            elements.add(DynamicTypeAdapterHolder.getInstance().deserialize(reader));
        }
        reader.endArray();
        return DFinal.of(elements);
    }
}
