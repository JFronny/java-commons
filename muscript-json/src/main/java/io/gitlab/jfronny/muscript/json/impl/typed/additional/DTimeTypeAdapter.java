package io.gitlab.jfronny.muscript.json.impl.typed.additional;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DTime;

import java.time.LocalTime;

@SerializerFor(targets = DTime.class, hierarchical = true)
public class DTimeTypeAdapter extends TypeAdapter<DTime> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DTime value, Writer writer) throws TEx, MalformedDataException {
        writer.value(value.toString());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DTime deserialize(Reader reader) throws TEx, MalformedDataException {
        String s = reader.nextString();
        return new DTime(() -> LocalTime.parse(s));
    }
}
