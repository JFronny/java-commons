package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.json.impl.DynamicTypeAdapterHolder;

import java.util.LinkedHashMap;
import java.util.Map;

@SerializerFor(targets = DObject.class, hierarchical = true)
public class DObjectTypeAdapter extends TypeAdapter<DObject> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DObject value, Writer writer) throws TEx, MalformedDataException {
        writer.beginObject();
        for (Map.Entry<String, ? extends Dynamic> entry : value.getValue().entrySet()) {
            writer.name(entry.getKey());
            DynamicTypeAdapterHolder.getInstance().serialize(entry.getValue(), writer);
        }
        writer.endObject();
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DObject deserialize(Reader reader) throws TEx, MalformedDataException {
        Map<String, Dynamic> elements = new LinkedHashMap<>();
        reader.beginObject();
        while (reader.peek() != Token.END_OBJECT) {
            elements.put(reader.nextName(), DynamicTypeAdapterHolder.getInstance().deserialize(reader));
        }
        reader.endObject();
        return DFinal.of(elements);
    }
}
