package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.dynamic.DynamicBase;

@SerializerFor(targets = DynamicBase.class, hierarchical = true)
public class DynamicBaseTypeAdapter extends TypeAdapter<DynamicBase> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DynamicBase value, Writer writer) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException("Tried to write unsupported custom dynamic: " + value);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DynamicBase deserialize(Reader reader) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException("Tried to read unsupported custom dynamic");
    }
}
