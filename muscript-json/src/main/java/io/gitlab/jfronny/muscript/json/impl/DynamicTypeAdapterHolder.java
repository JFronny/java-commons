package io.gitlab.jfronny.muscript.json.impl;

import java.util.Objects;

public class DynamicTypeAdapterHolder {
    private static DynamicTypeAdapter INSTANCE;

    public static void setInstance(DynamicTypeAdapter instance) {
        INSTANCE = instance;
    }

    public static DynamicTypeAdapter getInstance() {
        return Objects.requireNonNull(INSTANCE);
    }
}
