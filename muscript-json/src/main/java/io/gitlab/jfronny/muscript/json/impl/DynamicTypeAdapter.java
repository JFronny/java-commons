package io.gitlab.jfronny.muscript.json.impl;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DDate;
import io.gitlab.jfronny.muscript.data.additional.DEnum;
import io.gitlab.jfronny.muscript.data.additional.DTime;
import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.json.impl.typed.*;
import io.gitlab.jfronny.muscript.json.impl.typed.additional.*;

@SerializerFor(targets = Dynamic.class)
public class DynamicTypeAdapter extends TypeAdapter<Dynamic> {
    public final DObjectTypeAdapter dObject = new DObjectTypeAdapter();
    public final DListTypeAdapter dList = new DListTypeAdapter();
    public final DNullTypeAdapter dNull = new DNullTypeAdapter();
    public final DBoolTypeAdapter dBool = new DBoolTypeAdapter();
    public final DNumberTypeAdapter dNumber = new DNumberTypeAdapter();
    public final DStringTypeAdapter dString = new DStringTypeAdapter();
    public final DCallableTypeAdapter dCallable = new DCallableTypeAdapter();
    public final DynamicBaseTypeAdapter dynamicBase = new DynamicBaseTypeAdapter();
    public final DDateTypeAdapter dDate = new DDateTypeAdapter();
    public final DTimeTypeAdapter dTime = new DTimeTypeAdapter();
    public final DEnumTypeAdapter dEnum = new DEnumTypeAdapter();

    public DynamicTypeAdapter() {
        DynamicTypeAdapterHolder.setInstance(this);
    }

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Dynamic value, Writer writer) throws TEx, MalformedDataException {
        if (value == null) writer.nullValue();
        else if (value instanceof DDate date) dDate.serialize(date, writer);
        else if (value instanceof DTime time) dTime.serialize(time, writer);
        else if (value instanceof DEnum enm) dEnum.serialize(enm, writer);
        else if (value.isObject()) dObject.serialize(value.asObject(), writer);
        else if (value.isList()) dList.serialize(value.asList(), writer);
        else if (value instanceof DNull n) dNull.serialize(n, writer);
        else if (value.isBool()) dBool.serialize(value.asBool(), writer);
        else if (value.isNumber()) dNumber.serialize(value.asNumber(), writer);
        else if (value.isString()) dString.serialize(value.asString(), writer);
        else if (value.isCallable()) dCallable.serialize(value.asCallable(), writer);
        else if (value instanceof DynamicBase base) dynamicBase.serialize(base, writer);
        else throw new IllegalArgumentException("Unexpected dynamic type for: " + value);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Dynamic deserialize(Reader reader) throws TEx, MalformedDataException {
        return switch (reader.peek()) {
            case BEGIN_OBJECT -> dObject.deserialize(reader);
            case BEGIN_ARRAY -> dList.deserialize(reader);
            case NULL -> dNull.deserialize(reader);
            case BOOLEAN -> dBool.deserialize(reader);
            case NUMBER -> dNumber.deserialize(reader);
            case STRING -> dString.deserialize(reader);
            default -> throw new IllegalStateException("Unsupported token for beginning of Dynamic: " + reader.peek());
        };
    }
}
