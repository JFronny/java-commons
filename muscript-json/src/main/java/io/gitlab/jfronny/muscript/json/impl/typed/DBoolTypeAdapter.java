package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DBool;

@SerializerFor(targets = DBool.class, hierarchical = true)
public class DBoolTypeAdapter extends TypeAdapter<DBool> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DBool value, Writer writer) throws TEx, MalformedDataException {
        writer.value(value.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DBool deserialize(Reader reader) throws TEx, MalformedDataException {
        return DFinal.of(reader.nextBoolean());
    }
}
