package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.dynamic.DNumber;

@SerializerFor(targets = DNumber.class, hierarchical = true)
public class DNumberTypeAdapter extends TypeAdapter<DNumber> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DNumber value, Writer writer) throws TEx, MalformedDataException {
        double d = value.getValue();
        if (d % 1.0 != 0 || d > Long.MAX_VALUE) writer.value(d);
        else writer.value((long) d);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DNumber deserialize(Reader reader) throws TEx, MalformedDataException {
        return DFinal.of(reader.nextDouble());
    }
}
