package io.gitlab.jfronny.muscript.json;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class JsonLib {
    public static Scope addTo(Scope scope) {
        return new TransportLib<>(new LenientTransport(), new StrictTransport(), "toJson", "fromJson").addTo(scope);
    }

    private static class StrictTransport extends JsonTransport {
        @Override
        public JsonReader createReader(Reader source) {
            return super.createReader(source)
                    .setLenient(false)
                    .setSerializeSpecialFloatingPointValues(true);
        }

        @Override
        public JsonWriter createWriter(Writer target) throws IOException {
            return super.createWriter(target)
                    .setLenient(false)
                    .setNewline("")
                    .setIndent("")
                    .setSerializeSpecialFloatingPointValues(true)
                    .setSerializeNulls(true)
                    .setOmitQuotes(false);
        }
    }

    private static class LenientTransport extends JsonTransport {
        @Override
        public JsonReader createReader(Reader source) {
            return super.createReader(source)
                    .setLenient(true)
                    .setSerializeSpecialFloatingPointValues(true);
        }

        @Override
        public JsonWriter createWriter(Writer target) throws IOException {
            return super.createWriter(target)
                    .setLenient(true)
                    .setNewline("\n")
                    .setIndent("  ")
                    .setSerializeSpecialFloatingPointValues(true)
                    .setSerializeNulls(true)
                    .setOmitQuotes(true);
        }
    }
}
