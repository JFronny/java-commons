package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.dynamic.DCallable;

@SerializerFor(targets = DCallable.class, hierarchical = true)
public class DCallableTypeAdapter extends TypeAdapter<DCallable> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DCallable value, Writer writer) throws TEx, MalformedDataException {
        if (value.getName().equals(DCallable.DEFAULT_NAME)) throw new IllegalArgumentException("Unnamed callables cannot be serialized to json");
        else writer.value(value.getName());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DCallable deserialize(Reader reader) throws TEx, MalformedDataException {
        throw new UnsupportedOperationException("DCallables cannot be deserialized from json");
    }
}
