package io.gitlab.jfronny.muscript.json.impl.typed;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.dynamic.DNull;

@SerializerFor(targets = DNull.class, hierarchical = true)
public class DNullTypeAdapter extends TypeAdapter<DNull> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DNull value, Writer writer) throws TEx, MalformedDataException {
        writer.nullValue();
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DNull deserialize(Reader reader) throws TEx, MalformedDataException {
        reader.nextNull();
        return new DNull();
    }
}
