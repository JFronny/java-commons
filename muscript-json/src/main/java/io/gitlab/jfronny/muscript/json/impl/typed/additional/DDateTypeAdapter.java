package io.gitlab.jfronny.muscript.json.impl.typed.additional;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.data.additional.DDate;

import java.time.LocalDate;

@SerializerFor(targets = DDate.class, hierarchical = true)
public class DDateTypeAdapter extends TypeAdapter<DDate> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(DDate value, Writer writer) throws TEx, MalformedDataException {
        writer.value(value.toString());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> DDate deserialize(Reader reader) throws TEx, MalformedDataException {
        String s = reader.nextString();
        return new DDate(() -> LocalDate.parse(s));
    }
}
