# μScript-gson
μScript-gson bridges the gap between μScript and commons-serialize-json by supporting the serialization and deserialization of `Dynamic` to json and exposing this functionality to μScript scripts.
The gson type adapters are automatically registered via the ServiceLoader mechanism, but the μScript methods need to be added to your `Scope` manually via `JsonLib.addTo(scope)`.
Other than that, no setup is needed.

## Exposed functions
| signature                    | description                                           |
|------------------------------|-------------------------------------------------------|
| `toJson(src: any): string`   | Serializes `scr` to json                              |
| `fromJson(src: string): any` | Deserialized `src` into the appropriate μScript types |