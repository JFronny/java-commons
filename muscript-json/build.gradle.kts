import io.gitlab.jfronny.scripts.*
import org.gradle.api.publish.maven.internal.publication.DefaultMavenPublication

plugins {
    commons.library
    alias(libs.plugins.shadow)
}

dependencies {
    implementation(projects.commons)
    implementation(projects.muscriptDataAdditional)
    implementation(projects.commonsSerializeJson)
    implementation(projects.commonsSerializeDatabindApi)

    testImplementation(libs.junit.jupiter.api)
    testImplementation(projects.muscriptParser)
    testImplementation(projects.muscriptRuntime)
    testImplementation(projects.commonsSerializeDatabind)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

tasks.shadowJar {
    archiveClassifier = "nojpms"
    dependencies {
        exclude { true }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "muscript-json"

            from(components["java"])
        }
        create<MavenPublication>("mavenNoJPMS") {
            groupId = "io.gitlab.jfronny"
            artifactId = "muscript-json-nojpms"

            from(components["shadow"])

            if (this is DefaultMavenPublication) this.isAlias = true
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize/$version/raw", projects.commonsSerialize)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize-json/$version/raw", projects.commonsSerializeJson)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize-databind/$version/raw", projects.commonsSerializeJson)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/muscript-data-dynamic/$version/raw", projects.muscriptDataDynamic)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/muscript-data-additional/$version/raw", projects.muscriptDataAdditional)
}
