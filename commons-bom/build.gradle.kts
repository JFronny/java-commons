plugins {
    `java-platform`
    `maven-publish`
}

version = rootProject.version

publishing {
    repositories {
        mavenLocal()

        if (project.hasProperty("maven")) {
            maven {
                url = uri(project.property("maven").toString())
                name = "dynamic"

                credentials(PasswordCredentials::class) {
                    username = System.getenv()["MAVEN_NAME"]
                    password = System.getenv()["MAVEN_TOKEN"]
                }
                authentication {
                    create<BasicAuthentication>("basic")
                }
            }
        }
    }

    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-bom"

            from(components["javaPlatform"])
        }
    }
}

tasks.withType(GenerateModuleMetadata::class) {
    enabled = true
}

dependencies {
    constraints {
        for (proj in rootProject.allprojects) {
            if (proj == project || proj == rootProject) {
                continue
            }
            if (proj.name == "commons-catalog") {
                continue
            }

            api(project(proj.path))
        }
    }
}