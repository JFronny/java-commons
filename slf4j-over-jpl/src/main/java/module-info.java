module io.gitlab.jfronny.slf4jpl {
    requires org.slf4j;
    provides org.slf4j.spi.SLF4JServiceProvider with io.gitlab.jfronny.commons.slf4jpl.CommonsServiceProvider;
}