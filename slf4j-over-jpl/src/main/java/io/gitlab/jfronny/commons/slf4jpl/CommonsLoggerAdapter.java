package io.gitlab.jfronny.commons.slf4jpl;

import org.slf4j.Marker;
import org.slf4j.event.Level;
import org.slf4j.helpers.AbstractLogger;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.spi.LocationAwareLogger;

public class CommonsLoggerAdapter extends AbstractLogger implements LocationAwareLogger {
    private final System.Logger logger;

    public CommonsLoggerAdapter(System.Logger logger) {
        this.logger = logger;
        this.name = logger.getName();
    }

    @Override
    public boolean isTraceEnabled() {
        return logger.isLoggable(System.Logger.Level.TRACE);
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return isTraceEnabled();
    }

    @Override
    public boolean isDebugEnabled() {
        return logger.isLoggable(System.Logger.Level.DEBUG);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return isDebugEnabled();
    }

    @Override
    public boolean isInfoEnabled() {
        return logger.isLoggable(System.Logger.Level.INFO);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return isInfoEnabled();
    }

    @Override
    public boolean isWarnEnabled() {
        return logger.isLoggable(System.Logger.Level.WARNING);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return isWarnEnabled();
    }

    @Override
    public boolean isErrorEnabled() {
        return logger.isLoggable(System.Logger.Level.ERROR);
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return isErrorEnabled();
    }

    private static final String SELF = CommonsLoggerAdapter.class.getName();

    @Override
    protected String getFullyQualifiedCallerName() {
        return SELF;
    }

    @Override
    protected void handleNormalizedLoggingCall(Level level, Marker marker, String messagePattern, Object[] arguments, Throwable throwable) {
        innerNormalizedLoggingCall(SELF, switch (level) {
            case TRACE -> System.Logger.Level.TRACE;
            case DEBUG -> System.Logger.Level.DEBUG;
            case INFO -> System.Logger.Level.INFO;
            case WARN -> System.Logger.Level.WARNING;
            case ERROR -> System.Logger.Level.ERROR;
        }, marker, messagePattern, arguments, throwable);
    }

    @Override
    public void log(Marker marker, String fqcn, int level, String message, Object[] argArray, Throwable t) {
        innerNormalizedLoggingCall(fqcn, switch (level) {
            case LocationAwareLogger.TRACE_INT -> System.Logger.Level.TRACE;
            case LocationAwareLogger.DEBUG_INT -> System.Logger.Level.DEBUG;
            case LocationAwareLogger.INFO_INT -> System.Logger.Level.INFO;
            case LocationAwareLogger.WARN_INT -> System.Logger.Level.WARNING;
            case LocationAwareLogger.ERROR_INT -> System.Logger.Level.ERROR;
            default -> System.Logger.Level.INFO; // Should never happen
        }, marker, message, argArray, t);
    }

    private void innerNormalizedLoggingCall(String fqcn, System.Logger.Level level, Marker marker, String messagePattern, Object[] arguments, Throwable throwable) {
        if (logger.isLoggable(level)) {
            if (arguments != null) messagePattern = MessageFormatter.basicArrayFormat(messagePattern, arguments);
            if (throwable != null) logger.log(level, messagePattern, throwable);
            else logger.log(level, messagePattern);
        }
    }
}
