plugins {
    commons.library
}

dependencies {
    implementation(libs.slf4j.api)

    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "slf4j-over-jpl"

            from(components["java"])
        }
    }
}
