plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)

    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-flow"

            from(components["java"])
        }
    }
}

//tasks.compileJava {
//    options.compilerArgs.addAll(listOf("--add-exports", "java.base/jdk.internal.vm=io.gitlab.jfronny.commons"))
//}
