module io.gitlab.jfronny.commons.flow {
    uses io.gitlab.jfronny.commons.flow.spi.FlowBackend;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.commons.flow;
    exports io.gitlab.jfronny.commons.flow.spi;
}