package io.gitlab.jfronny.commons.flow.impl;

import io.gitlab.jfronny.commons.flow.PrioritizedRunnable;

public record WrappingPrioritizedRunnable(Runnable task, int priority) implements PrioritizedRunnable {
    public WrappingPrioritizedRunnable {
        task = task instanceof WrappingPrioritizedRunnable(var task1, var priority1) ? task1: task;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public void run() {
        task.run();
    }
}
