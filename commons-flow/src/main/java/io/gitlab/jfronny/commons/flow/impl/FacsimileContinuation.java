package io.gitlab.jfronny.commons.flow.impl;

import io.gitlab.jfronny.commons.flow.Continuation;
import io.gitlab.jfronny.commons.flow.ContinuationScope;

import java.util.Objects;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public final class FacsimileContinuation implements Continuation {
    private final FacsimileContinuationScope scope;
    private final Runnable target;
    private final Thread thread;
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();
    private final Condition awakeCondition = lock.newCondition();
    private FacsimileContinuationState state = FacsimileContinuationState.NEW;

    public FacsimileContinuation(FacsimileContinuationScope scope, Runnable target) {
        this.scope = scope;
        this.target = target;
        this.thread = new Thread(() -> {
            try {
                target.run();
            } finally {
                lock.lock();
                try {
                    state = FacsimileContinuationState.DONE;
                    awakeCondition.signal();
                } finally {
                    lock.unlock();
                }
            }
        }, "ContinuationThread-" + target.hashCode());
    }

    @Override
    public ContinuationScope getScope() {
        return scope;
    }

    @Override
    public void run() {
        lock.lock();
        try {
            switch (state) {
                case NEW -> {
                    state = FacsimileContinuationState.RUNNING;
                    scope.updateCurrentContinuation(this);
                    thread.start();
                }
                case YIELDED -> {
                    state = FacsimileContinuationState.RUNNING;
                    scope.updateCurrentContinuation(this);
                    lock.lock();
                    try {
                        condition.signal();
                    } finally {
                        lock.unlock();
                    }
                }
                case DONE -> throw new IllegalStateException("Continuation already done");
                case RUNNING -> throw new IllegalStateException("Continuation already running");
            }
            awakeCondition.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    public boolean yield() {
        if (Thread.currentThread() != thread) throw new IllegalStateException("Continuation must not be yielded from a different thread");
        if (state != FacsimileContinuationState.RUNNING) return false;
        state = FacsimileContinuationState.YIELDED;
        lock.lock();
        try {
            awakeCondition.signal();
            condition.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
        return true;
    }

    @Override
    public boolean isDone() {
        return state == FacsimileContinuationState.DONE;
    }

    @Override
    public boolean isPreempted() {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (FacsimileContinuation) obj;
        return Objects.equals(this.scope, that.scope) &&
                Objects.equals(this.target, that.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scope, target);
    }

    @Override
    public String toString() {
        return "FacsimileContinuation[" +
                "scope=" + scope + ", " +
                "target=" + target + ']';
    }
}
