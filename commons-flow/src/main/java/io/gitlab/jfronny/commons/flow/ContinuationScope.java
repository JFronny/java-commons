package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.flow.spi.FlowBackend;

/**
 * A class intended to behave like a JVM ContinuationScope but without the need to use the internal API.
 * The exact implementation may need to change in the future to adapt to changes in the internal API or how we access it.
 */
public interface ContinuationScope {
    static ContinuationScope create(String name) {
        return FlowBackend.INSTANCE.createScope(name);
    }

    static ContinuationScope create() {
        return create("io.gitlab.jfronny.commons.flow.ContinuationScope");
    }

    String getName();
}
