package io.gitlab.jfronny.commons.flow.impl;

import io.gitlab.jfronny.commons.flow.ContinuationScope;

import java.util.Objects;

public final class FacsimileContinuationScope implements ContinuationScope {
    private final String name;
    private FacsimileContinuation currentContinuation;

    public FacsimileContinuationScope(String name) {
        this.name = name;
    }

    void updateCurrentContinuation(FacsimileContinuation continuation) {
        currentContinuation = continuation;
    }

    FacsimileContinuation getCurrentContinuation() {
        return currentContinuation;
    }

    boolean yield() {
        FacsimileContinuation cnt = getCurrentContinuation();
        if (cnt == null) return false;
        return cnt.yield();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (FacsimileContinuationScope) obj;
        return Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "FacsimileContinuationScope[" +
                "name=" + name + ']';
    }
}
