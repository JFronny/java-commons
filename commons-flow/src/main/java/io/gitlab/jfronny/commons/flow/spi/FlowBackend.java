package io.gitlab.jfronny.commons.flow.spi;

import io.gitlab.jfronny.commons.flow.impl.FacsimileBackend;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ServiceLoader;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

@ApiStatus.OverrideOnly
public interface FlowBackend<Continuation extends io.gitlab.jfronny.commons.flow.Continuation, ContinuationScope extends io.gitlab.jfronny.commons.flow.ContinuationScope> {
    FlowBackend INSTANCE = ServiceLoader.load(FlowBackend.class).findFirst().orElseGet(FacsimileBackend::new);

    @NotNull ContinuationScope createScope(@NotNull String name);
    @NotNull Continuation createContinuation(@NotNull ContinuationScope scope, @NotNull Runnable target);
    @Nullable Continuation getCurrentContinuation(@NotNull ContinuationScope scope);
    boolean yield(ContinuationScope scope);
    @NotNull Thread newVirtualThread(@Nullable Executor scheduler, @Nullable String name, int characteristics, @NotNull Runnable task);
    @NotNull Thread.Builder.OfVirtual ofVirtual(@Nullable Executor scheduler);
    @NotNull
    ForkJoinWorkerThread createCarrierThread(@NotNull ForkJoinPool pool);
    void setVirtualThreadPriority(@NotNull Thread virtualThread, int priority);
    int getVirtualThreadPriority(@NotNull Thread virtualThread);
}
