package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.flow.spi.FlowBackend;

/**
 * A class intended to behave like a JVM Continuation but without the need to use the internal API.
 * The exact implementation may need to change in the future to adapt to changes in the internal API or how we access it.
 * For example, if our encapsulation bypass stops working, we may need to rely on threads to simulate continuations like <a href="https://github.com/forax/loom-fiber/blob/master/src/main/java/fr/umlv/loom/continuation/Continuation.java">here</a>.
 */
public interface Continuation {
    static Continuation create(ContinuationScope scope, Runnable target) {
        return FlowBackend.INSTANCE.createContinuation(scope, target);
    }

    static Continuation getCurrentContinuation(ContinuationScope scope) {
        return FlowBackend.INSTANCE.getCurrentContinuation(scope);
    }

    static boolean yield(ContinuationScope scope) {
        return FlowBackend.INSTANCE.yield(scope);
    }

    ContinuationScope getScope();
    void run();
    boolean isDone();
    boolean isPreempted();
}
