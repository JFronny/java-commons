package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.flow.impl.WrappingPrioritizedRunnable;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

/**
 * A runnable with an attached priority
 */
public interface PrioritizedRunnable extends Runnable, Comparable<PrioritizedRunnable> {
    Comparator<Runnable> COMPARATOR = Comparator.comparing(PrioritizedRunnable::of);
    int DEFAULT_PRIORITY = Thread.NORM_PRIORITY;

    static PrioritizedRunnable of(Runnable runnable) {
        return runnable instanceof PrioritizedRunnable pr ? pr : of(runnable, DEFAULT_PRIORITY);
    }

    static PrioritizedRunnable of(Runnable runnable, int priority) {
        return new WrappingPrioritizedRunnable(runnable, priority);
    }

    int getPriority();

    @Override
    @ApiStatus.NonExtendable
    default int compareTo(@NotNull PrioritizedRunnable o) {
        return Integer.compare(getPriority(), o.getPriority());
    }
}
