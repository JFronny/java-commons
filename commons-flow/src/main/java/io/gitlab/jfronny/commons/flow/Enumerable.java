package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.OnceSupplier;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Enumerable<T> implements Iterable<T> {
    private final Supplier<Enumerator.Body<T>> body;

    public Enumerable(Enumerator.Body<T> body) {
        this(new OnceSupplier<>(Objects.requireNonNull(body)));
    }

    public Enumerable(Supplier<Enumerator.Body<T>> body) {
        this.body = Objects.requireNonNull(body);
    }

    @NotNull
    @Override
    public Enumerator<T> iterator() {
        return new Enumerator<>(body.get());
    }

    public Stream<T> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}
