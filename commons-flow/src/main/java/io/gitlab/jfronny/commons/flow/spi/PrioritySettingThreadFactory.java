package io.gitlab.jfronny.commons.flow.spi;

import io.gitlab.jfronny.commons.flow.PrioritizedRunnable;
import io.gitlab.jfronny.commons.flow.ScheduledVirtualThreadBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ThreadFactory;

public class PrioritySettingThreadFactory implements ThreadFactory {
    private final ThreadFactory delegate;

    public PrioritySettingThreadFactory(ThreadFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public Thread newThread(@NotNull Runnable r) {
        Thread thread = delegate.newThread(r);
        if (r instanceof PrioritizedRunnable pr) ScheduledVirtualThreadBuilder.setPriority(thread, pr.getPriority());
        return thread;
    }
}
