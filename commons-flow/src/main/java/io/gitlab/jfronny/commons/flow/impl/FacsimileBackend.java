package io.gitlab.jfronny.commons.flow.impl;

import io.gitlab.jfronny.commons.flow.spi.FlowBackend;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

public class FacsimileBackend implements FlowBackend<FacsimileContinuation, FacsimileContinuationScope> {
    @Override
    public @NotNull FacsimileContinuationScope createScope(@NotNull String name) {
        return new FacsimileContinuationScope(name);
    }

    @Override
    public @NotNull FacsimileContinuation createContinuation(@NotNull FacsimileContinuationScope scope, @NotNull Runnable target) {
        return new FacsimileContinuation(scope, target);
    }

    @Override
    public @Nullable FacsimileContinuation getCurrentContinuation(@NotNull FacsimileContinuationScope scope) {
        return scope.getCurrentContinuation();
    }

    @Override
    public boolean yield(FacsimileContinuationScope scope) {
        return scope.yield();
    }

    @Override
    public @NotNull Thread newVirtualThread(@Nullable Executor scheduler, @Nullable String name, int characteristics, @NotNull Runnable task) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Thread.Builder.@NotNull OfVirtual ofVirtual(@Nullable Executor scheduler) {
        throw new UnsupportedOperationException();
    }

    @Override
    public @NotNull ForkJoinWorkerThread createCarrierThread(@NotNull ForkJoinPool pool) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setVirtualThreadPriority(@NotNull Thread virtualThread, int priority) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getVirtualThreadPriority(@NotNull Thread virtualThread) {
        throw new UnsupportedOperationException();
    }
}
