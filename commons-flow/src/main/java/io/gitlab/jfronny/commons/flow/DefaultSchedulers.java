package io.gitlab.jfronny.commons.flow;

import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;

public class DefaultSchedulers {
    public static ForkJoinPool createDefaultScheduler() {
        return createDefaultScheduler(Props.read());
    }

    public static ForkJoinPool createDefaultScheduler(Props props) {
        // See VirtualThread.createDefaultScheduler
        ForkJoinPool.ForkJoinWorkerThreadFactory factory = ScheduledVirtualThreadBuilder::createCarrierThread;
        Thread.UncaughtExceptionHandler handler = (t, e) -> { };
        boolean asyncMode = true; // FIFO
        return new ForkJoinPool(props.parallelism, factory, handler, asyncMode,
                0, props.maxPoolSize, props.minRunnable, pool -> true, 30, SECONDS);
    }

    public static ThreadPoolExecutor createPriorityScheduler() {
        return createPriorityScheduler(Props.read());
    }

    public static ThreadPoolExecutor createPriorityScheduler(Props props) {
        BlockingQueue<Runnable> workQueue = new PriorityBlockingQueue<>(props.parallelism, PrioritizedRunnable.COMPARATOR.reversed());
        return new ThreadPoolExecutor(0, props.maxPoolSize, 30, SECONDS, workQueue);
    }

    public record Props(int parallelism, int maxPoolSize, int minRunnable) {
        public static Props read() {
            int parallelism, maxPoolSize, minRunnable;
            String parallelismValue = System.getProperty("jdk.virtualThreadScheduler.parallelism");
            String maxPoolSizeValue = System.getProperty("jdk.virtualThreadScheduler.maxPoolSize");
            String minRunnableValue = System.getProperty("jdk.virtualThreadScheduler.minRunnable");
            if (parallelismValue != null) {
                parallelism = Integer.parseInt(parallelismValue);
            } else {
                parallelism = Runtime.getRuntime().availableProcessors();
            }
            if (maxPoolSizeValue != null) {
                maxPoolSize = Integer.parseInt(maxPoolSizeValue);
                parallelism = Integer.min(parallelism, maxPoolSize);
            } else {
                maxPoolSize = Integer.max(parallelism, 256);
            }
            if (minRunnableValue != null) {
                minRunnable = Integer.parseInt(minRunnableValue);
            } else {
                minRunnable = Integer.max(parallelism / 2, 1);
            }
            return new Props(parallelism, maxPoolSize, minRunnable);
        }
    }
}
