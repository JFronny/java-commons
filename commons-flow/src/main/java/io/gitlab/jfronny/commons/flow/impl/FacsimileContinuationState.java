package io.gitlab.jfronny.commons.flow.impl;

public enum FacsimileContinuationState {
    NEW, RUNNING, YIELDED, DONE
}
