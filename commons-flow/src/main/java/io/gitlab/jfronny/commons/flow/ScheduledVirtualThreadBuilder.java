package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.flow.spi.PrioritySettingThreadFactory;
import io.gitlab.jfronny.commons.flow.spi.FlowBackend;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.*;

/**
 * Utility class for creating schedulers and virtual threads using them.
 * NOTE: virtual threads DO NOT use the priority of their Runnable, since that is not stored in them.
 * Instead, you can use {@link ScheduledVirtualThreadBuilder#setPriority} to set their priority.
 */
public class ScheduledVirtualThreadBuilder {
    /**
     * Creates a new unstarted virtual thread with the given scheduler, name, characteristics and task.
     * Also copies the priority from the task to the thread.
     *
     * @param scheduler the scheduler to use, or null for the default scheduler
     * @param name the name of the thread, or null for the default name
     * @param characteristics the characteristics of the thread
     * @param task the task to run on the thread
     * @return the created thread
     */
    public static @NotNull Thread newVirtualThread(@Nullable Executor scheduler, @Nullable String name, int characteristics, @NotNull Runnable task) {
        return FlowBackend.INSTANCE.newVirtualThread(scheduler, name, characteristics, task);
    }

    /**
     * Creates a new virtual thread with the given scheduler and task, and starts it.
     * Also copies the priority from the task to the thread.
     *
     * @param scheduler the scheduler to use, or null for the default scheduler
     * @param task the task to run on the thread
     * @return the created and started thread
     */
    public static @NotNull Thread startVirtualThread(@Nullable Executor scheduler, @NotNull Runnable task) {
        Thread thread = newVirtualThread(scheduler, null, 0, task);
        thread.start();
        return thread;
    }

    /**
     * Creates an object that allows creating virtual threads with the given scheduler.
     * Does NOT copy the priority from the task to the thread, please ensure to set it manually.
     *
     * @param scheduler the scheduler to use, or null for the default scheduler
     * @return a builder for creating virtual threads
     */
    public static @NotNull Thread.Builder.OfVirtual ofVirtual(@Nullable Executor scheduler) {
        return FlowBackend.INSTANCE.ofVirtual(scheduler);
    }

    /**
     * Creates a new thread factory that creates virtual threads with the given scheduler.
     * Also copies the priority from submitted tasks to the thread.
     *
     * @param scheduler the scheduler to use, or null for the default scheduler
     * @return a new thread factory that creates virtual threads with the given scheduler
     */
    public static @NotNull ThreadFactory newThreadFactory(@Nullable Executor scheduler) {
        return new PrioritySettingThreadFactory(ofVirtual(scheduler).factory());
    }

    /**
     * Creates a new carrier thread for the given pool.
     * Should be used for creating carrier threads for virtual thread pools based on a ForkJoinPool.
     *
     * @param pool the pool to create the carrier thread for
     * @return a new carrier thread for the given pool
     */
    public static @NotNull ForkJoinWorkerThread createCarrierThread(@NotNull ForkJoinPool pool) {
        return FlowBackend.INSTANCE.createCarrierThread(pool);
    }

    /**
     * Creates a new virtual thread executor that creates virtual threads with the given scheduler.
     * Also tries to copy the priority from submitted tasks to the thread.
     * Do note that not all methods of ExecutorService may allow doing so. Please use with caution.
     *
     * @param scheduler the scheduler to use, or null for the default scheduler
     * @return a new virtual thread executor that creates virtual threads with the given scheduler
     */
    public static ExecutorService newVirtualThreadPerTaskExecutor(@Nullable Executor scheduler) {
        return Executors.newThreadPerTaskExecutor(newThreadFactory(scheduler));
    }

    public static void setPriority(Thread thread, int priority) {
        if (!thread.isVirtual()) thread.setPriority(priority);
        else FlowBackend.INSTANCE.setVirtualThreadPriority(thread, priority);
    }

    public static int getPriority(Thread thread) {
        if (!thread.isVirtual()) return thread.getPriority();
        return FlowBackend.INSTANCE.getVirtualThreadPriority(thread);
    }
}
