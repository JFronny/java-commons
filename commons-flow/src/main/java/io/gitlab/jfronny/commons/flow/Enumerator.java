package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.MultiConsumer;
import io.gitlab.jfronny.commons.SamWithReceiver;
import io.gitlab.jfronny.commons.data.PeekableIterator;

import java.util.NoSuchElementException;
import java.util.Objects;

public class Enumerator<T> implements PeekableIterator<T> {
    private final ContinuationScope scope;
    private final Continuation body;
    private boolean peeked = false;
    private T next;

    public Enumerator(Body<T> body) {
        this(ContinuationScope.create("Enumerator"), body);
    }

    public Enumerator(ContinuationScope scope, Body<T> body) {
        this.scope = Objects.requireNonNull(scope);
        this.body = Continuation.create(scope, () -> body.run(item -> {
            next = item;
            peeked = true;
            Continuation.yield(scope);
        }));
    }

    public ContinuationScope getScope() {
        return scope;
    }

    @Override
    public boolean hasNext() {
        synchronized (body) {
            if (peeked) return true;
            if (body.isDone()) return false;
            body.run();
            return peeked;
        }
    }

    @Override
    public T next() {
        synchronized (body) {
            if (!peeked) {
                if (!hasNext()) throw new NoSuchElementException("No more elements");
            }
            peeked = false;
            T next = this.next;
            this.next = null; // clear reference
            return next;
        }
    }

    @Override
    public T peek() {
        synchronized (body) {
            if (!peeked) {
                if (!hasNext()) throw new NoSuchElementException("No more elements");
            }
            return next;
        }
    }

    @FunctionalInterface
    @SamWithReceiver
    public interface Body<T> {
        void run(MultiConsumer<T> items);
    }
}
