package io.gitlab.jfronny.commons.flow;

import io.gitlab.jfronny.commons.flow.impl.RemovableThreadPoolExecutorWrapper;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

public interface RemovableExecutor extends Executor {
    static @Nullable RemovableExecutor of(Executor executor) {
        return switch (executor) {
            case RemovableExecutor xt -> xt;
            case ThreadPoolExecutor tp -> new RemovableThreadPoolExecutorWrapper(tp);
            case null, default -> null;
        };
    }

    boolean remove(Runnable runnable);
}
