package io.gitlab.jfronny.commons.flow.impl;

import io.gitlab.jfronny.commons.flow.RemovableExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ThreadPoolExecutor;

public record RemovableThreadPoolExecutorWrapper(ThreadPoolExecutor tpr) implements RemovableExecutor {
    @Override
    public boolean remove(Runnable runnable) {
        return tpr.remove(runnable);
    }

    @Override
    public void execute(@NotNull Runnable command) {
        tpr.execute(command);
    }
}
