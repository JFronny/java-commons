package io.gitlab.jfronny.commons.flow;

import java.util.ArrayDeque;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Consumer;

public class QueueingConsumer<T> implements Consumer<T> {
    private Queue<T> queue = new ArrayDeque<>();
    private Consumer<T> delegate = null;

    public synchronized void setDelegate(Consumer<T> delegate) {
        if (this.delegate != null) throw new IllegalStateException("Delegate already set");
        this.delegate = Objects.requireNonNull(delegate);
        for (T t : this.queue) {
            delegate.accept(t);
        }
        this.queue = null;
    }

    @Override
    public synchronized void accept(T t) {
        if (delegate != null) {
            delegate.accept(t);
        } else {
            queue.add(t);
        }
    }
}
