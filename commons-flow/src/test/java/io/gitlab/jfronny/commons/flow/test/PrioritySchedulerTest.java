package io.gitlab.jfronny.commons.flow.test;

import io.gitlab.jfronny.commons.flow.DefaultSchedulers;
import io.gitlab.jfronny.commons.flow.PrioritizedRunnable;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrioritySchedulerTest {
    class Ctx {
        String data;
    }
    Runnable simpleTask(String data, Ctx ctx) {
        return () -> {
            sleep(50);
            ctx.data = data;
            System.out.println(data);
        };
    }
    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    DefaultSchedulers.Props props = new DefaultSchedulers.Props(3, 1, 0);

    @Test
    void testInOrder() {
        Ctx ctx = new Ctx();
        Runnable lowPriority = PrioritizedRunnable.of(simpleTask("low", ctx), -10);
        Runnable noPriority = simpleTask("mid", ctx);
        Runnable highPriority = PrioritizedRunnable.of(simpleTask("high", ctx), 10);
        ThreadPoolExecutor scheduler = DefaultSchedulers.createPriorityScheduler(props);
        scheduler.execute(highPriority);
        scheduler.execute(noPriority);
        scheduler.execute(lowPriority);
        sleep(200);
        assertEquals("low", ctx.data);
    }

    @Test
    void testReverseOrder() {
        Ctx ctx = new Ctx();
        Runnable lowPriority = PrioritizedRunnable.of(simpleTask("low", ctx), -10);
        Runnable noPriority = simpleTask("mid", ctx);
        Runnable highPriority = PrioritizedRunnable.of(simpleTask("high", ctx), 10);
        ThreadPoolExecutor scheduler = DefaultSchedulers.createPriorityScheduler(props);
        scheduler.execute(lowPriority);
        scheduler.execute(noPriority);
        scheduler.execute(highPriority);
        sleep(200);
        assertEquals("low", ctx.data);
    }

    @Test
    void testOtherOrder() {
        Ctx ctx = new Ctx();
        Runnable lowPriority = PrioritizedRunnable.of(simpleTask("low", ctx), -10);
        Runnable noPriority = simpleTask("mid", ctx);
        Runnable highPriority = PrioritizedRunnable.of(simpleTask("high", ctx), 10);
        ThreadPoolExecutor scheduler = DefaultSchedulers.createPriorityScheduler(props);
        scheduler.execute(lowPriority);
        scheduler.execute(highPriority);
        scheduler.execute(noPriority);
        sleep(200);
        assertEquals("low", ctx.data);
    }
}
