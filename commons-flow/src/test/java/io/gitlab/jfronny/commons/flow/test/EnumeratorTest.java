package io.gitlab.jfronny.commons.flow.test;

import io.gitlab.jfronny.commons.flow.Enumerable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnumeratorTest {
    @Test
    void simpleTest() {
        String sd = "";
        for (String s : sampleFunction()) {
            sd += s;
        }
        assertEquals("HelloWorldTest", sd);
    }

    Enumerable<String> sampleFunction() {
        return new Enumerable<>(items -> {
            items.accept("Hello");
            items.accept("World");
            items.accept("Test");
        });
    }
}
