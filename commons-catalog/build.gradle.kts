plugins {
    `version-catalog`
    `maven-publish`
}

version = rootProject.version

publishing {
    repositories {
        mavenLocal()

        if (project.hasProperty("maven")) {
            maven {
                url = uri(project.property("maven").toString())
                name = "dynamic"

                credentials(PasswordCredentials::class) {
                    username = System.getenv()["MAVEN_NAME"]
                    password = System.getenv()["MAVEN_TOKEN"]
                }
                authentication {
                    create<BasicAuthentication>("basic")
                }
            }
        }
    }

    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-catalog"

            from(components["versionCatalog"])
        }
    }
}

tasks.withType(GenerateModuleMetadata::class) {
    enabled = true
}

tasks.register("configureCatalog") {
    doFirst {
        doConfigureCatalog()
    }
}
tasks.named("generateCatalogAsToml") {
    dependsOn("configureCatalog")
}

fun doConfigureCatalog() {
    for (proj in rootProject.allprojects) {
        if (proj == project || proj == rootProject) {
            continue
        }

        val catalogName = if (proj.name == "commons-bom") "bom" else proj.name

        catalog {
            versionCatalog {
                library(catalogName, "$group:${proj.name}:${proj.version}")
            }
        }
    }
}
