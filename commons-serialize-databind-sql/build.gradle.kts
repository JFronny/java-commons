import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    implementation(projects.commonsSerializeDatabind)

    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-serialize-databind-sql"

            from(components["java"])
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize/$version/raw", projects.commonsSerialize)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize-databind/$version/raw", projects.commonsSerializeDatabind)
    enabled = false
}
