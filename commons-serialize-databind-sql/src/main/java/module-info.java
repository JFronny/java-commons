module io.gitlab.jfronny.commons.serialize.databind.sql {
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.serialize;
    requires io.gitlab.jfronny.commons.serialize.databind;
    requires io.gitlab.jfronny.commons.serialize.databind.api;
    requires java.sql;
    requires static org.jetbrains.annotations;
    provides io.gitlab.jfronny.commons.serialize.databind.TypeAdapterFactory with io.gitlab.jfronny.commons.serialize.databind.sql.SqlTimestampTypeAdapterFactory;
    provides io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter with
            io.gitlab.jfronny.commons.serialize.databind.sql.SqlDateTypeAdapter,
            io.gitlab.jfronny.commons.serialize.databind.sql.SqlTimeTypeAdapter;
}