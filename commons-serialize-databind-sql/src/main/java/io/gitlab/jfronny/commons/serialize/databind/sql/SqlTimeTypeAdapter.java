/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.jfronny.commons.serialize.databind.sql;


import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Adapter for java.sql.Time. Although this class appears stateless, it is not. DateFormat captures
 * its time zone and locale when it is created, which gives this class state. DateFormat isn't
 * thread safe either, so this class has to synchronize its read and write methods.
 */
@SuppressWarnings("JavaUtilDate")
@SerializerFor(targets = Time.class)
public final class SqlTimeTypeAdapter extends TypeAdapter<Time> {
  private final DateFormat format = new SimpleDateFormat("hh:mm:ss a");

  @Override
  public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Time value, Writer writer) throws TEx, MalformedDataException {
    if (value == null) {
      writer.nullValue();
      return;
    }
    String timeString;
    synchronized (this) {
      timeString = format.format(value);
    }
    writer.value(timeString);
  }

  @Override
  public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Time deserialize(Reader reader) throws TEx, MalformedDataException {
    if (reader.peek() == Token.NULL) {
      reader.nextNull();
      return null;
    }
    String s = reader.nextString();
    synchronized (this) {
      TimeZone originalTimeZone = format.getTimeZone(); // Save the original time zone
      try {
        Date date = format.parse(s);
        return new Time(date.getTime());
      } catch (ParseException e) {
        throw new MalformedDataException(
                "Failed parsing '" + s + "' as SQL Time; at path " + reader.getPreviousPath(), e);
      } finally {
        format.setTimeZone(originalTimeZone); // Restore the original time zone
      }
    }
  }
}
