package io.gitlab.jfronny.commons.serialize.databind.test;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.databind.ObjectMapper;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.commons.serialize.emulated.DataElement;
import io.gitlab.jfronny.commons.serialize.emulated.EmulatedReader;
import io.gitlab.jfronny.commons.serialize.emulated.EmulatedWriter;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ObjectMapperTest {
    @Test
    public void simpleMapTEst() throws MalformedDataException {
        DataElement.Object de = new DataElement.Object();
        de.members().put("key", new DataElement.Primitive.String("value"));
        de.members().put("key2", new DataElement.Primitive.String("value2"));
        de.members().put("key3", new DataElement.Primitive.String("value3"));
        ObjectMapper om = new ObjectMapper();
        TypeAdapter<Map<String, String>> adapter = om.getAdapter(new TypeToken<>() {});
        Map<String, String> map;
        try (EmulatedReader er = new EmulatedReader(de)) {
            map = adapter.deserialize(er);
        }
        assertEquals(3, map.size());
        assertEquals("value", map.get("key"));
        assertEquals("value2", map.get("key2"));
        assertEquals("value3", map.get("key3"));
        try (EmulatedWriter ew = new EmulatedWriter()) {
            adapter.serialize(map, ew);
            assertEquals(de, ew.get());
        }
    }

    @Test
    public void testS2OMap() throws MalformedDataException {
        DataElement.Object de = new DataElement.Object();
        de.members().put("key", new DataElement.Primitive.String("value"));
        de.members().put("key2", new DataElement.Primitive.String("value2"));
        de.members().put("key3", new DataElement.Primitive.String("value3"));
        ObjectMapper om = new ObjectMapper();
        TypeAdapter<String2ObjectMap<String>> adapter = om.getAdapter(new TypeToken<>() {});
        String2ObjectMap<String> map;
        try (EmulatedReader er = new EmulatedReader(de)) {
            map = adapter.deserialize(er);
        }
        assertEquals(3, map.size());
        assertEquals("value", map.get("key"));
        assertEquals("value2", map.get("key2"));
        assertEquals("value3", map.get("key3"));
        try (EmulatedWriter ew = new EmulatedWriter()) {
            adapter.serialize(map, ew);
            assertEquals(de, ew.get());
        }
    }

    @Test
    public void testSet() throws MalformedDataException {
        DataElement.Array de = new DataElement.Array();
        de.elements().add(new DataElement.Primitive.String("value"));
        de.elements().add(new DataElement.Primitive.String("value2"));
        de.elements().add(new DataElement.Primitive.String("value3"));
        ObjectMapper om = new ObjectMapper();
        TypeAdapter<Set<String>> adapter = om.getAdapter(new TypeToken<>() {});
        Set<String> set;
        try (EmulatedReader er = new EmulatedReader(de)) {
            set = adapter.deserialize(er);
        }
        assertEquals(3, set.size());
        assertEquals(Set.of("value", "value2", "value3"), set);
        try (EmulatedWriter ew = new EmulatedWriter()) {
            adapter.serialize(set, ew);
            assertEquals(de, ew.get());
        }
    }

    @Test
    public void testSet2() throws MalformedDataException {
        DataElement.Array de = new DataElement.Array();
        de.elements().add(new DataElement.Primitive.String("value"));
        de.elements().add(new DataElement.Primitive.String("value2"));
        de.elements().add(new DataElement.Primitive.String("value3"));
        ObjectMapper om = new ObjectMapper();
        TypeAdapter<TreeSet<String>> adapter = om.getAdapter(new TypeToken<>() {});
        TreeSet<String> set;
        try (EmulatedReader er = new EmulatedReader(de)) {
            set = adapter.deserialize(er);
        }
        assertEquals(3, set.size());
        assertEquals(Set.of("value", "value2", "value3"), set);
        try (EmulatedWriter ew = new EmulatedWriter()) {
            adapter.serialize(set, ew);
            assertEquals(de, ew.get());
        }
    }
}
