import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.databind.TypeAdapterFactory;
import io.gitlab.jfronny.commons.serialize.databind.impl.adapter.*;

module io.gitlab.jfronny.commons.serialize.databind {
    uses TypeAdapterFactory;
    uses TypeAdapter;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.serialize;
    requires io.gitlab.jfronny.commons.serialize.databind.api;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.serialize.databind;
    provides TypeAdapter with
            TypeAdapters.BooleanTypeAdapter,
            TypeAdapters.ByteTypeAdapter,
            TypeAdapters.ShortTypeAdapter,
            TypeAdapters.IntegerTypeAdapter,
            TypeAdapters.LongTypeAdapter,
            TypeAdapters.FloatTypeAdapter,
            TypeAdapters.DoubleTypeAdapter,
            TypeAdapters.CharacterTypeAdapter,
            TypeAdapters.StringTypeAdapter,
            TypeAdapters.BitSetTypeAdapter,
            TypeAdapters.AtomicIntegerTypeAdapter,
            TypeAdapters.AtomicBooleanTypeAdapter,
            TypeAdapters.AtomicIntegerArrayTypeAdapter,
            TypeAdapters.BigDecimalTypeAdapter,
            TypeAdapters.BigIntegerTypeAdapter,
            TypeAdapters.NumberTypeAdapter,
            TypeAdapters.LazilyParsedNumberTypeAdapter,
            TypeAdapters.StringBuilderTypeAdapter,
            TypeAdapters.StringBufferTypeAdapter,
            TypeAdapters.URLTypeAdapter,
            TypeAdapters.URITypeAdapter,
            TypeAdapters.InetAddressTypeAdapter,
            TypeAdapters.UUIDTypeAdapter,
            TypeAdapters.CurrencyTypeAdapter,
            TypeAdapters.CalendarTypeAdapter,
            TypeAdapters.LocaleTypeAdapter,
            TypeAdapters.DataElementTypeAdapter,
            DefaultDateTypeAdapter;
    provides TypeAdapterFactory with
            SerializationAdapterTypeAdapterFactory,
            ArrayTypeAdapterFactory,
            EnumTypeAdapterFactory,
            MapTypeAdapterFactory,
            CollectionTypeAdapterFactory;
}