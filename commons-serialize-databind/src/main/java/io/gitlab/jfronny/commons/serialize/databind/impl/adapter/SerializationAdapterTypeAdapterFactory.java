package io.gitlab.jfronny.commons.serialize.databind.impl.adapter;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.databind.*;
import io.gitlab.jfronny.commons.serialize.databind.api.*;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

import java.util.Objects;

import static io.gitlab.jfronny.commons.throwable.Coerce.function;

public class SerializationAdapterTypeAdapterFactory implements TypeAdapterFactory {
    @Override
    public <T> TypeAdapter<T> create(ObjectMapper mapper, TypeToken<T> type) {
        SerializeWithAdapter adapter = type.getRawType().getAnnotation(SerializeWithAdapter.class);
        if (adapter == null) {
            return null;
        }
        ThrowingSupplier<TypeAdapter<T>, MalformedDataException> result;
        if (TypeAdapterFactory.class.isAssignableFrom(adapter.adapter())) {
            result = Objects.requireNonNull(TypeUtils.constructor(TypeToken.get((Class<TypeAdapterFactory>) adapter.adapter())), "Specified adapter has no public no-args constructor")
                    .andThen(function(factory -> factory.create(mapper, type)));
        } else if (TypeAdapter.class.isAssignableFrom(adapter.adapter())) {
            result = Objects.requireNonNull(TypeUtils.constructor(TypeToken.get((Class<TypeAdapter<T>>) adapter.adapter())), "Specified adapter has no public no-args constructor");
        } else {
            return null;
        }
        try {
            return adapter.nullSafe() ? result.get().nullSafe() : result.get();
        } catch (MalformedDataException e) {
            throw new RuntimeException(e); // This used to just return null, but that is a really bad idea
        }
    }
}
