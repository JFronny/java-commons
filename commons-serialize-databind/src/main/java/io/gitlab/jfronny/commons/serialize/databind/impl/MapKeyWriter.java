package io.gitlab.jfronny.commons.serialize.databind.impl;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;

import java.util.ArrayList;
import java.util.List;

public class MapKeyWriter extends SerializeWriter<MalformedDataException, MapKeyWriter> {
    private String result = null;
    private List<String> comments = new ArrayList<>();
    private boolean isFailureOrigin = false;

    public String getResult() {
        return result;
    }

    public List<String> getComments() {
        return comments;
    }

    public boolean isFailureOrigin() {
        return isFailureOrigin;
    }

    @Override
    public MapKeyWriter beginArray() throws MalformedDataException {
        isFailureOrigin = true;
        throw new MalformedDataException("Cannot write arrays in map keys");
    }

    @Override
    public MapKeyWriter endArray() throws MalformedDataException {
        isFailureOrigin = true;
        throw new MalformedDataException("Cannot end arrays in map keys");
    }

    @Override
    public MapKeyWriter beginObject() throws MalformedDataException {
        isFailureOrigin = true;
        throw new MalformedDataException("Cannot write objects in map keys");
    }

    @Override
    public MapKeyWriter endObject() throws MalformedDataException {
        isFailureOrigin = true;
        throw new MalformedDataException("Cannot end objects in map keys");
    }

    @Override
    public MapKeyWriter comment(String comment) throws MalformedDataException {
        comments.add(comment);
        return this;
    }

    @Override
    public MapKeyWriter name(String name) throws MalformedDataException {
        if (result != null) {
            isFailureOrigin = true;
            throw new MalformedDataException("Cannot write multiple names in map keys");
        }
        result = name;
        return this;
    }

    @Override
    public MapKeyWriter value(String value) throws MalformedDataException {
        if (result != null) {
            isFailureOrigin = true;
            throw new MalformedDataException("Cannot write multiple values in map keys");
        }
        result = value;
        return this;
    }

    @Override
    public MapKeyWriter literalValue(String value) throws MalformedDataException {
        if (result != null) {
            isFailureOrigin = true;
            throw new MalformedDataException("Cannot write multiple values in map keys");
        }
        result = value;
        return this;
    }

    @Override
    protected MalformedDataException createException(String message) {
        return new MalformedDataException(message + " in map key");
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() {
    }
}
