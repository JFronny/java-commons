package io.gitlab.jfronny.commons.serialize.databind.impl;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;

public class HierarchyAdapter<T> extends TypeAdapter<T> {
    private final TypeAdapter<? super T> baseAdapter;
    private final Class<T> type;

    public HierarchyAdapter(TypeAdapter<? super T> baseAdapter, Class<T> type) {
        this.baseAdapter = baseAdapter;
        this.type = type;
    }

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(T value, Writer writer) throws TEx, MalformedDataException {
        baseAdapter.serialize(value, writer);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> T deserialize(Reader reader) throws TEx, MalformedDataException {
        var result = baseAdapter.deserialize(reader);
        if (result == null) return null;
        if (!type.isInstance(result)) {
            throw new MalformedDataException("Expected " + type.getName() + " but got " + result.getClass().getName() + " at path " + reader.getPath());
        }
        return type.cast(result);
    }
}
