package io.gitlab.jfronny.commons.serialize.databind;

import io.gitlab.jfronny.commons.Serializer;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Transport;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.Objects;

public class DatabindSerializer<TEx extends Exception, Reader extends SerializeReader<TEx, Reader>, Writer extends SerializeWriter<TEx, Writer>> implements Serializer {
    private final Transport<TEx, Reader, Writer> transport;
    private final ObjectMapper mapper;

    public DatabindSerializer(Transport<TEx, Reader, Writer> transport, ObjectMapper mapper) {
        this.transport = Objects.requireNonNull(transport);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @Override
    public String serialize(Object object) throws IOException {
        try (StringWriter sw = new StringWriter(); Writer writer = transport.createWriter(sw)) {
            mapper.serialize(object, writer);
            writer.close();
            return sw.toString();
        } catch (Throwable e) {
            throw e instanceof IOException x ? x : new IOException(e);
        }
    }

    @Override
    public <T> T deserialize(java.io.Reader source, Type typeOfT) throws IOException {
        try (Reader reader = transport.createReader(source)) {
            return (T) mapper.getAdapter(TypeToken.get(typeOfT)).deserialize(reader);
        } catch (IOException e) {
            throw e;
        } catch (Throwable e) {
            throw new IOException(e);
        }
    }

    @Override
    public <T> T deserialize(String source, Type typeOfT) throws IOException {
        try (Reader reader = transport.createReader(source)) {
            return (T) mapper.getAdapter(TypeToken.get(typeOfT)).deserialize(reader);
        } catch (IOException e) {
            throw e;
        } catch (Throwable e) {
            throw new IOException(e);
        }
    }

    @Override
    public String getFormatMime() {
        return transport.getFormatMime();
    }
}
