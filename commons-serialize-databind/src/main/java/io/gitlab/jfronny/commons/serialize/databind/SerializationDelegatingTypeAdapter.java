package io.gitlab.jfronny.commons.serialize.databind;

import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;

public abstract class SerializationDelegatingTypeAdapter<T> extends TypeAdapter<T>  {
    public abstract TypeAdapter<T> getSerializationDelegate();
}
