package io.gitlab.jfronny.commons.serialize.databind;

import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;

public interface TypeAdapterFactory {
    <T> TypeAdapter<T> create(ObjectMapper mapper, TypeToken<T> type);
}
