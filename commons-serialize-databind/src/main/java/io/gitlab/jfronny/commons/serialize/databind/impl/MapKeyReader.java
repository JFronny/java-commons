package io.gitlab.jfronny.commons.serialize.databind.impl;

import io.gitlab.jfronny.commons.data.LazilyParsedNumber;
import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.Token;

import java.util.Locale;

public class MapKeyReader extends SerializeReader<MalformedDataException, MapKeyReader> {
    private final String path;
    private final String previousPath;
    private String content;

    public MapKeyReader(String path, String previousPath, String content) {
        this.path = path;
        this.previousPath = previousPath;
        this.content = content;
    }

    @Override
    public MapKeyReader beginArray() throws MalformedDataException {
        throw new MalformedDataException("Cannot read arrays in map keys");
    }

    @Override
    public MapKeyReader endArray() throws MalformedDataException {
        throw new MalformedDataException("Cannot end arrays in map keys");
    }

    @Override
    public MapKeyReader beginObject() throws MalformedDataException {
        throw new MalformedDataException("Cannot read objects in map keys");
    }

    @Override
    public MapKeyReader endObject() throws MalformedDataException {
        throw new MalformedDataException("Cannot end objects in map keys");
    }

    @Override
    public boolean hasNext() throws MalformedDataException {
        return content != null;
    }

    @Override
    public Token peek() throws MalformedDataException {
        return Token.STRING;
    }

    @Override
    public String nextName() throws MalformedDataException {
        throw new MalformedDataException("Cannot read names in map keys");
    }

    @Override
    public String nextString() throws MalformedDataException {
        if (content == null) {
            throw new MalformedDataException("Map key already exhausted");
        }
        String result = content;
        content = null;
        return result;
    }

    @Override
    public boolean nextBoolean() throws MalformedDataException {
        if (content == null) {
            throw new MalformedDataException("Map key already exhausted");
        }
        String result = content.toLowerCase(Locale.ROOT);
        if (result.equals("true")) {
            content = null;
            return true;
        } else if (result.equals("false")) {
            content = null;
            return false;
        } else {
            throw new MalformedDataException("Expected boolean but was " + result);
        }
    }

    @Override
    public void nextNull() throws MalformedDataException {
        if (content == null) {
            throw new MalformedDataException("Map key already exhausted");
        }
        content = null;
    }

    @Override
    public Number nextNumber() throws MalformedDataException {
        if (content == null) {
            throw new MalformedDataException("Map key already exhausted");
        }
        LazilyParsedNumber result = new LazilyParsedNumber(content);
        content = null;
        return result;
    }

    @Override
    public void skipValue() throws MalformedDataException {
        if (content == null) {
            throw new MalformedDataException("Map key already exhausted");
        }
        content = null;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getPreviousPath() {
        return previousPath;
    }

    @Override
    public void close() {
    }

    @Override
    protected MalformedDataException createException(String message) {
        return new MalformedDataException(message + " in map key" + locationString());
    }
}
