package io.gitlab.jfronny.commons.serialize.databind.impl.adapter;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.databind.ObjectMapper;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public class TypeAdapterRuntimeTypeWrapper<T> extends TypeAdapter<T> {
    private final ObjectMapper context;
    private final TypeAdapter<T> delegate;
    private final Type type;

    TypeAdapterRuntimeTypeWrapper(ObjectMapper context, TypeAdapter<T> delegate, Type type) {
        this.context = context;
        this.delegate = delegate;
        this.type = type;
    }

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(T value, Writer writer) throws TEx, MalformedDataException {
        TypeAdapter<T> chosen = delegate;
        Type runtimeType = type;
        if (value != null && (runtimeType instanceof Class<?> || runtimeType instanceof TypeVariable<?>)) {
            runtimeType = value.getClass();
        }
        if (runtimeType != type) {
            TypeAdapter<T> runtimeTypeAdapter = (TypeAdapter<T>) context.getAdapter(TypeToken.get(runtimeType));
            if (runtimeTypeAdapter != null) chosen = runtimeTypeAdapter;
        }
        chosen.serialize(value, writer);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> T deserialize(Reader reader) throws TEx, MalformedDataException {
        return delegate.deserialize(reader);
    }
}
