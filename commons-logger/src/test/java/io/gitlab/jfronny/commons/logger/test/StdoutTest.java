package io.gitlab.jfronny.commons.logger.test;

import io.gitlab.jfronny.commons.logger.MemoryLogger;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class StdoutTest {
    @Test
    void test() {
        MemoryLogger mlg = new MemoryLogger("test", System.Logger.Level.INFO);
        mlg.log(System.Logger.Level.INFO, "Hello, World!");
        mlg.redirectSystemOut();
        System.out.println("Hello Wrold!");
        System.out.printf("Hello %s", "World");
        System.out.println();
        Iterator<String> iterator = mlg.iterator();
        assertEquals("[I] Hello, World!", iterator.next());
        assertEquals("[I] [io.gitlab.jfronny.commons.logger.test.StdoutTest] Hello Wrold!", iterator.next());
        assertEquals("[I] [io.gitlab.jfronny.commons.logger.test.StdoutTest] Hello World", iterator.next());
        assertFalse(iterator.hasNext());
    }
}
