module io.gitlab.jfronny.commons.logger {
    uses io.gitlab.jfronny.commons.logger.LoggerConfiguration;
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.logger;
    provides System.LoggerFinder with io.gitlab.jfronny.commons.logger.HotswapLoggerFinder;
}