package io.gitlab.jfronny.commons.logger;

import org.jetbrains.annotations.Nullable;

/**
 * Abstract logger factory that optionally takes a log level.
 */
public abstract class LeveledLoggerFinder extends System.LoggerFinder {
    public static LeveledLoggerFinder get(Simple simple) {
        return new LeveledLoggerFinder() {
            @Override
            public SystemLoggerPlus getLogger(String name, Module module, @Nullable System.Logger.Level level) {
                return simple.getLogger(name, module, level);
            }
        };
    }

    public static LeveledLoggerFinder get(System.LoggerFinder factory) {
        return factory instanceof LeveledLoggerFinder fac ? fac : get((name, module, level) -> SystemLoggerPlus.get(factory.getLogger(name, module)));
    }

    public static LeveledLoggerFinder getLoggerFinder() {
        return get(System.LoggerFinder.getLoggerFinder());
    }

    @Override
    public SystemLoggerPlus getLogger(String s, Module module) {
        return getLogger(s, module, null);
    }

    public abstract SystemLoggerPlus getLogger(String name, Module module, @Nullable System.Logger.Level level);

    @FunctionalInterface
    public interface Simple {
        SystemLoggerPlus getLogger(String name, Module module, @Nullable System.Logger.Level level);
    }
}
