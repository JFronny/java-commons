package io.gitlab.jfronny.commons.logger.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

public class LoggingOutputStream extends OutputStream {
    private final System.Logger logger;
    private final System.Logger.Level level;
    private final ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
    private final String prefix;

    public LoggingOutputStream(String prefix, System.Logger logger, System.Logger.Level level) {
        this.prefix = prefix;
        this.logger = Objects.requireNonNull(logger);
        this.level = Objects.requireNonNull(level);
    }

    @Override
    public void write(int b) throws IOException {
        baos.write(b);
    }

    @Override
    public void flush() throws IOException {
        super.flush();
        String line = baos.toString();
        baos.reset();
        if (!line.isBlank()) {
            logger.log(level, prefix + line.stripTrailing());
        }
    }

    @Override
    public void close() throws IOException {
        super.close();
        baos.close();
    }
}
