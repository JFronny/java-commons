package io.gitlab.jfronny.commons.logger;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.PrintStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * A simple logger that writes to a PrintStream.
 */
public class PrintStreamLogger implements CompactLogger {
    private final @Nullable String name;
    private final boolean color;
    private final boolean thread;
    private final boolean timestamp;
    private final System.Logger.Level level;
    private final PrintStream target;

    /**
     * Creates a new PrintStreamLogger.
     * @param target the PrintStream to write to
     * @param name the name of the logger
     * @param level the minimum log level
     * @param color whether to color the output using ANSI escape codes
     * @param thread whether to include the thread name in the output
     * @param timestamp whether to include a timestamp in the output
     */
    public PrintStreamLogger(@NotNull PrintStream target, @Nullable String name, System.Logger.Level level, boolean color, boolean thread, boolean timestamp) {
        this.target = Objects.requireNonNull(target);
        this.name = name;
        this.level = level;
        this.color = color;
        this.thread = thread;
        this.timestamp = timestamp;
    }

    @Override
    public String getName() {
        return name == null ? "" : name;
    }

    @Override
    public System.Logger.Level getLevel() {
        return level;
    }

    @Override
    public void log(Level level, String message) {
        if (!isLoggable(level)) return;
        else if (level == Level.ALL) level = Level.TRACE;
        target.println(generateMessage(message, level));
    }

    @Override
    public void log(Level level, String message, Throwable throwable) {
        if (!isLoggable(level)) return;
        else if (level == Level.ALL) level = Level.TRACE;
        target.println(generateMessage(message, level));
        throwable.printStackTrace(target);
    }

    /**
     * Formats the message to write to the PrintStream.
     * @param msg the message to format
     * @param level the level of the message
     * @return the formatted message
     */
    protected String generateMessage(String msg, System.Logger.Level level) {
        StringBuilder sb = new StringBuilder();
        // Timestamp
        if (this.timestamp) {
            sb.append(DateTimeFormatter.ofPattern("HH:mm:ss.SSS").format(LocalTime.now())).append(' ');
        }
        // Thread name
        if (this.thread) {
            String thread = Thread.currentThread().getName();
            if (this.color) {
                sb.append(OutputColors.CYAN_BOLD).append(thread).append(OutputColors.RESET).append(' ');
            } else {
                sb.append('{').append(thread).append("} ");
            }
        }
        // Logger name
        if (name != null) {
            if (this.color) {
                sb.append(OutputColors.GREEN_BOLD).append(name).append(OutputColors.RESET).append(' ');
            } else {
                sb.append('(').append(name).append(") ");
            }
        }
        // Level
        if (this.color) {
            sb.append(OutputColors.byLevel(level)).append(level.name()).append(OutputColors.RESET).append(' ');
        } else {
            sb.append(level.name().charAt(0)).append(' ');
        }
        // Message
        sb.append(msg);
        return sb.toString();
    }
}
