package io.gitlab.jfronny.commons.logger;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;

/**
 * A logger that stores all log messages in memory.
 */
public class MemoryLogger implements CompactLogger, Iterable<String> {
    private final String name;
    private final List<String> lines;
    private final int size;
    private final Level level;

    public MemoryLogger(String name, Level level) {
        this.name = Objects.requireNonNull(name);
        this.lines = new LinkedList<>();
        this.size = -1;
        this.level = Objects.requireNonNull(level);
    }

    public MemoryLogger(String name, Level level, int size) {
        this.name = Objects.requireNonNull(name);
        this.lines = new ArrayList<>(size);
        this.size = size;
        this.level = Objects.requireNonNull(level);
    }

    @Override
    public Level getLevel() {
        return level;
    }

    @Override
    public void log(Level level, String message) {
        if (!isLoggable(level)) return;
        add("[" + switch (level) {
            case TRACE -> "T";
            case DEBUG -> "D";
            case INFO -> "I";
            case WARNING -> "W";
            case ERROR -> "E";
            default -> "I";
        } + "] " + message);
    }

    @Override
    public String getName() {
        return name;
    }

    private void add(String msg) {
        synchronized (lines) {
            int lz = lines.size();
            if (size != -1 && lz >= size - 1) {
                Iterator<String> iterator = lines.iterator();
                for (int i = 0, max = lz - size + 1; i < max; i++) iterator.remove();
            }
            lines.add(msg);
        }
    }

    @NotNull
    @Override
    public Iterator<String> iterator() {
        return lines.iterator();
    }

    @Override
    public void forEach(Consumer<? super String> action) {
        lines.forEach(action);
    }

    @Override
    public Spliterator<String> spliterator() {
        return lines.spliterator();
    }
}
