package io.gitlab.jfronny.commons.logger.impl;

import io.gitlab.jfronny.commons.StringFormatter;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Formatter {
    public static String format(ResourceBundle bundle, String format, Object... args) {
        format = getResourceStringOrMessage(bundle, format);
        if (args != null && args.length > 0) {
            String[] strings = new String[args.length];
            for (int i = 0; i < args.length; i++) {
                strings[i] = StringFormatter.toString(args[i]);
            }
            return new MessageFormat(format).format(strings);
        } else {
            return format;
        }
    }

    public static String getResourceStringOrMessage(ResourceBundle bundle, String msg) {
        // This method was taken from SLF4J and modified slightly
        if (bundle == null || msg == null) return msg;
        try {
            return bundle.getString(msg);
        } catch (MissingResourceException ex) {
            return msg;
        } catch (ClassCastException ex) {
            return bundle.getObject(msg).toString();
        }
    }
}
