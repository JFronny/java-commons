package io.gitlab.jfronny.commons.logger.impl;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;

import java.util.Objects;
import java.util.ResourceBundle;

public record DelegateLoggerPlus(System.Logger delegate) implements SystemLoggerPlus {
    public static SystemLoggerPlus get(System.Logger delegate) {
        return delegate instanceof SystemLoggerPlus slp ? slp : new DelegateLoggerPlus(delegate);
    }

    public DelegateLoggerPlus {
        Objects.requireNonNull(delegate);
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean isLoggable(Level level) {
        return delegate.isLoggable(level);
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String msg, Throwable thrown) {
        delegate.log(level, bundle, msg, thrown);
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String format, Object... params) {
        delegate.log(level, bundle, format, params);
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String format, Throwable t, Object... params) {
        switch (delegate) {
            case SystemLoggerPlus slp -> slp.log(level, format, t, params);
            case System.Logger sl -> sl.log(level, Formatter.format(bundle, format, params), t);
        }
    }
}
