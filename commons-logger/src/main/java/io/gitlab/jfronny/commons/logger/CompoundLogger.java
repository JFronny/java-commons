package io.gitlab.jfronny.commons.logger;


import io.gitlab.jfronny.commons.logger.impl.Formatter;

import java.util.ResourceBundle;

/**
 * Logger implementation that logs to multiple loggers at once.
 */
public class CompoundLogger implements SystemLoggerPlus {
    private final String name;
    private final System.Logger[] loggers;

    public CompoundLogger(String name, System.Logger[] loggers) {
        this.name = name;
        this.loggers = loggers;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isLoggable(Level level) {
        for (System.Logger logger : loggers) {
            if (logger.isLoggable(level)) return true;
        }
        return false;
    }

    @Override
    public void log(Level level, ResourceBundle resourceBundle, String s, Throwable throwable) {
        for (System.Logger logger : loggers) {
            if (logger.isLoggable(level)) logger.log(level, resourceBundle, s, throwable);
        }
    }

    @Override
    public void log(Level level, ResourceBundle resourceBundle, String s, Object... objects) {
        for (System.Logger logger : loggers) {
            if (logger.isLoggable(level)) logger.log(level, resourceBundle, s, objects);
        }
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String format, Throwable t, Object... params) {
        for (System.Logger logger : loggers) {
            switch (logger) {
                case SystemLoggerPlus slp -> slp.log(level, bundle, format, t, params);
                case System.Logger sl -> sl.log(level, Formatter.format(bundle, format, params), t);
            }
        }
    }
}
