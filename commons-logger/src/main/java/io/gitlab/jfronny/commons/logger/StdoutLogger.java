package io.gitlab.jfronny.commons.logger;

import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * A simple logger that writes to stdout.
 */
public class StdoutLogger extends PrintStreamLogger {
    /**
     * Creates a new StdoutLogger.
     * @param name the name of the logger
     * @param level the minimum log level
     */
    public StdoutLogger(@Nullable String name, Level level) {
        this(name, level, false);
    }

    /**
     * Creates a new StdoutLogger.
     * @param name the name of the logger
     * @param level the minimum log level
     * @param color whether to color the output using ANSI escape codes
     */
    public StdoutLogger(@Nullable String name, Level level, boolean color) {
        this(name, level, color, false, false);
    }

    /**
     * Creates a new StdoutLogger.
     * @param name the name of the logger
     * @param level the minimum log level
     * @param color whether to color the output using ANSI escape codes
     * @param thread whether to include the thread name in the output
     * @param timestamp whether to include a timestamp in the output
     */
    public StdoutLogger(@Nullable String name, Level level, boolean color, boolean thread, boolean timestamp) {
        super(stdout, name, level, color, thread, timestamp);
    }

    private static final PrintStream stdout = newPrintStream(new FileOutputStream(FileDescriptor.out), System.getProperty("stdout.encoding"));
    private static PrintStream newPrintStream(OutputStream out, String enc) {
        if (enc != null) {
            return new PrintStream(new BufferedOutputStream(out, 128), true,
                    Charset.forName(enc, StandardCharsets.UTF_8));
        }
        return new PrintStream(new BufferedOutputStream(out, 128), true);
    }

    /**
     * Creates a new StdoutLogger configured to be fancy.
     * @param name the name of the logger
     * @param level the minimum log level
     * @return a new StdoutLogger with the given name and level
     */
    public static StdoutLogger fancy(String name, Level level) {
        return new StdoutLogger(name, level, true, true, true);
    }

    /**
     * Creates a new StdoutLogger configured to be fancy, ignoring the module (use with LeveledLoggerFactory.get()).
     * @param name the name of the logger
     * @param module ignored
     * @param level the minimum log level
     * @return a new StdoutLogger with the given name and level
     */
    public static StdoutLogger fancy(String name, Module module, Level level) {
        return fancy(name, level);
    }
}
