package io.gitlab.jfronny.commons.logger;

import io.gitlab.jfronny.commons.logger.impl.DelegateLoggerPlus;
import io.gitlab.jfronny.commons.logger.impl.Formatter;
import io.gitlab.jfronny.commons.logger.impl.LoggingPrintStream;

import java.util.ResourceBundle;

public interface SystemLoggerPlus extends System.Logger {
    static SystemLoggerPlus forName(String name) {
        return get(System.getLogger(name));
    }

    static SystemLoggerPlus get(System.Logger logger) {
        return DelegateLoggerPlus.get(logger);
    }

    default void log(Level level, ResourceBundle bundle, String format, Throwable t, Object... params) {
        // Also in CompactLogger.java
        // Copied here for binary compatibility
        if (!isLoggable(level)) return;
        log(level, Formatter.format(bundle, format, params), t);
    }

    default void log(Level level, String format, Throwable t, Object... params) {
        log(level, null, format, t, params);
    }

    default void trace(String msg, Object... params) {
        log(Level.TRACE, msg, params);
    }

    default void trace(String msg, Throwable t) {
        log(Level.TRACE, msg, t);
    }

    default void trace(String msg, Throwable t, Object... params) {
        log(Level.TRACE, msg, t, params);
    }

    default void debug(String msg, Object... params) {
        log(Level.DEBUG, msg, params);
    }

    default void debug(String msg, Throwable t) {
        log(Level.DEBUG, msg, t);
    }

    default void debug(String msg, Throwable t, Object... params) {
        log(Level.DEBUG, msg, t, params);
    }

    default void info(String msg, Object... params) {
        log(Level.INFO, msg, params);
    }

    default void info(String msg, Throwable t) {
        log(Level.INFO, msg, t);
    }

    default void info(String msg, Throwable t, Object... params) {
        log(Level.INFO, msg, t, params);
    }

    default void warn(String msg, Object... params) {
        log(Level.WARNING, msg, params);
    }

    default void warn(String msg, Throwable t) {
        log(Level.WARNING, msg, t);
    }

    default void warn(String msg, Throwable t, Object... params) {
        log(Level.WARNING, msg, t, params);
    }

    default void error(String msg, Object... params) {
        log(Level.ERROR, msg, params);
    }

    default void error(String msg, Throwable t) {
        log(Level.ERROR, msg, t);
    }

    default void error(String msg, Throwable t, Object... params) {
        log(Level.ERROR, msg, t, params);
    }

    default void redirectSystemOut() {
        System.setOut(new LoggingPrintStream("STDOUT", this, Level.INFO));
    }

    default void redirectSystemErr() {
        System.setErr(new LoggingPrintStream("STDERR", this, Level.ERROR));
    }
}
