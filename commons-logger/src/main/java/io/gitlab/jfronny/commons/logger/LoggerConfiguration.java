package io.gitlab.jfronny.commons.logger;

/**
 * Run using the ServiceLoader mechanism when HotswapLoggerFinder is first used.
 */
public interface LoggerConfiguration {
    void configure();
}
