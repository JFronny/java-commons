package io.gitlab.jfronny.commons.logger.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.PrintStream;
import java.util.Formatter;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

public class LoggingPrintStream extends PrintStream {
    private final System.Logger logger;
    private final String defaultName;
    private final System.Logger.@NotNull Level level;

    public LoggingPrintStream(@NotNull String defaultName, @NotNull System.Logger logger, @NotNull System.Logger.Level level) {
        super(new LoggingOutputStream("[" + defaultName + "] ", logger, level), true);
        this.defaultName = Objects.requireNonNull(defaultName);
        this.logger = Objects.requireNonNull(logger);
        this.level = Objects.requireNonNull(level);
    }

    private void maybeLog(Object param) {
        if (param instanceof String s && s.isBlank()) return;
        logger.log(level, "[{0}] {1}", getCallerClassName().orElse(defaultName), param);
    }

    @Override
    public void println(boolean x) {
        maybeLog(x);
    }

    @Override
    public void println(char x) {
        maybeLog(x);
    }

    @Override
    public void println(int x) {
        maybeLog(x);
    }

    @Override
    public void println(long x) {
        maybeLog(x);
    }

    @Override
    public void println(float x) {
        maybeLog(x);
    }

    @Override
    public void println(double x) {
        maybeLog(x);
    }

    @Override
    public void println(char @NotNull [] x) {
        maybeLog(x);
    }

    @Override
    public void println(@Nullable String x) {
        maybeLog(x);
    }

    @Override
    public void println(@Nullable Object x) {
        maybeLog(x);
    }

    @Override
    public PrintStream format(@NotNull String format, Object... args) {
        maybeLog(new Formatter().format(format, args).toString().stripTrailing());
        return this;
    }

    @Override
    public PrintStream format(Locale l, @NotNull String format, Object... args) {
        maybeLog(new Formatter().format(l, format, args).toString().stripTrailing());
        return this;
    }

    private Optional<String> getCallerClassName() {
        return StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk((s) -> s
                        .dropWhile(f -> PrintStream.class.isAssignableFrom(f.getDeclaringClass()))
                        .findFirst()
                ).map(StackWalker.StackFrame::getClassName);
    }
}
