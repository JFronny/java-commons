package io.gitlab.jfronny.commons.logger;

import io.gitlab.jfronny.commons.logger.impl.Formatter;

import java.util.ResourceBundle;

/**
 * A logger that delegates all calls to another logger.
 */
public abstract class DelegateLogger implements SystemLoggerPlus {
    public static DelegateLogger create(System.Logger delegate) {
        return new DelegateLogger() {
            @Override
            protected System.Logger getDelegate() {
                return delegate;
            }
        };
    }

    protected abstract System.Logger getDelegate();

    @Override
    public String getName() {
        return getDelegate().getName();
    }

    @Override
    public boolean isLoggable(Level level) {
        return getDelegate().isLoggable(level);
    }

    @Override
    public void log(Level level, ResourceBundle resourceBundle, String s, Throwable throwable) {
        getDelegate().log(level, resourceBundle, s, throwable);
    }

    @Override
    public void log(Level level, ResourceBundle resourceBundle, String s, Object... objects) {
        getDelegate().log(level, resourceBundle, s, objects);
    }

    @Override
    public void log(Level level, ResourceBundle resourceBundle, String format, Throwable t, Object... params) {
        switch (getDelegate()) {
            case SystemLoggerPlus slp -> slp.log(level, format, t, params);
            case System.Logger sl -> sl.log(level, Formatter.format(resourceBundle, format, params), t);
        }
    }
}
