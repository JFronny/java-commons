package io.gitlab.jfronny.commons.logger;

import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.commons.logger.impl.Formatter;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public interface CompactLogger extends SystemLoggerPlus {
    Level getLevel();
    void log(Level level, String message);
    default void log(Level level, String message, Throwable throwable) {
        if (!isLoggable(level)) return;
        log(level, message + System.lineSeparator() + StringFormatter.toString(throwable));
    }

    @Override
    default boolean isLoggable(Level level) {
        if (level == Level.ALL) return true;
        if (level == Level.OFF) return false;
        return level.getSeverity() >= getLevel().getSeverity();
    }

    @Override
    default void log(Level level, ResourceBundle resourceBundle, String s, Throwable throwable) {
        if (!isLoggable(level)) return;
        log(level, Formatter.getResourceStringOrMessage(resourceBundle, s), throwable);
    }

    @Override
    default void log(Level level, ResourceBundle resourceBundle, String s, Object... objects) {
        if (!isLoggable(level)) return;
        log(level, Formatter.format(resourceBundle, s, objects));
    }

    @Override
    default void log(Level level, ResourceBundle bundle, String format, Throwable t, Object... params) {
        // Also in SystemLoggerPlus.java
        if (!isLoggable(level)) return;
        log(level, Formatter.format(bundle, format, params), t);
    }
}
