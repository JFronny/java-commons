import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons"

            from(components["java"])
        }
    }
}
