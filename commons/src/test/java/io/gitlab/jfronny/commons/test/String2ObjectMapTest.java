package io.gitlab.jfronny.commons.test;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.data.impl.node.Node;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.function.UnaryOperator;

import static org.junit.jupiter.api.Assertions.*;

class String2ObjectMapTest {
    @Test
    void testBuildTreeByHand() {
        // Build the tree by hand, as if the following strings were added: B, BA, BAN, BANDANA, BANAN, BANANA

        //    ○
        //    └── ○ B (1)
        //        └── ○ A (2)
        //            └── ○ N (3)
        //                ├── ○ AN (5)
        //                │   └── ○ A (6)
        //                └── ○ DANA (4)

        final Node<Integer> root, n1, n2, n3, n4, n5, n6;
        n6 = Node.of("A", 6, Collections.emptyList(), false);
        n5 = Node.of("AN", 5, Arrays.asList(n6), false);
        n4 = Node.of("DANA", 4, Collections.emptyList(), false);
        n3 = Node.of("N", 3, Arrays.asList(n4, n5), false); // note: it should sort alphabetically such that n5 is first
        n2 = Node.of("A", 2, Arrays.asList(n3), false);
        n1 = Node.of("B", 1, Arrays.asList(n2), false);
        root = Node.of("", Arrays.asList(n1), true);

        assertEquals("""
                        ○
                        └── ○ B (1)
                            └── ○ A (2)
                                └── ○ N (3)
                                    ├── ○ AN (5)
                                    │   └── ○ A (6)
                                    └── ○ DANA (4)
                        """, root.prettyPrint());
    }

    @Test
    void testPut_AddToRoot() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("A", 1);
        assertEquals("""
                        ○
                        └── ○ A (1)
                        """, tree.prettyPrint());
    }

    @Test
    void testPut_ChildNodeSorting() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("B", 1);
        tree.put("A", 2);
        assertEquals("""
                        ○
                        ├── ○ A (2)
                        └── ○ B (1)
                        """, tree.prettyPrint());
    }

    @Test
    void testPut_AppendChild() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("FOO", 1);
        tree.put("FOOBAR", 2);

        assertEquals("""
                        ○
                        └── ○ FOO (1)
                            └── ○ BAR (2)
                        """, tree.prettyPrint());
    }

    @Test
    void testPut_SplitEdge() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("FOOBAR", 1);
        tree.put("FOO", 2);

        assertEquals("""
                        ○
                        └── ○ FOO (2)
                            └── ○ BAR (1)
                        """, tree.prettyPrint());
    }

    @Test
    void testPut_SplitWithImplicitNode() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("FOOBAR", 1);
        tree.put("FOOD", 2);

        assertEquals("""
                        ○
                        └── ○ FOO
                            ├── ○ BAR (1)
                            └── ○ D (2)
                        """, tree.prettyPrint());
    }

    @Test
    void testPut_SplitAndMove() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();
        tree.put("TEST", 1);
        tree.put("TEAM", 2);
        tree.put("TOAST", 3);

        assertEquals("""
                          ○
                          └── ○ T
                              ├── ○ E
                              │   ├── ○ AM (2)
                              │   └── ○ ST (1)
                              └── ○ OAST (3)
                          """, tree.prettyPrint());
    }

    @Test
    void testPut_OverwriteValue() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();

        Integer existing;
        existing = tree.put("FOO", 1);
        assertNull(existing);
        existing = tree.put("FOO", 2);
        assertNotNull(existing);

        assertEquals(Integer.valueOf(1), existing);
        assertEquals(Integer.valueOf(2), tree.get("FOO"));
    }

    @Test
    void testPutIfAbsent_DoNotOverwriteValue() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();

        Integer existing = tree.putIfAbsent("FOO", 1);
        assertNull(existing);

        existing = tree.putIfAbsent("FOO", 2);
        assertNotNull(existing);

        assertEquals(Integer.valueOf(1), existing);
        assertEquals(Integer.valueOf(1), tree.get("FOO"));
    }

    @Test
    void testPutIfAbsent_SplitNode() {
        String2ObjectMap<Integer> tree = new String2ObjectMap<>();

        //    ○
        //    └── ○ FOO             // implicit node added automatically
        //        ├── ○ BAR (1)
        //        └── ○ D (1)

        Integer existing;
        existing = tree.putIfAbsent("FOOBAR", 1);
        assertNull(existing);
        existing = tree.putIfAbsent("FOOD", 1);
        assertNull(existing);

        // This tests 'overwrite' set to true and exact match for node,
        // but no existing value to return (i.e. implicit node above)...

        //    ○
        //    └── ○ FOO (2)
        //        ├── ○ BAR (1)
        //        └── ○ D (1)

        existing = tree.putIfAbsent("FOO", 2);
        assertNull(existing);
    }

    @Test
    void testPut_VoidValue_CharArrayNodeFactory() {
        String2ObjectMap<VoidValue> tree = new String2ObjectMap<>();
        tree.put("FOO", VoidValue.SINGLETON);
        tree.put("FOOBAR", VoidValue.SINGLETON);
        assertEquals("""
                        ○
                        └── ○ FOO (-)
                            └── ○ BAR (-)
                        """, tree.prettyPrint());
    }

    private enum VoidValue {
        SINGLETON;

        @Override
        public String toString() {
            return "-";
        }
    }

    @Test
    void testAsSubstitution() {
        String2ObjectMap<String> tree = new String2ObjectMap<>();
        tree.put("Joe", "Wilhelm");
        tree.put("Joe Biden", "Joachim Böden");
        tree.put("Joey", "Joe");

        assertEquals("""
                        ○
                        └── ○ Joe (Wilhelm)
                            ├── ○  Biden (Joachim Böden)
                            └── ○ y (Joe)
                        """, tree.prettyPrint());

        UnaryOperator<String> substitution = tree.asSubstitution();
        assertEquals("Jo", substitution.apply("Jo"));
        assertEquals("Wilhelm", substitution.apply("Joe"));
        assertEquals("Wilhelm B", substitution.apply("Joe B"));
        assertEquals("Wilhelm Wilhelm Bide Joachim Böden Joe Biden", substitution.apply("Joe Joe Bide Joe Biden Joey Biden"));
    }
}
