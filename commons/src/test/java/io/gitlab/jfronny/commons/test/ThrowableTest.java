package io.gitlab.jfronny.commons.test;

import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.throwable.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class ThrowableTest {
    private static final String NORMAL = "Hello, World!";
    private static final String ALT = "Alt";

    @Test
    void tryOrElse() {
        assertDoesNotThrow(() -> Try.orElse(this::throwNoReturn, this::noop));
        assertEquals(ALT, Try.orElse(this::throwReturn, this::alt));
        assertDoesNotThrow(() -> Try.orElse(this::noThrowNoReturn, this::fail));
        assertEquals(NORMAL, Try.orElse(this::noThrowReturn, this::alt));
    }

    @Test
    void tryOrThrow() {
        assertDoesNotThrow(() -> Try.orThrow(this::noThrowNoReturn));
        assertThrows(RuntimeException.class, () -> Try.orThrow(this::throwNoReturn));
        assertEquals(NORMAL, Try.orThrow(this::noThrowReturn));
        assertThrows(RuntimeException.class, () -> Try.orThrow(this::throwReturn));
    }

    @Test
    void tryCompose() {
        assertDoesNotThrow(() -> ((ThrowingSupplier<String, IOException>) this::noThrowReturn).andThen(String::toCharArray).andThen((ThrowingConsumer<char[], IOException>) R::nop).orThrow().run());
        assertThrows(RuntimeException.class, () -> ((ThrowingConsumer<char[], IOException>) R::nop).compose(String::toCharArray).compose(this::throwReturn).orThrow().run());
    }

    private <T> void noop(T t) {
    }

    private String alt(Throwable t) {
        return ALT;
    }

    private <T> void fail(T t) {
        throw new RuntimeException();
    }

    private String throwReturn() throws IOException {
        throw new IOException();
    }

    private void throwNoReturn() throws IOException {
        throw new IOException();
    }

    private String noThrowReturn() throws IOException {
        return NORMAL;
    }

    private void noThrowNoReturn() throws IOException {
    }
}
