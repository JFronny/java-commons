module io.gitlab.jfronny.commons {
    requires static org.jetbrains.annotations;
    requires java.net.http;
    requires java.logging;
    exports io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.commons.concurrent;
    exports io.gitlab.jfronny.commons.data;
    exports io.gitlab.jfronny.commons.data.delegate;
    exports io.gitlab.jfronny.commons.ref;
    exports io.gitlab.jfronny.commons.throwable;
    exports io.gitlab.jfronny.commons.tuple;
    exports io.gitlab.jfronny.commons.switchsupport;
}