package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

/**
 * A utility class to hide checked exceptions from the java compiler.
 */
public class Unchecked {
    /**
     * Throws a checked exception without the need to declare it.
     *
     * @param t the exception to throw
     */
    @Contract("_ -> fail")
    public static <T> T sneakyThrow(@NotNull Throwable t) {
        return sneaky(() -> {
            throw t;
        });
    }

    /**
     * Hides an exception from the java compiler, effectively making it unchecked.
     * Runs a runnable.
     *
     * @param runnable the runnable to run
     * @param <TEx>    the exception the runnable might throw
     */
    public static <TEx extends Throwable> void sneaky(@NotNull ThrowingRunnable<TEx> runnable) {
        runnable.assumeSafe().run();
    }

    /**
     * Hides an exception from the java compiler, effectively making it unchecked.
     * Runs a supplier and returns the result.
     *
     * @param supplier the supplier to run
     * @param <T>      the return type of the supplier
     * @param <TEx>    the exception the supplier might throw
     * @return         the result of the supplier
     */
    public static <T, TEx extends Throwable> T sneaky(@NotNull ThrowingSupplier<T, TEx> supplier) {
        return supplier.assumeSafe().get();
    }

    /**
     * Reintroduces an exception previously made sneaky, allowing you to catch it.
     * The call does nothing except tricking the compiler into thinking that the exception is thrown.
     *
     * @param <TEx> the exception to reintroduce into the set of possible exceptions
     * @throws TEx  the introduced exception
     */
    @SuppressWarnings("RedundantThrows")
    public static <TEx extends Throwable> void reintroduce() throws TEx {
    }

    /**
     * Reintroduces an exception previously made sneaky, allowing you to catch it.
     * Simply runs the runnable.
     *
     * @param runnable the runnable to run
     * @param <TEx>    the exception to reintroduce into the set of possible exceptions
     * @throws TEx     the exception that was thrown by the runnable
     */
    @SuppressWarnings("RedundantThrows")
    public static <TEx extends Throwable> void reintroduce(@NotNull Runnable runnable) throws TEx {
        runnable.run();
    }

    /**
     * Reintroduces an exception previously made sneaky, allowing you to catch it.
     * Simply runs the supplier and returns the result.
     *
     * @param supplier the supplier to run
     * @param <T>      the return type of the supplier
     * @param <TEx>    the exception to reintroduce into the set of possible exceptions
     * @return         the result of the supplier
     * @throws TEx     the exception that was thrown by the supplier
     */
    @SuppressWarnings("RedundantThrows")
    public static <T, TEx extends Throwable> T reintroduce(@NotNull Supplier<T> supplier) throws TEx {
        return supplier.get();
    }
}
