package io.gitlab.jfronny.commons.tuple;

import io.gitlab.jfronny.commons.throwable.ThrowingPredicate;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;
import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Function;

public record Single<T1>(@Nullable T1 val) {
    @Contract(value = "_ -> new", pure = true)
    public static <T1> @NotNull Single<T1> of(@Nullable T1 val) {
        return new Single<>(val);
    }

    @Contract(pure = true)
    public boolean isNull() {
        return val == null;
    }

    @Contract(pure = true)
    public boolean isPresent() {
        return val != null;
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull ThrowingPredicate<T1, RuntimeException> asEqualsPredicate() {
        return val == null ? Objects::isNull : val::equals;
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull ThrowingPredicate<T1, RuntimeException> asIdentityEqualsPredicate() {
        return v -> v == val;
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull ThrowingSupplier<T1, RuntimeException> asSupplier() {
        return () -> val;
    }

    @Contract(value = "_, -> new", pure = true)
    public <T> @NotNull Single<T> map(@NotNull Function<T1, T> mapper) {
        return new Single<>(Objects.requireNonNull(mapper).apply(val));
    }

    @Contract(pure = true)
    public @Nullable T1 get() {
        return val;
    }

    @Contract(value = "_ -> new", pure = true)
    public <T2> @NotNull Tuple<T1, T2> concat(@Nullable T2 val) {
        return Tuple.of(this.val, val);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public <T2, T3> @NotNull Triple<T1, T2, T3> concat(@Nullable T2 val2, @Nullable T3 val3) {
        return Triple.of(this.val, val2, val3);
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public <T2, T3, T4> @NotNull Quadruple<T1, T2, T3, T4> concat(@Nullable T2 val2, @Nullable T3 val3, @Nullable T4 val4) {
        return Quadruple.of(this.val, val2, val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T2> @NotNull Tuple<T1, T2> concat(@NotNull Single<T2> val) {
        return Tuple.of(this.val, val.val);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T2, T3> @NotNull Triple<T1, T2, T3> concat(@NotNull Tuple<T2, T3> val) {
        return Triple.of(this.val, val.left(), val.right());
    }

    @Contract(value = "_ -> new", pure = true)
    public <T2, T3, T4> @NotNull Quadruple<T1, T2, T3, T4> concat(@NotNull Triple<T2, T3, T4> val) {
        return Quadruple.of(this.val, val.val1(), val.val2(), val.val3());
    }
}
