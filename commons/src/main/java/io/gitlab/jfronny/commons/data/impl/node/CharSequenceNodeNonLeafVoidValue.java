package io.gitlab.jfronny.commons.data.impl.node;

import java.util.List;
import java.util.NoSuchElementException;

public class CharSequenceNodeNonLeafVoidValue<T> extends NonLeafNode<T> {
    public CharSequenceNodeNonLeafVoidValue(CharSequence edgeCharSequence, List<Node<T>> outgoingEdges) {
        super(edgeCharSequence, outgoingEdges);
    }

    @Override
    public T getValue() {
        throw new NoSuchElementException();
    }

    @Override
    public boolean hasValue() {
        return false;
    }

    @Override
    protected String getValueString() {
        return "-";
    }
}
