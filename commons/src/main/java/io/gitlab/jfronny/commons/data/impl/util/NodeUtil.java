package io.gitlab.jfronny.commons.data.impl.util;

import io.gitlab.jfronny.commons.data.impl.node.Node;

import java.util.*;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class NodeUtil {
    public static <T> int binarySearchForEdge(AtomicReferenceArray<Node<T>> childNodes, Character edgeFirstCharacter) {
        // inspired by Collections#indexedBinarySearch()
        int low = 0;
        int high = childNodes.length() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            Node<T> midVal = childNodes.get(mid);
            int cmp = midVal.getIncomingEdgeFirstCharacter().compareTo(edgeFirstCharacter);

            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid; // key found
        }
        return -(low + 1);  // key not found
    }

    public static <T> void precheckCreation(CharSequence edgeCharacters, List<Node<T>> children, boolean isRoot) {
        if (edgeCharacters == null) {
            throw new IllegalStateException("The edgeCharacters argument was null");
        }
        if (!isRoot && edgeCharacters.length() == 0) {
            throw new IllegalStateException("Invalid edge characters for non-root node: " + edgeCharacters);
        }
        if (children == null) {
            throw new IllegalStateException("The childNodes argument was null");
        }
        // Sanity check that no two nodes specify an edge with the same first character...
        Set<Character> uniqueChars = new HashSet<>(children.size());
        for (Node<T> node : children) {
            uniqueChars.add(node.getIncomingEdgeFirstCharacter());
        }
        if (children.size() != uniqueChars.size()) {
            throw new IllegalStateException("Duplicate edge detected in list of nodes supplied: " + children);
        }
    }
}
