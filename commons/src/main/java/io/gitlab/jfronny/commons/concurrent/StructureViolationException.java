package io.gitlab.jfronny.commons.concurrent;

public class StructureViolationException extends RuntimeException {
    public StructureViolationException() {
    }

    public StructureViolationException(String message) {
        super(message);
    }
}
