package io.gitlab.jfronny.commons.throwable;

import io.gitlab.jfronny.commons.switchsupport.Result;
import org.jetbrains.annotations.*;

import java.lang.reflect.Type;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

@FunctionalInterface
public interface ThrowingSupplier<T, TEx extends Throwable> {
    static <T> @NotNull ThrowingSupplier<T, RuntimeException> of(@NotNull Supplier<T> supplier) {
        return Objects.requireNonNull(supplier)::get;
    }

    T get() throws TEx;

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingSupplier<V, TEx> andThen(@NotNull ThrowingFunction<? super T, ? extends V, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return () -> after.apply(this.get());
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingSupplier<V, TEx> cast(@Nullable Class<?> type) {
        if (type == null) return () -> (V) this.get();
        return andThen(Coerce.cast(type));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingRunnable<TEx> andThen(@NotNull ThrowingConsumer<? super T, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return () -> after.accept(this.get());
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Supplier<T> addHandler(@NotNull Function<Throwable, ? extends T> handler) {
        Objects.requireNonNull(handler);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                return handler.apply(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Supplier<T> addHandler(@NotNull Class<TEx> exception, @NotNull Function<TEx, ? extends T> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    return handler.apply((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Supplier<T> orThrow() {
        return orThrow(ExceptionWrapper::wrap)::get;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingSupplier<T, TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Supplier<Result<T, TEx>> toResult(@NotNull Class<TEx> exception) {
        return () -> {
            try {
                return Result.success(this.get());
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass())) return Result.failure((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the supplier in a supplier not marked to throw the checked exception.
     *
     * @return a supplier that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default Supplier<T> assumeSafe() {
        ThrowingSupplier<T, RuntimeException> supplier = (ThrowingSupplier<T, RuntimeException>) (ThrowingSupplier) this;
        return supplier::get;
    }
}
