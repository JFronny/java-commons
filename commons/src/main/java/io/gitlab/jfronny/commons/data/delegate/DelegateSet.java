package io.gitlab.jfronny.commons.data.delegate;

import java.util.Set;

public class DelegateSet<T, S extends Set<T>> extends DelegateCollection<T, S> implements Set<T> {
    public static class Simple<T> extends DelegateSet<T, Set<T>> {
        public Simple(Set<T> delegate) {
            super(delegate);
        }
    }

    public DelegateSet(S delegate) {
        super(delegate);
    }
}
