package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
public interface ThrowingConsumer<T, TEx extends Throwable> {
    static <T> @NotNull ThrowingConsumer<T, RuntimeException> of(@NotNull Consumer<T> consumer) {
        return Objects.requireNonNull(consumer)::accept;
    }

    void accept(T var1) throws TEx;

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingConsumer<V, TEx> compose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (t) -> this.accept(before.apply(t));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingRunnable<TEx> compose(@NotNull ThrowingSupplier<? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return () -> this.accept(before.get());
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingConsumer<T, TEx> andThen(@NotNull ThrowingConsumer<? super T, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t) -> {
            this.accept(t);
            after.accept(t);
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingConsumer<T, TEx> andThen(@NotNull ThrowingRunnable<? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t) -> {
            this.accept(t);
            after.run();
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiConsumer<T, V, TEx> compound(@NotNull ThrowingConsumer<? super V, ? extends TEx> other) {
        Objects.requireNonNull(other);
        return (l, r) -> {
            this.accept(l);
            other.accept(r);
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Consumer<T> addHandler(@NotNull Consumer<Throwable> handler) {
        Objects.requireNonNull(handler);
        return (t) -> {
            try {
                this.accept(t);
            } catch (Throwable e) {
                handler.accept(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Consumer<T> addHandler(@NotNull Class<TEx> exception, @NotNull Consumer<TEx> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return (t) -> {
            try {
                this.accept(t);
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    handler.accept((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Consumer<T> orThrow() {
        return orThrow(ExceptionWrapper::wrap)::accept;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingConsumer<T, TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return (t) -> {
            try {
                this.accept(t);
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the consumer in a consumer not marked to throw the checked exception.
     *
     * @return a consumer that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default Consumer<T> assumeSafe() {
        ThrowingConsumer<T, RuntimeException> consumer = (ThrowingConsumer<T, java.lang.RuntimeException>) (ThrowingConsumer) this;
        return consumer::accept;
    }
}
