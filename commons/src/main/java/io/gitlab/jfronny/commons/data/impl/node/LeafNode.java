package io.gitlab.jfronny.commons.data.impl.node;

import java.util.Collections;
import java.util.List;

public abstract class LeafNode<T> extends Node<T> {
    public LeafNode(CharSequence edgeCharSequence) {
        super(edgeCharSequence);
    }

    @Override
    public List<Node<T>> getOutgoingEdges() {
        return Collections.emptyList();
    }
    @Override
    public Node<T> getOutgoingEdge(Character edgeFirstCharacter) {
        return null;
    }

    @Override
    public void updateOutgoingEdge(Node<T> childNode) {
        throw new IllegalStateException("Cannot update the reference to the following child node for the edge starting with '" + childNode.getIncomingEdgeFirstCharacter() +"', no such edge already exists: " + childNode);
    }
}
