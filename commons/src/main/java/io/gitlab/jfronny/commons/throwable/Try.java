package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.function.*;

/**
 * try-catch without the hassle (intended for field declarations or lambdas)
 */
public class Try {
    /**
     * Run a method or an exception handler
     *
     * @param tr          The method that should be run
     * @param alternative A method to run if tr fails
     */
    public static void orElse(@NotNull ThrowingRunnable<?> tr, @NotNull Consumer<Throwable> alternative) {
        Objects.requireNonNull(tr).addHandler(alternative).run();
    }

    /**
     * Run a method or an exception handler
     *
     * @param tr          The method that should be run
     * @param alternative A method to run if tr fails, should return a default value
     */
    public static <T> @NotNull T orElse(@NotNull ThrowingSupplier<T, ?> tr, @NotNull Function<Throwable, ? extends T> alternative) {
        return Objects.requireNonNull(tr).addHandler(alternative).get();
    }

    /**
     * Run a method or throw a runtime exception (aka just ignore that it might throw something)
     *
     * @param tr The method that should be run
     */
    public static void orThrow(@NotNull ThrowingRunnable<?> tr) {
        Objects.requireNonNull(tr).orThrow().run();
    }

    /**
     * Run a method or throw a runtime exception (aka just ignore that it might throw something)
     *
     * @param tr The method that should be run
     */
    public static <T> @NotNull T orThrow(@NotNull ThrowingSupplier<T, ?> tr) {
        return Objects.requireNonNull(tr).orThrow().get();
    }

    /**
     * Converts a throwing BiConsumer to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails
     * @return A normal BiConsumer
     */
    @Contract(pure = true)
    public static <T, U> @NotNull BiConsumer<T, U> handle(@NotNull ThrowingBiConsumer<T, U, ?> tr, @NotNull Consumer<Throwable> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }

    /**
     * Converts a throwing BiFunction to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails, should return a default value
     * @return A normal BiFunction
     */
    @Contract(pure = true)
    public static <T, U, R> @NotNull BiFunction<T, U, R> handle(@NotNull ThrowingBiFunction<T, U, R, ?> tr, @NotNull Function<Throwable, ? extends R> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }

    /**
     * Converts a throwing Consumer to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails
     * @return A normal Consumer
     */
    @Contract(pure = true)
    public static <T> @NotNull Consumer<T> handle(@NotNull ThrowingConsumer<T, ?> tr, @NotNull Consumer<Throwable> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }

    /**
     * Converts a throwing Function to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails, should return a default value
     * @return A normal Function
     */
    @Contract(pure = true)
    public static <T, R> @NotNull Function<T, R> handle(@NotNull ThrowingFunction<T, R, ?> tr, @NotNull Function<Throwable, ? extends R> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }

    /**
     * Converts a throwing Runnable to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails
     * @return A normal Runnable
     */
    @Contract(pure = true)
    public static @NotNull Runnable handle(@NotNull ThrowingRunnable<?> tr, @NotNull Consumer<Throwable> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }

    /**
     * Converts a throwing Supplier to a normal one by adding an exception handler
     *
     * @param tr      The method that should be run
     * @param handler A method to run if tr fails, should return a default value
     * @return A normal Supplier
     */
    @Contract(pure = true)
    public static <T> @NotNull Supplier<T> handle(@NotNull ThrowingSupplier<T, ?> tr, @NotNull Function<Throwable, ? extends T> handler) {
        return Objects.requireNonNull(tr).addHandler(handler);
    }
}
