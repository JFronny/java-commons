package io.gitlab.jfronny.commons.data.immutable;

import java.util.Iterator;
import java.util.function.Consumer;

public class ImmutableIterator<T, S extends Iterator<T>> extends ImmutableObject<S> implements Iterator<T> {
    public ImmutableIterator(S delegate) {
        super(delegate);
    }

    @Override
    public boolean hasNext() {
        return delegate.hasNext();
    }

    @Override
    public T next() {
        return delegate.next();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        delegate.forEachRemaining(action);
    }
}
