package io.gitlab.jfronny.commons.throwable;

import io.gitlab.jfronny.commons.tuple.Tuple;
import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.*;

@FunctionalInterface
public interface ThrowingBiConsumer<T, U, TEx extends Throwable> {
    static <T, U, TEx extends Throwable> @NotNull ThrowingBiConsumer<T, U, TEx> of(@NotNull BiConsumer<T, U> consumer) {
        return Objects.requireNonNull(consumer)::accept;
    }

    void accept(T var1, U var2) throws TEx;

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingConsumer<Tuple<T, U>, TEx> fromTuple() {
        return (t) -> this.accept(t.left(), t.right());
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingConsumer<V, ? super TEx> compose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> left, @NotNull ThrowingFunction<? super V, ? extends U, ? extends TEx> right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return (t) -> this.accept(left.apply(t), right.apply(t));
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V, W> @NotNull ThrowingBiConsumer<V, W, TEx> biCompose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> left, @NotNull ThrowingFunction<? super W, ? extends U, ? extends TEx> right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return (l, r) -> this.accept(left.apply(l), right.apply(r));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiConsumer<V, U, TEx> composeLeft(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (l, r) -> this.accept(before.apply(l), r);
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiConsumer<T, V, TEx> composeRight(@NotNull ThrowingFunction<? super V, ? extends U, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (l, r) -> this.accept(l, before.apply(r));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBiConsumer<T, U, TEx> andThen(@NotNull ThrowingBiConsumer<? super T, ? super U, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (l, r) -> {
            this.accept(l, r);
            after.accept(l, r);
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBiConsumer<T, U, TEx> andThen(@NotNull ThrowingRunnable<? extends TEx> after) {
        Objects.requireNonNull(after);
        return (l, r) -> {
            this.accept(l, r);
            after.run();
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiConsumer<T, U> addHandler(@NotNull Consumer<Throwable> handler) {
        Objects.requireNonNull(handler);
        return (l, r) -> {
            try {
                this.accept(l, r);
            } catch (Throwable e) {
                handler.accept(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiConsumer<T, U> addHandler(@NotNull Class<TEx> exception, @NotNull Consumer<TEx> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return (l, r) -> {
            try {
                this.accept(l, r);
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    handler.accept((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiConsumer<T, U> orThrow() {
        return orThrow(ExceptionWrapper::wrap)::accept;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingBiConsumer<T, U, TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return (l, r) -> {
            try {
                this.accept(l, r);
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the consumer in a consumer not marked to throw the checked exception.
     *
     * @return a consumer that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default BiConsumer<T, U> assumeSafe() {
        ThrowingBiConsumer<T, U, RuntimeException> consumer = (ThrowingBiConsumer<T, U, java.lang.RuntimeException>) (ThrowingBiConsumer) this;
        return consumer::accept;
    }
}
