package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
public interface ThrowingRunnable<TEx extends Throwable> {
    static @NotNull ThrowingRunnable<RuntimeException> of(@NotNull Runnable runnable) {
        return Objects.requireNonNull(runnable)::run;
    }

    void run() throws TEx;

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingRunnable<TEx> compose(@NotNull ThrowingRunnable<? extends TEx> before) {
        Objects.requireNonNull(before);
        return () -> {
            before.run();
            this.run();
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingRunnable<TEx> andThen(@NotNull ThrowingRunnable<? extends TEx> after) {
        Objects.requireNonNull(after);
        return () -> {
            this.run();
            after.run();
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Runnable addHandler(@NotNull Consumer<Throwable> handler) {
        Objects.requireNonNull(handler);
        return () -> {
            try {
                this.run();
            } catch (Throwable e) {
                handler.accept(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Runnable addHandler(@NotNull Class<TEx> exception, @NotNull Consumer<TEx> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return () -> {
            try {
                this.run();
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    handler.accept((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Runnable orThrow() {
        return orThrow(ExceptionWrapper::wrap)::run;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingRunnable<TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return () -> {
            try {
                this.run();
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making the runnable unchecked.
     * Wraps the runnable in a runnable not marked to throw the checked exception.
     *
     * @return a runnable not marked to throw the checked exception
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default Runnable assumeSafe() {
        ThrowingRunnable<RuntimeException> runnable = (ThrowingRunnable<RuntimeException>) (ThrowingRunnable) this;
        return runnable::run;
    }
}
