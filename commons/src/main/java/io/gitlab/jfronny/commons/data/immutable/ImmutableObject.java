package io.gitlab.jfronny.commons.data.immutable;

public class ImmutableObject<S> {
    protected final S delegate;

    public ImmutableObject(S delegate) {
        this.delegate = delegate;
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ImmutableObject<?> ob ? delegate.equals(ob.delegate) : delegate.equals(obj);
    }

    @Override
    public String toString() {
        return delegate.toString();
    }
}
