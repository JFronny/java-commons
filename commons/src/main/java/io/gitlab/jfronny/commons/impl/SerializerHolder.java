package io.gitlab.jfronny.commons.impl;

import io.gitlab.jfronny.commons.Serializer;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Holds a serializer for use elsewhere
 */
public class SerializerHolder {
    private static Serializer instance;

    /**
     * Get the current serializer. Configure via setInstance
     *
     * @return The current serializer
     */
    public static @NotNull Serializer getInstance() {
        if (instance == null) throw new RuntimeException("No serializer was configured but one was requested");
        return instance;
    }

    /**
     * Configure a serializer to use for serialization
     *
     * @param instance The serializer to use from now on
     */
    public static void setInstance(@NotNull Serializer instance) {
        SerializerHolder.instance = Objects.requireNonNull(instance);
    }
}
