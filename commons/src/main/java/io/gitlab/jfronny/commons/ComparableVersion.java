package io.gitlab.jfronny.commons;

import java.util.ArrayList;
import java.util.List;

public class ComparableVersion implements Comparable<ComparableVersion> {
    private final String string;
    private final Long[] numbers;

    public ComparableVersion(String string) {
        this.string = string;
        String[] split = string.split("\\D+");
        List<Long> numbersList = new ArrayList<>();
        for (String s : split) {
            if (!s.isEmpty())
                numbersList.add(Long.parseLong(s));
        }
        this.numbers = numbersList.toArray(Long[]::new);
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public int compareTo(ComparableVersion version) {
        final int maxLength = Math.max(numbers.length, version.numbers.length);
        for (int i = 0; i < maxLength; i++) {
            final long left = i < numbers.length ? numbers[i] : 0;
            final long right = i < version.numbers.length ? version.numbers[i] : 0;
            if (left != right) {
                return left < right ? -1 : 1;
            }
        }
        return 0;
    }

    public int compareTo(String version) {
        return compareTo(new ComparableVersion(version));
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ComparableVersion cv && compareTo(cv) == 0 || obj instanceof String str && compareTo(str) == 0;
    }
}
