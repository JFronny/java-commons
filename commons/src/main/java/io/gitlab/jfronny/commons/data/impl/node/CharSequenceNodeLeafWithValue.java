package io.gitlab.jfronny.commons.data.impl.node;

import java.util.Objects;

public class CharSequenceNodeLeafWithValue<T> extends LeafNode<T> {
    private final T value;

    public CharSequenceNodeLeafWithValue(CharSequence edgeCharSequence, T value) {
        super(edgeCharSequence);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public boolean hasValue() {
        return true;
    }

    @Override
    protected String getValueString() {
        return String.valueOf(value);
    }
}
