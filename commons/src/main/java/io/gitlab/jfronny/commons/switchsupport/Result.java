package io.gitlab.jfronny.commons.switchsupport;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public sealed interface Result<T, X> {
    record Success<T, X>(T value) implements Result<T, X> {
        public static <T, X> Success<T, X> of(T value) {
            return new Success<>(value);
        }
    }

    record Failure<T, X>(X error) implements Result<T, X> {
        public static <T, X> Failure<T, X> of(X error) {
            return new Failure<>(error);
        }
    }

    static <T, X> Result<T, X> of(Optional<T> value, Supplier<X> errorSupplier) {
        return of(Opt.over(value), errorSupplier);
    }

    static <T, X> Result<T, X> of(Opt<T> value, Supplier<X> errorSupplier) {
        return value.toResult(errorSupplier);
    }

    default Opt<T> toOpt() {
        return switch (this) {
            case Success(var value) -> Opt.of(value);
            case Failure(var error) -> Opt.empty();
        };
    }

    default Optional<T> toOptional() {
        return switch (this) {
            case Success(var value) -> Optional.of(value);
            case Failure(var error) -> Optional.empty();
        };
    }

    default <Y> Result<Y, X> map(Function<T, Y> mapper) {
        return switch (this) {
            case Success(var value) -> Result.success(mapper.apply(value));
            case Failure(var error) -> Result.failure(error);
        };
    }

    static <T, X> Result<T, X> success(T value) {
        return Success.of(value);
    }

    static <T, X> Result<T, X> failure(X error) {
        return Failure.of(error);
    }

    default T get() {
        return switch (this) {
            case Success(var value) -> value;
            case Failure(Throwable error) -> throw new IllegalStateException("Result is Failure", error);
            case Failure(var error) -> throw new IllegalStateException("Result is Failure (" + error + ")");
        };
    }

    default Throwable getError() {
        return switch (this) {
            case Success s -> throw new IllegalStateException("Result is Success");
            case Failure(Throwable error) -> error;
            case Failure(var error) -> new IllegalStateException("Result is Failure (" + error + ")");
        };
    }
}
