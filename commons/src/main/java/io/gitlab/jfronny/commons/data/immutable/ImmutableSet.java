package io.gitlab.jfronny.commons.data.immutable;

import java.util.Set;

public class ImmutableSet<T, S extends Set<T>> extends ImmutableCollection<T, S> implements Set<T> {
    public static class Simple<T> extends ImmutableSet<T, Set<T>> {
        public Simple(Set<T> delegate) {
            super(delegate);
        }
    }

    public ImmutableSet(S delegate) {
        super(delegate);
    }
}
