package io.gitlab.jfronny.commons.data.delegate;

import java.util.Objects;

public class DelegateObject<S> {
    protected final S delegate;

    public DelegateObject(S delegate) {
        this.delegate = Objects.requireNonNull(delegate) instanceof DelegateObject<?> dob ? (S) dob.unwrap() : delegate;
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DelegateObject<?> dob ? delegate.equals(dob.delegate) : delegate.equals(obj);
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    private S unwrap() {
        return delegate instanceof DelegateObject<?> dob ? (S) dob.unwrap() : delegate;
    }
}
