package io.gitlab.jfronny.commons.data;

import io.gitlab.jfronny.commons.data.immutable.*;

import java.util.*;

public class ImmCollection {
    public static <T> Collection<T> of(Collection<T> collection) {
        return new ImmutableCollection<>(collection);
    }

    public static <T> List<T> of(List<T> list) {
        return new ImmutableList<>(list);
    }

    public static <T> List<T> copyOf(List<T> list) {
        return of(new ArrayList<>(list));
    }

    public static <T> Set<T> of(Set<T> set) {
        return new ImmutableSet<>(set);
    }

    public static <T> Set<T> copyOf(Set<T> set) {
        return of(new LinkedHashSet<>(set));
    }

    public static <K, V> Map<K, V> of(Map<K, V> map) {
        return new ImmutableMap<>(map);
    }

    public static <K, V> Map<K, V> copyOf(Map<K, V> map) {
        return of(new LinkedHashMap<>(map));
    }

    public static <T> Iterable<T> of(Iterable<T> iterable) {
        return new ImmutableIterable<>(iterable);
    }

    public static <T> Iterator<T> of(Iterator<T> iterator) {
        return new ImmutableIterator<>(iterator);
    }

    public static <T> ListIterator<T> of(ListIterator<T> listIterator) {
        return new ImmutableListIterator<>(listIterator);
    }
}
