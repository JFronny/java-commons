package io.gitlab.jfronny.commons;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public record StreamIterable<T>(Supplier<Stream<T>> source) implements Iterable<T> {
    public StreamIterable(Stream<T> source) {
        this(new OnceSupplier<>(source));
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return source.get().iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        this.source.get().forEach(action);
    }
}
