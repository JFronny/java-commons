package io.gitlab.jfronny.commons;

import java.util.*;
import java.util.stream.Collectors;

public class ArgumentsTokenizer {
    public static String[] tokenize(String toProcess) {
        List<String> tokens = new ArrayList<>();
        StringBuilder currentToken = new StringBuilder();
        State state = State.None;
        char[] chars = toProcess.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            switch (chars[i]) {
                case '\'' -> state = switch (state) {
                    case None -> State.QuoteSingle;
                    case QuoteSingle -> State.None;
                    case QuoteDouble -> {
                        currentToken.append('\'');
                        yield State.QuoteDouble;
                    }
                };
                case '"' -> state = switch (state) {
                    case None -> State.QuoteDouble;
                    case QuoteSingle -> {
                        currentToken.append('"');
                        yield State.QuoteSingle;
                    }
                    case QuoteDouble -> State.None;
                };
                case '\\' -> {
                    if (i++ < chars.length) {
                        currentToken.append(switch (chars[i]) {
                            case 'b' -> '\b';
                            case 'f' -> '\f';
                            case 'n' -> '\n';
                            case 'r' -> '\r';
                            case 't' -> '\t';
                            default -> chars[i];
                        });
                    } else currentToken.append('\\');
                }
                case ' ' -> {
                    if (state == State.None) {
                        tokens.add(currentToken.toString());
                        currentToken = new StringBuilder();
                    } else {
                        currentToken.append(' ');
                    }
                }
                default -> currentToken.append(chars[i]);
            }
        }
        if (!currentToken.isEmpty()) tokens.add(currentToken.toString());
        return tokens.toArray(String[]::new);
    }

    public static String join(String[] args) {
        return Arrays.stream(args)
                .map(arg -> {
                    arg = arg
                            .replace("\\", "\\\\")
                            .replace("\b", "\\b")
                            .replace("\f", "\\f")
                            .replace("\n", "\\n")
                            .replace("\r", "\\r")
                            .replace("\t", "\\t")
                            .replace("\"", "\\\"");
                    if (arg.contains("\\") || arg.contains(" "))
                        arg = "\"" + arg + "\"";
                    return arg;
                })
                .collect(Collectors.joining(" "));
    }

    private enum State {
        None, QuoteSingle, QuoteDouble
    }
}
