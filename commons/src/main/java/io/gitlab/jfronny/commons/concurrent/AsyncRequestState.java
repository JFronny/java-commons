package io.gitlab.jfronny.commons.concurrent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AsyncRequestState {
    private final Lock lock = new ReentrantLock();

    private boolean isRunning = false;
    private boolean isScheduled = false;


    public FinishedResponse emitFinished() {
        lock.lock();
        try {
            if (isScheduled) {
                isScheduled = false;
                return new FinishedResponse(true);
            } else {
                isRunning = false;
                return new FinishedResponse(false);
            }
        } finally {
            lock.unlock();
        }
    }

    public RequestResponse request() {
        lock.lock();
        try {
            if (isRunning) {
                isScheduled = true;
                return new RequestResponse(false);
            } else {
                isRunning = true;
                return new RequestResponse(true);
            }
        } finally {
            lock.unlock();
        }
    }

    public void cancel() {
        lock.lock();
        try {
            isScheduled = false;
        } finally {
            lock.unlock();
        }
    }

    public record FinishedResponse(boolean shouldContinue) {}
    public record RequestResponse(boolean shouldStart) {}
}
