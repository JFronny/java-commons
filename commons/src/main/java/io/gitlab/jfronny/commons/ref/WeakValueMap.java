package io.gitlab.jfronny.commons.ref;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.ref.*;
import java.util.*;

public class WeakValueMap<K, V> implements Map<K, V> {
    private final ReferenceQueue<V> queue = new ReferenceQueue<>();
    private final HashMap<K, WeakValue<K, V>> delegate = new HashMap<>();
    private final EntrySet entrySet = new EntrySet();
    private final ValueCollection values = new ValueCollection();

    @Override
    public synchronized int size() {
        processQueue();
        return delegate.size();
    }

    @Override
    public synchronized boolean isEmpty() {
        processQueue();
        return delegate.isEmpty();
    }

    @Override
    public synchronized boolean containsKey(Object o) {
        processQueue();
        return delegate.containsKey(o);
    }

    @Override
    public synchronized boolean containsValue(Object o) {
        return delegate.containsValue(WeakValue.create(o));
    }

    @Override
    public synchronized V get(Object o) {
        return unwrap(delegate.get(o));
    }

    @Nullable
    @Override
    public synchronized V put(K k, V v) {
        processQueue();
        return unwrap(delegate.put(k, WeakValue.create(k, v, queue)));
    }

    @Override
    public synchronized V remove(Object o) {
        return unwrap(delegate.remove(o));
    }

    @Override
    public synchronized void putAll(@NotNull Map<? extends K, ? extends V> map) {
        processQueue();
        for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
            delegate.put(entry.getKey(), WeakValue.create(entry.getKey(), entry.getValue(), queue));
        }
    }

    @Override
    public synchronized void clear() {
        delegate.clear();
    }

    @NotNull
    @Override
    public synchronized Set<K> keySet() {
        processQueue();
        return delegate.keySet();
    }

    @NotNull
    @Override
    public Set<Entry<K, V>> entrySet() {
        return entrySet;
    }

    @NotNull
    @Override
    public Collection<V> values() {
        return values;
    }

    @Nullable
    private static <T> T unwrap(@Nullable Reference<T> ref) {
        return ref == null ? null : ref.get();
    }

    public void processQueue() {
        WeakValue<K, V> ref;
        while ((ref = (WeakValue<K, V>) queue.poll()) != null) {
            delegate.remove(ref.key);
        }
    }

    private static class WeakValue<K, V> extends WeakReference<V> {
        private K key;

        private WeakValue(V value) {
            super(value);
        }

        private WeakValue(K key, V value, ReferenceQueue<V> queue) {
            super(value, queue);
            this.key = key;
        }

        private static <K, V> WeakValue<K, V> create(V value) {
            if (value == null) return null;
            else return new WeakValue<>(value);
        }

        private static <K, V> WeakValue<K, V> create(K key, V value, ReferenceQueue<V> queue) {
            if (value == null) return null;
            else return new WeakValue<>(key, value, queue);
        }

        @Override
        public int hashCode() {
            V ref = this.get();
            return ref == null ? 0 : ref.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (!(obj instanceof WeakValueMap.WeakValue<?, ?> v)) return false;
            return Objects.equals(this.get(), v.get());
        }

        @Override
        public String toString() {
            return Objects.toString(get());
        }
    }

    private class DelegateEntry implements Entry<K, V> {
        private final Entry<K, WeakValue<K, V>> ent;
        private V value;

        DelegateEntry(Entry<K, WeakValue<K, V>> ent, V value) {
            this.ent = ent;
            this.value = value;
        }

        @Override
        public K getKey() {
            return ent.getKey();
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(Object o) {
            Object oldValue = this.value;
            this.value = (V) o;
            ent.setValue(WeakValue.create(getKey(), this.value, queue));
            return (V) oldValue;
        }

        @Override
        public int hashCode() {
            K k;
            return ((((k = ent.getKey()) == null) ? 0 : k.hashCode())
                    ^ ((value == null) ? 0 : value.hashCode()));
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Entry e)) return false;
            return Objects.equals(getKey(), e.getKey()) && Objects.equals(getValue(), e.getValue());
        }
    }

    private class EntrySet extends AbstractSet<Entry<K, V>> {
        @Override
        public @NotNull Iterator<Entry<K, V>> iterator() {
            processQueue();

            return new Iterator<>() {
                final Iterator<Entry<K, WeakValue<K, V>>> delegate = WeakValueMap.this.delegate.entrySet().iterator();
                Entry<K, V> next = null;

                @Override
                public boolean hasNext() {
                    synchronized (WeakValueMap.this) {
                        if (delegate.hasNext()) {
                            Entry<K, WeakValue<K, V>> ent = delegate.next();
                            V value = unwrap(ent.getValue());
                            if (value == null) return hasNext();
                            next = new DelegateEntry(ent, value);
                            return true;
                        }
                    }
                    return false;
                }

                @Override
                public Entry<K, V> next() {
                    if ((next == null) && !hasNext())
                        throw new NoSuchElementException();
                    Entry<K, V> e = next;
                    next = null;
                    return e;
                }

                @Override
                public void remove() {
                    delegate.remove();
                }
            };
        }

        @Override
        public boolean isEmpty() {
            return WeakValueMap.this.isEmpty();
        }

        @Override
        public int size() {
            return WeakValueMap.this.size();
        }

        @Override
        public boolean remove(Object o) {
            if (!(o instanceof Map.Entry<?, ?> e)) return false;
            return WeakValueMap.this.remove(e.getKey(), e.getValue());
        }
    }

    private class ValueCollection extends AbstractCollection<V> {
        @Override
        public @NotNull Iterator<V> iterator() {
            return new Iterator<>() {
                final Iterator<Entry<K, V>> delegate = entrySet().iterator();

                @Override
                public boolean hasNext() {
                    return delegate.hasNext();
                }

                @Override
                public V next() {
                    return delegate.next().getValue();
                }

                @Override
                public void remove() {
                    delegate.remove();
                }
            };
        }

        @Override
        public int size() {
            return WeakValueMap.this.size();
        }

        @Override
        public boolean contains(Object o) {
            return WeakValueMap.this.containsValue(o);
        }
    }
}
