package io.gitlab.jfronny.commons.tuple;

import org.jetbrains.annotations.*;

import java.util.*;
import java.util.function.Function;

public record Tuple<T1, T2>(@Nullable T1 left, @Nullable T2 right) {
    @Contract(value = "_ -> new", pure = true)
    public static <T1> @NotNull Single<T1> of(@Nullable T1 val) {
        return new Single<>(val);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <T1, T2> @NotNull Tuple<T1, T2> of(@Nullable T1 left, @Nullable T2 right) {
        return new Tuple<>(left, right);
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static <T1, T2, T3> @NotNull Triple<T1, T2, T3> of(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3) {
        return new Triple<>(val1, val2, val3);
    }

    @Contract(value = "_, _, _, _ -> new", pure = true)
    public static <T1, T2, T3, T4> @NotNull Quadruple<T1, T2, T3, T4> of(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3, @Nullable T4 val4) {
        return new Quadruple<>(val1, val2, val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public static <T1, T2> @NotNull Tuple<T1, T2> from(@NotNull Map.Entry<T1, T2> entry) {
        Objects.requireNonNull(entry);
        return new Tuple<>(entry.getKey(), entry.getValue());
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Tuple<T, T2> mapLeft(@NotNull Function<T1, T> mapper) {
        return new Tuple<>(Objects.requireNonNull(mapper).apply(left), right);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Tuple<T1, T> mapRight(@NotNull Function<T2, T> mapper) {
        return new Tuple<>(left, Objects.requireNonNull(mapper).apply(right));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<T2, T1> swap() {
        return new Tuple<>(right, left);
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull <T3> Triple<T1, T2, T3> concat(@Nullable T3 val3) {
        return Triple.of(left, right, val3);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public @NotNull <T3, T4> Quadruple<T1, T2, T3, T4> concat(@Nullable T3 val3, @Nullable T4 val4) {
        return Quadruple.of(left, right, val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull <T3> Triple<T1, T2, T3> concat(@NotNull Single<T3> val3) {
        return Triple.of(left, right, val3.val());
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull <T3, T4> Quadruple<T1, T2, T3, T4> concat(@NotNull Tuple<T3, T4> val3) {
        return Quadruple.of(left, right, val3.left(), val3.right());
    }

    @Contract(value = "_ -> new", pure = true)
    public static <T1, T2> @NotNull Set<Tuple<T1, T2>> from(@NotNull Map<T1, T2> map) {
        Objects.requireNonNull(map);
        return new AbstractSet<>() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public boolean isEmpty() {
                return map.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                if (o instanceof Tuple(var left, var right)) {
                    if (map.containsKey(left) && map.get(left) == right)
                        return true;
                }
                if (map.containsKey(o)) return true;
                return false;
            }

            @Override
            public @NotNull Iterator<Tuple<T1, T2>> iterator() {
                return map.entrySet().stream().map(Tuple::from).iterator();
            }

            @Override
            public @NotNull Object @NotNull [] toArray() {
                return map.entrySet().stream().map(Tuple::from).toArray();
            }

            @Override
            public boolean add(Tuple<T1, T2> t1T2Tuple) {
                return map.put(t1T2Tuple.left, t1T2Tuple.right) == null;
            }

            @Override
            public boolean remove(Object o) {
                if (o instanceof Tuple(var left, var right)) {
                    return map.remove(left, right);
                }
                return map.remove(o) != null;
            }

            @Override
            public void clear() {
                map.clear();
            }
        };
    }
}
