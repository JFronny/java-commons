package io.gitlab.jfronny.commons.data;

import java.math.BigDecimal;
import java.util.Objects;

public class LazilyParsedNumber extends Number {
    private final String value;

    public LazilyParsedNumber(String value) {
        this.value = value;
    }

    private BigDecimal asBigDecimal() {
        BigDecimal decimal = NumberLimits.parseBigDecimal(value);
        // Cast to long to avoid issues with abs when value is Integer.MIN_VALUE
        if (Math.abs((long) decimal.scale()) >= 10_000) {
            throw new NumberFormatException("Number has unsupported scale: " + value);
        }
        return decimal;
    }

    @Override
    public int intValue() {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(value);
            } catch (NumberFormatException nfe) {
                return asBigDecimal().intValue();
            }
        }
    }

    @Override
    public long longValue() {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            return asBigDecimal().longValue();
        }
    }

    @Override
    public float floatValue() {
        return Float.parseFloat(value);
    }

    @Override
    public double doubleValue() {
        return Double.parseDouble(value);
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof LazilyParsedNumber that && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }
}
