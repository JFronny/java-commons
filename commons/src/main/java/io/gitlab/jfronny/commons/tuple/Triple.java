package io.gitlab.jfronny.commons.tuple;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Function;

public record Triple<T1, T2, T3>(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3) {
    @Contract(value = "_, _, _ -> new", pure = true)
    public static <T1, T2, T3> @NotNull Triple<T1, T2, T3> of(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3) {
        return new Triple<>(val1, val2, val3);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Triple<T, T2, T3> map1(@NotNull Function<T1, T> mapper) {
        return new Triple<>(Objects.requireNonNull(mapper).apply(val1), val2, val3);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Triple<T1, T, T3> map2(@NotNull Function<T2, T> mapper) {
        return new Triple<>(val1, Objects.requireNonNull(mapper).apply(val2), val3);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Triple<T1, T2, T> map3(@NotNull Function<T3, T> mapper) {
        return new Triple<>(val1, val2, Objects.requireNonNull(mapper).apply(val3));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<T1, Tuple<T2, T3>> slice1() {
        return Tuple.of(val1, Tuple.of(val2, val3));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<Tuple<T1, T2>, T3> slice2() {
        return Tuple.of(Tuple.of(val1, val2), val3);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Triple<T2, T3, T1> lShift() {
        return new Triple<>(val2, val3, val1);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Triple<T3, T1, T2> rShift() {
        return new Triple<>(val3, val1, val2);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Triple<T2, T1, T3> swapL() {
        return new Triple<>(val2, val1, val3);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Triple<T1, T3, T2> swapR() {
        return new Triple<>(val1, val3, val2);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Triple<T3, T2, T1> reverse() {
        return new Triple<>(val3, val2, val1);
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull <T4> Quadruple<T1, T2, T3, T4> concat(@Nullable T4 val4) {
        return Quadruple.of(val1, val2, val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public @NotNull <T4> Quadruple<T1, T2, T3, T4> concat(@NotNull Single<T4> val4) {
        return Quadruple.of(val1, val2, val3, val4.val());
    }
}
