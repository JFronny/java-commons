package io.gitlab.jfronny.commons.ref;

/**
 * Class containing methods for nop lambdas. Use these instead of () -> {}
 */
public class R {
    public static void nop() {
    }

    public static void nop(Object o1) {
    }

    public static void nop(Object o1, Object o2) {
    }
}
