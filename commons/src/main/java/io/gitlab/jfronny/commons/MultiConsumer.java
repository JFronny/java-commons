package io.gitlab.jfronny.commons;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

@FunctionalInterface
public interface MultiConsumer<T> extends Consumer<T> {
    /**
     * Accepts all elements from the iterable.
     *
     * @param iterable the iterable to accept elements from
     */
    default void acceptAll(Iterable<T> iterable) {
        for (T t : iterable) accept(t);
    }

    /**
     * Accepts all elements from the iterator, draining it.
     *
     * @param iterator the iterator to accept elements from
     */
    default void acceptAll(Iterator<T> iterator) {
        while (iterator.hasNext()) accept(iterator.next());
    }

    /**
     * Drains elements from the iterator up to and including the first element that matches the predicate.
     *
     * @param iterator the iterator to drain
     * @param predicate when to stop draining
     * @return the first element that matches the predicate, or null if the iterator is empty
     */
    default T drainUntil(Iterator<T> iterator, Predicate<T> predicate) {
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (predicate.test(next)) return next;
            accept(next);
        }
        return null;
    }

    @NotNull
    @Override
    default MultiConsumer<T> andThen(@NotNull Consumer<? super T> after) {
        Objects.requireNonNull(after);
        return t -> {
            accept(t);
            after.accept(t);
        };
    }
}
