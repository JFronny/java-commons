package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.*;

@FunctionalInterface
public interface ThrowingBooleanSupplier<TEx extends Throwable> {
    static <TEx extends Throwable> @NotNull ThrowingBooleanSupplier<TEx> of(@NotNull BooleanSupplier supplier) {
        return Objects.requireNonNull(supplier)::getAsBoolean;
    }

    boolean get() throws TEx;

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <T> @NotNull ThrowingPredicate<T, TEx> and(@NotNull ThrowingPredicate<? super T, ? extends TEx> other) {
        Objects.requireNonNull(other);
        return (t) -> this.get() && other.test(t);
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBooleanSupplier<TEx> and(@NotNull ThrowingBooleanSupplier<? extends TEx> other) {
        Objects.requireNonNull(other);
        return () -> this.get() && other.get();
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <T> @NotNull ThrowingPredicate<T, TEx> or(@NotNull ThrowingPredicate<? super T, ? extends TEx> other) {
        Objects.requireNonNull(other);
        return (t) -> this.get() || other.test(t);
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBooleanSupplier<TEx> or(@NotNull ThrowingBooleanSupplier<? extends TEx> other) {
        Objects.requireNonNull(other);
        return () -> this.get() || other.get();
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <T> @NotNull ThrowingPredicate<T, TEx> xor(@NotNull ThrowingPredicate<? super T, ? extends TEx> other) {
        Objects.requireNonNull(other);
        return (t) -> this.get() ^ other.test(t);
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBooleanSupplier<TEx> xor(@NotNull ThrowingBooleanSupplier<? extends TEx> other) {
        Objects.requireNonNull(other);
        return () -> this.get() ^ other.get();
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBooleanSupplier<TEx> negate() {
        return () -> !this.get();
    }

    @Contract(value = "_ -> new", pure = true)
    static <TEx extends Throwable> @NotNull ThrowingBooleanSupplier<TEx> not(@NotNull ThrowingBooleanSupplier<TEx> target) {
        return Objects.requireNonNull(target).negate();
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BooleanSupplier addHandler(@NotNull Predicate<Throwable> handler) {
        Objects.requireNonNull(handler);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                return handler.test(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BooleanSupplier addHandler(@NotNull Class<TEx> exception, @NotNull Predicate<TEx> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    return handler.test((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BooleanSupplier orThrow() {
        return orThrow(ExceptionWrapper::wrap)::get;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingBooleanSupplier<TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return () -> {
            try {
                return this.get();
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the supplier in a supplier not marked to throw the checked exception.
     *
     * @return a supplier that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default BooleanSupplier assumeSafe() {
        ThrowingBooleanSupplier<RuntimeException> supplier = (ThrowingBooleanSupplier<RuntimeException>) (ThrowingBooleanSupplier) this;
        return supplier::get;
    }
}
