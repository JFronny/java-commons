package io.gitlab.jfronny.commons.data.impl.node;

import io.gitlab.jfronny.commons.data.impl.util.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicReferenceArray;

public abstract class NonLeafNode<T> extends Node<T> {
    private final AtomicReferenceArray<Node<T>> outgoingEdges;
    private final List<Node<T>> outgoingEdgesAsList;

    public NonLeafNode(CharSequence edgeCharSequence, List<Node<T>> outgoingEdges) {
        super(edgeCharSequence);
        Node<T>[] childNodeArray = outgoingEdges.toArray(new Node[outgoingEdges.size()]);
        // Sort the child nodes...
        Arrays.sort(childNodeArray, null);
        this.outgoingEdges = new AtomicReferenceArray<>(childNodeArray);
        this.outgoingEdgesAsList = new AtomicReferenceArrayListAdapter<>(this.outgoingEdges);
    }

    @Override
    public Node<T> getOutgoingEdge(Character edgeFirstCharacter) {
        // Binary search for the index of the node whose edge starts with the given character.
        // Note that this binary search is safe in the face of concurrent modification due to constraints
        // we enforce on use of the array, as documented in the binarySearchForEdge method...
        int index = NodeUtil.binarySearchForEdge(outgoingEdges, edgeFirstCharacter);
        if (index < 0) {
            // No such edge exists...
            return null;
        }
        // Atomically return the child node at this index...
        return outgoingEdges.get(index);
    }

    @Override
    public void updateOutgoingEdge(Node<T> childNode) {
        // Binary search for the index of the node whose edge starts with the given character.
        // Note that this binary search is safe in the face of concurrent modification due to constraints
        // we enforce on use of the array, as documented in the binarySearchForEdge method...
        int index = NodeUtil.binarySearchForEdge(outgoingEdges, childNode.getIncomingEdgeFirstCharacter());
        if (index < 0) {
            throw new IllegalStateException("Cannot update the reference to the following child node for the edge starting with '" + childNode.getIncomingEdgeFirstCharacter() +"', no such edge already exists: " + childNode);
        }
        // Atomically update the child node at this index...
        outgoingEdges.set(index, childNode);
    }

    @Override
    public List<Node<T>> getOutgoingEdges() {
        return outgoingEdgesAsList;
    }
}
