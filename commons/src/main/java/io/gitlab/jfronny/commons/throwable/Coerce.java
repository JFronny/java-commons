package io.gitlab.jfronny.commons.throwable;

import org.jetbrains.annotations.*;

import java.util.Objects;

/**
 * Removes the need for casting when composing throwables
 */
public class Coerce {
    /**
     * Coerce the java compiler to interpret a lambda as a throwing biConsumer
     *
     * @param tr A throwing biConsumer
     * @return The throwing biConsumer
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, U, TEx extends Throwable> @NotNull ThrowingBiConsumer<T, U, TEx> biConsumer(@NotNull ThrowingBiConsumer<T, U, TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing biFunction
     *
     * @param tr A throwing biFunction
     * @return The throwing biFunction
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, U, R, TEx extends Throwable> @NotNull ThrowingBiFunction<T, U, R, TEx> biFunction(@NotNull ThrowingBiFunction<T, U, R, TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing booleanSupplier
     *
     * @param tr A throwing booleanSupplier
     * @return The throwing booleanSupplier
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <TEx extends Throwable> @NotNull ThrowingBooleanSupplier<TEx> booleanSupplier(@NotNull ThrowingBooleanSupplier<TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing consumer
     *
     * @param tr A throwing consumer
     * @return The throwing consumer
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, TEx extends Throwable> @NotNull ThrowingConsumer<T, TEx> consumer(@NotNull ThrowingConsumer<T, TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing function
     *
     * @param tr A throwing function
     * @return The throwing function
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, R, TEx extends Throwable> @NotNull ThrowingFunction<T, R, TEx> function(@NotNull ThrowingFunction<T, R, TEx> tr) {
        return tr;
    }

    /**
     * Create a lambda that casts a value to a type
     *
     * @param type The type to cast to
     * @return The throwing function
     */
    @Contract(value = "_ -> new", pure = true)
    public static <T, R, TEx extends Throwable> @NotNull ThrowingFunction<T, R, TEx> cast(@Nullable Class<?> type) {
        return type == null ? value -> (R) value : value -> (R) type.cast(value);
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing predicate
     *
     * @param tr A throwing predicate
     * @return The throwing predicate
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, TEx extends Throwable> @NotNull ThrowingPredicate<T, TEx> predicate(@NotNull ThrowingPredicate<T, TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing runnable
     *
     * @param tr A throwing runnable
     * @return The throwing runnable
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <TEx extends Throwable> @NotNull ThrowingRunnable<TEx> runnable(@NotNull ThrowingRunnable<TEx> tr) {
        return tr;
    }

    /**
     * Coerce the java compiler to interpret a lambda as a throwing supplier
     *
     * @param tr A throwing supplier
     * @return The throwing supplier
     */
    @Contract(value = "_ -> param1", pure = true)
    public static <T, TEx extends Throwable> @NotNull ThrowingSupplier<T, TEx> supplier(@NotNull ThrowingSupplier<T, TEx> tr) {
        return tr;
    }

    /**
     * Example:
     * <pre>
     * {@code
     * interface Test<T> {
     *     T get();
     *     void consume(T t);
     * }
     * class Example {
     *     Test<?> instance;
     *     void function() {
     *         Coerce.pin(instance.get(), v -> instance.consume(v)); // This compiles
     *         instance.consume(instance.get()); // This does not
     *     }
     * }
     * }
     * </pre>
     *
     * @param value  The value to pin
     * @param func   The function to apply to the pinned value
     * @param <TIn>  The type of the value to pin
     * @param <TOut> The return type of the function
     * @param <TEx>  An exception type (if needed)
     * @return The result of the function
     * @throws TEx If the function throws, nothing is handled here
     */
    public static <TIn, TOut, TEx extends Throwable> TOut pinF(@Nullable TIn value, @NotNull ThrowingFunction<TIn, TOut, TEx> func) throws TEx {
        return Objects.requireNonNull(func).apply(value);
    }

    /**
     * @param value The value to pin
     * @param func  The function to apply to the pinned value
     * @param <TIn> The type of the value to pin
     * @param <TEx> An exception type (if needed)
     * @throws TEx If the function throws, nothing is handled here
     */
    public static <TIn, TEx extends Throwable> void pinC(@Nullable TIn value, @NotNull ThrowingConsumer<TIn, TEx> func) throws TEx {
        Objects.requireNonNull(func).accept(value);
    }
}
