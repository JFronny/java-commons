package io.gitlab.jfronny.commons.data.delegate;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;

public class DelegateIterable<T, S extends Iterable<T>> extends DelegateObject<S> implements Iterable<T> {
    public static class Simple<T> extends DelegateIterable<T, Iterable<T>> {
        public Simple(Iterable<T> delegate) {
            super(delegate);
        }
    }

    public DelegateIterable(S delegate) {
        super(delegate);
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return delegate.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        delegate.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return delegate.spliterator();
    }
}
