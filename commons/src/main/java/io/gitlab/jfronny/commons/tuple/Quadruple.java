package io.gitlab.jfronny.commons.tuple;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Function;

public record Quadruple<T1, T2, T3, T4>(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3, @Nullable T4 val4) {
    @Contract(value = "_, _, _, _ -> new", pure = true)
    public static <T1, T2, T3, T4> @NotNull Quadruple<T1, T2, T3, T4> of(@Nullable T1 val1, @Nullable T2 val2, @Nullable T3 val3, @Nullable T4 val4) {
        return new Quadruple<>(val1, val2, val3, val4);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <T1, T2, T3, T4> @NotNull Quadruple<T1, T2, T3, T4> of(@NotNull Tuple<T1, T2> left, @NotNull Tuple<T3, T4> right) {
        return new Quadruple<>(left.left(), left.right(), right.left(), right.right());
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<T1, Triple<T2, T3, T4>> slice1() {
        return new Tuple<>(val1, new Triple<>(val2, val3, val4));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<Tuple<T1, T2>, Tuple<T3, T4>> slice2() {
        return new Tuple<>(new Tuple<>(val1, val2), new Tuple<>(val3, val4));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Tuple<Triple<T1, T2, T3>, T4> slice3() {
        return new Tuple<>(new Triple<>(val1, val2, val3), val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Quadruple<T, T2, T3, T4> map1(@NotNull Function<T1, T> mapper) {
        return new Quadruple<>(Objects.requireNonNull(mapper).apply(val1), val2, val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Quadruple<T1, T, T3, T4> map2(@NotNull Function<T2, T> mapper) {
        return new Quadruple<>(val1, Objects.requireNonNull(mapper).apply(val2), val3, val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Quadruple<T1, T2, T, T4> map3(@NotNull Function<T3, T> mapper) {
        return new Quadruple<>(val1, val2, Objects.requireNonNull(mapper).apply(val3), val4);
    }

    @Contract(value = "_ -> new", pure = true)
    public <T> @NotNull Quadruple<T1, T2, T3, T> map4(@NotNull Function<T4, T> mapper) {
        return new Quadruple<>(val1, val2, val3, Objects.requireNonNull(mapper).apply(val4));
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T2, T1, T3, T4> swap12() {
        return new Quadruple<>(val2, val1, val3, val4);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T3, T2, T1, T4> swap13() {
        return new Quadruple<>(val3, val2, val1, val4);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T4, T2, T3, T1> swap14() {
        return new Quadruple<>(val4, val2, val3, val1);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T1, T3, T2, T4> swap23() {
        return new Quadruple<>(val1, val3, val2, val4);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T1, T4, T3, T2> swap24() {
        return new Quadruple<>(val1, val4, val3, val2);
    }

    @Contract(value = "-> new", pure = true)
    public @NotNull Quadruple<T1, T2, T4, T3> swap34() {
        return new Quadruple<>(val1, val2, val4, val3);
    }
}
