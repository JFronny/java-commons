package io.gitlab.jfronny.commons.throwable;

import io.gitlab.jfronny.commons.switchsupport.Result;
import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface ThrowingFunction<T, R, TEx extends Throwable> {
    static <T, R> @NotNull ThrowingFunction<T, R, RuntimeException> of(@NotNull Function<T, R> function) {
        return Objects.requireNonNull(function)::apply;
    }

    R apply(T var1) throws TEx;

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingSupplier<R, TEx> compose(@NotNull ThrowingSupplier<? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return () -> this.apply(before.get());
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingFunction<V, R, TEx> compose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (v) -> this.apply(before.apply(v));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingFunction<T, V, TEx> andThen(@NotNull ThrowingFunction<? super R, ? extends V, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t) -> after.apply(this.apply(t));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingFunction<T, V, TEx> cast(@Nullable Class<?> type) {
        if (type == null) return (v) -> (V) this.apply(v);
        return andThen(Coerce.cast(type));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingConsumer<T, TEx> andThen(@NotNull ThrowingConsumer<? super R, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t) -> after.accept(this.apply(t));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Function<T, R> addHandler(@NotNull Function<Throwable, ? extends R> handler) {
        Objects.requireNonNull(handler);
        return (t) -> {
            try {
                return this.apply(t);
            } catch (Throwable e) {
                return handler.apply(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Function<T, R> addHandler(@NotNull Class<TEx> exception, @NotNull Function<TEx, ? extends R> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return (t) -> {
            try {
                return this.apply(t);
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    return handler.apply((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Function<T, R> orThrow() {
        return orThrow(ExceptionWrapper::wrap)::apply;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingFunction<T, R, TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return (t) -> {
            try {
                return this.apply(t);
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull Function<T, Result<R, TEx>> toResult(@NotNull Class<TEx> exception) {
        return v -> {
            try {
                return Result.success(this.apply(v));
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass())) return Result.failure((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the function in a function not marked to throw the checked exception.
     *
     * @return a function that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default Function<T, R> assumeSafe() {
        ThrowingFunction<T, R, RuntimeException> function = (ThrowingFunction<T, R, RuntimeException>) (ThrowingFunction) this;
        return function::apply;
    }
}
