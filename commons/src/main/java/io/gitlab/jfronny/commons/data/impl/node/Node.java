package io.gitlab.jfronny.commons.data.impl.node;

import io.gitlab.jfronny.commons.data.impl.util.NodeUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public abstract class Node<T> implements Serializable, Comparable<Node<T>> {
    public static <T> Node<T> of(CharSequence edgeCharacters, T value, List<Node<T>> children, boolean isRoot) {
        NodeUtil.precheckCreation(edgeCharacters, children, isRoot);
        Objects.requireNonNull(value, "Use the other factory method for nodes without values!");
        if (children.isEmpty()) {
            return new CharSequenceNodeLeafWithValue<>(edgeCharacters, value);
        }
        else {
            return new CharSequenceNodeNonLeafWithValue<>(edgeCharacters, value, children);
        }
    }

    public static <T> Node<T> of(CharSequence edgeCharacters, List<Node<T>> children, boolean isRoot) {
        NodeUtil.precheckCreation(edgeCharacters, children, isRoot);
        if (children.isEmpty()) {
            return new CharSequenceNodeLeafVoidValue<>(edgeCharacters);
        }
        else {
            return new CharSequenceNodeNonLeafVoidValue<>(edgeCharacters, children);
        }
    }

    private final CharSequence incomingEdgeCharSequence;

    public Node(CharSequence edgeCharSequence) {
        this.incomingEdgeCharSequence = edgeCharSequence;
    }

    public CharSequence getIncomingEdge() {
        return incomingEdgeCharSequence;
    }
    public Character getIncomingEdgeFirstCharacter() {
        return incomingEdgeCharSequence.charAt(0);
    }

    protected abstract String getValueString();

    @Override
    public String toString() {
        return "Node{" +
                "edge=" + getIncomingEdge() +
                ", value=" + getValueString() +
                ", edges=" + getOutgoingEdges() +
                "}";
    }

    @Override
    public int compareTo(@NotNull Node<T> o) {
        // Technically not a compliant implementation, but this should be fine given our use case
        return String.valueOf(getIncomingEdge()).compareTo(String.valueOf(o.getIncomingEdge()));
    }

    public abstract T getValue();
    public abstract boolean hasValue();
    public abstract Node<T> getOutgoingEdge(Character edgeFirstCharacter);
    public abstract void updateOutgoingEdge(Node<T> childNode);
    public abstract List<Node<T>> getOutgoingEdges();

    public Node<T> copyWithEdgeCharacters(CharSequence edgeCharacters, boolean isRoot) {
        return hasValue()
                ? of(edgeCharacters, getValue(), getOutgoingEdges(), isRoot)
                : of(edgeCharacters, getOutgoingEdges(), isRoot);
    }

    public Node<T> copyWithoutValue(boolean isRoot) {
        return of(getIncomingEdge(), getOutgoingEdges(), isRoot);
    }

    public Node<T> copyWithChildren(List<Node<T>> children, boolean isRoot) {
        return hasValue()
                ? of(getIncomingEdge(), getValue(), children, isRoot)
                : of(getIncomingEdge(), children, isRoot);
    }

    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();
        prettyPrint(sb, "", true, true);
        return sb.toString();
    }

    protected void prettyPrint(Appendable sb, String prefix, boolean isTail, boolean isRoot) {
        try {
            StringBuilder label = new StringBuilder();
            if (isRoot) {
                label.append("○");
                if (!getIncomingEdge().isEmpty()) {
                    label.append(" ");
                }
            }
            label.append(getIncomingEdge());
            if (hasValue()) {
                label.append(" (").append(getValue()).append(")");
            }
            sb.append(prefix).append(isTail ? isRoot ? "" : "└── ○ " : "├── ○ ").append(label).append("\n");
            List<Node<T>> children = getOutgoingEdges();
            for (int i = 0; i < children.size() - 1; i++) {
                children.get(i).prettyPrint(sb, prefix + (isTail ? isRoot ? "" : "    " : "│   "), false, false);
            }
            if (!children.isEmpty()) {
                children.get(children.size() - 1).prettyPrint(sb, prefix + (isTail ? isRoot ? "" : "    " : "│   "), true, false);
            }
        } catch (IOException ioException) {
            // Rethrow the checked exception as a runtime exception...
            throw new IllegalStateException(ioException);
        }
    }
}
