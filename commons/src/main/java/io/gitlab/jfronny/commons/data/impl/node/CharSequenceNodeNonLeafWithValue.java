package io.gitlab.jfronny.commons.data.impl.node;

import java.util.List;
import java.util.Objects;

public class CharSequenceNodeNonLeafWithValue<T> extends NonLeafNode<T> {
    private final T value;

    public CharSequenceNodeNonLeafWithValue(CharSequence edgeCharSequence, T value, List<Node<T>> outgoingEdges) {
        super(edgeCharSequence, outgoingEdges);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public boolean hasValue() {
        return true;
    }

    @Override
    protected String getValueString() {
        return String.valueOf(value);
    }
}
