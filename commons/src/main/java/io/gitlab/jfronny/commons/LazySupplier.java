package io.gitlab.jfronny.commons;

import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A supplier that proxies to a separate supplier and caches its result
 */
public class LazySupplier<T> implements Supplier<T> {
    private final Supplier<T> supplier;
    private T cache = null;
    private boolean initialized;

    /**
     * Create a lazy supplier from a pre-initialized value.
     * The backing supplier will be empty and never called.
     *
     * @param value The value to initialize the cache with
     */
    public LazySupplier(@Nullable T value) {
        this.supplier = () -> {
            throw new RuntimeException("Supplier should have never been called");
        };
        initialized = true;
        cache = value;
    }

    /**
     * Create a lazy supplier backed with the specified supplier.
     * It will at most be called once when this supplier is first used.
     *
     * @param supplier The backing supplier
     */
    public LazySupplier(@NotNull Supplier<T> supplier) {
        this.supplier = Objects.requireNonNull(supplier);
        initialized = false;
    }

    @Override
    public @NotNull T get() {
        if (!initialized) {
            cache = supplier.get();
            initialized = true;
        }
        return cache;
    }

    /**
     * Generates a new lazy supplier from a function.
     * This function is provided with this lazy supplier as a data source for transformation.
     * Allows complex transformations to not be run if a later supplier has a shortcut
     *
     * @param after A backing function for the new lazy supplier
     * @return A new lazy supplier
     */
    @Contract(value = "_ -> new", pure = true)
    public @NotNull LazySupplier<T> andThen(@NotNull Function<LazySupplier<T>, T> after) {
        return new LazySupplier<>(() -> after.apply(this));
    }
}
