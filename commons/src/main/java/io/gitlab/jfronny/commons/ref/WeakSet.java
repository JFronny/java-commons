package io.gitlab.jfronny.commons.ref;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.ref.*;
import java.util.*;

public class WeakSet<E> extends AbstractSet<E> {
    private final ReferenceQueue<E> queue = new ReferenceQueue<>();
    private final HashSet<Reference<E>> delegate = new HashSet<>();

    @NotNull
    @Override
    public synchronized Iterator<E> iterator() {
        return new Iterator<>() {
            final Iterator<Reference<E>> delegate = WeakSet.this.delegate.iterator();
            E next = null;

            @Override
            public boolean hasNext() {
                synchronized (WeakSet.this) {
                    if (delegate.hasNext()) {
                        next = unwrap(delegate.next());
                        return next != null || hasNext();
                    }
                }
                return false;
            }

            @Override
            public E next() {
                if ((next == null) && !hasNext())
                    throw new NoSuchElementException();
                E e = next;
                next = null;
                return e;
            }

            @Override
            public void remove() {
                delegate.remove();
            }
        };
    }

    @Override
    public synchronized int size() {
        processQueue();
        return delegate.size();
    }

    @Override
    public synchronized boolean contains(Object o) {
        return delegate.contains(WeakElement.create(o));
    }

    @Override
    public synchronized boolean add(E e) {
        processQueue();
        return delegate.add(WeakElement.create(e, this.queue));
    }

    @Override
    public synchronized boolean remove(Object o) {
        boolean ret = delegate.remove(WeakElement.create(o));
        processQueue();
        return ret;
    }

    @Override
    public synchronized void clear() {
        processQueue();
        delegate.clear();
    }

    @Nullable
    private static <T> T unwrap(@Nullable Reference<T> ref) {
        return ref == null ? null : ref.get();
    }

    private void processQueue() {
        Reference<? extends E> wv;

        while ((wv = this.queue.poll()) != null) {
            delegate.remove(wv);
        }
    }

    private static class WeakElement<E> extends WeakReference<E> {
        private final int hash;

        private WeakElement(E value) {
            super(value);
            hash = value.hashCode();
        }

        private WeakElement(E value, ReferenceQueue<E> queue) {
            super(value, queue);
            hash = value.hashCode();
        }

        private static <V> WeakElement<V> create(V value) {
            return value == null ? null : new WeakElement<>(value);
        }

        private static <V> WeakElement<V> create(V value, ReferenceQueue<V> queue) {
            return value == null ? null : new WeakElement<>(value, queue);
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (!(obj instanceof WeakElement<?> v)) return false;
            return Objects.equals(this.get(), v.get());
        }

        @Override
        public String toString() {
            return Objects.toString(get());
        }
    }
}
