package io.gitlab.jfronny.commons;

import java.util.Objects;
import java.util.function.Supplier;

public final class OnceSupplier<T> implements Supplier<T> {
    private final T value;
    private boolean supplied = false;

    public OnceSupplier(T value) {
        this.value = value;
    }

    @Override
    public T get() {
        if (supplied) throw new IllegalStateException("Attempted to use already used OnceSupplier");
        supplied = true;
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof OnceSupplier<?> that)) return false;
        return Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
