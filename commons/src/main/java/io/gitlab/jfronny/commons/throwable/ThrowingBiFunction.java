package io.gitlab.jfronny.commons.throwable;

import io.gitlab.jfronny.commons.switchsupport.Result;
import io.gitlab.jfronny.commons.tuple.Tuple;
import org.jetbrains.annotations.*;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

@FunctionalInterface
public interface ThrowingBiFunction<T, U, R, TEx extends Throwable> {
    static <T, U, R, TEx extends Throwable> @NotNull ThrowingBiFunction<T, U, R, TEx> of(@NotNull BiFunction<T, U, R> function) {
        return Objects.requireNonNull(function)::apply;
    }

    R apply(T var1, U var2) throws TEx;

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingFunction<Tuple<T, U>, R, TEx> fromTuple() {
        return (t) -> this.apply(t.left(), t.right());
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingFunction<V, R, TEx> compose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> left, @NotNull ThrowingFunction<? super V, ? extends U, ? extends TEx> right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return (t) -> this.apply(left.apply(t), right.apply(t));
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V, W> @NotNull ThrowingBiFunction<V, W, R, TEx> biCompose(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> left, @NotNull ThrowingFunction<? super W, ? extends U, ? extends TEx> right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return (l, r) -> this.apply(left.apply(l), right.apply(r));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiFunction<V, U, R, TEx> composeLeft(@NotNull ThrowingFunction<? super V, ? extends T, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (l, r) -> this.apply(before.apply(l), r);
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiFunction<T, V, R, TEx> composeRight(@NotNull ThrowingFunction<? super V, ? extends U, ? extends TEx> before) {
        Objects.requireNonNull(before);
        return (l, r) -> this.apply(l, before.apply(r));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiFunction<T, U, V, TEx> andThen(@NotNull ThrowingFunction<? super R, ? extends V, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t, u) -> after.apply(this.apply(t, u));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <V> @NotNull ThrowingBiFunction<T, U, V, TEx> cast(@Nullable Class<?> type) {
        if (type == null) return (t, u) -> (V) this.apply(t, u);
        return andThen(Coerce.cast(type));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull ThrowingBiConsumer<T, U, TEx> andThen(@NotNull ThrowingConsumer<? super R, ? extends TEx> after) {
        Objects.requireNonNull(after);
        return (t, u) -> after.accept(this.apply(t, u));
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiFunction<T, U, R> addHandler(@NotNull Function<Throwable, ? extends R> handler) {
        Objects.requireNonNull(handler);
        return (t, u) -> {
            try {
                return this.apply(t, u);
            } catch (Throwable e) {
                return handler.apply(e);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiFunction<T, U, R> addHandler(@NotNull Class<TEx> exception, @NotNull Function<TEx, ? extends R> handler) {
        Objects.requireNonNull(exception);
        Objects.requireNonNull(handler);
        return (t, u) -> {
            try {
                return this.apply(t, u);
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass()))
                    return handler.apply((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiFunction<T, U, R> orThrow() {
        return orThrow(ExceptionWrapper::wrap)::apply;
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default <TEx1 extends Throwable> @NotNull ThrowingBiFunction<T, U, R, TEx1> orThrow(@NotNull Function<Throwable, TEx1> generator) {
        Objects.requireNonNull(generator);
        return (t, u) -> {
            try {
                return this.apply(t, u);
            } catch (Throwable e) {
                throw generator.apply(e);
            }
        };
    }

    @Contract(value = "_ -> new", pure = true)
    @ApiStatus.NonExtendable
    default @NotNull BiFunction<T, U, Result<R, TEx>> toResult(@NotNull Class<TEx> exception) {
        return (v, u) -> {
            try {
                return Result.success(this.apply(v, u));
            } catch (Throwable e) {
                if (exception.isAssignableFrom(e.getClass())) return Result.failure((TEx) e);
                else throw ExceptionWrapper.wrap(e);
            }
        };
    }

    /**
     * Hides the exception this could throw from the java compiler, effectively making it unchecked.
     * Wraps the function in a function not marked to throw the checked exception.
     *
     * @return a function that does not throw checked exceptions
     */
    @Contract(value = "-> new", pure = true)
    @ApiStatus.NonExtendable
    default BiFunction<T, U, R> assumeSafe() {
        ThrowingBiFunction<T, U, R, RuntimeException> function = (ThrowingBiFunction<T, U, R, RuntimeException>) (ThrowingBiFunction) this;
        return function::apply;
    }
}
