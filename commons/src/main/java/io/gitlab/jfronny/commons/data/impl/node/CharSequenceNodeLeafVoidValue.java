package io.gitlab.jfronny.commons.data.impl.node;

import java.util.*;

public class CharSequenceNodeLeafVoidValue<T> extends LeafNode<T> {
    public CharSequenceNodeLeafVoidValue(CharSequence edgeCharSequence) {
        super(edgeCharSequence);
    }

    @Override
    public T getValue() {
        throw new NoSuchElementException();
    }

    @Override
    public boolean hasValue() {
        return false;
    }

    @Override
    protected String getValueString() {
        return "-";
    }
}
