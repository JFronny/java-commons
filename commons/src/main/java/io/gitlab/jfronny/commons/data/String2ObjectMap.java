package io.gitlab.jfronny.commons.data;

import io.gitlab.jfronny.commons.data.impl.node.Node;
import io.gitlab.jfronny.commons.data.impl.util.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.gitlab.jfronny.commons.data.String2ObjectMap.SearchResult.Classification;

public class String2ObjectMap<V> extends AbstractMap<String, V> implements Serializable, Iterable<Map.Entry<String, V>> {
    protected volatile Node<V> root;

    private final Lock lock = new ReentrantLock();

    public String2ObjectMap() {
        this.root = Node.of("", Collections.emptyList(), true);
    }

    protected void acquireWriteLock() {
        lock.lock();
    }

    protected void releaseWriteLock() {
        lock.unlock();
    }

    @Override
    public V put(String key, V value) {
        return putInternal(key, value, true);  // putInternal acquires write lock
    }

    @Nullable
    @Override
    public V putIfAbsent(String key, V value) {
        return putInternal(key, value, false); // putInternal acquires write lock
    }

    @Override
    public V get(Object key) {
        if (!(key instanceof CharSequence seq)) return null;
        SearchResult<V> searchResult = searchTree(seq);
        if (searchResult.classification.equals(Classification.EXACT_MATCH)) {
            return searchResult.nodeFound.hasValue()
                    ? searchResult.nodeFound.getValue()
                    : null;
        }
        return null;
    }

    @Override
    public boolean containsKey(Object key) {
        if (!(key instanceof CharSequence seq)) return false;
        SearchResult<V> searchResult = searchTree(seq);
        return searchResult.classification.equals(Classification.EXACT_MATCH);
    }

    public Iterable<CharSequence> getKeysStartingWith(CharSequence prefix) {
        return getForKeysStartingWith(prefix, this::getDescendantKeys, Collections::emptySet);
    }

    public Iterable<V> getValuesForKeysStartingWith(CharSequence prefix) {
        return getForKeysStartingWith(prefix, this::getDescendantValues, Collections::emptySet);
    }

    public Iterable<? extends Entry<String, V>> getKeyValuePairsForKeysStartingWith(CharSequence prefix) {
        return getForKeysStartingWith(prefix, this::getDescendantKeyValuePairs, Collections::emptySet);
    }

    interface GetForKeysStartingWith<V, T> {
        T transform(CharSequence prefix, Node<V> startNode);
    }

    private <T> T getForKeysStartingWith(CharSequence prefix, GetForKeysStartingWith<V, T> transform, Supplier<T> def) {
        SearchResult<V> searchResult = searchTree(prefix);
        Classification classification = searchResult.classification;
        switch (classification) {
            case EXACT_MATCH -> {
                return transform.transform(prefix, searchResult.nodeFound);
            }
            case KEY_ENDS_MID_EDGE -> {
                // Append the remaining characters of the edge to the key.
                // For example if we searched for CO, but first matching node was COFFEE,
                // the key associated with the first node should be COFFEE...
                CharSequence edgeSuffix = CharSequences.getSuffix(searchResult.nodeFound.getIncomingEdge(), searchResult.charsMatchedInNodeFound);
                prefix = CharSequences.concatenate(prefix, edgeSuffix);
                return transform.transform(prefix, searchResult.nodeFound);
            }
            default -> {
                // Incomplete match means key is not a prefix of any node...
                return def.get();
            }
        }
    }

    @Override
    public V remove(Object key) {
        if (!(key instanceof CharSequence csq)) throw new ClassCastException();
        acquireWriteLock();
        try {
            SearchResult<V> searchResult = searchTree(csq);
            SearchResult.Classification classification = searchResult.classification;
            if (Objects.requireNonNull(classification) != Classification.EXACT_MATCH) {
                return null;
            }
            if (!searchResult.nodeFound.hasValue()) {
                // This node was created automatically as a split between two branches (implicit node).
                // No need to remove it...
                return null;
            }

            // Proceed with deleting the node...
            List<Node<V>> childEdges = searchResult.nodeFound.getOutgoingEdges();
            if (childEdges.size() > 1) {
                // This node has more than one child, so if we delete the value from this node, we still need
                // to leave a similar node in place to act as the split between the child edges.
                // Just delete the value associated with this node.
                // -> Clone this node without its value, preserving its child nodes...
                Node<V> cloned = searchResult.nodeFound.copyWithoutValue(false);
                // Re-add the replacement node to the parent...
                searchResult.parentNode.updateOutgoingEdge(cloned);
            } else if (childEdges.size() == 1) {
                // Node has one child edge.
                // Create a new node which is the concatenation of the edges from this node and its child,
                // and which has the outgoing edges of the child and the value from the child.
                Node<V> child = childEdges.get(0);
                CharSequence concatenatedEdges = CharSequences.concatenate(searchResult.nodeFound.getIncomingEdge(), child.getIncomingEdge());
                Node<V> mergedNode = child.copyWithEdgeCharacters(concatenatedEdges, false);
                // Re-add the merged node to the parent...
                searchResult.parentNode.updateOutgoingEdge(mergedNode);
            } else {
                // Node has no children. Delete this node from its parent,
                // which involves re-creating the parent rather than simply updating its child edge
                // (this is why we need parentNodesParent).
                // However if this would leave the parent with only one remaining child edge,
                // and the parent itself has no value (is a split node), and the parent is not the root node
                // (a special case which we never merge), then we also need to merge the parent with its
                // remaining child.

                List<Node<V>> currentEdgesFromParent = searchResult.parentNode.getOutgoingEdges();
                // Create a list of the outgoing edges of the parent which will remain
                // if we remove this child...
                // Use a non-resizable list, as a sanity check to force ArrayIndexOutOfBounds...
                List<Node<V>> newEdgesOfParent = Arrays.asList(new Node[searchResult.parentNode.getOutgoingEdges().size() - 1]);
                for (int i = 0, added = 0, numParentEdges = currentEdgesFromParent.size(); i < numParentEdges; i++) {
                    Node<V> node = currentEdgesFromParent.get(i);
                    if (node != searchResult.nodeFound) {
                        newEdgesOfParent.set(added++, node);
                    }
                }

                // Note the parent might actually be the root node (which we should never merge)...
                boolean parentIsRoot = (searchResult.parentNode == root);
                Node<V> newParent;
                if (newEdgesOfParent.size() == 1 && !searchResult.parentNode.hasValue() && !parentIsRoot) {
                    // Parent is a non-root split node with only one remaining child, which can now be merged.
                    Node<V> parentsRemainingChild = newEdgesOfParent.get(0);
                    // Merge the parent with its only remaining child...
                    CharSequence concatenatedEdges = CharSequences.concatenate(searchResult.parentNode.getIncomingEdge(), parentsRemainingChild.getIncomingEdge());
                    newParent = parentsRemainingChild.copyWithEdgeCharacters(concatenatedEdges, false);
                } else {
                    // Parent is a node which either has a value of its own, has more than one remaining
                    // child, or is actually the root node (we never merge the root node).
                    // Create new parent node which is the same as is currently just without the edge to the
                    // node being deleted...
                    newParent = searchResult.parentNode.copyWithChildren(newEdgesOfParent, parentIsRoot);
                }
                // Re-add the parent node to its parent...
                if (parentIsRoot) {
                    // Replace the root node...
                    this.root = newParent;
                } else {
                    // Re-add the parent node to its parent...
                    searchResult.parentNodesParent.updateOutgoingEdge(newParent);
                }
            }
            return searchResult.nodeFound.getValue();
        }
        finally {
            releaseWriteLock();
        }
    }

    public Iterable<CharSequence> getClosestKeys(CharSequence candidate) {
        return getForClosestKeys(candidate, this::getDescendantKeys);
    }

    public Iterable<V> getValuesForClosestKeys(CharSequence candidate) {
        return getForClosestKeys(candidate, this::getDescendantValues);
    }

    public Iterable<? extends Entry<String, V>> getKeyValuePairsForClosestKeys(CharSequence candidate) {
        return getForClosestKeys(candidate, this::getDescendantKeyValuePairs);
    }

    interface GetForClosestKeys<V, T> {
        T transform(CharSequence candidate, Node<V> startNode);
    }

    private <T> Iterable<T> getForClosestKeys(CharSequence candidate, GetForClosestKeys<V, Iterable<T>> transform) {
        SearchResult<V> searchResult = searchTree(candidate);
        Classification classification = searchResult.classification;
        return switch (classification) {
            case EXACT_MATCH -> transform.transform(candidate, searchResult.nodeFound);
            case KEY_ENDS_MID_EDGE -> {
                // Append the remaining characters of the edge to the key.
                // For example if we searched for CO, but first matching node was COFFEE,
                // the key associated with the first node should be COFFEE...
                CharSequence edgeSuffix = CharSequences.getSuffix(searchResult.nodeFound.getIncomingEdge(), searchResult.charsMatchedInNodeFound);
                candidate = CharSequences.concatenate(candidate, edgeSuffix);
                yield transform.transform(candidate, searchResult.nodeFound);
            }
            case INCOMPLETE_MATCH_TO_MIDDLE_OF_EDGE -> {
                // Example: if we searched for CX, but deepest matching node was CO,
                // the results should include node CO and its descendants...
                CharSequence keyOfParentNode = CharSequences.getPrefix(candidate, searchResult.charsMatched - searchResult.charsMatchedInNodeFound);
                CharSequence keyOfNodeFound = CharSequences.concatenate(keyOfParentNode, searchResult.nodeFound.getIncomingEdge());
                yield transform.transform(keyOfNodeFound, searchResult.nodeFound);
            }
            case INCOMPLETE_MATCH_TO_END_OF_EDGE -> {
                if (searchResult.charsMatched == 0) {
                    // Closest match is the root node, we don't consider this a match for anything...
                    yield Collections.emptySet();
                }
                // Example: if we searched for COFFEE, but deepest matching node was CO,
                // the results should include node CO and its descendants...
                CharSequence keyOfNodeFound = CharSequences.getPrefix(candidate, searchResult.charsMatched);
                yield transform.transform(keyOfNodeFound, searchResult.nodeFound);
            }
            default -> Collections.emptySet();
        };
    }

    @Override
    public int size() {
        Deque<Node<V>> stack = new LinkedList<>();
        stack.push(this.root);
        int count = 0;
        while (true) {
            if (stack.isEmpty()) {
                return count;
            }
            Node<V> current = stack.pop();
            stack.addAll(current.getOutgoingEdges());
            if (current.hasValue()) {
                count++;
            }
        }
    }

    /**
     * Atomically adds the given value to the tree, creating a node for the value as necessary. If the value is already
     * stored for the same key, either overwrites the existing value, or simply returns the existing value, depending
     * on the given value of the <code>overwrite</code> flag.
     *
     * @param key The key against which the value should be stored
     * @param value The value to store against the key
     * @param overwrite If true, should replace any existing value, if false should not replace any existing value
     * @return The existing value for this key, if there was one, otherwise null
     */
    V putInternal(CharSequence key, V value, boolean overwrite) {
        if (key == null) {
            throw new IllegalArgumentException("The key argument was null");
        }
        if (key.length() == 0) {
            throw new IllegalArgumentException("The key argument was zero-length");
        }
        if (value == null) {
            throw new IllegalArgumentException("The value argument was null");
        }
        acquireWriteLock();
        try {
            // Note we search the tree here after we have acquired the write lock...
            SearchResult<V> searchResult = searchTree(key);
            SearchResult.Classification classification = searchResult.classification;

            switch (classification) {
                case EXACT_MATCH -> {
                    // Search found an exact match for all edges leading to this node.
                    // -> Add or update the value in the node found, by replacing
                    // the existing node with a new node containing the value...

                    // First check if existing node has a value, and if we are allowed to overwrite it.
                    // Return early without overwriting if necessary...
                    V existingValue = searchResult.nodeFound.hasValue() ? searchResult.nodeFound.getValue() : null;
                    if (!overwrite && searchResult.nodeFound.hasValue()) {
                        return existingValue;
                    }
                    // Create a replacement for the existing node containing the new value...
                    Node<V> replacementNode = Node.of(searchResult.nodeFound.getIncomingEdge(), value, searchResult.nodeFound.getOutgoingEdges(), false);
                    searchResult.parentNode.updateOutgoingEdge(replacementNode);
                    // Return the existing value...
                    return existingValue;
                }
                case KEY_ENDS_MID_EDGE -> {
                    // Search ran out of characters from the key while in the middle of an edge in the node.
                    // -> Split the node in two: Create a new parent node storing the new value,
                    // and a new child node holding the original value and edges from the existing node...
                    CharSequence keyCharsFromStartOfNodeFound = key.subSequence(searchResult.charsMatched - searchResult.charsMatchedInNodeFound, key.length());
                    CharSequence commonPrefix = CharSequences.getCommonPrefix(keyCharsFromStartOfNodeFound, searchResult.nodeFound.getIncomingEdge());
                    CharSequence suffixFromExistingEdge = CharSequences.subtractPrefix(searchResult.nodeFound.getIncomingEdge(), commonPrefix);

                    // Create new nodes...
                    Node<V> newChild = searchResult.nodeFound.copyWithEdgeCharacters(suffixFromExistingEdge, false);
                    Node<V> newParent = Node.of(commonPrefix, value, Arrays.asList(newChild), false);

                    // Add the new parent to the parent of the node being replaced (replacing the existing node)...
                    searchResult.parentNode.updateOutgoingEdge(newParent);

                    // Return null for the existing value...
                    return null;
                }
                case INCOMPLETE_MATCH_TO_END_OF_EDGE -> {
                    // Search found a difference in characters between the key and the start of all child edges leaving the
                    // node, the key still has trailing unmatched characters.
                    // -> Add a new child to the node, containing the trailing characters from the key.

                    // NOTE: this is the only branch which allows an edge to be added to the root.
                    // (Root node's own edge is "" empty string, so is considered a prefixing edge of every key)

                    // Create a new child node containing the trailing characters...
                    CharSequence keySuffix = key.subSequence(searchResult.charsMatched, key.length());
                    Node<V> newChild = Node.of(keySuffix, value, Collections.emptyList(), false);

                    // Clone the current node adding the new child...
                    List<Node<V>> edges = new ArrayList<>(searchResult.nodeFound.getOutgoingEdges().size() + 1);
                    edges.addAll(searchResult.nodeFound.getOutgoingEdges());
                    edges.add(newChild);
                    Node<V> clonedNode = searchResult.nodeFound.copyWithChildren(edges, searchResult.nodeFound == root);

                    // Re-add the cloned node to its parent node...
                    if (searchResult.nodeFound == root) {
                        this.root = clonedNode;
                    } else {
                        searchResult.parentNode.updateOutgoingEdge(clonedNode);
                    }

                    // Return null for the existing value...
                    return null;
                }
                case INCOMPLETE_MATCH_TO_MIDDLE_OF_EDGE -> {
                    // Search found a difference in characters between the key and the characters in the middle of the
                    // edge in the current node, and the key still has trailing unmatched characters.
                    // -> Split the node in three:
                    // Let's call node found: NF
                    // (1) Create a new node N1 containing the unmatched characters from the rest of the key, and the
                    // value supplied to this method
                    // (2) Create a new node N2 containing the unmatched characters from the rest of the edge in NF, and
                    // copy the original edges and the value from NF unmodified into N2
                    // (3) Create a new node N3, which will be the split node, containing the matched characters from
                    // the key and the edge, and add N1 and N2 as child nodes of N3
                    // (4) Re-add N3 to the parent node of NF, effectively replacing NF in the tree

                    CharSequence keyCharsFromStartOfNodeFound = key.subSequence(searchResult.charsMatched - searchResult.charsMatchedInNodeFound, key.length());
                    CharSequence commonPrefix = CharSequences.getCommonPrefix(keyCharsFromStartOfNodeFound, searchResult.nodeFound.getIncomingEdge());
                    CharSequence suffixFromExistingEdge = CharSequences.subtractPrefix(searchResult.nodeFound.getIncomingEdge(), commonPrefix);
                    CharSequence suffixFromKey = key.subSequence(searchResult.charsMatched, key.length());

                    // Create new nodes...
                    Node<V> n1 = Node.of(suffixFromKey, value, Collections.emptyList(), false);
                    Node<V> n2 = searchResult.nodeFound.copyWithEdgeCharacters(suffixFromExistingEdge, false);
                    Node<V> n3 = Node.of(commonPrefix, Arrays.asList(n1, n2), false);

                    searchResult.parentNode.updateOutgoingEdge(n3);

                    // Return null for the existing value...
                    return null;
                }
                default -> {
                    // This is a safeguard against a new enum constant being added in future.
                    throw new IllegalStateException("Unexpected classification for search result: " + searchResult);
                }
            }
        }
        finally {
            releaseWriteLock();
        }
    }

    /**
     * Returns a lazy iterable which will return {@link CharSequence} keys for which the given key is a prefix.
     * The results inherently will not contain duplicates (duplicate keys cannot exist in the tree).
     * <p/>
     * Note that this method internally converts {@link CharSequence}s to {@link String}s, to avoid set equality issues,
     * because equals() and hashCode() are not specified by the CharSequence API contract.
     */
    @SuppressWarnings({"JavaDoc"})
    Iterable<CharSequence> getDescendantKeys(final CharSequence startKey, final Node<V> startNode) {
        return getDescendantThing(startKey, startNode, (keyString, value) -> keyString);
    }

    /**
     * Returns a lazy iterable which will return values which are associated with keys in the tree for which
     * the given key is a prefix.
     */
    @SuppressWarnings({"JavaDoc"})
    Iterable<V> getDescendantValues(final CharSequence startKey, final Node<V> startNode) {
        return getDescendantThing(startKey, startNode, (keyString, value) -> value);
    }

    /**
     * Returns a lazy iterable which will return {@link KeyValuePair} objects each containing a key and a value,
     * for which the given key is a prefix of the key in the {@link KeyValuePair}. These results inherently will not
     * contain duplicates (duplicate keys cannot exist in the tree).
     * <p/>
     * Note that this method internally converts {@link CharSequence}s to {@link String}s, to avoid set equality issues,
     * because equals() and hashCode() are not specified by the CharSequence API contract.
     */
    @SuppressWarnings({"JavaDoc"})
    Iterable<KeyValuePair<String, V>> getDescendantKeyValuePairs(final CharSequence startKey, final Node<V> startNode) {
        return getDescendantThing(startKey, startNode, (keyString, value) -> new KeyValuePair<>(keyString, value, this));
    }

    interface GetDescendantThing<V, T> {
        T transform(String keyString, V value);
    }

    private <T> Iterable<T> getDescendantThing(final CharSequence startKey, final Node<V> startNode, final GetDescendantThing<V, T> transform) {
        return LazyIterator.iterable(() -> {
            Iterator<NodeKeyPair<V>> descendantNodes = lazyTraverseDescendants(startKey, startNode).iterator();
            return scope -> {
                // Traverse to the next matching node in the tree and return its key and value...
                while (descendantNodes.hasNext()) {
                    NodeKeyPair<V> nodeKeyPair = descendantNodes.next();
                    if (nodeKeyPair.node.hasValue()) {
                        // Dealing with a node explicitly added to tree (rather than an automatically-added split node).

                        // -> Convert the CharSequence to a String before returning, to avoid set equality issues,
                        // because equals() and hashCode() is not specified by the CharSequence API contract...
                        String keyString = String.valueOf(nodeKeyPair.key);
                        return transform.transform(keyString, nodeKeyPair.node.getValue());
                    }
                }
                // Finished traversing the tree, no more matching nodes to return...
                return scope.endOfData();
            };
        });
    }

    /**
     * Traverses the tree using depth-first, preordered traversal, starting at the given node, using lazy evaluation
     * such that the next node is only determined when next() is called on the iterator returned.
     * The traversal algorithm uses iteration instead of recursion to allow deep trees to be traversed without
     * requiring large JVM stack sizes.
     * <p>
     * Each node that is encountered is returned from the iterator along with a key associated with that node,
     * in a NodeKeyPair object. The key will be prefixed by the given start key, and will be generated by appending
     * to the start key the edges traversed along the path to that node from the start node.
     *
     * @param startKey The key which matches the given start node
     * @param startNode The start node
     * @return An iterator which when iterated traverses the tree using depth-first, preordered traversal,
     * starting at the given start node
     */
    protected Iterable<NodeKeyPair<V>> lazyTraverseDescendants(final CharSequence startKey, final Node<V> startNode) {
        return LazyIterator.iterable(() -> {
            Deque<NodeKeyPair<V>> stack = new LinkedList<>();
            stack.push(new NodeKeyPair<>(startNode, startKey));
            return scope -> {
                if (stack.isEmpty()) {
                    return scope.endOfData();
                }
                NodeKeyPair<V> current = stack.pop();
                List<Node<V>> childNodes = current.node.getOutgoingEdges();

                // -> Iterate child nodes in reverse order and so push them onto the stack in reverse order,
                // to counteract that pushing them onto the stack alone would otherwise reverse their processing order.
                // This ensures that we actually process nodes in ascending alphabetical order.
                for (int i = childNodes.size(); i > 0; i--) {
                    Node<V> child = childNodes.get(i - 1);
                    stack.push(new NodeKeyPair<>(child, CharSequences.concatenate(current.key, child.getIncomingEdge())));
                }
                return current;
            };
        });
    }

    /**
     * Encapsulates a node and its associated key. Used internally by {@link #lazyTraverseDescendants}.
     */
    protected static class NodeKeyPair<V> {
        public final Node<V> node;
        public final CharSequence key;

        public NodeKeyPair(Node<V> node, CharSequence key) {
            this.node = node;
            this.key = key;
        }
    }

    /**
     * Traverses the tree and finds the node which matches the longest prefix of the given key.
     * <p>
     * The node returned might be an <u>exact match</u> for the key, in which case {@link SearchResult#charsMatched}
     * will equal the length of the key.
     * <p>
     * The node returned might be an <u>inexact match</u> for the key, in which case {@link SearchResult#charsMatched}
     * will be less than the length of the key.
     * <p>
     * There are two types of inexact match:
     * <ul>
     *     <li>
     *         An inexact match which ends evenly at the boundary between a node and its children (the rest of the key
     *         not matching any children at all). In this case if we we wanted to add nodes to the tree to represent the
     *         rest of the key, we could simply add child nodes to the node found.
     *     </li>
     *     <li>
     *         An inexact match which ends in the middle of a the characters for an edge stored in a node (the key
     *         matching only the first few characters of the edge). In this case if we we wanted to add nodes to the
     *         tree to represent the rest of the key, we would have to split the node (let's call this node found: NF):
     *         <ol>
     *             <li>
     *                 Create a new node (N1) which will be the split node, containing the matched characters from the
     *                 start of the edge in NF
     *             </li>
     *             <li>
     *                 Create a new node (N2) which will contain the unmatched characters from the rest of the edge
     *                 in NF, and copy the original edges from NF unmodified into N2
     *             </li>
     *             <li>
     *                 Create a new node (N3) which will be the new branch, containing the unmatched characters from
     *                 the rest of the key
     *             </li>
     *             <li>
     *                 Add N2 as a child of N1
     *             </li>
     *             <li>
     *                 Add N3 as a child of N1
     *             </li>
     *             <li>
     *                 In the <b>parent node of NF</b>, replace the edge pointing to NF with an edge pointing instead
     *                 to N1. If we do this step atomically, reading threads are guaranteed to never see "invalid"
     *                 data, only either the old data or the new data
     *             </li>
     *         </ol>
     *     </li>
     * </ul>
     * The {@link SearchResult#classification} is an enum value based on its classification of the
     * match according to the descriptions above.
     *
     * @param key a key for which the node matching the longest prefix of the key is required
     * @return A {@link SearchResult} object which contains the node matching the longest prefix of the key, its
     * parent node, the number of characters of the key which were matched in total and within the edge of the
     * matched node, and a {@link SearchResult#classification} of the match as described above
     */
    public SearchResult<V> searchTree(CharSequence key) {
        Node<V> parentNodesParent = null;
        Node<V> parentNode = null;
        Node<V> currentNode = root;
        int charsMatched = 0, charsMatchedInNodeFound = 0;

        final int keyLength = key.length();
        outer_loop: while (charsMatched < keyLength) {
            Node<V> nextNode = currentNode.getOutgoingEdge(key.charAt(charsMatched));
            if (nextNode == null) {
                // Next node is a dead end...
                //noinspection UnnecessaryLabelOnBreakStatement
                break outer_loop;
            }

            parentNodesParent = parentNode;
            parentNode = currentNode;
            currentNode = nextNode;
            charsMatchedInNodeFound = 0;
            CharSequence currentNodeEdgeCharacters = currentNode.getIncomingEdge();
            for (int i = 0, numEdgeChars = currentNodeEdgeCharacters.length(); i < numEdgeChars && charsMatched < keyLength; i++) {
                if (currentNodeEdgeCharacters.charAt(i) != key.charAt(charsMatched)) {
                    // Found a difference in chars between character in key and a character in current node.
                    // Current node is the deepest match (inexact match)....
                    break outer_loop;
                }
                charsMatched++;
                charsMatchedInNodeFound++;
            }
        }
        return new SearchResult<>(key, currentNode, charsMatched, charsMatchedInNodeFound, parentNode, parentNodesParent);
    }

    /**
     * Finds the entry whose key matches the largest substring from the start of the provided key
     */
    public @Nullable Entry<String, V> searchTreeForLongestSubstring(CharSequence key) {
        String sKey = key.toString();

        Node<V> currentNode = root;
        KeyValuePair<String, V> lastWithValue = null;
        int charsMatched = 0;

        final int keyLength = sKey.length();
        outer_loop: while (charsMatched < keyLength) {
            Node<V> nextNode = currentNode.getOutgoingEdge(sKey.charAt(charsMatched));
            if (nextNode == null) {
                // Next node is a dead end...
                //noinspection UnnecessaryLabelOnBreakStatement
                break outer_loop;
            }

            currentNode = nextNode;
            int charsMatchedInNodeFound = 0;
            CharSequence currentNodeEdgeCharacters = currentNode.getIncomingEdge();
            for (int j = 0, numEdgeChars = currentNodeEdgeCharacters.length(); j < numEdgeChars && charsMatched < keyLength; j++) {
                if (currentNodeEdgeCharacters.charAt(j) != sKey.charAt(charsMatched)) {
                    // Found a difference in chars between character in key and a character in current node.
                    // Current node is the deepest match (inexact match)....
                    break outer_loop;
                }
                charsMatched++;
                charsMatchedInNodeFound++;
            }
            if (charsMatchedInNodeFound == currentNodeEdgeCharacters.length() && currentNode.hasValue()) {
                lastWithValue = new KeyValuePair<>(sKey.substring(0, charsMatched), currentNode.getValue(), String2ObjectMap.this);
            }
        }

        return lastWithValue;
    }

    /**
     * Encapsulates results of searching the tree for a node for which a given key is a prefix. Encapsulates the node
     * found, its parent node, its parent's parent node, and the number of characters matched in the current node and
     * in total.
     * <p>
     * Also classifies the search result so that algorithms in methods which use this SearchResult, when adding nodes
     * and removing nodes from the tree, can select appropriate strategies based on the classification.
     */
    public static class SearchResult<V> {
        final CharSequence key;
        final Node<V> nodeFound;
        final int charsMatched;
        final int charsMatchedInNodeFound;
        final Node<V> parentNode;
        final Node<V> parentNodesParent;
        final Classification classification;

        public enum Classification {
            EXACT_MATCH,
            INCOMPLETE_MATCH_TO_END_OF_EDGE,
            INCOMPLETE_MATCH_TO_MIDDLE_OF_EDGE,
            KEY_ENDS_MID_EDGE
        }

        SearchResult(CharSequence key, Node<V> nodeFound, int charsMatched, int charsMatchedInNodeFound, Node<V> parentNode, Node<V> parentNodesParent) {
            this.key = key;
            this.nodeFound = nodeFound;
            this.charsMatched = charsMatched;
            this.charsMatchedInNodeFound = charsMatchedInNodeFound;
            this.parentNode = parentNode;
            this.parentNodesParent = parentNodesParent;

            // Classify this search result...
            this.classification = classify(key, nodeFound, charsMatched, charsMatchedInNodeFound);
        }

        protected Classification classify(CharSequence key, Node<V> nodeFound, int charsMatched, int charsMatchedInNodeFound) {
            if (charsMatched == key.length()) {
                if (charsMatchedInNodeFound == nodeFound.getIncomingEdge().length()) {
                    return Classification.EXACT_MATCH;
                }
                else if (charsMatchedInNodeFound < nodeFound.getIncomingEdge().length()) {
                    return Classification.KEY_ENDS_MID_EDGE;
                }
            }
            else if (charsMatched < key.length()) {
                if (charsMatchedInNodeFound == nodeFound.getIncomingEdge().length()) {
                    return Classification.INCOMPLETE_MATCH_TO_END_OF_EDGE;
                }
                else if (charsMatchedInNodeFound < nodeFound.getIncomingEdge().length()) {
                    return Classification.INCOMPLETE_MATCH_TO_MIDDLE_OF_EDGE;
                }
            }
            throw new IllegalStateException("Unexpected failure to classify SearchResult: " + this);
        }

        @Override
        public String toString() {
            return "SearchResult{" +
                    "key=" + key +
                    ", nodeFound=" + nodeFound +
                    ", charsMatched=" + charsMatched +
                    ", charsMatchedInNodeFound=" + charsMatchedInNodeFound +
                    ", parentNode=" + parentNode +
                    ", parentNodesParent=" + parentNodesParent +
                    ", classification=" + classification +
                    '}';
        }
    }

    @NotNull
    @Override
    public Set<Entry<String, V>> entrySet() {
        return new AbstractSet<>() {
            @Override
            public Iterator<Entry<String, V>> iterator() {
                return String2ObjectMap.this.iterator();
            }

            @Override
            public int size() {
                return String2ObjectMap.this.size();
            }
        };
    }

    public Iterator<Entry<String, V>> iterator() {
        Deque<KeyValuePair<String, V>> result = StreamSupport
                .stream(getDescendantKeyValuePairs("", root).spliterator(), false)
                .collect(Collectors.toCollection(LinkedList::new));

        return new Iterator<>() {
            Entry<String, V> previous = null;

            @Override
            public boolean hasNext() {
                return !result.isEmpty();
            }

            @Override
            public Entry<String, V> next() {
                if (result.isEmpty()) throw new NoSuchElementException();
                return previous = result.pop();
            }

            @Override
            public void remove() {
                if (previous == null) throw new IllegalStateException();
                String2ObjectMap.this.remove(previous.getKey());
                previous = null;
            }
        };
    }

    public String prettyPrint() {
        return root.prettyPrint();
    }

    /**
     * A unary operator that replaces all occurrences of keys of this map in the string with their values (via String.valueOf)
     * The largest possible replacement will be used.
     * The results of a replacement are not subject to further modification.
     */
    public UnaryOperator<String> asSubstitution() {
        return origin -> {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < origin.length();) {
                Entry<String, V> lastWithValue = searchTreeForLongestSubstring(origin.substring(i));

                if (lastWithValue == null) {
                    result.append(origin.charAt(i));
                    i++;
                } else {
                    result.append(lastWithValue.getValue());
                    i += lastWithValue.getKey().length();
                }
            }
            return result.toString();
        };
    }
}
