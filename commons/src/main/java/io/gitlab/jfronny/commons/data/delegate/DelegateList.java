package io.gitlab.jfronny.commons.data.delegate;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.UnaryOperator;

public class DelegateList<T, S extends List<T>> extends DelegateCollection<T, S> implements List<T> {
    public static class Simple<T> extends DelegateList<T, List<T>> {
        public Simple(List<T> delegate) {
            super(delegate);
        }
    }

    public DelegateList(S delegate) {
        super(delegate);
    }

    @Override
    public boolean addAll(int i, @NotNull Collection<? extends T> collection) {
        return delegate.addAll(i, collection);
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        delegate.replaceAll(operator);
    }

    @Override
    public void sort(Comparator<? super T> c) {
        delegate.sort(c);
    }

    @Override
    public T get(int i) {
        return delegate.get(i);
    }

    @Override
    public T set(int i, T t) {
        return delegate.set(i, t);
    }

    @Override
    public void add(int i, T t) {
        delegate.add(i, t);
    }

    @Override
    public T remove(int i) {
        return delegate.remove(i);
    }

    @Override
    public int indexOf(Object o) {
        return delegate.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return delegate.lastIndexOf(o);
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator() {
        return delegate.listIterator();
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator(int i) {
        return delegate.listIterator(i);
    }

    @NotNull
    @Override
    public List<T> subList(int i, int i1) {
        return delegate.subList(i, i1);
    }
}
