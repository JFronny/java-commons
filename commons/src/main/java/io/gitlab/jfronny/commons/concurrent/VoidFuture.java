package io.gitlab.jfronny.commons.concurrent;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.*;

public record VoidFuture(Future<?> future) implements Future<Void> {
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return future.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return future.isCancelled();
    }

    @Override
    public boolean isDone() {
        return future.isDone();
    }

    @Override
    public Void get() throws InterruptedException, ExecutionException {
        future.get();
        return null;
    }

    @Override
    public Void get(long timeout, @NotNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        future.get(timeout, unit);
        return null;
    }
}
