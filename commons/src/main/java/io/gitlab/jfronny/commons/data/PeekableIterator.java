package io.gitlab.jfronny.commons.data;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator that allows peeking the next element without consuming it.
 *
 * @param <E> the type of elements returned by this iterator
 */
public interface PeekableIterator<E> extends Iterator<E> {
    /**
     * Returns the next element in the iteration but does not remove it.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    E peek();
}
