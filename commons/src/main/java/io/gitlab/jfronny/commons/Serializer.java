package io.gitlab.jfronny.commons;

import io.gitlab.jfronny.commons.impl.SerializerHolder;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;

/**
 * Represents an externally defined serializer. Configure the one to use here via {@code SerializerHolder.setInstance}
 */
public interface Serializer {
    static Serializer getInstance() {
        return SerializerHolder.getInstance();
    }

    static void setInstance(Serializer instance) {
        SerializerHolder.setInstance(instance);
    }

    /**
     * Serialize an object to a string
     *
     * @param object The object to serialize
     * @return The serialized object as a string
     */
    String serialize(Object object) throws IOException;

    default void serialize(Object object, Appendable writer) throws IOException {
        writer.append(serialize(object));
    }

    /**
     * Deserialize data from a reader to an object
     *
     * @param source  The source from which the object should be deserialized
     * @param typeOfT The type of the desired object
     * @param <T>     The type of the desired object
     * @return The deserialized object
     * @throws SerializeException if there was a problem during serialization
     */
    default <T> T deserialize(Reader source, Class<T> typeOfT) throws IOException {
        return deserialize(source, (Type) typeOfT);
    }

    /**
     * Deserialize data from a reader to an object
     *
     * @param source  The source from which the object should be deserialized
     * @param typeOfT The type of the desired object
     * @param <T>     The type of the desired object
     * @return The deserialized object
     * @throws SerializeException if there was a problem during serialization
     */
    <T> T deserialize(Reader source, Type typeOfT) throws IOException;

    /**
     * Deserialize data from a reader to an object
     *
     * @param source  The source from which the object should be deserialized
     * @param typeOfT The type of the desired object
     * @param <T>     The type of the desired object
     * @return The deserialized object
     * @throws SerializeException if there was a problem during serialization
     */
    default <T> T deserialize(String source, Class<T> typeOfT) throws IOException {
        return deserialize(source, (Type) typeOfT);
    }

    /**
     * Deserialize data from a reader to an object
     *
     * @param source  The source from which the object should be deserialized
     * @param typeOfT The type of the desired object
     * @param <T>     The type of the desired object
     * @return The deserialized object
     * @throws SerializeException if there was a problem during serialization
     */
    <T> T deserialize(String source, Type typeOfT) throws IOException;

    /**
     * The MIME type for serialized data, for example application/json
     *
     * @return The MIME type
     */
    String getFormatMime();

    /**
     * An exception to be thrown when deserialization fails
     */
    class SerializeException extends IOException {
        public SerializeException(String message) {
            super(message);
        }

        public SerializeException(String message, Throwable cause) {
            super(message, cause);
        }

        public SerializeException(Throwable cause) {
            super(cause);
        }
    }
}
