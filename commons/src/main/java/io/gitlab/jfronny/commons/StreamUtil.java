package io.gitlab.jfronny.commons;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class StreamUtil {
    /**
     * A BiConsumer that keeps only elements of the specified type.
     * To be used with {@link Stream}.mapMulti.
     *
     * @param type The type to keep
     * @param <E> The type of the elements
     * @param <T> The type to keep
     * @return A BiConsumer that keeps only elements of the specified type
     */
    public static <E, T> BiConsumer<E, Consumer<T>> keepOnly(Class<T> type) {
        return (element, consumer) -> {
            if (type.isInstance(element)) {
                consumer.accept(type.cast(element));
            }
        };
    }
}
