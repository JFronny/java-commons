package io.gitlab.jfronny.commons.data.immutable;

import java.util.ListIterator;

public class ImmutableListIterator<T, S extends ListIterator<T>> extends ImmutableIterator<T, S> implements ListIterator<T> {
    public ImmutableListIterator(S delegate) {
        super(delegate);
    }

    @Override
    public boolean hasPrevious() {
        return delegate.hasPrevious();
    }

    @Override
    public T previous() {
        return delegate.previous();
    }

    @Override
    public int nextIndex() {
        return delegate.nextIndex();
    }

    @Override
    public int previousIndex() {
        return delegate.previousIndex();
    }

    @Override
    public void set(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(T t) {
        throw new UnsupportedOperationException();
    }
}
