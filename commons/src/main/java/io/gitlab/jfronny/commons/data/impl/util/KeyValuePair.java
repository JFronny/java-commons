package io.gitlab.jfronny.commons.data.impl.util;

import java.util.Map;
import java.util.Objects;

public final class KeyValuePair<K, V> implements Map.Entry<K, V> {
    private final Map<K, V> source;
    private final K key;
    private V value;

    public KeyValuePair(K key, V value, Map<K, V> source) {
        this.key = key;
        this.value = value;
        this.source = source;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        V result = source.put(key, value);
        this.value = value;
        return result;
    }

    @Override
    public String toString() {
        return "(" + key + ", " + value + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof KeyValuePair<?,?> that)) return false;
        return Objects.equals(this.key, that.key) &&
                Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}
