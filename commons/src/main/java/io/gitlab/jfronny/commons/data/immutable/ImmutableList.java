package io.gitlab.jfronny.commons.data.immutable;

import io.gitlab.jfronny.commons.data.ImmCollection;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.UnaryOperator;

public class ImmutableList<T, S extends List<T>> extends ImmutableCollection<T, S> implements List<T> {
    public ImmutableList(S delegate) {
        super(delegate);
    }

    @Override
    public boolean addAll(int i, @NotNull Collection<? extends T> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sort(Comparator<? super T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T get(int i) {
        return delegate.get(i);
    }

    @Override
    public T set(int i, T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int i, T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        return delegate.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return delegate.lastIndexOf(o);
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator() {
        return ImmCollection.of(delegate.listIterator());
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator(int i) {
        return ImmCollection.of(delegate.listIterator(i));
    }

    @NotNull
    @Override
    public List<T> subList(int i, int i1) {
        return ImmCollection.of(delegate.subList(i, i1));
    }
}
