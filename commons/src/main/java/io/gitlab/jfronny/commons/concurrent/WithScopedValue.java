package io.gitlab.jfronny.commons.concurrent;

import io.gitlab.jfronny.commons.throwable.*;
import org.jetbrains.annotations.ApiStatus;

public interface WithScopedValue<T> {
    @ApiStatus.Internal ScopedValue<T> getAttached();
    @ApiStatus.Internal T self();

    default <TEx1 extends Throwable, TEx2 extends Throwable> void withContext(Action<TEx1, TEx2> action) throws TEx1, TEx2 {
        Unchecked.<TEx1>reintroduce();
        Unchecked.<TEx2>reintroduce(() -> ScopedValue.runWhere(getAttached(), self(), action.assumeSafe()));
    }
    default <T, TEx1 extends Throwable, TEx2 extends Throwable> T withContext(Returnable<T, TEx1, TEx2> action) throws TEx1, TEx2 {
        Unchecked.<TEx1>reintroduce();
        return Unchecked.<T, TEx2>reintroduce(() -> ScopedValue.getWhere(getAttached(), self(), action.assumeSafe()));
    }

    interface Returnable<T, TEx1 extends Throwable, TEx2 extends Throwable> extends ThrowingSupplier<T, Throwable> {
        T run() throws TEx1, TEx2;

        @Override
        default T get() throws Throwable {
            return run();
        }
    }

    interface Action<TEx1 extends Throwable, TEx2 extends Throwable> extends ThrowingRunnable<Throwable> {
        void run() throws TEx1, TEx2;
    }
}
