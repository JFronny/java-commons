package io.gitlab.jfronny.commons.throwable;

import java.util.Objects;

public class ExceptionWrapper extends RuntimeException {
    public ExceptionWrapper(Throwable e) {
        super(e);
        if (e instanceof RuntimeException) {
            throw new IllegalArgumentException("ExceptionWrapper cannot wrap RuntimeExceptions");
        }
    }

    public static RuntimeException wrap(Throwable t) {
        Objects.requireNonNull(t);
        return t instanceof RuntimeException r ? r : new ExceptionWrapper(t);
    }

    public static Throwable unwrap(Throwable t) {
        Objects.requireNonNull(t);
        return t instanceof ExceptionWrapper w ? w.getCause() : t;
    }
}
