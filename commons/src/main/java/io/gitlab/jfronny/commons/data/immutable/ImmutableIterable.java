package io.gitlab.jfronny.commons.data.immutable;

import io.gitlab.jfronny.commons.data.ImmCollection;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ImmutableIterable<T, S extends Iterable<T>> extends ImmutableObject<S> implements Iterable<T> {
    public ImmutableIterable(S delegate) {
        super(delegate);
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return ImmCollection.of(delegate.iterator());
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        delegate.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return delegate.spliterator();
    }
}
