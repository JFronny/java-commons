package io.gitlab.jfronny.commons.data;

import java.util.function.Consumer;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public final class Either<T1, T2> {
    public static <T1, T2> Either<T1, T2> left(T1 value) {
        return new Either<>(value, true);
    }

    public static <T1, T2> Either<T1, T2> right(T2 value) {
        return new Either<>(value, false);
    }

    private final Object value;
    private final boolean isLeft;

    private Either(Object value, boolean isLeft) {
        this.value = value;
        this.isLeft = isLeft;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public boolean isRight() {
        return !isLeft;
    }

    public T1 left() {
        if (!isLeft) throw new IllegalStateException("This Either does not represent a left value");
        return (T1) value;
    }

    public T2 right() {
        if (isLeft) throw new IllegalStateException("This Either does not represent a right value");
        return (T2) value;
    }

    public void apply(Consumer<? super T1> lFunc, Consumer<? super T2> rFunc) {
        if (isLeft) relax(lFunc).accept(value);
        else relax(rFunc).accept(value);
    }

    public <T3> T3 fold(Function<? super T1, ? extends T3> lFunc, Function<? super T2, ? extends T3> rFunc) {
        return isLeft ? relax(lFunc).apply(value) : relax(rFunc).apply(value);
    }

    public <T3> Either<T3, T2> mapLeft(Function<? super T1, ? extends T3> func) {
        return isLeft ? Either.left(relax(func).apply(value)) : Either.right((T2) value);
    }

    public <T3> Either<T1, T3> mapRight(Function<? super T2, ? extends T3> func) {
        return isLeft ? Either.left((T1) value) : Either.right(relax(func).apply(value));
    }

    private static Consumer<Object> relax(Consumer<?> func) {
        return (Consumer<Object>) func;
    }

    private static <T> Function<Object, T> relax(Function<?, T> func) {
        return (Function<Object, T>) func;
    }
}
