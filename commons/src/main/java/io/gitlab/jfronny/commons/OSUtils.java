package io.gitlab.jfronny.commons;

import java.io.File;
import java.nio.file.*;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class OSUtils {
    public static final Type TYPE;
    public static final String USER_DIR;

    static {
        String os = System.getProperty("os.name", "generic").toLowerCase(Locale.ROOT);
        if ((os.contains("mac")) || (os.contains("darwin"))) TYPE = Type.MAC_OS;
        else if (os.contains("win")) TYPE = Type.WINDOWS;
        else if (os.contains("nux")) TYPE = Type.LINUX;
        else throw new RuntimeException("Unrecognized OS");
        USER_DIR = System.getProperty("user.home");
    }

    public static boolean executablePathContains(String executableName) {
        try {
            return Stream.of(System.getenv("PATH").split(Pattern.quote(File.pathSeparator)))
                    .map(path -> path.replace("\"", ""))
                    .map(Paths::get)
                    .anyMatch(path -> Files.exists(path.resolve(executableName))
                            && Files.isExecutable(path.resolve(executableName)));
        } catch (Exception e) {
            return false;
        }
    }

    public static Path getJvmBinary(Path jvmDir) {
        Path f = jvmDir.resolve("bin");
        Path t = f.resolve("java");
        if (!Files.exists(t)) t = f.resolve("javaw");
        if (!Files.exists(t)) t = f.resolve("java.exe");
        if (!Files.exists(t)) t = f.resolve("javaw.exe");
        return t;
    }

    public static String getJvmBinary() {
        return getJvmBinary(Paths.get(System.getProperty("java.home"))).toAbsolutePath().toString();
    }

    public enum Type {
        WINDOWS("Windows", "windows", ".dll"),
        MAC_OS("OSX", "osx", ".dylib"),
        LINUX("Linux", "linux", ".so");

        public final String displayName;
        public final String mojName;
        public final String libraryExtension;

        Type(String displayName, String mojName, String libraryExtension) {
            this.displayName = displayName;
            this.mojName = mojName;
            this.libraryExtension = libraryExtension;
        }
    }
}
