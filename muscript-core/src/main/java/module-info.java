module io.gitlab.jfronny.commons.muscript.core {
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.muscript.core;
}