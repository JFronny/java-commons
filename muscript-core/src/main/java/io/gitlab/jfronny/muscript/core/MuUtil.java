package io.gitlab.jfronny.muscript.core;

import java.util.Arrays;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MuUtil {
    public static final Set<String> RESERVED_IDS = Set.of("null", "true", "false");
    public static final Pattern IDENTIFIER = Pattern.compile("[a-zA-Z_$][a-zA-Z_$0-9]*");

    public static boolean isValidId(String id) {
        return IDENTIFIER.matcher(id).matches() && !RESERVED_IDS.contains(id);
    }

    /**
     * Creates quotes around a string, supports strings containing quotes
     */
    public static String enquote(String literalText) {
        if (!literalText.contains("'")) return "'" + literalText + "'";
        if (!literalText.contains("\"")) return "\"" + literalText + "\"";
        return Arrays.stream(literalText.split("'")).map(s -> "'" + s + "'")
                .collect(Collectors.joining(" || \"'\" || "));
    }
}
