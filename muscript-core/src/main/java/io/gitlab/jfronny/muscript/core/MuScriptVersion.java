package io.gitlab.jfronny.muscript.core;

public enum MuScriptVersion {
    V1, V2, V3, V4;

    public static final MuScriptVersion DEFAULT = V4;

    public boolean contains(MuScriptVersion version) {
        return compareTo(version) >= 0;
    }
}
