package io.gitlab.jfronny.muscript.core;

public sealed interface StackFrame {
    record Raw(String file, String name, int chStart) implements StackFrame {
        @Override
        public String toString() {
            return name + " (call: character " + chStart + (file == null ? ")" : " in " + file + ")");
        }

        public Lined lined(String source) {
            int lineStart = source.lastIndexOf('\n', chStart);
            int lineIndex = lineStart > 0 ? (int) source.substring(0, lineStart).chars().filter(c -> c == '\n').count() : 0;
            return new Lined(file, name, lineIndex + 1);
        }
    }

    record Lined(String file, String name, int row) implements StackFrame {
        @Override
        public String toString() {
            return name + " (call: line " + row + (file == null ? ")" : " in " + file + ")");
        }
    }
}
