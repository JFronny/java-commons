package io.gitlab.jfronny.muscript.core;

public enum Order {
    Script,
    Conditional,
    And,
    Or,
    Equality,
    Concat,
    Comparison,
    Term,
    Factor,
    Exp,
    Unary,
    Call,
    Primary;
}
