package io.gitlab.jfronny.muscript.core;

import org.jetbrains.annotations.Nullable;

public record CodeLocation(int chStart, int chEnd, @Nullable String source, @Nullable String file) {
    public static final CodeLocation NONE = new CodeLocation(-1, -1);

    public CodeLocation(int chStart, int chEnd) {
        this(chStart, chEnd, null, null);
    }

    public CodeLocation(int ch) {
        this(ch, ch);
    }

    public CodeLocation withSource(String source) {
        return new CodeLocation(chStart, chEnd, source, file);
    }

    public CodeLocation withFile(String file) {
        return new CodeLocation(chStart, chEnd, source, file);
    }
}
