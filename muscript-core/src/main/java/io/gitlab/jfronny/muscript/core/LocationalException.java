package io.gitlab.jfronny.muscript.core;

import java.util.LinkedList;
import java.util.List;

/**
 * An exception type with a location
 * For use in MuScript, can be converted to a pretty LocationalError with asPrintable
 */
public class LocationalException extends RuntimeException {
    private final CodeLocation location;
    private final List<StackFrame> callStack = new LinkedList<>();

    private String messageCache;

    public LocationalException(CodeLocation location) {
        super();
        this.location = location;
    }

    public LocationalException(CodeLocation location, String message) {
        super(message);
        this.location = location;
    }

    public LocationalException(CodeLocation location, String message, Throwable cause) {
        super(message, cause);
        this.location = location;
    }

    public LocationalException(CodeLocation location, Throwable cause) {
        super(cause);
        this.location = location;
    }

    @Override
    public String getLocalizedMessage() {
        // Not getMessage() to fix locations
        return messageCache != null ? messageCache : (messageCache = asPrintable().toString());
    }

    public PrettyPrintError asPrintable() {
        return (location.source() == null ? PrettyPrintError.builder() : PrettyPrintError.builder(location))
                .setMessage(super.getMessage())
                .setCallStack(callStack)
                .build();
    }

    public LocationalException appendStack(StackFrame frame) {
        messageCache = null;
        callStack.add(frame);
        return this;
    }
}
