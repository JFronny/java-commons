package io.gitlab.jfronny.muscript.core;

import java.util.Map;

public interface SourceFS {
    String read(String file);

    record Immutable(Map<String, String> files) implements SourceFS {
        @Override
        public String read(String file) {
            return files.get(file);
        }
    }
}
