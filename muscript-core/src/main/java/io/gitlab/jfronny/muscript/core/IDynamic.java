package io.gitlab.jfronny.muscript.core;

/**
 * Represents a Dynamic in the source code.
 * To be implemented by the parser.
 */
public interface IDynamic {
    /**
     * @return true if the dynamic is a direct dynamic (it is constructed in a pure manner and non-recursively), false otherwise.
     */
    default boolean isDirect() {
        return false;
    }
}
