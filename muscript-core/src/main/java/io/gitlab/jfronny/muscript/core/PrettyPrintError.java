package io.gitlab.jfronny.muscript.core;

import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Class for storing errors with code context
 * Can be generated from a LocationalException with asPrintable
 *
 * @param message The error message
 * @param start   The location of the start of this error. May be null.
 * @param end     The location of the end of this error. May be null.
 */
public record PrettyPrintError(String message, @Nullable Location start, @Nullable Location end, List<? extends StackFrame> callStack) {
    /**
     * A location in the source code
     *
     * @param line   The code of the line this is in
     * @param column The column of this character, starting with 0
     * @param row    The row of this line, starting with 1
     */
    public record Location(String line, int column, int row) {
        public static Location create(@Nullable String source, int index) {
            if (source == null || index < 0) return new Location("", -1, -1);
            int lineStart = source.lastIndexOf('\n', index);
            int lineEnd = source.indexOf('\n', index);
            if (lineEnd == -1) lineEnd = source.length();

            int lineIndex = lineStart > 0 ? (int) source.substring(0, lineStart).chars().filter(c -> c == '\n').count() : 0;
            int column = index - lineStart - 1;

            String line = lineStart == source.length() - 1 ? "" : source.substring(lineStart + 1, lineEnd);

            return new Location(line, column, lineIndex + 1);
        }

        @Override
        public String toString() {
            if (column >= line.length()) return "character " + (column + 1) + " of line " + row;
            else return "'" + line.charAt(column) + "' (character " + (column + 1) + ")";
        }

        public String prettyPrint() {
            String linePrefix = String.format("%1$6d", row) + " | ";
            return linePrefix + line + "\n" + " ".repeat(linePrefix.length() + column) + "^-- ";
        }
    }

    /**
     * Converts this error to a human-readable error message
     *
     * @return A string representation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (start == null || start.column < 0) sb.append("Error at unknown location: ").append(message);
        else {
            sb.append("Error at ").append(start).append(": ").append(message).append("\n");
            if (end == null) sb.append(start.prettyPrint()).append("Here");
            else if (start.row == end.row) {
                String linePrefix = String.format("%1$6d", start.row) + " | ";
                sb.append(linePrefix).append(start.line).append("\n").append(" ".repeat(linePrefix.length() + start.column)).append("^").append("-".repeat(end.column - start.column - 1)).append("^-- Here");
            }
            else sb.append(start.prettyPrint()).append("From here").append(end.prettyPrint()).append("to here");
        }
        callStack.forEach(frame -> sb.append("\n  at ").append(frame));
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(CodeLocation location) {
        return builder().setSource(location.source()).setLocation(location.chStart(), location.chEnd());
    }

    public static class Builder {
        private String message;
        private Location start;
        private @Nullable Location end;
        private List<? extends StackFrame> callStack = List.of();
        private String source;

        public Builder setSource(String source) {
            this.source = source;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setLocation(int chStart) {
            return setLocation(Location.create(source, chStart), null);
        }

        public Builder setLocation(int chStart, int chEnd) {
            if (chEnd == chStart) return setLocation(chStart);
            if (chEnd < chStart) {
                int a = chEnd;
                chEnd = chStart;
                chStart = a;
            }
            return setLocation(Location.create(source, chStart), Location.create(source, chEnd));
        }

        public Builder setLocation(Location start, Location end) {
            this.start = start;
            this.end = end;
            return this;
        }

        public Builder setCallStack(List<StackFrame> callStack) {
            this.callStack = callStack.stream()
                    .map(frame -> frame instanceof StackFrame.Lined lined
                            ? lined
                            : source == null ? frame : ((StackFrame.Raw) frame).lined(source))
                    .toList();
            return this;
        }

        public PrettyPrintError build() {
            return new PrettyPrintError(message, start, end, callStack);
        }
    }
}
