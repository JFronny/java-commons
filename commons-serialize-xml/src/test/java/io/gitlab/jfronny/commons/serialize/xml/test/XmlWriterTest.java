package io.gitlab.jfronny.commons.serialize.xml.test;

import io.gitlab.jfronny.commons.serialize.xml.wrapper.XmlWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;

import static com.google.common.truth.Truth.assertThat;

public class XmlWriterTest {
    @Test
    public void testWriteArray() throws IOException {
        StringWriter writer = new StringWriter();
        XmlWriter xmlWriter = new XmlWriter(writer);

        xmlWriter.beginArray();
        xmlWriter.name("mytem");
        xmlWriter.value("value");
        xmlWriter.value("value2");
        xmlWriter.endArray();

        String expected = "<item><mytem>value</mytem><item>value2</item></item>";
        assertThat(writer.toString()).isEqualTo(expected);
    }

    @Test
    public void testWriteObject() throws IOException {
        StringWriter writer = new StringWriter();
        XmlWriter xmlWriter = new XmlWriter(writer);

        xmlWriter.name("w");
        xmlWriter.beginObject();
        xmlWriter.name("test");
        xmlWriter.value("value");
        xmlWriter.endObject();

        String expected = "<w><test>value</test></w>";
        assertThat(writer.toString()).isEqualTo(expected);
    }
}
