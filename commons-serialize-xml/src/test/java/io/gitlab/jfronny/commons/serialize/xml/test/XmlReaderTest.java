package io.gitlab.jfronny.commons.serialize.xml.test;

import io.gitlab.jfronny.commons.serialize.xml.wrapper.XmlReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static com.google.common.truth.Truth.assertThat;
import static io.gitlab.jfronny.commons.serialize.Token.*;

public class XmlReaderTest {
    @Test
    public void testParseArray() throws IOException {
        XmlReader reader = new XmlReader(reader("<items>value<!---->value2</items>"));
        assertThat(reader.peek()).isEqualTo(BEGIN_ARRAY);
        reader.beginArray();
        assertThat(reader.peek()).isEqualTo(STRING);
        assertThat(reader.nextName()).isEqualTo("item");
        assertThat(reader.peek()).isEqualTo(STRING);
        assertThat(reader.nextString()).isEqualTo("value");
        assertThat(reader.peek()).isEqualTo(STRING);
        assertThat(reader.nextName()).isEqualTo("item");
        assertThat(reader.peek()).isEqualTo(STRING);
        assertThat(reader.nextString()).isEqualTo("value2");
        assertThat(reader.peek()).isEqualTo(END_ARRAY);
        reader.endArray();
    }

    @Test
    public void testParseObject() throws IOException {
        XmlReader reader = new XmlReader(reader("<w><test>value</test></w>"));
        assertThat(reader.peek()).isEqualTo(BEGIN_OBJECT);
        reader.beginObject();
        assertThat(reader.peek()).isEqualTo(NAME);
        assertThat(reader.nextName()).isEqualTo("test");
        assertThat(reader.peek()).isEqualTo(STRING);
        assertThat(reader.nextString()).isEqualTo("value");
        assertThat(reader.peek()).isEqualTo(END_OBJECT);
        reader.endObject();
    }

    /** Returns a reader that returns one character at a time. */
    private static Reader reader(final String s) {
        return new StringReader(s);
    }
}
