/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.jfronny.commons.serialize.xml.test;

import io.gitlab.jfronny.commons.serialize.xml.NativeXmlReader;
import io.gitlab.jfronny.commons.serialize.xml.XmlToken;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static com.google.common.truth.Truth.assertThat;
import static io.gitlab.jfronny.commons.serialize.xml.XmlToken.*;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

@SuppressWarnings("resource")
public final class NativeXmlReaderTest {
    /**
     * Test for issue 212.
     */
    @Test
    public void testParseComments() throws IOException {
        String json = "<tag>\n"
                + "  <!--this is a comment-->\n"
                + "  someText\n"
                + "  <!--this is -> another -- com>ment */ -->\n"
                + "  <![CDATA[\"b\",\n"
                + "  \"c\"]]>\n"
                + "</tag>";

        NativeXmlReader reader = new NativeXmlReader(reader(json)).setLenient(true);
        assertThat(reader.peek()).isEqualTo(BEGIN_TAG);
        assertThat(reader.beginTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(TEXT);
        assertThat(reader.nextText()).isEqualTo("someText");
        assertThat(reader.peek()).isEqualTo(CDATA);
        assertThat(reader.nextCData()).isEqualTo("\"b\",\n  \"c\"");
        assertThat(reader.peek()).isEqualTo(END_TAG);
        assertThat(reader.endTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(EOF);
    }

    @Test
    public void testSetLenientTrue() {
        NativeXmlReader reader = new NativeXmlReader(reader("<tag></tag>"));
        reader.setLenient(true);
        assertThat(reader.isLenient()).isEqualTo(true);
    }

    @Test
    public void testSetLenientFalse() {
        NativeXmlReader reader = new NativeXmlReader(reader("<tag></tag>"));
        reader.setLenient(false);
        assertThat(reader.isLenient()).isEqualTo(false);
    }

    @Test
    public void testStrictModeFailsToParseUnescapedControlCharacter() throws IOException {
        String json = "<t a='\0'/>";
        NativeXmlReader reader = new NativeXmlReader(reader(json));
        reader.setLenient(false);

        assertThat(reader.beginTag()).isEqualTo("t");
        assertThat(reader.nextAttributeName()).isEqualTo("a");
        IOException expected = assertThrows(IOException.class, reader::nextAttributeValue);
        assertThat(expected)
                .hasMessageThat()
                .startsWith("Control character in attribute value at line 1 column 7 (char '\\0') path t.a");

        json = "<t b='\u001F'/>";
        reader = new NativeXmlReader(reader(json));
        reader.setLenient(false);

        assertThat(reader.beginTag()).isEqualTo("t");
        assertThat(reader.nextAttributeName()).isEqualTo("b");
        expected = assertThrows(IOException.class, reader::nextAttributeValue);
        assertThat(expected)
                .hasMessageThat()
                .startsWith("Control character in attribute value at line 1 column 7 (char '\\u001f') path t.b");
    }

    @Test
    public void testStrictModeAllowsOtherControlCharacters() throws IOException {
        // JSON specification only forbids control characters U+0000 - U+001F, other control characters
        // should be allowed
        String json = "\"\u007F\u009F\"";
        NativeXmlReader reader = new NativeXmlReader(reader(json));
        reader.setLenient(false);
        assertThat(reader.nextText()).isEqualTo("\"\u007F\u009F\"");
    }

    @Test
    public void testNonStrictModeParsesUnescapedControlCharacter() throws IOException {
        String json = "\"\t\"";
        NativeXmlReader reader = new NativeXmlReader(reader(json)).setLenient(true);
        assertThat(reader.nextText()).isEqualTo("\"\t\"");
    }

    @Test
    public void testReadArray() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<array><item></item><item/></array>"));
        assertThat(reader.beginTag()).isEqualTo("array");
        assertThat(reader.beginTag()).isEqualTo("item");
        assertThat(reader.endTag()).isEqualTo("item");
        assertThat(reader.beginTag()).isEqualTo("item");
        assertThat(reader.endTag()).isEqualTo("item");
        assertThat(reader.endTag()).isEqualTo("array");
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
    }

    @Test
    public void testReadEmptyArray() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<array></array>"));
        assertThat(reader.hasNext()).isTrue();
        reader.beginTag();
        assertThat(reader.hasNext()).isFalse();
        reader.endTag();
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
        assertThat(reader.hasNext()).isFalse();
    }

    @Test
    public void testReadEmptyArrayWithWhitespace() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<array> </array>"));
        assertThat(reader.hasNext()).isTrue();
        reader.beginTag();
        assertThat(reader.hasNext()).isFalse();
        reader.endTag();
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
        assertThat(reader.hasNext()).isFalse();
    }

    @Test
    public void testReadEmptyArrayWithComment() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<array><!-- comment --></array>"));
        assertThat(reader.hasNext()).isTrue();
        reader.beginTag();
        assertThat(reader.hasNext()).isFalse();
        reader.endTag();
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
        assertThat(reader.hasNext()).isFalse();
    }

    @Test
    public void testReadConciseEmptyArray() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<array/>"));
        assertThat(reader.hasNext()).isTrue();
        reader.beginTag();
        assertThat(reader.hasNext()).isFalse();
        reader.endTag();
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
        assertThat(reader.hasNext()).isFalse();
    }

    @Test
    public void testReadAttributes() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<tag a='1' b='2' c='3'/>"));
        assertThat(reader.beginTag()).isEqualTo("tag");
        assertThat(reader.nextAttributeName()).isEqualTo("a");
        assertThat(reader.nextAttributeValue()).isEqualTo("1");
        assertThat(reader.nextAttributeName()).isEqualTo("b");
        assertThat(reader.nextAttributeValue()).isEqualTo("2");
        assertThat(reader.nextAttributeName()).isEqualTo("c");
        assertThat(reader.nextAttributeValue()).isEqualTo("3");
        assertThat(reader.endTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
    }

    @Test
    public void testReadAttributesWithBody() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<tag a='1' b='2' c='3'>body<secondElement />body2</tag>"));
        assertThat(reader.beginTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_NAME);
        assertThat(reader.nextAttributeName()).isEqualTo("a");
        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_VALUE);
        assertThat(reader.nextAttributeValue()).isEqualTo("1");
        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_NAME);
        assertThat(reader.nextAttributeName()).isEqualTo("b");
        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_VALUE);
        assertThat(reader.nextAttributeValue()).isEqualTo("2");
        assertThat(reader.nextAttributeName()).isEqualTo("c");
        assertThat(reader.nextAttributeValue()).isEqualTo("3");
        assertThat(reader.peek()).isEqualTo(TEXT);
        assertThat(reader.nextText()).isEqualTo("body");
        assertThat(reader.peek()).isEqualTo(BEGIN_TAG);
        assertThat(reader.beginTag()).isEqualTo("secondElement");
        assertThat(reader.peek()).isEqualTo(END_TAG);
        assertThat(reader.endTag()).isEqualTo("secondElement");
        assertThat(reader.nextText()).isEqualTo("body2");
        assertThat(reader.endTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
    }

    @Test
    public void testDirectDescendant() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<tag><tag2>content</tag2></tag>"));
        assertThat(reader.beginTag()).isEqualTo("tag");
        assertThat(reader.beginTag()).isEqualTo("tag2");
        assertThat(reader.nextText()).isEqualTo("content");
        assertThat(reader.endTag()).isEqualTo("tag2");
        assertThat(reader.endTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);

    }

    @Test
    public void testXmlHeader() throws IOException {
        NativeXmlReader reader = new NativeXmlReader(reader("<?xml version=\"1.0\" encoding=\"UTF-8\"?><tag>content</tag>"));
        assertThrows(IOException.class, reader::peek);
        reader = new NativeXmlReader(reader("<?xml version=\"1.0\" encoding=\"UTF-8\"?><tag>content</tag>"));
        reader.setLenient(true);
        assertThat(reader.peek()).isEqualTo(XmlToken.BEGIN_TAG);
        assertThat(reader.beginTag()).isEqualTo("tag");
        assertThat(reader.nextText()).isEqualTo("content");
        assertThat(reader.endTag()).isEqualTo("tag");
        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
    }

//    @Test
//    public void testHelloWorld() throws IOException {
//        String json =
//                "{\n" //
//                        + "   \"hello\": true,\n" //
//                        + "   \"foo\": [\"world\"]\n" //
//                        + "}";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("hello");
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextName()).isEqualTo("foo");
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo("world");
//        reader.endArray();
//        reader.endObject();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testInvalidJsonInput() throws IOException {
//        String json =
//                "{\n" //
//                        + "   \"h\\ello\": true,\n" //
//                        + "   \"foo\": [\"world\"]\n" //
//                        + "}";
//
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginObject();
//        try {
//            reader.nextName();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Invalid escape sequence at line 2 column 8 (char 'l') path $.");
//        }
//    }
//
//    @SuppressWarnings("unused")
//    @Test
//    public void testNulls() {
//        try {
//            new BaseXmlReader(null);
//            fail();
//        } catch (NullPointerException expected) {
//        }
//    }
//
//    @Test
//    public void testEmptyString() throws IOException {
//        try {
//            new BaseXmlReader(reader("")).beginArray();
//            fail();
//        } catch (EOFException expected) {
//        }
//        try {
//            new BaseXmlReader(reader("")).beginObject();
//            fail();
//        } catch (EOFException expected) {
//        }
//    }
//
//    @Test
//    public void testCharacterUnescaping() throws IOException {
//        String json =
//                "[\"a\","
//                        + "\"a\\\"\","
//                        + "\"\\\"\","
//                        + "\":\","
//                        + "\",\","
//                        + "\"\\b\","
//                        + "\"\\f\","
//                        + "\"\\n\","
//                        + "\"\\r\","
//                        + "\"\\t\","
//                        + "\" \","
//                        + "\"\\\\\","
//                        + "\"{\","
//                        + "\"}\","
//                        + "\"[\","
//                        + "\"]\","
//                        + "\"\\u0000\","
//                        + "\"\\u0019\","
//                        + "\"\\u20AC\""
//                        + "]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo("a");
//        assertThat(reader.nextText()).isEqualTo("a\"");
//        assertThat(reader.nextText()).isEqualTo("\"");
//        assertThat(reader.nextText()).isEqualTo(":");
//        assertThat(reader.nextText()).isEqualTo(",");
//        assertThat(reader.nextText()).isEqualTo("\b");
//        assertThat(reader.nextText()).isEqualTo("\f");
//        assertThat(reader.nextText()).isEqualTo("\n");
//        assertThat(reader.nextText()).isEqualTo("\r");
//        assertThat(reader.nextText()).isEqualTo("\t");
//        assertThat(reader.nextText()).isEqualTo(" ");
//        assertThat(reader.nextText()).isEqualTo("\\");
//        assertThat(reader.nextText()).isEqualTo("{");
//        assertThat(reader.nextText()).isEqualTo("}");
//        assertThat(reader.nextText()).isEqualTo("[");
//        assertThat(reader.nextText()).isEqualTo("]");
//        assertThat(reader.nextText()).isEqualTo("\0");
//        assertThat(reader.nextText()).isEqualTo("\u0019");
//        assertThat(reader.nextText()).isEqualTo("\u20AC");
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testReaderDoesNotTreatU2028U2029AsNewline() throws IOException {
//        // This test shows that the JSON string [\n"whatever"] is seen as valid
//        // And the JSON string [\u2028"whatever"] is not.
//        String jsonInvalid2028 = "[\u2028\"whatever\"]";
//        BaseXmlReader readerInvalid2028 = new BaseXmlReader(reader(jsonInvalid2028));
//        readerInvalid2028.beginArray();
//        assertThrows(IOException.class, readerInvalid2028::nextString);
//
//        String jsonInvalid2029 = "[\u2029\"whatever\"]";
//        BaseXmlReader readerInvalid2029 = new BaseXmlReader(reader(jsonInvalid2029));
//        readerInvalid2029.beginArray();
//        assertThrows(IOException.class, readerInvalid2029::nextString);
//
//        String jsonValid = "[\n\"whatever\"]";
//        BaseXmlReader readerValid = new BaseXmlReader(reader(jsonValid));
//        readerValid.beginArray();
//        assertThat(readerValid.nextText()).isEqualTo("whatever");
//
//        // And even in STRICT mode U+2028 and U+2029 are not considered control characters
//        // and can appear unescaped in JSON string
//        String jsonValid2028And2029 = "\"whatever\u2028\u2029\"";
//        BaseXmlReader readerValid2028And2029 = new BaseXmlReader(reader(jsonValid2028And2029));
//        readerValid2028And2029.setLenient(false);
//        assertThat(readerValid2028And2029.nextText()).isEqualTo("whatever\u2028\u2029");
//    }
//
//    @Test
//    public void testEscapeCharacterQuoteInStrictMode() throws IOException {
//        String json = "\"\\'\"";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.setLenient(false);
//
//        IOException expected = assertThrows(IOException.class, reader::nextString);
//        assertThat(expected)
//                .hasMessageThat()
//                .startsWith("Invalid escaped character \"'\" in strict mode");
//    }
//
//    @Test
//    public void testEscapeCharacterQuoteWithoutStrictMode() throws IOException {
//        String json = "\"\\'\"";
//        BaseXmlReader reader = new BaseXmlReader(reader(json)).setLenient(true);
//        assertThat(reader.nextText()).isEqualTo("'");
//    }
//
//    @Test
//    public void testUnescapingInvalidCharacters() throws IOException {
//        String json = "[\"\\u000g\"]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Malformed Unicode escape \\u000g at line 1 column 5 (char '0') path $[0]");
//        }
//    }
//
//    @Test
//    public void testUnescapingTruncatedCharacters() throws IOException {
//        String json = "[\"\\u000";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Unterminated escape sequence at line 1 column 5 (char '0') path $[0]");
//        }
//    }
//
//    @Test
//    public void testUnescapingTruncatedSequence() throws IOException {
//        String json = "[\"\\";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Unterminated escape sequence at line 1 column 4 (char '[') path $[0]");
//        }
//    }
//
//    @Test
//    public void testIntegersWithFractionalPartSpecified() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[1.0,1.0,1.0]"));
//        reader.beginArray();
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextInt()).isEqualTo(1);
//        assertThat(reader.nextLong()).isEqualTo(1L);
//    }
//
//    @Test
//    public void testDoubles() throws IOException {
//        String json =
//                "[-0.0,"
//                        + "1.0,"
//                        + "1.7976931348623157E308,"
//                        + "4.9E-324,"
//                        + "0.0,"
//                        + "0.00,"
//                        + "-0.5,"
//                        + "2.2250738585072014E-308,"
//                        + "3.141592653589793,"
//                        + "2.718281828459045,"
//                        + "0,"
//                        + "0.01,"
//                        + "0e0,"
//                        + "1e+0,"
//                        + "1e-0,"
//                        + "1e0000," // leading 0 is allowed for exponent
//                        + "1e00001,"
//                        + "1e+1]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        assertThat(reader.nextDouble()).isEqualTo(-0.0);
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextDouble()).isEqualTo(1.7976931348623157E308);
//        assertThat(reader.nextDouble()).isEqualTo(4.9E-324);
//        assertThat(reader.nextDouble()).isEqualTo(0.0);
//        assertThat(reader.nextDouble()).isEqualTo(0.0);
//        assertThat(reader.nextDouble()).isEqualTo(-0.5);
//        assertThat(reader.nextDouble()).isEqualTo(2.2250738585072014E-308);
//        assertThat(reader.nextDouble()).isEqualTo(3.141592653589793);
//        assertThat(reader.nextDouble()).isEqualTo(2.718281828459045);
//        assertThat(reader.nextDouble()).isEqualTo(0.0);
//        assertThat(reader.nextDouble()).isEqualTo(0.01);
//        assertThat(reader.nextDouble()).isEqualTo(0.0);
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextDouble()).isEqualTo(10.0);
//        assertThat(reader.nextDouble()).isEqualTo(10.0);
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testStrictNonFiniteDoubles() throws IOException {
//        String json = "[NaN]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.nextDouble();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 'N') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictQuotedNonFiniteDoubles() throws IOException {
//        String json = "[\"NaN\"]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.nextDouble();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "JSON forbids NaN and infinities: NaN at line 1 column 7 (char ']') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientNonFiniteDoubles() throws IOException {
//        String json = "[NaN, -Infinity, Infinity]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextDouble()).isNaN();
//        assertThat(reader.nextDouble()).isEqualTo(Double.NEGATIVE_INFINITY);
//        assertThat(reader.nextDouble()).isEqualTo(Double.POSITIVE_INFINITY);
//        reader.endArray();
//    }
//
//    @Test
//    public void testLenientQuotedNonFiniteDoubles() throws IOException {
//        String json = "[\"NaN\", \"-Infinity\", \"Infinity\"]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextDouble()).isNaN();
//        assertThat(reader.nextDouble()).isEqualTo(Double.NEGATIVE_INFINITY);
//        assertThat(reader.nextDouble()).isEqualTo(Double.POSITIVE_INFINITY);
//        reader.endArray();
//    }
//
//    @Test
//    public void testStrictNonFiniteDoublesWithSkipValue() throws IOException {
//        String json = "[NaN]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 'N') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLongs() throws IOException {
//        String json =
//                "[0,0,0," + "1,1,1," + "-1,-1,-1," + "-9223372036854775808," + "9223372036854775807]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        assertThat(reader.nextLong()).isEqualTo(0L);
//        assertThat(reader.nextInt()).isEqualTo(0);
//        assertThat(reader.nextDouble()).isEqualTo(0.0);
//        assertThat(reader.nextLong()).isEqualTo(1L);
//        assertThat(reader.nextInt()).isEqualTo(1);
//        assertThat(reader.nextDouble()).isEqualTo(1.0);
//        assertThat(reader.nextLong()).isEqualTo(-1L);
//        assertThat(reader.nextInt()).isEqualTo(-1);
//        assertThat(reader.nextDouble()).isEqualTo(-1.0);
//        try {
//            reader.nextInt();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextLong()).isEqualTo(Long.MIN_VALUE);
//        try {
//            reader.nextInt();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextLong()).isEqualTo(Long.MAX_VALUE);
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    @Disabled(
//            "BaseXmlReader advances after exception for invalid number was thrown; to be decided if that is"
//                    + " acceptable")
//    public void testNumberWithOctalPrefix() throws IOException {
//        String json = "[01]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char '0') path $[0]");
//        }
//        try {
//            reader.nextInt();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "TODO");
//        }
//        try {
//            reader.nextLong();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "TODO");
//        }
//        try {
//            reader.nextDouble();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "TODO");
//        }
//        assertThat(reader.nextText()).isEqualTo("01");
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testBooleans() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true,false]"));
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextBoolean()).isFalse();
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testPeekingUnquotedStringsPrefixedWithBooleans() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[truey]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "a boolean", "TEXT", "line 1 column 2 (char 't') path $[0]");
//        }
//        assertThat(reader.nextText()).isEqualTo("truey");
//        reader.endArray();
//    }
//
//    @Test
//    public void testMalformedNumbers() throws IOException {
//        assertNotANumber("-");
//        assertNotANumber(".");
//
//        // plus sign is not allowed for integer part
//        assertNotANumber("+1");
//
//        // leading 0 is not allowed for integer part
//        assertNotANumber("00");
//        assertNotANumber("01");
//
//        // exponent lacks digit
//        assertNotANumber("e");
//        assertNotANumber("0e");
//        assertNotANumber(".e");
//        assertNotANumber("0.e");
//        assertNotANumber("-.0e");
//
//        // no integer
//        assertNotANumber("e1");
//        assertNotANumber(".e1");
//        assertNotANumber("-e1");
//
//        // trailing characters
//        assertNotANumber("1x");
//        assertNotANumber("1.1x");
//        assertNotANumber("1e1x");
//        assertNotANumber("1ex");
//        assertNotANumber("1.1ex");
//        assertNotANumber("1.1e1x");
//
//        // fraction has no digit
//        assertNotANumber("0.");
//        assertNotANumber("-0.");
//        assertNotANumber("0.e1");
//        assertNotANumber("-0.e1");
//
//        // no leading digit
//        assertNotANumber(".0");
//        assertNotANumber("-.0");
//        assertNotANumber(".0e1");
//        assertNotANumber("-.0e1");
//    }
//
//    private static void assertNotANumber(String s) throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(s));
//        reader.setLenient(true);
//        assertThat(reader.peek()).isEqualTo(XmlToken.TEXT);
//        assertThat(reader.nextText()).isEqualTo(s);
//
//        BaseXmlReader strictReader = new BaseXmlReader(reader(s));
//        try {
//            strictReader.nextDouble();
//            fail("Should have failed reading " + s + " as double");
//        } catch (MalformedDataException e) {
//            assertThat(e)
//                    .hasMessageThat()
//                    .startsWith("Use BaseXmlReader.setLenient(true) to accept malformed JSON");
//        }
//    }
//
//    @Test
//    public void testPeekingUnquotedStringsPrefixedWithIntegers() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[12.34e5x]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//        try {
//            reader.nextInt();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextText()).isEqualTo("12.34e5x");
//    }
//
//    @Test
//    public void testPeekLongMinValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[-9223372036854775808]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        assertThat(reader.nextLong()).isEqualTo(-9223372036854775808L);
//    }
//
//    @Test
//    public void testPeekLongMaxValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[9223372036854775807]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        assertThat(reader.nextLong()).isEqualTo(9223372036854775807L);
//    }
//
//    @Test
//    public void testLongLargerThanMaxLongThatWrapsAround() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[22233720368547758070]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        try {
//            reader.nextLong();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//    }
//
//    @Test
//    public void testLongLargerThanMinLongThatWrapsAround() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[-22233720368547758070]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        try {
//            reader.nextLong();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//    }
//
//    /** Issue 1053, negative zero. */
//    @Test
//    public void testNegativeZero() throws Exception {
//        BaseXmlReader reader = new BaseXmlReader(reader("[-0]"));
//        reader.setLenient(false);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        assertThat(reader.nextText()).isEqualTo("-0");
//    }
//
//    /**
//     * This test fails because there's no double for 9223372036854775808, and our long parsing uses
//     * Double.parseDouble() for fractional values.
//     */
//    @Test
//    @Disabled
//    public void testPeekLargerThanLongMaxValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[9223372036854775808]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        try {
//            reader.nextLong();
//            fail();
//        } catch (NumberFormatException e) {
//        }
//    }
//
//    /**
//     * This test fails because there's no double for -9223372036854775809, and our long parsing uses
//     * Double.parseDouble() for fractional values.
//     */
//    @Test
//    @Disabled
//    public void testPeekLargerThanLongMinValue() throws IOException {
//        @SuppressWarnings("FloatingPointLiteralPrecision")
//        double d = -9223372036854775809d;
//        BaseXmlReader reader = new BaseXmlReader(reader("[-9223372036854775809]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        try {
//            reader.nextLong();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextDouble()).isEqualTo(d);
//    }
//
//    /**
//     * This test fails because there's no double for 9223372036854775806, and our long parsing uses
//     * Double.parseDouble() for fractional values.
//     */
//    @Test
//    @Disabled
//    public void testHighPrecisionLong() throws IOException {
//        String json = "[9223372036854775806.000]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        assertThat(reader.nextLong()).isEqualTo(9223372036854775806L);
//        reader.endArray();
//    }
//
//    @Test
//    public void testPeekMuchLargerThanLongMinValue() throws IOException {
//        @SuppressWarnings("FloatingPointLiteralPrecision")
//        double d = -92233720368547758080d;
//        BaseXmlReader reader = new BaseXmlReader(reader("[-92233720368547758080]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(NUMBER);
//        try {
//            reader.nextLong();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextDouble()).isEqualTo(d);
//    }
//
//    @Test
//    public void testQuotedNumberWithEscape() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[\"12\\u00334\"]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//        assertThat(reader.nextInt()).isEqualTo(1234);
//    }
//
//    @Test
//    public void testMixedCaseLiterals() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[True,TruE,False,FALSE,NULL,nulL]")).setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextBoolean()).isFalse();
//        assertThat(reader.nextBoolean()).isFalse();
//        reader.nextNull();
//        reader.nextNull();
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testMissingValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected value at line 1 column 6 (char '}') path $.a");
//        }
//    }
//
//    @Test
//    public void testPrematureEndOfInput() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":true,"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.nextName();
//            fail();
//        } catch (EOFException expected) {
//        }
//    }
//
//    @Test
//    public void testPrematurelyClosed() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":[]}"));
//        reader.beginObject();
//        reader.close();
//        try {
//            reader.nextName();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertThat(expected).hasMessageThat().isEqualTo("BaseXmlReader is closed");
//        }
//
//        reader = new BaseXmlReader(reader("{\"a\":[]}"));
//        reader.close();
//        try {
//            reader.beginObject();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertThat(expected).hasMessageThat().isEqualTo("BaseXmlReader is closed");
//        }
//
//        reader = new BaseXmlReader(reader("{\"a\":true}"));
//        reader.beginObject();
//        String unused1 = reader.nextName();
//        XmlToken unused2 = reader.peek();
//        reader.close();
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertThat(expected).hasMessageThat().isEqualTo("BaseXmlReader is closed");
//        }
//    }
//
//    @Test
//    public void testNextFailuresDoNotAdvance() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":true}"));
//        reader.beginObject();
//        try {
//            String unused = reader.nextText();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "a string", "NAME", "line 1 column 3 (char 'a') path $.");
//        }
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            String unused = reader.nextName();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "a name", "BOOLEAN", "line 1 column 10 (char '}') path $.a");
//        }
//        try {
//            reader.beginArray();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "BEGIN_TAG", "BOOLEAN", "line 1 column 10 (char '}') path $.a");
//        }
//        try {
//            reader.endArray();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "END_TAG", "BOOLEAN", "line 1 column 10 (char '}') path $.a");
//        }
//        try {
//            reader.beginObject();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "BEGIN_TAG", "BOOLEAN", "line 1 column 10 (char '}') path $.a");
//        }
//        try {
//            reader.endObject();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "END_TAG", "BOOLEAN", "line 1 column 10 (char '}') path $.a");
//        }
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.nextText();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "a string", "END_TAG", "line 1 column 11 (char '\\0') path $.a");
//        }
//        try {
//            reader.nextName();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "a name", "END_TAG", "line 1 column 11 (char '\\0') path $.a");
//        }
//        try {
//            reader.beginArray();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "BEGIN_TAG", "END_TAG", "line 1 column 11 (char '\\0') path $.a");
//        }
//        try {
//            reader.endArray();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(
//                    expected, "END_TAG", "END_TAG", "line 1 column 11 (char '\\0') path $.a");
//        }
//        reader.endObject();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//        reader.close();
//    }
//
//    @Test
//    public void testIntegerMismatchFailuresDoNotAdvance() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[1.5]"));
//        reader.beginArray();
//        try {
//            reader.nextInt();
//            fail();
//        } catch (NumberFormatException expected) {
//        }
//        assertThat(reader.nextDouble()).isEqualTo(1.5d);
//        reader.endArray();
//    }
//
//    @Test
//    public void testStringNullIsNotNull() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[\"null\"]"));
//        reader.beginArray();
//        try {
//            reader.nextNull();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "null", "TEXT", "line 1 column 3 (char 'n') path $[0]");
//        }
//    }
//
//    @Test
//    public void testNullLiteralIsNotAString() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[null]"));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertUnexpectedStructureError(expected, "a string", "NULL", "line 1 column 6 (char ']') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictNameValueSeparator() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\"=true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char 't') path $.a");
//        }
//
//        reader = new BaseXmlReader(reader("{\"a\"=>true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char '>') path $.a");
//        }
//    }
//
//    @Test
//    public void testLenientNameValueSeparator() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\"=true}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextBoolean()).isTrue();
//
//        reader = new BaseXmlReader(reader("{\"a\"=>true}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextBoolean()).isTrue();
//    }
//
//    @Test
//    public void testStrictNameValueSeparatorWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\"=true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char 't') path $.a");
//        }
//
//        reader = new BaseXmlReader(reader("{\"a\"=>true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char '>') path $.a");
//        }
//    }
//
//    @Test
//    public void testCommentsInStringValue() throws Exception {
//        BaseXmlReader reader = new BaseXmlReader(reader("[\"// comment\"]"));
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo("// comment");
//        reader.endArray();
//
//        reader = new BaseXmlReader(reader("{\"a\":\"#someComment\"}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextText()).isEqualTo("#someComment");
//        reader.endObject();
//
//        reader = new BaseXmlReader(reader("{\"#//a\":\"#some //Comment\"}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("#//a");
//        assertThat(reader.nextText()).isEqualTo("#some //Comment");
//        reader.endObject();
//    }
//
//    @Test
//    public void testStrictComments() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[// comment \n true]"));
//        reader.beginArray();
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char '/') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[# comment \n true]"));
//        reader.beginArray();
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ' ') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[/* comment */ true]"));
//        reader.beginArray();
//        try {
//            reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char '*') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientComments() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[// comment \n true]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//
//        reader = new BaseXmlReader(reader("[# comment \n true]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//
//        reader = new BaseXmlReader(reader("[/* comment */ true]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//    }
//
//    @Test
//    public void testStrictCommentsWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[// comment \n true]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char '/') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[# comment \n true]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ' ') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[/* comment */ true]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char '*') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictUnquotedNames() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{a:true}"));
//        reader.beginObject();
//        try {
//            reader.nextName();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ':') path $.");
//        }
//    }
//
//    @Test
//    public void testLenientUnquotedNames() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{a:true}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//    }
//
//    @Test
//    public void testStrictUnquotedNamesWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{a:true}"));
//        reader.beginObject();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ':') path $.");
//        }
//    }
//
//    @Test
//    public void testStrictSingleQuotedNames() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{'a':true}"));
//        reader.beginObject();
//        try {
//            reader.nextName();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 'a') path $.");
//        }
//    }
//
//    @Test
//    public void testLenientSingleQuotedNames() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{'a':true}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//    }
//
//    @Test
//    public void testStrictSingleQuotedNamesWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{'a':true}"));
//        reader.beginObject();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 'a') path $.");
//        }
//    }
//
//    @Test
//    public void testStrictUnquotedStrings() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[a]"));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 'a') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictUnquotedStringsWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[a]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 'a') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientUnquotedStrings() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[a]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo("a");
//    }
//
//    @Test
//    public void testStrictSingleQuotedStrings() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("['a']"));
//        reader.beginArray();
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 'a') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientSingleQuotedStrings() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("['a']"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo("a");
//    }
//
//    @Test
//    public void testStrictSingleQuotedStringsWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("['a']"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 'a') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictSemicolonDelimitedArray() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true;true]"));
//        reader.beginArray();
//        try {
//            boolean unused = reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 't') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientSemicolonDelimitedArray() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true;true]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextBoolean()).isTrue();
//    }
//
//    @Test
//    public void testStrictSemicolonDelimitedArrayWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true;true]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char 't') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictSemicolonDelimitedNameValuePair() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":true;\"b\":true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            boolean unused = reader.nextBoolean();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char 't') path $.a");
//        }
//    }
//
//    @Test
//    public void testLenientSemicolonDelimitedNameValuePair() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":true;\"b\":true}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.nextName()).isEqualTo("b");
//    }
//
//    @Test
//    public void testStrictSemicolonDelimitedNameValuePairWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":true;\"b\":true}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 6 (char 't') path $.a");
//        }
//    }
//
//    @Test
//    public void testStrictUnnecessaryArraySeparators() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true,,true]"));
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.nextNull();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 8 (char 't') path $[1]");
//        }
//
//        reader = new BaseXmlReader(reader("[,true]"));
//        reader.beginArray();
//        try {
//            reader.nextNull();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 't') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[true,]"));
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.nextNull();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertThat(expected).hasMessageThat().startsWith("Expected null but was END_TAG at line 1 column 8 (char '\\0') path $[1]");
//        }
//
//        reader = new BaseXmlReader(reader("[,]"));
//        reader.beginArray();
//        try {
//            reader.nextNull();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ']') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientUnnecessaryArraySeparators() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true,,true]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
////    reader.nextNull();
//        assertThat(reader.nextBoolean()).isTrue();
//        reader.endArray();
//
//        reader = new BaseXmlReader(reader("[,true]"));
//        reader.setLenient(true);
//        reader.beginArray();
////    reader.nextNull();
//        assertThat(reader.nextBoolean()).isTrue();
//        reader.endArray();
//
//        reader = new BaseXmlReader(reader("[true,]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
////    reader.nextNull();
//        reader.endArray();
//
//        reader = new BaseXmlReader(reader("[,]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        //reader.nextNull();
//        //reader.nextNull();
//        reader.endArray();
//    }
//
//    @Test
//    public void testStrictUnnecessaryArraySeparatorsWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[true,,true]"));
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 8 (char 't') path $[1]");
//        }
//
//        reader = new BaseXmlReader(reader("[,true]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char 't') path $[0]");
//        }
//
//        reader = new BaseXmlReader(reader("[true,]"));
//        reader.beginArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (IllegalStateException expected) {
//            assertThat(expected).hasMessageThat().isEqualTo("Attempt to skip led outside its parent");
//        }
//
//        reader = new BaseXmlReader(reader("[,]"));
//        reader.beginArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 3 (char ']') path $[0]");
//        }
//    }
//
//    @Test
//    public void testStrictMultipleTopLevelValues() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[] []"));
//        reader.beginArray();
//        reader.endArray();
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 5 (char ']') path $");
//        }
//    }
//
//    @Test
//    public void testLenientMultipleTopLevelValues() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[] true {}"));
//        reader.setLenient(true);
//        reader.beginArray();
//        reader.endArray();
//        assertThat(reader.nextBoolean()).isTrue();
//        reader.beginObject();
//        reader.endObject();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testStrictMultipleTopLevelValuesWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[] []"));
//        reader.beginArray();
//        reader.endArray();
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 5 (char ']') path $");
//        }
//    }
//
//    @Test
//    public void testTopLevelValueTypes() throws IOException {
//        BaseXmlReader reader1 = new BaseXmlReader(reader("true"));
//        assertThat(reader1.nextBoolean()).isTrue();
//        assertThat(reader1.peek()).isEqualTo(XmlToken.EOF);
//
//        BaseXmlReader reader2 = new BaseXmlReader(reader("false"));
//        assertThat(reader2.nextBoolean()).isFalse();
//        assertThat(reader2.peek()).isEqualTo(XmlToken.EOF);
//
//        BaseXmlReader reader3 = new BaseXmlReader(reader("null"));
//        assertThat(reader3.peek()).isEqualTo(XmlToken.NULL);
//        reader3.nextNull();
//        assertThat(reader3.peek()).isEqualTo(XmlToken.EOF);
//
//        BaseXmlReader reader4 = new BaseXmlReader(reader("123"));
//        assertThat(reader4.nextInt()).isEqualTo(123);
//        assertThat(reader4.peek()).isEqualTo(XmlToken.EOF);
//
//        BaseXmlReader reader5 = new BaseXmlReader(reader("123.4"));
//        assertThat(reader5.nextDouble()).isEqualTo(123.4);
//        assertThat(reader5.peek()).isEqualTo(XmlToken.EOF);
//
//        BaseXmlReader reader6 = new BaseXmlReader(reader("\"a\""));
//        assertThat(reader6.nextText()).isEqualTo("a");
//        assertThat(reader6.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testTopLevelValueTypeWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("true"));
//        reader.skipValue();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testStrictNonExecutePrefix() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(")]}'\n []"));
//        try {
//            reader.beginArray();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 1 (char ')') path $");
//        }
//    }
//
//    @Test
//    public void testStrictNonExecutePrefixWithSkipValue() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(")]}'\n []"));
//        try {
//            reader.skipValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 1 (char ')') path $");
//        }
//    }
//
//    @Test
//    public void testLenientNonExecutePrefix() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(")]}'\n []"));
//        reader.setLenient(true);
//        reader.beginArray();
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testLenientNonExecutePrefixWithLeadingWhitespace() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("\r\n \t)]}'\n []"));
//        reader.setLenient(true);
//        reader.beginArray();
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testLenientPartialNonExecutePrefix() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(")]}' []"));
//        reader.setLenient(true);
//        assertThat(reader.nextText()).isEqualTo(")");
//        try {
//            reader.nextText();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Unexpected value at line 1 column 3 (char '}') path $");
//        }
//    }
//
//    @Test
//    public void testBomIgnoredAsFirstCharacterOfDocument() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("\ufeff[]"));
//        reader.beginArray();
//        reader.endArray();
//    }
//
//    @Test
//    public void testBomForbiddenAsOtherCharacterInDocument() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[\ufeff]"));
//        reader.beginArray();
//        try {
//            reader.endArray();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char '\uFEFF') path $[0]");
//        }
//    }
//
//    @SuppressWarnings("UngroupedOverloads")
//    @Test
//    public void testFailWithPosition() throws IOException {
//        testFailWithPosition("Expected value at line 6 column 5 (char '}') path $[1]", "[\n\n\n\n\n\"a\",}]");
//    }
//
//    @Test
//    public void testFailWithPositionGreaterThanBufferSize() throws IOException {
//        String spaces = repeat(' ', 8192);
//        testFailWithPosition(
//                "Expected value at line 6 column 5 (char '}') path $[1]", "[\n\n" + spaces + "\n\n\n\"a\",}]");
//    }
//
//    @Test
//    public void testFailWithPositionOverSlashSlashEndOfLineComment() throws IOException {
//        testFailWithPosition(
//                "Expected value at line 5 column 6 (char '}') path $[1]", "\n// foo\n\n//bar\r\n[\"a\",}");
//    }
//
//    @Test
//    public void testFailWithPositionOverHashEndOfLineComment() throws IOException {
//        testFailWithPosition(
//                "Expected value at line 5 column 6 (char '}') path $[1]", "\n# foo\n\n#bar\r\n[\"a\",}");
//    }
//
//    @Test
//    public void testFailWithPositionOverCStyleComment() throws IOException {
//        testFailWithPosition(
//                "Expected value at line 6 column 12 (char '}') path $[1]", "\n\n/* foo\n*\n*\r\nbar */[\"a\",}");
//    }
//
//    @Test
//    public void testFailWithPositionOverQuotedString() throws IOException {
//        testFailWithPosition(
//                "Expected value at line 5 column 3 (char '}') path $[1]", "[\"foo\nbar\r\nbaz\n\",\n  }");
//    }
//
//    @Test
//    public void testFailWithPositionOverUnquotedString() throws IOException {
//        testFailWithPosition("Expected value at line 5 column 2 (char '}') path $[1]", "[\n\nabcd\n\n,}");
//    }
//
//    @Test
//    public void testFailWithEscapedNewlineCharacter() throws IOException {
//        testFailWithPosition("Expected value at line 5 column 3 (char '}') path $[1]", "[\n\n\"\\\n\n\",}");
//    }
//
//    @Test
//    public void testFailWithPositionIsOffsetByBom() throws IOException {
//        testFailWithPosition("Expected value at line 1 column 6 (char '}') path $[1]", "\ufeff[\"a\",}]");
//    }
//
//    private static void testFailWithPosition(String message, String json) throws IOException {
//        // Validate that it works reading the string normally.
//        BaseXmlReader reader1 = new BaseXmlReader(reader(json));
//        reader1.setLenient(true);
//        reader1.beginArray();
//        String unused1 = reader1.nextText();
//        try {
//            XmlToken unused2 = reader1.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(message);
//        }
//
//        // Also validate that it works when skipping.
//        BaseXmlReader reader2 = new BaseXmlReader(reader(json));
//        reader2.setLenient(true);
//        reader2.beginArray();
//        reader2.skipValue();
//        try {
//            XmlToken unused3 = reader2.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(message);
//        }
//    }
//
//    @Test
//    public void testFailWithPositionDeepPath() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[1,{\"a\":[2,3,}"));
//        reader.beginArray();
//        int unused1 = reader.nextInt();
//        reader.beginObject();
//        String unused2 = reader.nextName();
//        reader.beginArray();
//        int unused3 = reader.nextInt();
//        int unused4 = reader.nextInt();
//        try {
//            XmlToken unused5 = reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected value at line 1 column 14 (char '}') path $[1].a[2]");
//        }
//    }
//
//    @Test
//    public void testStrictVeryLongNumber() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[0." + repeat('9', 8192) + "]")).setLenient(false);
//        reader.beginArray();
//        try {
//            reader.nextDouble();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertStrictError(expected, "line 1 column 2 (char '0') path $[0]");
//        }
//    }
//
//    @Test
//    public void testLenientVeryLongNumber() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[0." + repeat('9', 8192) + "]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.TEXT);
//        assertThat(reader.nextDouble()).isEqualTo(1d);
//        reader.endArray();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testVeryLongUnquotedLiteral() throws IOException {
//        String literal = "a" + repeat('b', 8192) + "c";
//        BaseXmlReader reader = new BaseXmlReader(reader("[" + literal + "]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo(literal);
//        reader.endArray();
//    }
//
//    @Test
//    public void testDeeplyNestedArrays() throws IOException {
//        // this is nested 40 levels deep; Gson is tuned for nesting is 30 levels deep or fewer
//        BaseXmlReader reader =
//                new BaseXmlReader(
//                        reader(
//                                "[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"));
//        for (int i = 0; i < 40; i++) {
//            reader.beginArray();
//        }
//        assertThat(reader.getPath())
//                .isEqualTo(
//                        "$[0][0][0][0][0][0][0][0][0][0][0][0][0][0][0]"
//                                + "[0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0][0]");
//        for (int i = 0; i < 40; i++) {
//            reader.endArray();
//        }
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testDeeplyNestedObjects() throws IOException {
//        // Build a JSON document structured like {"a":{"a":{"a":{"a":true}}}}, but 40 levels deep
//        String array = "{\"a\":%s}";
//        String json = "true";
//        for (int i = 0; i < 40; i++) {
//            json = String.format(array, json);
//        }
//
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        for (int i = 0; i < 40; i++) {
//            reader.beginObject();
//            assertThat(reader.nextName()).isEqualTo("a");
//        }
//        assertThat(reader.getPath())
//                .isEqualTo(
//                        "$.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a"
//                                + ".a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a");
//        assertThat(reader.nextBoolean()).isTrue();
//        for (int i = 0; i < 40; i++) {
//            reader.endObject();
//        }
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    // http://code.google.com/p/google-gson/issues/detail?id=409
//    @Test
//    public void testStringEndingInSlash() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("/"));
//        reader.setLenient(true);
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected value at line 1 column 1 (char '/') path $");
//        }
//    }
//
//    @Test
//    public void testDocumentWithCommentEndingInSlash() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("/* foo *//"));
//        reader.setLenient(true);
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected value at line 1 column 10 (char '/') path $");
//        }
//    }
//
//    @Test
//    public void testStringWithLeadingSlash() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("/x"));
//        reader.setLenient(true);
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected value at line 1 column 1 (char '/') path $");
//        }
//    }
//
//    @Test
//    public void testUnterminatedObject() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":\"android\"x"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextText()).isEqualTo("android");
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Unterminated object at x at line 1 column 16 (char '\\0') path $.a");
//        }
//    }
//
//    @Test
//    public void testVeryLongQuotedString() throws IOException {
//        char[] stringChars = new char[1024 * 16];
//        Arrays.fill(stringChars, 'x');
//        String string = new String(stringChars);
//        String json = "[\"" + string + "\"]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo(string);
//        reader.endArray();
//    }
//
//    @Test
//    public void testVeryLongUnquotedString() throws IOException {
//        char[] stringChars = new char[1024 * 16];
//        Arrays.fill(stringChars, 'x');
//        String string = new String(stringChars);
//        String json = "[" + string + "]";
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo(string);
//        reader.endArray();
//    }
//
//    @Test
//    public void testVeryLongUnterminatedString() throws IOException {
//        char[] stringChars = new char[1024 * 16];
//        Arrays.fill(stringChars, 'x');
//        String string = new String(stringChars);
//        String json = "[" + string;
//        BaseXmlReader reader = new BaseXmlReader(reader(json));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.nextText()).isEqualTo(string);
//        try {
//            reader.peek();
//            fail();
//        } catch (EOFException expected) {
//        }
//    }
//
//    @Test
//    public void testSkipVeryLongUnquotedString() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[" + repeat('x', 8192) + "]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        reader.skipValue();
//        reader.endArray();
//    }
//
//    @Test
//    public void testSkipTopLevelUnquotedString() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(repeat('x', 8192)));
//        reader.setLenient(true);
//        reader.skipValue();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testSkipVeryLongQuotedString() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[\"" + repeat('x', 8192) + "\"]"));
//        reader.beginArray();
//        reader.skipValue();
//        reader.endArray();
//    }
//
//    @Test
//    public void testSkipTopLevelQuotedString() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("\"" + repeat('x', 8192) + "\""));
//        reader.setLenient(true);
//        reader.skipValue();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testStringAsNumberWithTruncatedExponent() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[123e]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//    }
//
//    @Test
//    public void testStringAsNumberWithDigitAndNonDigitExponent() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[123e4b]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//    }
//
//    @Test
//    public void testStringAsNumberWithNonDigitExponent() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("[123eb]"));
//        reader.setLenient(true);
//        reader.beginArray();
//        assertThat(reader.peek()).isEqualTo(TEXT);
//    }
//
//    @Test
//    public void testEmptyStringName() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"\":true}"));
//        reader.setLenient(true);
//        assertThat(reader.peek()).isEqualTo(BEGIN_TAG);
//        reader.beginObject();
//        assertThat(reader.peek()).isEqualTo(NAME);
//        assertThat(reader.nextName()).isEqualTo("");
//        assertThat(reader.peek()).isEqualTo(XmlToken.BOOLEAN);
//        assertThat(reader.nextBoolean()).isTrue();
//        assertThat(reader.peek()).isEqualTo(XmlToken.END_TAG);
//        reader.endObject();
//        assertThat(reader.peek()).isEqualTo(XmlToken.EOF);
//    }
//
//    @Test
//    public void testStrictExtraCommasInMaps() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":\"b\",}"));
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextText()).isEqualTo("b");
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected name at line 1 column 11 (char '\\0') path $.a");
//        }
//    }
//
//    @Test
//    public void testLenientExtraCommasInMaps() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("{\"a\":\"b\",}"));
//        reader.setLenient(true);
//        reader.beginObject();
//        assertThat(reader.nextName()).isEqualTo("a");
//        assertThat(reader.nextText()).isEqualTo("b");
//        try {
//            reader.peek();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Expected name at line 1 column 11 (char '\\0') path $.a");
//        }
//    }
//
//    private static String repeat(char c, int count) {
//        char[] array = new char[count];
//        Arrays.fill(array, c);
//        return new String(array);
//    }
//
//    @Test
//    public void testMalformedDocuments() throws IOException {
//        assertDocument("{]", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("{,", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("{{", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("{[", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("{:", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("{\"name\",", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\",", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\":}", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\"::", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\":,", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\"=}", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{\"name\"=>}", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument(
//                "{\"name\"=>\"string\":", BEGIN_TAG, NAME, TEXT, MalformedDataException.class);
//        assertDocument(
//                "{\"name\"=>\"string\"=", BEGIN_TAG, NAME, TEXT, MalformedDataException.class);
//        assertDocument(
//                "{\"name\"=>\"string\"=>", BEGIN_TAG, NAME, TEXT, MalformedDataException.class);
//        assertDocument("{\"name\"=>\"string\",", BEGIN_TAG, NAME, TEXT, EOFException.class);
//        assertDocument("{\"name\"=>\"string\",\"name\"", BEGIN_TAG, NAME, TEXT, NAME);
//        assertDocument("[}", BEGIN_TAG, MalformedDataException.class);
//        assertDocument("[,]", BEGIN_TAG, END_TAG);
//        assertDocument("{", BEGIN_TAG, EOFException.class);
//        assertDocument("{\"name\"", BEGIN_TAG, NAME, EOFException.class);
//        assertDocument("{\"name\",", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{'name'", BEGIN_TAG, NAME, EOFException.class);
//        assertDocument("{'name',", BEGIN_TAG, NAME, MalformedDataException.class);
//        assertDocument("{name", BEGIN_TAG, NAME, EOFException.class);
//        assertDocument("[", BEGIN_TAG, EOFException.class);
//        assertDocument("[string", BEGIN_TAG, TEXT, EOFException.class);
//        assertDocument("[\"string\"", BEGIN_TAG, TEXT, EOFException.class);
//        assertDocument("['string'", BEGIN_TAG, TEXT, EOFException.class);
//        assertDocument("[123", BEGIN_TAG, NUMBER, EOFException.class);
//        assertDocument("[123,", BEGIN_TAG, NUMBER, EOFException.class);
//        assertDocument("{\"name\":123", BEGIN_TAG, NAME, NUMBER, EOFException.class);
//        assertDocument("{\"name\":123,", BEGIN_TAG, NAME, NUMBER, EOFException.class);
//        assertDocument("{\"name\":\"string\"", BEGIN_TAG, NAME, TEXT, EOFException.class);
//        assertDocument("{\"name\":\"string\",", BEGIN_TAG, NAME, TEXT, EOFException.class);
//        assertDocument("{\"name\":'string'", BEGIN_TAG, NAME, TEXT, EOFException.class);
//        assertDocument("{\"name\":'string',", BEGIN_TAG, NAME, TEXT, EOFException.class);
//        assertDocument("{\"name\":false", BEGIN_TAG, NAME, BOOLEAN, EOFException.class);
//        assertDocument("{\"name\":false,,", BEGIN_TAG, NAME, BOOLEAN, MalformedDataException.class);
//    }
//
//    /**
//     * This test behaves slightly differently in Gson 2.2 and earlier. It fails during peek rather
//     * than during nextText().
//     */
//    @Test
//    public void testUnterminatedStringFailure() throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader("<tag val=\"string"));
//        reader.setLenient(true);
//        reader.beginTag();
//        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_NAME);
//        assertThat(reader.nextAttributeName()).isEqualTo("val");
//        assertThat(reader.peek()).isEqualTo(ATTRIBUTE_VALUE);
//        try {
//            reader.nextAttributeValue();
//            fail();
//        } catch (MalformedDataException expected) {
//            assertThat(expected)
//                    .hasMessageThat()
//                    .isEqualTo(
//                            "Unterminated string at line 1 column 9 (char '[') path $[0]");
//        }
//    }
//
//    private static void assertStrictError(MalformedDataException exception, String expectedLocation) {
//        assertThat(exception)
//                .hasMessageThat()
//                .isEqualTo(
//                        "Use BaseXmlReader.setLenient(true) to accept malformed JSON at "
//                                + expectedLocation);
//    }
//
//    private static void assertUnexpectedStructureError(
//            IllegalStateException exception,
//            String expectedXmlToken,
//            String actualXmlToken,
//            String expectedLocation) {
//        assertThat(exception)
//                .hasMessageThat()
//                .isEqualTo(
//                        "Expected "
//                                + expectedXmlToken
//                                + " but was "
//                                + actualXmlToken
//                                + " at "
//                                + expectedLocation);
//    }
//
//    private static void assertDocument(String document, Object... expectations) throws IOException {
//        BaseXmlReader reader = new BaseXmlReader(reader(document));
//        reader.setLenient(true);
//        for (Object expectation : expectations) {
//            if (expectation == BEGIN_TAG) {
//                assertThat(reader.beginTag()).isEqualTo("tag");
//            } else if (expectation == END_TAG) {
//                assertThat(reader.endTag()).isEqualTo("tag");
//            } else if (expectation == ATTRIBUTE_NAME) {
//                assertThat(reader.nextAttributeName()).isEqualTo("name");
//            } else if (expectation == ATTRIBUTE_VALUE) {
//                assertThat(reader.nextAttributeValue()).isEqualTo("string");
//            } else if (expectation == TEXT) {
//                assertThat(reader.nextText()).isEqualTo("string");
//            } else if (expectation == CDATA) {
//                assertThat(reader.nextCData()).isEqualTo("data");
//            } else if (expectation instanceof Class
//                    && Exception.class.isAssignableFrom((Class<?>) expectation)) {
//                try {
//                    reader.peek();
//                    fail();
//                } catch (Exception expected) {
//                    assertThat(expected.getClass()).isEqualTo(expectation);
//                }
//            } else {
//                throw new AssertionError("Unsupported expectation value: " + expectation);
//            }
//        }
//    }

    /** Returns a reader that returns one character at a time. */
    private static Reader reader(final String s) {
        return new StringReader(s);
    }
}
