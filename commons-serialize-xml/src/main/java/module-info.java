module io.gitlab.jfronny.commons.serialize.xml {
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.serialize;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.serialize.xml;
    exports io.gitlab.jfronny.commons.serialize.xml.wrapper;
}