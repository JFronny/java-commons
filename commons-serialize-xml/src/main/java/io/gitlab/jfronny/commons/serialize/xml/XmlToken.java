package io.gitlab.jfronny.commons.serialize.xml;

public enum XmlToken {
    BEGIN_TAG,
    END_TAG,
    ATTRIBUTE_NAME,
    ATTRIBUTE_VALUE,
    TEXT,
    CDATA,
    EOF
}
