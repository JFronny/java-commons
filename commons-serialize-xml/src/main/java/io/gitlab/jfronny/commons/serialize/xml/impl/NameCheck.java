package io.gitlab.jfronny.commons.serialize.xml.impl;

public enum NameCheck {
    FIRST, BOTH, NONE;

    public static NameCheck isNameStart(char ch, char chNext) {
        if ('A' <= ch && ch <= 'Z') return NameCheck.FIRST;
        if ('a' <= ch && ch <= 'z') return NameCheck.FIRST;
        return switch (ch) {
            case ':', '_' -> NameCheck.FIRST;
            case '\u2070' -> chNext == '\u218F' ? NameCheck.BOTH : NameCheck.NONE;
            case '\u2C00' -> chNext == '\u2FEF' ? NameCheck.BOTH : NameCheck.NONE;
            case '\u3001' -> chNext == '\uD7FF' ? NameCheck.BOTH : NameCheck.NONE;
            case '\uF900' -> chNext == '\uFDCF' ? NameCheck.BOTH : NameCheck.NONE;
            case '\uFDF0' -> chNext == '\uFFFD' ? NameCheck.BOTH : NameCheck.NONE;
            default -> NameCheck.NONE;
        };
    }

    public static NameCheck isName(char ch, char chNext) {
        var nameStart = isNameStart(ch, chNext);
        if (nameStart != NameCheck.NONE) return nameStart;
        if ('0' <= ch && ch <= '9') return NameCheck.FIRST;
        return switch (ch) {
            case '-', '.', '\u00B7' -> NameCheck.FIRST;
            case '\u0300' -> chNext == '\u036F' ? NameCheck.BOTH : NameCheck.NONE;
            case '\u203F' -> chNext == '\u2040' ? NameCheck.BOTH : NameCheck.NONE;
            default -> NameCheck.NONE;
        };
    }
}
