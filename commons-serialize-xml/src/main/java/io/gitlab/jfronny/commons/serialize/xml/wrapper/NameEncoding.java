package io.gitlab.jfronny.commons.serialize.xml.wrapper;

public interface NameEncoding {
    String encode(String name);
    String decode(String name);

    NameEncoding DEFAULT = new NameEncoding() {
        @Override
        public String encode(String name) {
            return name;
        }

        @Override
        public String decode(String name) {
            return name;
        }
    };
}
