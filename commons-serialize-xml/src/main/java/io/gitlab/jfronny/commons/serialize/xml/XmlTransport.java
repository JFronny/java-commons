package io.gitlab.jfronny.commons.serialize.xml;

import io.gitlab.jfronny.commons.serialize.Transport;
import io.gitlab.jfronny.commons.serialize.xml.wrapper.XmlReader;
import io.gitlab.jfronny.commons.serialize.xml.wrapper.XmlWriter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class XmlTransport implements Transport<IOException, XmlReader, XmlWriter> {
    @Override
    public XmlReader createReader(Reader source) throws IOException {
        return new XmlReader(source);
    }

    @Override
    public XmlWriter createWriter(Writer target) throws IOException {
        return new XmlWriter(target);
    }

    @Override
    public String getFormatMime() {
        return "application/xml";
    }
}
