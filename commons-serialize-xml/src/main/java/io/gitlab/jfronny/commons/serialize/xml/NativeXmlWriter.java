package io.gitlab.jfronny.commons.serialize.xml;

import io.gitlab.jfronny.commons.serialize.xml.impl.NameCheck;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import static io.gitlab.jfronny.commons.serialize.xml.impl.XmlScope.*;

public class NativeXmlWriter implements Closeable, Flushable {
    private final Writer out;
    private int[] stack = new int[32];
    private int stackSize = 0;

    {
        push(EMPTY_DOCUMENT);
    }

    private String[] pathNames = new String[32];

    private String newline;
    private String indent;
    private boolean usesEmptyNewlineAndIndent;

    private final List<String> deferredComments = new LinkedList<>();
    private String deferredText = null;
    private boolean wasText = false;

    private boolean lenient = false;
    private boolean conciseTags = true;
    private boolean escapeNonAscii = true;
    public NativeXmlWriter(Writer out) {
        this.out = Objects.requireNonNull(out, "out == null");
        newline = indent = "";
        setIndent("");
        setNewline("");
    }

    public NativeXmlWriter setLenient(boolean lenient) {
        this.lenient = lenient;
        return this;
    }

    public boolean isLenient() {
        return lenient;
    }

    public NativeXmlWriter setEscapeNonAscii(boolean escapeNonAscii) {
        this.escapeNonAscii = escapeNonAscii;
        return this;
    }

    public boolean isEscapeNonAscii() {
        return escapeNonAscii;
    }

    public NativeXmlWriter setIndent(String indent) {
        if (indent == null || indent.isEmpty()) {
            this.indent = "";
            this.usesEmptyNewlineAndIndent = newline.isEmpty();
        } else {
            this.newline = "\n"; // if someone sets an indent, this is probably intended
            this.indent = indent;
            this.usesEmptyNewlineAndIndent = false;
        }
        return this;
    }

    public String getIndent() {
        return indent;
    }

    public NativeXmlWriter setNewline(String newline) {
        if (newline == null || newline.isEmpty()) {
            this.newline = "";
            this.usesEmptyNewlineAndIndent = indent.isEmpty();
        } else {
            this.newline = newline;
            this.usesEmptyNewlineAndIndent = false;
        }
        return this;
    }

    public String getNewline() {
        return newline;
    }

    public NativeXmlWriter configureFormatting(String indent, String newline) {
        setIndent(indent);
        setNewline(newline);
        return this;
    }

    public NativeXmlWriter setConciseTags(boolean conciseTags) {
        this.conciseTags = conciseTags;
        return this;
    }

    public boolean isConciseTags() {
        return conciseTags;
    }

    public NativeXmlWriter beginTag(String name) throws IOException {
        Objects.requireNonNull(name, "name == null");
        wasText = false;
        beforeValue();
        pathNames[stackSize - 1] = name;
        push(TAG_HEAD);
        out.write('<');
        name(name);
        return this;
    }

    public NativeXmlWriter endTag() throws IOException {
        int context = peek();
        if (context == DANGLING_NAME) {
            throw new IllegalStateException("Dangling name.");
        }
        if (context != TAG_HEAD && context != TAG_BODY) {
            throw new IllegalStateException("Nesting problem.");
        }
        wasText = false;
        boolean simple = false; // true if the last tag only contained simple text

        if (!deferredComments.isEmpty()) {
            if (context == TAG_HEAD && deferredComments.size() == 1) {
                out.write('>');
                if (deferredText == null) simple = true;
                else newline();
                writeDeferredComment();
            } else {
                if (context == TAG_HEAD) out.write('>');
                newline();
                writeDeferredComment();
            }
            context = TAG_BODY;
        }

        if (deferredText != null) {
            if (context == TAG_HEAD) {
                out.write('>');
                escapeText(deferredText, true);
                context = TAG_BODY;
                simple = true;
            } else text(deferredText);
            deferredText = null;
        }

        if (context == TAG_HEAD && !conciseTags) {
            out.write('>');
            context = TAG_BODY;
            simple = true;
        }

        stackSize--;
        if (context == TAG_BODY) {
            if (!simple) newline();
            out.write("</");
            name(pathNames[stackSize - 1]);
            out.write('>');
        } else {
            out.write("/>");
        }
        pathNames[stackSize - 1] = null;
        return this;
    }

    private void push(int newTop) {
        if (stackSize == stack.length) {
            int newLength = stackSize * 2;
            stack = Arrays.copyOf(stack, newLength);
            pathNames = Arrays.copyOf(pathNames, newLength);
        }
        stack[stackSize++] = newTop;
    }

    /** Returns the value on the top of the stack. */
    private int peek() {
        if (stackSize == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        }
        return stack[stackSize - 1];
    }

    /** Replace the value on the top of the stack with the given value. */
    private void replaceTop(int topOfStack) {
        stack[stackSize - 1] = topOfStack;
    }

    public NativeXmlWriter comment(String comment) throws IOException {
        if (comment == null || comment.isBlank()) return this;
        wasText = false;
        comment = comment.replace("-->", "--&gt;");
        String[] parts = comment.split("\n");
        Collections.addAll(deferredComments, parts);
        if (peek() == NONEMPTY_DOCUMENT) {
            newline();
            writeDeferredComment();
        }
        return this;
    }

    private void writeDeferredComment() throws IOException {
        if (!deferredComments.isEmpty()) {
            if (newline.isEmpty()) {
                out.append("<!-- ")
                        .append(String.join(" / ", deferredComments.stream()
                                .filter(s -> s != null && !s.isBlank()).toList())
                        ).append(" -->");
            } else {
                boolean first = true;
                for (String s : deferredComments) {
                    if (!first) newline();
                    first = false;
                    if (s == null || s.isBlank()) out.append("<!---->");
                    else out.append("<!-- ").append(s).append(" -->");
                }
            }
            deferredComments.clear();
        }
    }

    public NativeXmlWriter attribute(String name, String value) throws IOException {
        return attributeName(name).attributeValue(value);
    }

    public NativeXmlWriter attributeName(String name) throws IOException {
        Objects.requireNonNull(name, "name == null");
        if (peek() != TAG_HEAD) {
            throw new IllegalStateException("Nesting problem.");
        }
        wasText = false;
        replaceTop(DANGLING_NAME);
        out.write(' ');
        name(name);
        return this;
    }

    public NativeXmlWriter attributeValue(String value) throws IOException {
        value = value == null ? "null" : value;
        if (peek() != DANGLING_NAME) {
            throw new IllegalStateException("Nesting problem.");
        }
        wasText = false;
        replaceTop(TAG_HEAD);
        out.write('=');
        out.write('"');
        escapeText(value, lenient);
        out.write('"');
        return this;
    }

    public NativeXmlWriter text(String text) throws IOException {
        text = text == null ? "null" : text;
        if (peek() == TAG_HEAD && !text.contains("\n") && deferredText == null && deferredComments.isEmpty()) {
            deferredText = text;
            wasText = true;
            return this;
        }
        if (wasText && deferredComments.isEmpty()) deferredComments.add("");
        beforeValue();
        escapeText(text, true);
        wasText = true;
        return this;
    }

    public NativeXmlWriter reference(String reference) throws IOException {
        reference = reference == null ? "" : reference;
        if (!reference.matches("[a-zA-Z_:][a-zA-Z0-9._:-]*")) {
            throw new IllegalArgumentException("Invalid reference: " + reference);
        }
        wasText = false;
        beforeValue();
        out.write("&");
        out.write(reference);
        out.write(";");
        return this;
    }

    public NativeXmlWriter cdata(String cdata) throws IOException {
        cdata = cdata == null ? "" : cdata;
        if (cdata.contains("]]>")) throw new IllegalArgumentException("CDATA cannot contain ']]>'");
        wasText = false;
        beforeValue();
        out.write("<![CDATA[");
        out.write(cdata);
        out.write("]]>");
        return this;
    }

    @Override
    public void flush() throws IOException {
        if (stackSize == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        }
        out.flush();
    }

    @Override
    public void close() throws IOException {
        out.close();

        int size = stackSize;
        if (size > 1 || (size == 1 && stack[size - 1] != NONEMPTY_DOCUMENT)) {
            throw new IOException("Incomplete document");
        }
        stackSize = 0;
    }

    private void name(String name) throws IOException {
        if (name == null) throw new NullPointerException("name == null");
        if (stackSize == 0) throw new IllegalStateException("JsonWriter is closed.");
        // Check name for illegal characters
        int last = 0;
        int length = name.length();
        for (int i = 0; i < length; i++) {
            char c = name.charAt(i);
            char n = i + 1 < length ? name.charAt(i + 1) : '\0';
            NameCheck check = i == 0 ? NameCheck.isNameStart(c, n) : NameCheck.isName(c, n);
            switch (check) {
                case NONE -> {
                    if (!lenient) {
                        throw new IllegalArgumentException("Illegal character in name: " + name);
                    }
                    if (last < i) out.write(name, last, i - last);
                    out.write('_');
                    last = i + 1;
                }
                case BOTH -> i++;
                case FIRST -> {}
            }
        }
        if (last < length) out.write(name, last, length - last);
    }

    private void escapeText(String text, boolean permitControl) throws IOException {
        int last = 0;
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if (!permitControl && c < 0x20 && c != 0x09) throw new IllegalArgumentException("Illegal control character in text: " + text);
            String replacement = switch(c) {
                case '&' -> "&amp;";
                case '<' -> "&lt;";
                case '>' -> "&gt;";
                case '"' -> "&quot;";
                case '\'' -> "&apos;";
                default -> escapeNonAscii && c > 127 ? "&#" + (int) c + ";" : null;
            };
            if (replacement == null) continue;
            if (last < i) out.write(text, last, i - last);
            out.write(replacement);
            last = i + 1;
        }
        if (last < length) out.write(text, last, length - last);
    }

    private void newline() throws IOException {
        if (usesEmptyNewlineAndIndent) {
            return;
        }

        out.write(newline);
        for (int i = 1, size = stackSize; i < size; i++) {
            out.write(indent);
        }
    }

    private void beforeValue() throws IOException {
        switch (peek()) {
            case NONEMPTY_DOCUMENT:
                if (!lenient) {
                    throw new IllegalStateException("XML must have only one top-level value.");
                }
            case EMPTY_DOCUMENT:
                replaceTop(NONEMPTY_DOCUMENT);
                if (!deferredComments.isEmpty()) {
                    writeDeferredComment();
                    newline();
                }
                if (deferredText != null) {
                    escapeText(deferredText, true);
                    newline();
                    deferredText = null;
                }
                break;
            case DANGLING_NAME:
                if (!lenient) {
                    throw new IllegalStateException("Attribute name must be followed by a value");
                }
                attributeValue("null");
            case TAG_HEAD:
                out.write('>');
                replaceTop(TAG_BODY);
            case TAG_BODY:
                newline();
                if (deferredText != null) {
                    escapeText(deferredText, true);
                    newline();
                    deferredText = null;
                }
                if (!deferredComments.isEmpty()) {
                    writeDeferredComment();
                    newline();
                }
                break;
            default:
                throw new IllegalStateException("Nesting problem.");
        }
    }
}
