package io.gitlab.jfronny.commons.serialize.xml.impl;

public class WrapperScope {
    public static final int OBJECT_VALUE_WRAPPER = 1;
    public static final int OBJECT_VALUE_WRAPPER_USED = 2;
    public static final int ARRAY = 3;
    public static final int OBJECT = 4;
    public static final int DOCUMENT = 5;
}
