package io.gitlab.jfronny.commons.serialize.xml.impl;

public class XmlScope {
    public static final int TAG_HEAD = 1;
    public static final int TAG_BODY = 2;
    public static final int DANGLING_NAME = 3;
    public static final int EMPTY_DOCUMENT = 4;
    public static final int NONEMPTY_DOCUMENT = 5;
    public static final int CLOSED = 7;
}
