module io.gitlab.jfronny.commons.muscript.data {
    uses io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
    uses io.gitlab.jfronny.muscript.data.dynamic.context.ISimpleDynamic;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.muscript.core;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.muscript.data.dynamic;
    exports io.gitlab.jfronny.muscript.data.dynamic.context;
    exports io.gitlab.jfronny.muscript.data.dynamic.lens;
    exports io.gitlab.jfronny.muscript.data.dynamic.type;
}