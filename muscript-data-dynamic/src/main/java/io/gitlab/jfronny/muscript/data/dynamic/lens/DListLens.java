package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public final class DListLens extends DLens implements DList {
    private final Supplier<List<? extends Dynamic>> value;

    public DListLens(Dynamic source, Supplier<List<? extends Dynamic>> value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public List<? extends Dynamic> getValue() {
        return value.get();
    }
}
