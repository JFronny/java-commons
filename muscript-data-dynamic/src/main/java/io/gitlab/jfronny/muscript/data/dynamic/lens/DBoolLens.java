package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DBool;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.Objects;
import java.util.function.BooleanSupplier;

public final class DBoolLens extends DLens implements DBool {
    private final BooleanSupplier value;

    public DBoolLens(Dynamic source, BooleanSupplier value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public Boolean getValue() {
        return value.getAsBoolean();
    }
}
