package io.gitlab.jfronny.muscript.data.dynamic.type;

public enum DTypePrimitive implements DType {
    BOOL, NUMBER, STRING, NULL;

    @Override
    public String toString() {
        return DType.toString(this);
    }
}
