package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DCallable;
import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

public final class DCallableLens extends DLens implements DCallable {
    private final Supplier<Function<DList, Dynamic>> value;

    public DCallableLens(Dynamic source, Supplier<Function<DList, Dynamic>> value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public Function<DList, Dynamic> getValue() {
        return value.get();
    }
}
