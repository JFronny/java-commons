package io.gitlab.jfronny.muscript.data.dynamic;

public non-sealed interface DString extends Dynamic {
    @Override
    String getValue();
}
