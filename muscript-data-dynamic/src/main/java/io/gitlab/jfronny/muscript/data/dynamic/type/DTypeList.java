package io.gitlab.jfronny.muscript.data.dynamic.type;

import org.jetbrains.annotations.Nullable;

public record DTypeList(@Nullable DType entryType) implements DType {
    @Override
    public String toString() {
        return DType.toString(this);
    }
}
