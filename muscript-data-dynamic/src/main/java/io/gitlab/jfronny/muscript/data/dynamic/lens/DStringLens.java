package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.Objects;
import java.util.function.Supplier;

public final class DStringLens extends DLens implements DString {
    private final Supplier<String> value;

    public DStringLens(Dynamic source, Supplier<String> value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public String getValue() {
        return value.get();
    }
}
