package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DNumber;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.Objects;
import java.util.function.DoubleSupplier;

public final class DNumberLens extends DLens implements DNumber {
    private final DoubleSupplier value;

    public DNumberLens(Dynamic source, DoubleSupplier value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public Double getValue() {
        return value.getAsDouble();
    }
}
