package io.gitlab.jfronny.muscript.data.dynamic;

import io.gitlab.jfronny.muscript.data.dynamic.context.ISimpleDynamic;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.stream.Collectors;

public non-sealed interface DObject extends Dynamic {
    default @Nullable Dynamic get(String key) {
        return getValue().get(key);
    }

    default boolean has(String key) {
        return getValue().containsKey(key);
    }

    @Override
    Map<String, ? extends Dynamic> getValue();

    @Override
    default DString asString() {
        return ISimpleDynamic.INSTANCE.of(getValue()
                .entrySet()
                .stream()
                .map(e -> e.getKey() + " = " + e.getValue().asString().getValue())
                .collect(Collectors.joining(", ", "{", "}")));
    }
}
