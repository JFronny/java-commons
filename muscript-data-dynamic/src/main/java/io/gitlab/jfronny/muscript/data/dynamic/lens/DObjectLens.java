package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

public final class DObjectLens extends DLens implements DObject {
    private final Supplier<Map<String, ? extends Dynamic>> value;

    public DObjectLens(Dynamic source, Supplier<Map<String, ? extends Dynamic>> value) {
        super(source);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public Map<String, ? extends Dynamic> getValue() {
        return value.get();
    }
}
