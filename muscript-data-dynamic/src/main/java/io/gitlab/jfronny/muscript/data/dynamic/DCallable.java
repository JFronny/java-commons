package io.gitlab.jfronny.muscript.data.dynamic;

import io.gitlab.jfronny.muscript.data.dynamic.context.ISimpleDynamic;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeCallable;
import io.gitlab.jfronny.muscript.data.dynamic.type.TypeMatcher;

import java.util.List;
import java.util.function.Function;

public non-sealed interface DCallable extends Dynamic {
    String DEFAULT_NAME = "<unnamed>";

    default Dynamic call(DList args) {
        DType signature = getSignature();
        if (signature != null) {
            List<DType> argTypes = args.getValue().stream().map(Dynamic::getSignature).toList();
            if (!TypeMatcher.match(signature, argTypes)) {
                List<DTypeCallable.Arg> extraArgs = argTypes.stream().map(s -> new DTypeCallable.Arg(null, s, false)).toList();
                DTypeCallable callSignature = new DTypeCallable(extraArgs, null);
                throw new SignatureMismatchException(getName(), signature, callSignature);
            }
        }
        return getValue().apply(args);
    }

    default Dynamic call(Dynamic... args) {
        return call(ISimpleDynamic.INSTANCE.of(args));
    }

    default String getName() {
        return DEFAULT_NAME;
    }

    default DCallable named(String name) {
        return new NamedDCallable(this, name);
    }

    @Override
    Function<DList, Dynamic> getValue();
}
