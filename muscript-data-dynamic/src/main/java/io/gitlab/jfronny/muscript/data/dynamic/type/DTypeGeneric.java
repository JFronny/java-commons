package io.gitlab.jfronny.muscript.data.dynamic.type;

public record DTypeGeneric(int index) implements DType {
    @Override
    public String toString() {
        return DType.toString(this);
    }
}
