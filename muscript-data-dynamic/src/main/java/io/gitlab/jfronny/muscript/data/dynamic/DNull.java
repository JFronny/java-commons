package io.gitlab.jfronny.muscript.data.dynamic;

import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypePrimitive;

public final class DNull implements Dynamic {
    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public DType getSignature() {
        return DTypePrimitive.NULL;
    }

    @Override
    public String toString() {
        return "null";
    }

    @Override
    public boolean isDirect() {
        return true;
    }
}
