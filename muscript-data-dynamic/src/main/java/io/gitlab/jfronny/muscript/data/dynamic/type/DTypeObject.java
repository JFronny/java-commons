package io.gitlab.jfronny.muscript.data.dynamic.type;

import org.jetbrains.annotations.Nullable;

//TODO redesign for complex objects
public record DTypeObject(@Nullable DType entryType) implements DType {
    @Override
    public String toString() {
        return DType.toString(this);
    }
}
