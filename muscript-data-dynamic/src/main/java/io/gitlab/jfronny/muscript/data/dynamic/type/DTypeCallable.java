package io.gitlab.jfronny.muscript.data.dynamic.type;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public record DTypeCallable(@Nullable List<Arg> from, @Nullable DType to) implements DType {
    public DTypeCallable {
        if (from != null) {
            for (Arg arg : from) {
                if (arg == null) throw new IllegalArgumentException("Contents of 'from' cannot be null");
            }
        }
    }

    @Override
    public String toString() {
        return DType.toString(this);
    }

    public record Arg(@Nullable String name, @Nullable DType type, boolean variadic) {
        @Override
        public String toString() {
            return (name == null ? DType.toString(type) : name + ": " + DType.toString(type)) + (variadic ? "..." : "");
        }
    }
}
