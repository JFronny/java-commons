package io.gitlab.jfronny.muscript.data.dynamic;

public non-sealed interface DBool extends Dynamic {
    @Override
    Boolean getValue();
}
