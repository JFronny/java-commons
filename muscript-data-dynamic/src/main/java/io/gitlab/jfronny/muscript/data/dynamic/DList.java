package io.gitlab.jfronny.muscript.data.dynamic;

import java.util.List;

public non-sealed interface DList extends Dynamic {
    default Dynamic get(int i) {
        return getValue().get(i);
    }

    default int size() {
        return getValue().size();
    }

    default boolean isEmpty() {
        return getValue().isEmpty();
    }

    @Override
    List<? extends Dynamic> getValue();
}
