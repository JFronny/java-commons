package io.gitlab.jfronny.muscript.data.dynamic;

/**
 * Use this for ABSTRACT (!) implementations of custom dynamic containers.
 * Any concrete implementation MUST still implement a valid Dynamic subtype (DBool, DString, ...)
 */
public non-sealed interface DynamicBase extends Dynamic {
}
