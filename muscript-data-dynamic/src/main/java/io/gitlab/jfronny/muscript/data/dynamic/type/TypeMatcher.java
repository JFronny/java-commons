package io.gitlab.jfronny.muscript.data.dynamic.type;

import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeMatcher {
    public static boolean match(DType fn, List<DType> args) {
        if (fn instanceof DTypeAnd ds) {
            for (DType element : ds.elements()) {
                if (match(element, args)) return true;
            }
            return false;
        } else if (fn instanceof DTypeCallable ds) {
            return match(ds, args);
        } else return false; // Callable may have multiple other representations
    }

    public static boolean match(DTypeCallable fn, List<DType> args) {
        return fn.from() == null || match(fn.from(), args);
    }

    public static boolean match(List<DTypeCallable.Arg> declared, List<DType> provided) {
        return match(declared, provided, new MatchScope());
    }

    private static boolean match(List<DTypeCallable.Arg> declared, List<DType> provided, MatchScope scope) {
        if (declared.isEmpty()) return provided.isEmpty();
        if (provided.isEmpty()) return declared.stream().allMatch(DTypeCallable.Arg::variadic);
        if (!declared.get(0).variadic()) {
            DType ct = declared.get(0).type();
            DType pt = provided.get(0);
            declared = declared.subList(1, declared.size());
            provided = provided.subList(1, provided.size());
            if (ct == null) return match(declared, provided, scope);
            Result r = match(ct, pt, scope);
            if (r == Result.MATCH) return match(declared, provided, scope);
            if (r == Result.FAILED) return false;
            for (Map<Integer, DType> s2 : ((Result.PossibleMatch) r).possibleHydrations()) {
                if (match(declared, provided, scope.fork(s2))) return true;
            }
            return false;
        }
        DType cArg = declared.get(0).type();
        List<DTypeCallable.Arg> nextDeclared = declared.subList(1, declared.size());
        while (!provided.isEmpty()) {
            if (match(nextDeclared, provided, scope)) return true;
            DType dt = provided.get(0);
            provided = provided.subList(1, provided.size());
            if (cArg == null) continue;
            Result r = match(cArg, dt, scope);
            if (r == Result.FAILED) {
                match(cArg, dt, scope);
                return false;
            }
            if (r == Result.MATCH) continue;
            for (Map<Integer, DType> s2 : ((Result.PossibleMatch) r).possibleHydrations()) {
                if (match(declared, provided, scope.fork(s2))) return true;
            }
            return false;
        }
        return match(nextDeclared, provided, scope);
    }

    private static Result match(DType declared, @Nullable DType provided, MatchScope scope) {
        if (declared instanceof DTypeGeneric dg) {
            if (scope.hydrations.containsKey(dg.index())) {
                Result r = match(scope.hydrations.get(dg.index()), provided, scope);
                if (r == Result.FAILED) {
                    if (provided != null)
                        return new Result.PossibleMatch(Set.of(Map.of(dg.index(), scope.hydrations.get(dg.index()).or(provided))));
                    return Result.MATCH;
                }
                return r;
            }
            // Ideally, we would actually store our null value, but Map doesn't support that and this works for now
            return new Result.PossibleMatch(provided == null ? Set.of(Map.of()) : Set.of(Map.of(dg.index(), provided)));
        } else if (provided instanceof DTypeSum dc) {
            Result currentResult = Result.MATCH;
            for (DType element : dc.elements()) {
                currentResult = currentResult.and(match(declared, element, scope), scope);
            }
            return currentResult;
        } else if (provided instanceof DTypeAnd dc && !(declared instanceof DTypeAnd)) {
            Result currentResult = Result.FAILED;
            for (DType element : dc.elements()) {
                currentResult = currentResult.or(match(declared, element, scope), scope);
            }
            return currentResult;
        } else if (declared instanceof DTypeCallable dg) {
            if (!(provided instanceof DTypeCallable dc)) return Result.FAILED;
            Result currentResult = Result.MATCH;
            if (dg.from() != null) {
                if (dc.from() == null) return Result.MATCH;
                List<DTypeCallable.Arg> dgf = dg.from();
                List<DTypeCallable.Arg> dcf = dc.from();
                // This would theoretically need more logic to properly test whether the functions match
                // but for now this bypass will do
//                if (dgf.size() != dcf.size()) return Result.FAILED;
//                for (int i = 0; i < dgf.size(); i++) {
//                    if (dgf.get(i).variadic() != dcf.get(i).variadic()) return Result.FAILED;
//                    currentResult = currentResult.and(match(dgf.get(i).type(), dcf.get(i).type(), scope), scope);
//                    if (currentResult == Result.FAILED) return Result.FAILED;
//                }
            }
            if (dg.to() == null) return currentResult;
            if (dc.to() == null) return currentResult; // Temporary workaround: muScript closures always return unidentifiable types for now
            return currentResult.and(match(dg.to(), dc.to(), scope), scope);
        } else if (declared instanceof DTypeList dg) {
            if (!(provided instanceof DTypeList dc)) return Result.FAILED;
            if (dg.entryType() == null) return Result.MATCH;
            return match(dg.entryType(), dc.entryType(), scope);
        } else if (declared instanceof DTypeObject dg) {
            if (!(provided instanceof DTypeObject dc)) return Result.FAILED;
            if (dg.entryType() == null) return Result.MATCH;
            return match(dg.entryType(), dc.entryType(), scope);
        } else if (declared instanceof DTypePrimitive dg) {
            if (!(provided instanceof DTypePrimitive dc)) return Result.FAILED;
            return dg == dc ? Result.MATCH : Result.FAILED;
        } else if (declared instanceof DTypeSum dg) {
            Result currentResult = Result.FAILED;
            for (DType element : dg.elements()) {
                currentResult = currentResult.or(match(element, provided, scope), scope);
            }
            return currentResult;
        } else if (declared instanceof DTypeAnd dg) {
            Result currentResult = Result.MATCH;
            for (DType element : dg.elements()) {
                currentResult = currentResult.and(match(element, provided, scope), scope);
            }
            return currentResult;
        } else throw new IllegalArgumentException("Unexpected DType implementation: " + declared.getClass());
    }

    private record MatchScope(Map<Integer, DType> hydrations) {
        public MatchScope() {
            this(Map.of());
        }

        public MatchScope fork(Map<Integer, DType> additional) {
            Map<Integer, DType> fork = new LinkedHashMap<>(hydrations);
            fork.putAll(additional);
            return new MatchScope(Map.copyOf(fork));
        }
    }

    private sealed interface Result {
        default Result and(Result other, MatchScope scope) {
            if (other instanceof PossibleMatch pm) return pm.and(this, scope);
            if (other == FAILED) return FAILED;
            if (other == MATCH) return this;
            throw new IllegalArgumentException("Unexpected Result implementation");
        }

        default Result or(Result other, MatchScope scope) {
            if (other instanceof PossibleMatch pm) return pm.and(this, scope);
            if (other == FAILED) return this;
            if (other == MATCH) return MATCH;
            throw new IllegalArgumentException("Unexpected Result implementation");
        }

        Const FAILED = Const.FAILED;
        Const MATCH = Const.MATCH;
        enum Const implements Result { FAILED, MATCH }
        record PossibleMatch(Set<Map<Integer, DType>> possibleHydrations) implements Result {
            @Override
            public Result and(Result result, MatchScope scope) {
                if (result == FAILED) return FAILED;
                if (result == MATCH) return this;
                Set<Map<Integer, DType>> neu = ((PossibleMatch) result).possibleHydrations.stream().flatMap(left -> possibleHydrations.stream().filter(right ->
                        Stream.concat(left.keySet().stream(), right.keySet().stream())
                                .filter(left::containsKey)
                                .filter(right::containsKey)
                                .allMatch(s -> {
                                    Result r = match(left.get(s), right.get(s), scope);
                                    if (r == FAILED) return false;
                                    if (r == MATCH) return true;
                                    throw new IllegalArgumentException("Unexpected post-match Result implementation");
                                })
                )).collect(Collectors.toCollection(LinkedHashSet::new));
                if (neu.isEmpty()) return FAILED;
                return new PossibleMatch(neu);
            }

            @Override
            public Result or(Result other, MatchScope scope) {
                if (other == FAILED) return this;
                Stream<Map<Integer, DType>> oth = other == MATCH ? Stream.of(Map.of()) : ((PossibleMatch) other).possibleHydrations.stream();
                return new PossibleMatch(Stream.concat(possibleHydrations.stream(), oth).collect(Collectors.toCollection(LinkedHashSet::new)));
            }
        }
    }
}
