package io.gitlab.jfronny.muscript.data.dynamic.context;

import io.gitlab.jfronny.muscript.data.dynamic.DList;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.util.List;
import java.util.ServiceLoader;

public interface ISimpleDynamic {
    ISimpleDynamic INSTANCE = ServiceLoader.load(ISimpleDynamic.class)
            .findFirst()
            .orElseGet(() -> new ISimpleDynamic() {
                @Override
                public DString of(String value) {
                    return new SimpleDString(value);
                }

                @Override
                public DList of(Dynamic... values) {
                    return new SimpleDList(List.of(values));
                }
            });

    DString of(String value);
    DList of(Dynamic... values);

    record SimpleDString(String getValue) implements DString {}
    record SimpleDList(List<Dynamic> getValue) implements DList {}
}
