package io.gitlab.jfronny.muscript.data.dynamic;

import io.gitlab.jfronny.muscript.data.dynamic.type.DType;

import java.util.function.Function;

public record NamedDCallable(DCallable inner, String name) implements DCallable {
    @Override
    public String getName() {
        return name;
    }

    @Override
    public DCallable named(String name) {
        return new NamedDCallable(inner, name);
    }

    @Override
    public Function<DList, Dynamic> getValue() {
        return inner.getValue();
    }

    @Override
    public DType getSignature() {
        return inner.getSignature();
    }
}
