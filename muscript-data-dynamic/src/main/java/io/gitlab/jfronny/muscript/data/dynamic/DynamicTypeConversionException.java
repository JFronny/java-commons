package io.gitlab.jfronny.muscript.data.dynamic;

public class DynamicTypeConversionException extends RuntimeException {
    private static final String MESSAGE = "Could not convert dynamic of type %s to %s";
    private final String target;
    private final String actual;

    public DynamicTypeConversionException(String target, Dynamic dynamic) {
        super(MESSAGE.formatted(dynamic == null ? "null" : dynamic.getClass().getSimpleName(), target));
        this.target = target;
        this.actual = dynamic == null ? "null" : dynamic.getClass().getSimpleName();
    }
}
