package io.gitlab.jfronny.muscript.data.dynamic;

public non-sealed interface DNumber extends Dynamic {
    @Override
    Double getValue();
}
