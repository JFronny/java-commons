package io.gitlab.jfronny.muscript.data.dynamic;

import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeCallable;

public class SignatureMismatchException extends IllegalArgumentException {
    public SignatureMismatchException(String method, DType expected, DTypeCallable actual) {
        super("Signature mismatch for " + method + ": expected <" + expected + "> but got <" + actual + ">");
    }
}
