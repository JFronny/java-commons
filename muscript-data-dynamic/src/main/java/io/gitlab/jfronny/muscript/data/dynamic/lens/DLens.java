package io.gitlab.jfronny.muscript.data.dynamic.lens;

import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.data.dynamic.type.DType;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public abstract sealed class DLens implements DynamicBase permits DBoolLens, DCallableLens, DListLens, DNumberLens, DObjectLens, DStringLens {
    protected final Dynamic source;

    protected DLens(Dynamic source) {
        this.source = Objects.requireNonNull(source);
    }

    public Dynamic getSource() {
        return source;
    }

    @Override
    public boolean isBool() {
        return source.isBool();
    }

    @Override
    public DBool asBool() {
        return source.asBool();
    }

    @Override
    public boolean isNumber() {
        return source.isNumber();
    }

    @Override
    public DNumber asNumber() {
        return source.asNumber();
    }

    @Override
    public boolean isString() {
        return source.isString();
    }

    @Override
    public DString asString() {
        return source.asString();
    }

    @Override
    public boolean isObject() {
        return source.isObject();
    }

    @Override
    public DObject asObject() {
        return source.asObject();
    }

    @Override
    public boolean isList() {
        return source.isList();
    }

    @Override
    public DList asList() {
        return source.asList();
    }

    @Override
    public boolean isCallable() {
        return source.isCallable();
    }

    @Override
    public DCallable asCallable() {
        return source.asCallable();
    }

    @Override
    public @Nullable DType getSignature() {
        return source.getSignature();
    }

    @Override
    public int hashCode() {
        return source.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DLens lens ? source.equals(lens.source) : source.equals(obj);
    }

    @Override
    public String toString() {
        return source.toString();
    }
}
