package io.gitlab.jfronny.muscript.data.dynamic.type;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public class DSL {
    public static final DTypePrimitive BOOL = DTypePrimitive.BOOL;
    public static final DTypePrimitive STRING = DTypePrimitive.STRING;
    public static final DTypePrimitive NUMBER = DTypePrimitive.NUMBER;
    public static final DTypePrimitive NULL = DTypePrimitive.NULL;

    public static DTypeGeneric generic(int index) {
        return new DTypeGeneric(index);
    }

    public static DTypeList list(@Nullable DType entryType) {
        return new DTypeList(entryType);
    }

    public static DTypeObject object(@Nullable DType entryType) {
        return new DTypeObject(entryType);
    }

    public static DTypeCallable callable(@Nullable List<DTypeCallable.Arg> from, @Nullable DType to) {
        return new DTypeCallable(from, to);
    }

    public static DTypeCallable callable(@Nullable DType to, DTypeCallable.Arg... from) {
        return new DTypeCallable(List.of(from), to);
    }

    public static DTypeCallable.Arg arg(@Nullable String name, @Nullable DType type) {
        return new DTypeCallable.Arg(name, type, false);
    }

    public static DTypeCallable.Arg arg(@Nullable String name, @Nullable DType type, boolean variadic) {
        return new DTypeCallable.Arg(name, type, variadic);
    }
}
