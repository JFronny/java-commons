package io.gitlab.jfronny.muscript.data.dynamic.context;

import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.muscript.core.ExprWriter;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;

import java.io.IOException;
import java.util.ServiceLoader;

public interface DynamicSerializer {
    DynamicSerializer INSTANCE = ServiceLoader.load(DynamicSerializer.class)
            .findFirst()
            .orElseGet(() -> (writer, value) -> writer.append(StringFormatter.toString(value.getValue())));

    default String serialize(Dynamic value) {
        return ExprWriter.write(writer -> serialize(writer, value), false);
    }
    void serialize(ExprWriter writer, Dynamic value) throws IOException;
}
