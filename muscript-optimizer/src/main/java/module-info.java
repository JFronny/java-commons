module io.gitlab.jfronny.commons.muscript.optimizer {
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.ast;
    requires io.gitlab.jfronny.commons.muscript.core;
    requires io.gitlab.jfronny.commons.muscript.data;
    requires io.gitlab.jfronny.commons.muscript.data.additional;
    exports io.gitlab.jfronny.muscript.optimizer;
}