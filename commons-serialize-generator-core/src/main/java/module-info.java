module io.gitlab.jfronny.commons.serialize.generator.core {
    requires java.compiler;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.commons.serialize.generator.core;
    exports io.gitlab.jfronny.commons.serialize.generator.core.value;
}