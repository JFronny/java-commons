package io.gitlab.jfronny.commons.serialize.generator.core;

import io.gitlab.jfronny.commons.serialize.generator.core.value.ValueCreator;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractProcessor2 extends AbstractProcessor {
    protected Map<String, String> options;
    protected Messager message;
    protected Filer filer;
    protected ValueCreator valueCreator;
    protected Elements elements;
    protected Types types;
    protected SourceVersion sourceVersion;
    protected Locale locale;
    protected boolean isPreviewEnabled;
    protected boolean hasManifold = false;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        try {
            Class.forName("manifold.ext.Model");
            System.out.println("Detected manifold!");
            hasManifold = true;
        } catch (ClassNotFoundException e) {
            hasManifold = false;
        }
        options = processingEnv.getOptions();
        message = processingEnv.getMessager();
        filer = processingEnv.getFiler();
        elements = processingEnv.getElementUtils();
        types = processingEnv.getTypeUtils();
        sourceVersion = processingEnv.getSourceVersion();
        locale = processingEnv.getLocale();
        isPreviewEnabled = processingEnv.isPreviewEnabled();
        valueCreator = new ValueCreator(processingEnv);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Optional.ofNullable(this.getClass().getAnnotation(SupportedAnnotationTypes.class))
                .map(SupportedAnnotationTypes::value)
                .map(Set::of)
                .or(() -> Optional.ofNullable(this.getClass().getAnnotation(SupportedAnnotationTypes2.class))
                        .map(SupportedAnnotationTypes2::value)
                        .map(Arrays::stream)
                        .map(s -> s.map(Class::getCanonicalName).collect(Collectors.toSet()))
                ).orElse(Set.of());
    }
}
