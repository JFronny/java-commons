package io.gitlab.jfronny.commons.serialize.generator.core.value;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.Objects;
import java.util.stream.Collectors;

public class Value {
    private final ProcessingEnvironment env;
    private final ConstructionSource constructionSource;
    private final TypeElement element;
    private Properties properties;

    public Value(ProcessingEnvironment env, ConstructionSource constructionSource) {
        this.env = env;
        this.constructionSource = constructionSource;
        this.element = constructionSource.getTargetClass();
    }

    public Properties getProperties() throws ElementException {
        return properties != null ? properties : (properties = Properties.build(env.getTypeUtils(), constructionSource, constructionSource.isStatic()));
    }

    public ConstructionSource getConstructionSource() {
        return constructionSource;
    }

    public TypeElement getElement() {
        return element;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Value value)) return false;
        return element.equals(value.element);
    }

    @Override
    public int hashCode() {
        return element.hashCode();
    }

    @Override
    public String toString() {
        try {
            return element.toString() + '{' + getProperties().stream().map(Objects::toString).collect(Collectors.joining(", "));
        } catch (ElementException e) {
            return element.toString();
        }
    }
}
