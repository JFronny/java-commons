package io.gitlab.jfronny.commons.serialize.generator.core;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SupportedAnnotationTypes2 {
    Class<?>[] value();
}
