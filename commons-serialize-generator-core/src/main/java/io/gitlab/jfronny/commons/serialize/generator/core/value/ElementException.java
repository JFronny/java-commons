package io.gitlab.jfronny.commons.serialize.generator.core.value;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import java.util.List;
import java.util.stream.Collectors;

public class ElementException extends Exception {
    private final List<Message> messages;

    public ElementException(String message, Element element) {
        this(List.of(new Message(message, element)));
    }

    public ElementException(List<Message> messages) {
        super(messages.stream().map(Message::message).collect(Collectors.joining("\n")));
        this.messages = messages;
    }

    public void printMessage(Messager messager) {
        for (Message message : messages) {
            if (message.element != null) messager.printMessage(Diagnostic.Kind.ERROR, message.message, message.element);
            else messager.printMessage(Diagnostic.Kind.ERROR, message.message);
        }
    }

    public record Message(String message, Element element) {}
}
