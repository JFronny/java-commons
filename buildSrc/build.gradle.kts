plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation(libs.plugin.kotlin)
    implementation(libs.plugin.convention)
}
