import io.gitlab.jfronny.scripts.*

plugins {
    `java-library`
    `maven-publish`
    jf.umldoc
}

version = rootProject.version

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts/")
}

dependencies {
    compileOnly(libs.findLibrary("annotations").orElseThrow())

    testImplementation(libs.findLibrary("junit-jupiter-api").orElseThrow())
    testRuntimeOnly(libs.findLibrary("junit-jupiter-engine").orElseThrow())
}

tasks.test {
    useJUnitPlatform()
}

java {
    withSourcesJar()
}

publishing {
    repositories {
        mavenLocal()

        if (project.hasProperty("maven")) {
            maven {
                url = uri(project.property("maven").toString())
                name = "dynamic"

                credentials(PasswordCredentials::class) {
                    username = System.getenv()["MAVEN_NAME"]
                    password = System.getenv()["MAVEN_TOKEN"]
                }
                authentication {
                    create<BasicAuthentication>("basic")
                }
            }
        }
    }
}

tasks.javadoc {
    opts.links("https://javadoc.io/doc/org.jetbrains/annotations/${libs.findVersion("annotations").orElseThrow().preferredVersion}")
}