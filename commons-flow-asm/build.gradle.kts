plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    implementation(projects.commonsFlow)
    api(libs.asm)

    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-flow-asm"

            from(components["java"])
        }
    }
}

//tasks.compileJava {
//    options.compilerArgs.addAll(listOf("--add-exports", "java.base/jdk.internal.vm=io.gitlab.jfronny.commons"))
//}
