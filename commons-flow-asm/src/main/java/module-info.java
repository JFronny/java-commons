module io.gitlab.jfronny.commons.flow.asm {
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.flow;
    requires org.objectweb.asm;
    exports io.gitlab.jfronny.commons.flow.asm;
}