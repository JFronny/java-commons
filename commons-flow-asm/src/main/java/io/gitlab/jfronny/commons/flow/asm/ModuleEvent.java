package io.gitlab.jfronny.commons.flow.asm;

public sealed interface ModuleEvent {
    record MainClass(String mainClass) implements ModuleEvent { }
    record Package(String packaze) implements ModuleEvent { }
    record Require(String module, int access, String version) implements ModuleEvent { }
    record Export(String packaze, int access, String... modules) implements ModuleEvent { }
    record Open(String packaze, int access, String... modules) implements ModuleEvent { }
    record Use(String service) implements ModuleEvent { }
    record Provide(String service, String... providers) implements ModuleEvent { }
}
