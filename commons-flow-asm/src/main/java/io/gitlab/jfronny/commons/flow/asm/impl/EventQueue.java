package io.gitlab.jfronny.commons.flow.asm.impl;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class EventQueue<T> {
    private final Queue<Pushable<T>> enqueued = new ArrayDeque<>();
    private final Consumer<T> target;
    private final Runnable postEnd;
    private int depth = 1;

    public EventQueue(Consumer<T> target, Runnable postEnd) {
        this.target = Objects.requireNonNull(target);
        this.postEnd = Objects.requireNonNull(postEnd);
    }

    public void add(T event) {
        if (depth == 0) throw new IllegalStateException("Already ended");
        if (enqueued.isEmpty()) {
            target.accept(event);
            return;
        }
        Iterator<Pushable<T>> iterator = enqueued.iterator();
        while (iterator.hasNext()) {
            switch (iterator.next()) {
                case Pushable.Concrete(var value) -> {
                    target.accept(value);
                    iterator.remove();
                }
                case Pushable.Later(var data, var finished) when finished.get() -> {
                    target.accept(data.get());
                    iterator.remove();
                }
                default -> {
                    enqueued.add(Pushable.of(event));
                    return;
                }
            }
        }
        target.accept(event);
    }

    public void add(Pushable<T> event) {
        if (depth == 0) throw new IllegalStateException("Already ended");
        if (enqueued.isEmpty()) {
            switch (event) {
                case Pushable.Concrete(var value) -> target.accept(value);
                case Pushable.Later(var data, var finished) when finished.get() -> target.accept(data.get());
                case Pushable.Later(var data, var finished) -> enqueued.add(event);
            }
            return;
        }
        Iterator<Pushable<T>> iterator = enqueued.iterator();
        while (iterator.hasNext()) {
            switch (iterator.next()) {
                case Pushable.Concrete(var value) -> {
                    target.accept(value);
                    iterator.remove();
                }
                case Pushable.Later(var data, var finished) when finished.get() -> {
                    target.accept(data.get());
                    iterator.remove();
                }
                default -> {
                    enqueued.add(event);
                    return;
                }
            }
        }
        switch (event) {
            case Pushable.Concrete(var value) -> target.accept(value);
            case Pushable.Later(var data, var finished) when finished.get() -> target.accept(data.get());
            case Pushable.Later ev -> enqueued.add(ev);
        }
    }

    private void deferEnd() {
        depth++;
    }

    public void end() {
        if (depth > 1) {
            depth--;
            return;
        }
        for (Pushable<T> pushable : enqueued) target.accept(pushable.value());
        postEnd.run();
        depth = 0;
    }

    public boolean hasEnded() {
        return depth == 0;
    }

    public <Flow, Element> Flow addByFlow(BiFunction<Consumer<Element>, Runnable, Flow> factory, Function<Iterator<Element>, T> mapper) {
        if (depth == 0) throw new IllegalStateException("Already ended");
        List<Element> buffer = new LinkedList<>();
        AtomicBoolean finished = new AtomicBoolean(false);
        deferEnd();
        Flow flow = factory.apply(buffer::add, () -> {
            finished.set(true);
            end();
        });
        if (finished.get()) {
            add(mapper.apply(buffer.iterator()));
        } else {
            add(new Pushable.Later<>(() -> {
                if (finished.get()) return mapper.apply(buffer.iterator());
                throw new IllegalStateException("Not finished");
            }, finished));
        }
        return flow;
    }
}
