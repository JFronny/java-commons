package io.gitlab.jfronny.commons.flow.asm;

import org.objectweb.asm.TypePath;

import java.util.Iterator;

public sealed interface ClassEvent {
    record Header(int version, int access, String name, String signature, String superName, String[] interfaces) implements ClassEvent { }
    record Source(String source, String debug) implements ClassEvent { }
    record Module(String name, int access, String version, Iterator<ModuleEvent> events) implements ClassEvent { }
    record NestHost(String nestHost) implements ClassEvent { }
    record OuterClass(String owner, String name, String descriptor) implements ClassEvent { }
    record Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements ClassEvent { }
    record TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements ClassEvent { }
    record Attribute(org.objectweb.asm.Attribute attribute) implements ClassEvent { }
    record NestMember(String nestMember) implements ClassEvent { }
    record PermittedSubclass(String permittedSubclass) implements ClassEvent { }
    record InnerClass(String name, String outerName, String innerName, int access) implements ClassEvent { }
    record RecordComponent(String name, String descriptor, String signature, Iterator<RecordComponentEvent> events) implements ClassEvent { }
    record Field(int access, String name, String descriptor, String signature, Object value, Iterator<FieldEvent> events) implements ClassEvent { }
    record Method(int access, String name, String descriptor, String signature, String[] exceptions, Iterator<MethodEvent> events) implements ClassEvent { }
}
