package io.gitlab.jfronny.commons.flow.asm.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;

public sealed interface Pushable<T> {
    T value();
    <R> Pushable<R> map(Function<T, R> mapper);

    record Concrete<T>(T value) implements Pushable<T> {
        @Override
        public <R> Pushable<R> map(Function<T, R> mapper) {
            return new Concrete<>(mapper.apply(value));
        }
    }

    record Later<T>(Supplier<T> data, AtomicBoolean finished) implements Pushable<T> {
        @Override
        public T value() {
            return data.get();
        }

        public <R> Later<R> map(Function<T, R> mapper) {
            return new Later<>(() -> mapper.apply(value()), finished);
        }
    }

    static <T> Pushable<T> of(T value) {
        return new Concrete<>(value);
    }
}
