package io.gitlab.jfronny.commons.flow.asm;

import org.objectweb.asm.Handle;
import org.objectweb.asm.TypePath;

import java.util.Iterator;

public sealed interface MethodEvent {
    record Parameter(String name, int access) implements MethodEvent { }
    record AnnotationDefault(Iterator<AnnotationEvent> events) implements MethodEvent { }
    record Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record AnnotableParameterCount(int parameterCount, boolean visible) implements MethodEvent { }
    record ParameterAnnotation(int parameter, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record Attribute(org.objectweb.asm.Attribute attribute) implements MethodEvent { }
    record Code() implements MethodEvent { }
    record Frame(int type, int numLocal, Object[] local, int numStack, Object[] stack) implements MethodEvent { }
    record Insn(int opcode) implements MethodEvent { }
    record IntInsn(int opcode, int operand) implements MethodEvent { }
    record VarInsn(int opcode, int varIndex) implements MethodEvent { }
    record TypeInsn(int opcode, String type) implements MethodEvent { }
    record FieldInsn(int opcode, String owner, String name, String descriptor) implements MethodEvent { }
    record MethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) implements MethodEvent {
        public MethodInsn(int opcode, String owner, String name, String descriptor) {
            this(opcode, owner, name, descriptor, opcode == org.objectweb.asm.Opcodes.INVOKEINTERFACE);
        }
    }
    record InvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) implements MethodEvent { }
    record JumpInsn(int opcode, org.objectweb.asm.Label label) implements MethodEvent { }
    record Label(org.objectweb.asm.Label label) implements MethodEvent { }
    record LdcInsn(Object value) implements MethodEvent { }
    record IincInsn(int varIndex, int increment) implements MethodEvent { }
    record TableSwitchInsn(int min, int max, org.objectweb.asm.Label dflt, org.objectweb.asm.Label... labels) implements MethodEvent { }
    record LookupSwitchInsn(org.objectweb.asm.Label dflt, int[] keys, org.objectweb.asm.Label[] labels) implements MethodEvent { }
    record MultiANewArrayInsn(String descriptor, int numDimensions) implements MethodEvent { }
    record InsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record TryCatchBlock(org.objectweb.asm.Label start, org.objectweb.asm.Label end, org.objectweb.asm.Label handler, String type) implements MethodEvent { }
    record TryCatchAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record LocalVariable(String name, String descriptor, String signature, org.objectweb.asm.Label start, org.objectweb.asm.Label end, int index) implements MethodEvent { }
    record LocalVariableAnnotation(int typeRef, TypePath typePath, org.objectweb.asm.Label[] start, org.objectweb.asm.Label[] end, int[] index, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements MethodEvent { }
    record LineNumber(int line, org.objectweb.asm.Label start) implements MethodEvent { }
    record Maxs(int maxStack, int maxLocals) implements MethodEvent { }
}
