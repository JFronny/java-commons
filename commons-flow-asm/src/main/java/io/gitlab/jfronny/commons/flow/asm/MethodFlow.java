package io.gitlab.jfronny.commons.flow.asm;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.MethodEvent.*;
import io.gitlab.jfronny.commons.flow.asm.MethodEvent.Attribute;
import io.gitlab.jfronny.commons.flow.asm.MethodEvent.Label;
import io.gitlab.jfronny.commons.flow.asm.impl.EventQueue;
import io.gitlab.jfronny.commons.ref.R;
import org.objectweb.asm.*;

import java.util.Iterator;
import java.util.function.Consumer;

import static org.objectweb.asm.Opcodes.ASM9;

public class MethodFlow extends MethodVisitor {
    private final EventQueue<MethodEvent> target;

    protected MethodFlow(Consumer<MethodEvent> target, Runnable end) {
        super(ASM9);
        this.target = new EventQueue<>(target, end);
    }

    public static Enumerator<MethodEvent> begin(Consumer<MethodVisitor> apply) {
        return new Enumerator<>(a -> apply.accept(new MethodFlow(a, R::nop)));
    }

    public static void end(Iterator<MethodEvent> events, MethodVisitor target) {
        while (events.hasNext()) {
            switch (events.next()) {
                case Parameter(String name, int access) -> target.visitParameter(name, access);
                case AnnotationDefault(Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitAnnotationDefault());
                case Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitAnnotation(descriptor, visible));
                case TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitTypeAnnotation(typeRef, typePath, descriptor, visible));
                case AnnotableParameterCount(int parameterCount, boolean visible) -> target.visitAnnotableParameterCount(parameterCount, visible);
                case ParameterAnnotation(int parameter, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitParameterAnnotation(parameter, descriptor, visible));
                case Attribute(org.objectweb.asm.Attribute attribute) -> target.visitAttribute(attribute);
                case Code code -> target.visitCode();
                case Frame(int type, int numLocal, Object[] local, int numStack, Object[] stack) -> target.visitFrame(type, numLocal, local, numStack, stack);
                case Insn(int opcode) -> target.visitInsn(opcode);
                case IntInsn(int opcode, int operand) -> target.visitIntInsn(opcode, operand);
                case VarInsn(int opcode, int varIndex) -> target.visitVarInsn(opcode, varIndex);
                case TypeInsn(int opcode, String type) -> target.visitTypeInsn(opcode, type);
                case FieldInsn(int opcode, String owner, String name, String descriptor) -> target.visitFieldInsn(opcode, owner, name, descriptor);
                case MethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) -> target.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
                case InvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object[] bootstrapMethodArguments) -> target.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
                case JumpInsn(int opcode, org.objectweb.asm.Label label) -> target.visitJumpInsn(opcode, label);
                case Label(org.objectweb.asm.Label label) -> target.visitLabel(label);
                case LdcInsn(Object value) -> target.visitLdcInsn(value);
                case IincInsn(int varIndex, int increment) -> target.visitIincInsn(varIndex, increment);
                case TableSwitchInsn(int min, int max, org.objectweb.asm.Label dflt, org.objectweb.asm.Label[] labels) -> target.visitTableSwitchInsn(min, max, dflt, labels);
                case LookupSwitchInsn(org.objectweb.asm.Label dflt, int[] keys, org.objectweb.asm.Label[] labels) -> target.visitLookupSwitchInsn(dflt, keys, labels);
                case MultiANewArrayInsn(String descriptor, int numDimensions) -> target.visitMultiANewArrayInsn(descriptor, numDimensions);
                case InsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitInsnAnnotation(typeRef, typePath, descriptor, visible));
                case TryCatchBlock(org.objectweb.asm.Label start, org.objectweb.asm.Label end, org.objectweb.asm.Label handler, String type) -> target.visitTryCatchBlock(start, end, handler, type);
                case TryCatchAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible));
                case LocalVariable(String name, String descriptor, String signature, org.objectweb.asm.Label start, org.objectweb.asm.Label end, int index) -> target.visitLocalVariable(name, descriptor, signature, start, end, index);
                case LocalVariableAnnotation(int typeRef, TypePath typePath, org.objectweb.asm.Label[] start, org.objectweb.asm.Label[] end, int[] index, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible));
                case LineNumber(int line, org.objectweb.asm.Label start) -> target.visitLineNumber(line, start);
                case Maxs(int maxStack, int maxLocals) -> target.visitMaxs(maxStack, maxLocals);
            }
        }
        target.visitEnd();
    }

    @Override
    public void visitParameter(String name, int access) {
        target.add(new Parameter(name, access));
    }

    @Override
    public AnnotationVisitor visitAnnotationDefault() {
        return target.addByFlow(AnnotationFlow::new, AnnotationDefault::new);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new Annotation(descriptor, visible, events));
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new TypeAnnotation(typeRef, typePath, descriptor, visible, events));
    }

    @Override
    public void visitAnnotableParameterCount(int parameterCount, boolean visible) {
        target.add(new AnnotableParameterCount(parameterCount, visible));
    }

    @Override
    public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new ParameterAnnotation(parameter, descriptor, visible, events));
    }

    @Override
    public void visitAttribute(org.objectweb.asm.Attribute attribute) {
        target.add(new Attribute(attribute));
    }

    @Override
    public void visitCode() {
        target.add(new Code());
    }

    @Override
    public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack) {
        target.add(new Frame(type, numLocal, local, numStack, stack));
    }

    @Override
    public void visitInsn(int opcode) {
        target.add(new Insn(opcode));
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        target.add(new IntInsn(opcode, operand));
    }

    @Override
    public void visitVarInsn(int opcode, int varIndex) {
        target.add(new VarInsn(opcode, varIndex));
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        target.add(new TypeInsn(opcode, type));
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
        target.add(new FieldInsn(opcode, owner, name, descriptor));
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
        target.add(new MethodInsn(opcode, owner, name, descriptor, isInterface));
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) {
        target.add(new InvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments));
    }

    @Override
    public void visitJumpInsn(int opcode, org.objectweb.asm.Label label) {
        target.add(new JumpInsn(opcode, label));
    }

    @Override
    public void visitLabel(org.objectweb.asm.Label label) {
        target.add(new Label(label));
    }

    @Override
    public void visitLdcInsn(Object value) {
        target.add(new LdcInsn(value));
    }

    @Override
    public void visitIincInsn(int varIndex, int increment) {
        target.add(new IincInsn(varIndex, increment));
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, org.objectweb.asm.Label dflt, org.objectweb.asm.Label... labels) {
        target.add(new TableSwitchInsn(min, max, dflt, labels));
    }

    @Override
    public void visitLookupSwitchInsn(org.objectweb.asm.Label dflt, int[] keys, org.objectweb.asm.Label[] labels) {
        target.add(new LookupSwitchInsn(dflt, keys, labels));
    }

    @Override
    public void visitMultiANewArrayInsn(String descriptor, int numDimensions) {
        target.add(new MultiANewArrayInsn(descriptor, numDimensions));
    }

    @Override
    public AnnotationVisitor visitInsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new InsnAnnotation(typeRef, typePath, descriptor, visible, events));
    }

    @Override
    public void visitTryCatchBlock(org.objectweb.asm.Label start, org.objectweb.asm.Label end, org.objectweb.asm.Label handler, String type) {
        target.add(new TryCatchBlock(start, end, handler, type));
    }

    @Override
    public AnnotationVisitor visitTryCatchAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new TryCatchAnnotation(typeRef, typePath, descriptor, visible, events));
    }

    @Override
    public void visitLocalVariable(String name, String descriptor, String signature, org.objectweb.asm.Label start, org.objectweb.asm.Label end, int index) {
        target.add(new LocalVariable(name, descriptor, signature, start, end, index));
    }

    @Override
    public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, org.objectweb.asm.Label[] start, org.objectweb.asm.Label[] end, int[] index, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new LocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible, events));
    }

    @Override
    public void visitLineNumber(int line, org.objectweb.asm.Label start) {
        target.add(new LineNumber(line, start));
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        target.add(new Maxs(maxStack, maxLocals));
    }

    @Override
    public void visitEnd() {
        target.end();
    }
}
