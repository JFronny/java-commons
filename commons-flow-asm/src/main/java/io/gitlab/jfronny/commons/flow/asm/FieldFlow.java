package io.gitlab.jfronny.commons.flow.asm;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.FieldEvent.*;
import io.gitlab.jfronny.commons.flow.asm.impl.EventQueue;
import io.gitlab.jfronny.commons.ref.R;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.TypePath;

import java.util.Iterator;
import java.util.function.Consumer;

import static org.objectweb.asm.Opcodes.ASM9;

public class FieldFlow extends FieldVisitor {
    private final EventQueue<FieldEvent> target;

    protected FieldFlow(Consumer<FieldEvent> target, Runnable end) {
        super(ASM9);
        this.target = new EventQueue<>(target, end);
    }

    public static Enumerator<FieldEvent> begin(Consumer<FieldVisitor> apply) {
        return new Enumerator<>(a -> apply.accept(new FieldFlow(a, R::nop)));
    }

    public static void end(Iterator<FieldEvent> events, FieldVisitor target) {
        while (events.hasNext()) {
            switch (events.next()) {
                case Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitAnnotation(descriptor, visible));
                case TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitTypeAnnotation(typeRef, typePath, descriptor, visible));
                case Attribute(org.objectweb.asm.Attribute attribute) -> target.visitAttribute(attribute);
            }
        }
        target.visitEnd();
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new Annotation(descriptor, visible, events));
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new TypeAnnotation(typeRef, typePath, descriptor, visible, events));
    }

    @Override
    public void visitAttribute(org.objectweb.asm.Attribute attribute) {
        target.add(new Attribute(attribute));
    }

    @Override
    public void visitEnd() {
        target.end();
    }
}
