package io.gitlab.jfronny.commons.flow.asm;

import org.objectweb.asm.TypePath;

import java.util.Iterator;

public sealed interface FieldEvent {
    record Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements FieldEvent { }
    record TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements FieldEvent { }
    record Attribute(org.objectweb.asm.Attribute attribute) implements FieldEvent { }
}
