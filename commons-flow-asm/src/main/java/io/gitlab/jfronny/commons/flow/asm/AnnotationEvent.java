package io.gitlab.jfronny.commons.flow.asm;

import java.util.Iterator;

public sealed interface AnnotationEvent {
    record Begin(String name, Object value) implements AnnotationEvent { }
    record Enum(String name, String descriptor, String value) implements AnnotationEvent { }
    record Annotation(String name, String descriptor, Iterator<AnnotationEvent> events) implements AnnotationEvent { }
    record Array(String name, Iterator<AnnotationEvent> events) implements AnnotationEvent { }
}
