package io.gitlab.jfronny.commons.flow.asm;

import org.objectweb.asm.TypePath;

import java.util.Iterator;

public sealed interface RecordComponentEvent {
    record Annotation(String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements RecordComponentEvent { }
    record TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events) implements RecordComponentEvent { }
    record Attribute(org.objectweb.asm.Attribute attribute) implements RecordComponentEvent { }
}
