package io.gitlab.jfronny.commons.flow.asm;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.ModuleEvent.*;
import io.gitlab.jfronny.commons.flow.asm.ModuleEvent.Package;
import io.gitlab.jfronny.commons.ref.R;
import org.objectweb.asm.ModuleVisitor;

import java.util.Iterator;
import java.util.function.Consumer;

import static org.objectweb.asm.Opcodes.ASM9;

public class ModuleFlow extends ModuleVisitor {
    private final Consumer<ModuleEvent> target;
    private final Runnable end;

    protected ModuleFlow(Consumer<ModuleEvent> target, Runnable end) {
        super(ASM9);
        this.target = target;
        this.end = end;
    }

    public static Enumerator<ModuleEvent> begin(Consumer<ModuleVisitor> apply) {
        return new Enumerator<>(a -> apply.accept(new ModuleFlow(a, R::nop)));
    }

    public static void end(Iterator<ModuleEvent> events, ModuleVisitor target) {
        while (events.hasNext()) {
            switch (events.next()) {
                case MainClass(String mainClass) -> target.visitMainClass(mainClass);
                case Package(String packaze) -> target.visitPackage(packaze);
                case Require(String module, int access, String version) -> target.visitRequire(module, access, version);
                case Export(String packaze, int access, String[] modules) -> target.visitExport(packaze, access, modules);
                case Open(String packaze, int access, String[] modules) -> target.visitOpen(packaze, access, modules);
                case Use(String service) -> target.visitUse(service);
                case Provide(String service, String[] providers) -> target.visitProvide(service, providers);
            }
        }
        target.visitEnd();
    }

    @Override
    public void visitMainClass(String mainClass) {
        target.accept(new MainClass(mainClass));
    }

    @Override
    public void visitPackage(String packaze) {
        target.accept(new Package(packaze));
    }

    @Override
    public void visitRequire(String module, int access, String version) {
        target.accept(new Require(module, access, version));
    }

    @Override
    public void visitExport(String packaze, int access, String... modules) {
        target.accept(new Export(packaze, access, modules));
    }

    @Override
    public void visitOpen(String packaze, int access, String... modules) {
        target.accept(new Open(packaze, access, modules));
    }

    @Override
    public void visitUse(String service) {
        target.accept(new Use(service));
    }

    @Override
    public void visitProvide(String service, String... providers) {
        target.accept(new Provide(service, providers));
    }

    @Override
    public void visitEnd() {
        end.run();
    }
}
