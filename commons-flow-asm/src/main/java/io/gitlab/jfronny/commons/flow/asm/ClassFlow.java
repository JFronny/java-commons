package io.gitlab.jfronny.commons.flow.asm;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.ClassEvent.*;
import io.gitlab.jfronny.commons.flow.asm.ClassEvent.Attribute;
import io.gitlab.jfronny.commons.flow.asm.ClassEvent.Module;
import io.gitlab.jfronny.commons.flow.asm.impl.EventQueue;
import io.gitlab.jfronny.commons.ref.R;
import org.objectweb.asm.*;

import java.util.Iterator;
import java.util.function.Consumer;

import static org.objectweb.asm.Opcodes.ASM9;

public class ClassFlow extends ClassVisitor {
    private final EventQueue<ClassEvent> target;

    protected ClassFlow(Consumer<ClassEvent> target) {
        super(ASM9);
        this.target = new EventQueue<>(target, R::nop);
    }

    public static Enumerator<ClassEvent> begin(Consumer<ClassVisitor> apply) {
        return new Enumerator<>(a -> {
            ClassFlow flow = new ClassFlow(a);
            apply.accept(flow);
            if (!flow.target.hasEnded()) {
                throw new IllegalStateException("ClassFlow has not ended");
            }
        });
    }

    public static Enumerator<ClassEvent> begin(byte[] klazz, int parsingOptions) {
        return begin(visitor -> new ClassReader(klazz).accept(visitor, parsingOptions));
    }

    public static void end(Iterator<ClassEvent> events, ClassVisitor target) {
        while (events.hasNext()) {
            switch (events.next()) {
                case Header(int version, int access, String name, String signature, String superName, String[] interfaces) -> target.visit(version, access, name, signature, superName, interfaces);
                case Source(String source, String debug) -> target.visitSource(source, debug);
                case Module(String name, int access, String version, Iterator<ModuleEvent> events1) -> ModuleFlow.end(events1, target.visitModule(name, access, version));
                case NestHost(String nestHost) -> target.visitNestHost(nestHost);
                case OuterClass(String owner, String name, String descriptor) -> target.visitOuterClass(owner, name, descriptor);
                case Annotation(var descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitAnnotation(descriptor, visible));
                case TypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitTypeAnnotation(typeRef, typePath, descriptor, visible));
                case Attribute(org.objectweb.asm.Attribute attribute) -> target.visitAttribute(attribute);
                case NestMember(String nestMember) -> target.visitNestMember(nestMember);
                case PermittedSubclass(String permittedSubclass) -> target.visitPermittedSubclass(permittedSubclass);
                case InnerClass(String name, String outerName, String innerName, int access) -> target.visitInnerClass(name, outerName, innerName, access);
                case RecordComponent(String name, String descriptor, String signature, Iterator<RecordComponentEvent> events1) -> RecordComponentFlow.end(events1, target.visitRecordComponent(name, descriptor, signature));
                case Field(int access, String name, String descriptor, String signature, Object value, Iterator<FieldEvent> events1) -> FieldFlow.end(events1, target.visitField(access, name, descriptor, signature, value));
                case Method(int access, String name, String descriptor, String signature, String[] exceptions, Iterator<MethodEvent> events1) -> MethodFlow.end(events1, target.visitMethod(access, name, descriptor, signature, exceptions));
            }
        }
        target.visitEnd();
    }

    public static byte[] end(Iterator<ClassEvent> events, int flags) {
        ClassWriter writer = new ClassWriter(flags);
        end(events, writer);
        return writer.toByteArray();
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        target.add(new Header(version, access, name, signature, superName, interfaces));
    }

    @Override
    public void visitSource(String source, String debug) {
        target.add(new Source(source, debug));
    }

    @Override
    public ModuleVisitor visitModule(String name, int access, String version) {
        return target.addByFlow(ModuleFlow::new, events -> new Module(name, access, version, events));
    }

    @Override
    public void visitNestHost(String nestHost) {
        target.add(new NestHost(nestHost));
    }

    @Override
    public void visitOuterClass(String owner, String name, String descriptor) {
        target.add(new OuterClass(owner, name, descriptor));
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new Annotation(descriptor, visible, events));
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
        return target.addByFlow(AnnotationFlow::new, events -> new TypeAnnotation(typeRef, typePath, descriptor, visible, events));
    }

    @Override
    public void visitAttribute(org.objectweb.asm.Attribute attribute) {
        target.add(new Attribute(attribute));
    }

    @Override
    public void visitNestMember(String nestMember) {
        target.add(new NestMember(nestMember));
    }

    @Override
    public void visitPermittedSubclass(String permittedSubclass) {
        target.add(new PermittedSubclass(permittedSubclass));
    }

    @Override
    public void visitInnerClass(String name, String outerName, String innerName, int access) {
        target.add(new InnerClass(name, outerName, innerName, access));
    }

    @Override
    public RecordComponentVisitor visitRecordComponent(String name, String descriptor, String signature) {
        return target.addByFlow(RecordComponentFlow::new, events -> new RecordComponent(name, descriptor, signature, events));
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        return target.addByFlow(FieldFlow::new, events -> new Field(access, name, descriptor, signature, value, events));
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        return target.addByFlow(MethodFlow::new, events -> new Method(access, name, descriptor, signature, exceptions, events));
    }

    @Override
    public void visitEnd() {
        target.end();
    }
}
