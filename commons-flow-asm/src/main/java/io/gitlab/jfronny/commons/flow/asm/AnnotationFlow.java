package io.gitlab.jfronny.commons.flow.asm;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.AnnotationEvent.*;
import io.gitlab.jfronny.commons.flow.asm.AnnotationEvent.Enum;
import io.gitlab.jfronny.commons.flow.asm.impl.EventQueue;
import io.gitlab.jfronny.commons.ref.R;
import org.objectweb.asm.*;

import java.util.Iterator;
import java.util.function.Consumer;

import static org.objectweb.asm.Opcodes.ASM9;

public class AnnotationFlow extends AnnotationVisitor {
    private final EventQueue<AnnotationEvent> target;

    protected AnnotationFlow(Consumer<AnnotationEvent> target, Runnable end) {
        super(ASM9);
        this.target = new EventQueue<>(target, end);
    }

    public static Enumerator<AnnotationEvent> begin(Consumer<AnnotationVisitor> apply) {
        return new Enumerator<>(a -> apply.accept(new AnnotationFlow(a, R::nop)));
    }

    public static void end(Iterator<AnnotationEvent> events, AnnotationVisitor target) {
        while (events.hasNext()) {
            switch (events.next()) {
                case Begin(String name, Object value) -> target.visit(name, value);
                case Enum(String name, String descriptor, String value) -> target.visitEnum(name, descriptor, value);
                case Annotation(String name, String descriptor, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitAnnotation(name, descriptor));
                case Array(String name, Iterator<AnnotationEvent> events1) -> AnnotationFlow.end(events1, target.visitArray(name));
            }
        }
        target.visitEnd();
    }

    @Override
    public void visit(String name, Object value) {
        target.add(new Begin(name, value));
    }

    @Override
    public void visitEnum(String name, String descriptor, String value) {
        target.add(new Enum(name, descriptor, value));
    }

    @Override
    public AnnotationVisitor visitAnnotation(String name, String descriptor) {
        return target.addByFlow(AnnotationFlow::new, events -> new Annotation(name, descriptor, events));
    }

    @Override
    public AnnotationVisitor visitArray(String name) {
        return target.addByFlow(AnnotationFlow::new, events -> new Array(name, events));
    }

    @Override
    public void visitEnd() {
        target.end();
    }
}
