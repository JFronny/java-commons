package io.gitlab.jfronny.commons.flow.asm.test;

import io.gitlab.jfronny.commons.flow.Enumerator;
import io.gitlab.jfronny.commons.flow.asm.ClassEvent.*;
import io.gitlab.jfronny.commons.flow.asm.ClassFlow;
import io.gitlab.jfronny.commons.flow.asm.MethodEvent.*;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.ClassWriter;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.objectweb.asm.Opcodes.*;

public class AsmApiTest {
    @Test
    void testAsm() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String className = "io.gitlab.jfronny.commons.flow.asm.test.Evil";
        byte[] klazz = ClassFlow.end(new Enumerator<>(out -> {
            out.accept(new Header(V21, ACC_PUBLIC | ACC_SUPER, className.replace('.', '/'), null, "java/lang/Object", null));
            out.accept(new Source("Evil.java", null));
            out.accept(new Method(ACC_PUBLIC | ACC_STATIC, "main", "()Ljava/util/ArrayList;", null, null, new Enumerator<>(out2 -> {
                out2.accept(new Code());
                out2.accept(new TypeInsn(NEW, "java/util/ArrayList"));
                out2.accept(new Insn(DUP));
                out2.accept(new MethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false));
                out2.accept(new Insn(ARETURN));
                out2.accept(new Maxs(1, 1));
            })));
        }), ClassWriter.COMPUTE_FRAMES);
        Class<?> loadedClass = new ClassLoader() {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                return name.equals(className) ? defineClass(className, klazz, 0, klazz.length) : super.findClass(name);
            }
        }.loadClass(className);

        ArrayList<String> list = (ArrayList<String>) loadedClass.getDeclaredMethod("main").invoke(null);
        list.add("Hello!");
        assertEquals("[Hello!]", list.toString());
    }
}
