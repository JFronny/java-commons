package io.gitlab.jfronny.commons.flow.asm.test;

import io.gitlab.jfronny.commons.flow.asm.ClassFlow;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.objectweb.asm.ClassReader.SKIP_FRAMES;
import static org.objectweb.asm.ClassWriter.COMPUTE_FRAMES;
import static org.objectweb.asm.Opcodes.*;

public class AsmTest {
    public static byte[] dump() throws Exception {
        ClassWriter classWriter = new ClassWriter(0);
        dump(classWriter);
        return classWriter.toByteArray();
    }

    // Computer-generated from Example.java via ASMifier
    private static void dump(ClassVisitor classWriter) {
        FieldVisitor fieldVisitor;
        RecordComponentVisitor recordComponentVisitor;
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V21, ACC_PUBLIC | ACC_SUPER, "io/gitlab/jfronny/commons/flow/asm/test/Example", null, "java/lang/Object", null);

        classWriter.visitSource("Example.java", null);

        classWriter.visitNestMember("io/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation2");

        classWriter.visitNestMember("io/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation");

        classWriter.visitInnerClass("io/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation2", "io/gitlab/jfronny/commons/flow/asm/test/Example", "TestAnnotation2", ACC_STATIC | ACC_ANNOTATION | ACC_ABSTRACT | ACC_INTERFACE);

        classWriter.visitInnerClass("io/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation", "io/gitlab/jfronny/commons/flow/asm/test/Example", "TestAnnotation", ACC_STATIC | ACC_ANNOTATION | ACC_ABSTRACT | ACC_INTERFACE);

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(3, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(RETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lio/gitlab/jfronny/commons/flow/asm/test/Example;", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
            {
                annotationVisitor0 = methodVisitor.visitAnnotation("Lio/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation2;", false);
                annotationVisitor0.visitEnd();
            }
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(6, label0);
            methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
            methodVisitor.visitLdcInsn("Hello, World!");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(7, label1);
            methodVisitor.visitInsn(RETURN);
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLocalVariable("args", "[Ljava/lang/String;", null, label0, label2, 0);
            methodVisitor.visitMaxs(2, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "toString", "()Ljava/lang/String;", null, null);
            {
                annotationVisitor0 = methodVisitor.visitAnnotation("Lio/gitlab/jfronny/commons/flow/asm/test/Example$TestAnnotation;", false);
                annotationVisitor0.visit("value", "Ae");
                annotationVisitor0.visitEnd();
            }
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(11, label0);
            methodVisitor.visitLdcInsn("Example{}");
            methodVisitor.visitInsn(ARETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lio/gitlab/jfronny/commons/flow/asm/test/Example;", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "test", "()Z", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(15, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitLdcInsn("Hi!");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "equals", "(Ljava/lang/Object;)Z", false);
            methodVisitor.visitInsn(IRETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lio/gitlab/jfronny/commons/flow/asm/test/Example;", null, label0, label1, 0);
            methodVisitor.visitMaxs(2, 1);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();
    }

    @Test
    void simpleTest() throws Exception {
        byte[] klazz = dump();
        assertArrayEquals(klazz, ClassFlow.end(ClassFlow.begin(AsmTest::dump), COMPUTE_FRAMES));
        assertArrayEquals(klazz, ClassFlow.end(ClassFlow.begin(klazz, SKIP_FRAMES), COMPUTE_FRAMES));
    }
}
