package io.gitlab.jfronny.commons.flow.asm.test;

public class Example {
    @TestAnnotation2
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    @Override
    public @TestAnnotation("Ae") String toString() {
        return "Example{}";
    }

    public boolean test() {
        return this.equals("Hi!");
    }

    @interface TestAnnotation {
        String value();
    }

    @interface TestAnnotation2 {
        String value() default "Hi!";
    }
}
