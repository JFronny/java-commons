package io.gitlab.jfronny.commons.serialize.generator;

import com.palantir.javapoet.ClassName;
import com.palantir.javapoet.TypeName;
import io.gitlab.jfronny.commons.serialize.generator.core.value.ElementException;
import org.jetbrains.annotations.Nullable;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public record SerializableClass(TypeElement classElement, ClassName generatedClassName, @Nullable TypeMirror adapter, boolean adapterIsStatic, @Nullable TypeMirror builder, @Nullable TypeMirror configure, boolean generateAdapter, boolean isStatic) {
    public static SerializableClass of(TypeElement element, @Nullable TypeMirror with, boolean withIsStatic, @Nullable TypeMirror builder, @Nullable TypeMirror configure, boolean generateAdapter, boolean isStatic, boolean manifold) throws ElementException {
        ClassName className = ClassName.get(element);
        String pkg = manifold ? "serializeprocessor.extensions." + className.packageName() + '.' + className.simpleNames().getFirst() : className.packageName();
        ClassName generatedClassName = ClassName.get(pkg, "GC_" + className.simpleNames().getFirst(), className.simpleNames().subList(1, className.simpleNames().size()).toArray(String[]::new));
        return new SerializableClass(element, generatedClassName, voidToNull(with), withIsStatic, voidToNull(builder), voidToNull(configure), generateAdapter, isStatic).validate();
    }

    public ClassName getClassName() {
        return ClassName.get(classElement);
    }

    public TypeName getTypeName() {
        return TypeName.get(classElement.asType());
    }

    private SerializableClass validate() throws ElementException {
        if (adapter != null && builder != null) throw new ElementException("@GSerializable with both an adapter and a builder. This is unsupported!", classElement);
        if (isStatic && builder != null) throw new ElementException("@GSerializable which is static and has a builder. This is unsupported!", classElement);
        if (isStatic && generateAdapter) throw new ElementException("@GSerializable which is static and generates an adapter. This is unsupported!", classElement);
        return this;
    }

    private static TypeMirror voidToNull(TypeMirror type) {
        return type == null || type.toString().equals("void") ? null : type;
    }
}
