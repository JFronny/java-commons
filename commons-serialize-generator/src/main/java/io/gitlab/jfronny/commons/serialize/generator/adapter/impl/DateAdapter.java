package io.gitlab.jfronny.commons.serialize.generator.adapter.impl;

import com.palantir.javapoet.CodeBlock;
import com.palantir.javapoet.MethodSpec;
import com.palantir.javapoet.TypeName;
import io.gitlab.jfronny.commons.serialize.generator.Cl;
import io.gitlab.jfronny.commons.serialize.generator.adapter.Adapter;

import javax.lang.model.element.Modifier;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;

import static io.gitlab.jfronny.commons.serialize.generator.adapter.impl.PoetUtils.methodSpecs;

public class DateAdapter extends Adapter<DateAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return TypeName.get(type).withoutAnnotations().toString().equals(Date.class.getCanonicalName());
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("writer.value($T.format(", Cl.GISO8601UTILS);
            writeGet.run();
            code.add("));\n");
        }

        @Override
        public void generateRead() {
            if (methodSpecs(klazz).stream().noneMatch(s -> s.name().equals("parseDate"))) {
                klazz.addMethod(
                        MethodSpec.methodBuilder("parseDate")
                                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                                .returns(Date.class)
                                .addParameter(String.class, "date")
                                .addException(Cl.MALFORMED_DATA_EXCEPTION)
                                .addCode(
                                        CodeBlock.builder()
                                                .beginControlFlow("try")
                                                .addStatement("return $T.parse(date, new $T(0))", Cl.GISO8601UTILS, ParsePosition.class)
                                                .nextControlFlow("catch ($T e)", ParseException.class)
                                                .addStatement("throw new $T(\"Failed Parsing '\" + date + \"' as Date\", e)", Cl.MALFORMED_DATA_EXCEPTION)
                                                .endControlFlow()
                                                .build()
                                )
                                .build()
                );
            }

            code.add("parseDate(reader.nextString())");
        }
    }
}
