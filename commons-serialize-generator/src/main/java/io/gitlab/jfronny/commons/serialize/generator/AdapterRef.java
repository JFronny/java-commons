package io.gitlab.jfronny.commons.serialize.generator;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public record AdapterRef(TypeElement in, TypeMirror[] targets, boolean nullSafe) {
}
