package io.gitlab.jfronny.commons.serialize.generator.adapter.impl;

import com.palantir.javapoet.TypeName;
import io.gitlab.jfronny.commons.serialize.generator.Cl;
import io.gitlab.jfronny.commons.serialize.generator.adapter.AdapterAdapter;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.DeclaredType;
import java.util.List;
import java.util.Map;

public class DeclaredAdapter extends AdapterAdapter<DeclaredAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends AdapterAdapter<Hydrated>.Hydrated {
        private DeclaredType typeAdapterClass;

        @Override
        public boolean applies() {
            return typeAdapterClass != null;
        }

        @Override
        protected void afterHydrate() {
            this.typeAdapterClass = findTypeAdapterClass(annotations);
        }

        @Override
        protected TypeName createAdapter(String typeAdapterName) {
            return TypeName.get(typeAdapterClass);
        }

        private static DeclaredType findTypeAdapterClass(List<? extends AnnotationMirror> annotations) {
            for (AnnotationMirror annotation : annotations) {
                String typeName = annotation.getAnnotationType().toString();
                if (typeName.equals(Cl.GWITH.toString())) {
                    Map<? extends ExecutableElement, ? extends AnnotationValue> elements = annotation.getElementValues();
                    if (!elements.isEmpty()) {
                        AnnotationValue value = elements.values().iterator().next();
                        return (DeclaredType) value.getValue();
                    }
                }
            }
            return null;
        }
    }
}
