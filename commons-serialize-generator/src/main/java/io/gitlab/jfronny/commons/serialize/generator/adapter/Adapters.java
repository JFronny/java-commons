package io.gitlab.jfronny.commons.serialize.generator.adapter;

import com.palantir.javapoet.CodeBlock;
import com.palantir.javapoet.TypeSpec;
import com.palantir.javapoet.TypeVariableName;
import io.gitlab.jfronny.commons.serialize.generator.AdapterRef;
import io.gitlab.jfronny.commons.serialize.generator.SerializableClass;
import io.gitlab.jfronny.commons.serialize.generator.adapter.impl.*;
import io.gitlab.jfronny.commons.serialize.generator.core.value.Property;

import javax.annotation.processing.Messager;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class Adapters {
    public static final List<Adapter<?>> ADAPTERS = List.of(
            new DeclaredAdapter(),
            new InferredAdapter(),
            new PrimitiveAdapter(),
            new StringAdapter(),
            new DateAdapter(),
            new EnumAdapter(),
            new ArrayAdapter(),
            new CollectionAdapter(),
            new MapAdapter(),
            new OtherSerializableAdapter(),
            new ReflectAdapter()
    );

    public static void generateRead(Property<?> prop, TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> otherAdapters, Set<AdapterRef> refs, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message) {
        withAdapter(prop, klazz, code, typeVariables, otherAdapters, refs, exceptionType, readerType, writerType, message, Adapter.Hydrated::generateRead);
    }

    public static void generateWrite(Property<?> prop, TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> otherAdapters, Set<AdapterRef> refs, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message, Runnable writeGet) {
        withAdapter(prop, klazz, code, typeVariables, otherAdapters, refs, exceptionType, readerType, writerType, message, adapter -> adapter.generateWrite(writeGet));
    }

    private static void withAdapter(Property<?> prop, TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> otherAdapters, Set<AdapterRef> refs, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message, Consumer<Adapter<?>.Hydrated> action) {
        withAdapter(klazz, code, typeVariables, otherAdapters, refs, prop.getType(), prop.getName(), prop.getAnnotations(), prop.getElement(), exceptionType, readerType, writerType, message, action);
    }

    public static void generateRead(TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> other, Set<AdapterRef> refs, TypeMirror type, String propName, List<? extends AnnotationMirror> annotations, Element sourceElement, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message) {
        withAdapter(klazz, code, typeVariables, other, refs, type, propName, annotations, sourceElement, exceptionType, readerType, writerType, message, Adapter.Hydrated::generateRead);
    }

    public static void generateWrite(TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> other, Set<AdapterRef> refs, TypeMirror type, String propName, List<? extends AnnotationMirror> annotations, Element sourceElement, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message, Runnable writeGet) {
        withAdapter(klazz, code, typeVariables, other, refs, type, propName, annotations, sourceElement, exceptionType, readerType, writerType, message, adapter -> adapter.generateWrite(writeGet));
    }

    private static void withAdapter(TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> other, Set<AdapterRef> refs, TypeMirror type, String propName, List<? extends AnnotationMirror> annotations, Element sourceElement, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType, Messager message, Consumer<Adapter<?>.Hydrated> action) {
        for (Adapter<?> adapter : Adapters.ADAPTERS) {
            Adapter<?>.Hydrated hydrated = adapter.hydrate(klazz, code, typeVariables, other, refs, type, propName, annotations, sourceElement, exceptionType, readerType, writerType);
            if (hydrated.applies()) {
                action.accept(hydrated);
                return;
            }
        }
        message.printMessage(Diagnostic.Kind.ERROR, "Could not find applicable adapter for property " + propName, sourceElement);
    }
}
