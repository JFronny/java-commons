package io.gitlab.jfronny.commons.serialize.generator.adapter.impl;

import com.palantir.javapoet.FieldSpec;
import com.palantir.javapoet.MethodSpec;
import com.palantir.javapoet.TypeSpec;
import io.gitlab.jfronny.commons.throwable.Try;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.util.List;

public class PoetUtils {
    private static final @Nullable Field methodSpecs;
    private static final @Nullable Field fieldSpecs;

    static {
        methodSpecs = Try.orElse(() -> TypeSpec.Builder.class.getDeclaredField("methodSpecs"), t -> null);
        fieldSpecs = Try.orElse(() -> TypeSpec.Builder.class.getDeclaredField("fieldSpecs"), t -> null);
        Try.orThrow(() -> {
            if (methodSpecs != null) methodSpecs.setAccessible(true);
            if (methodSpecs != null) fieldSpecs.setAccessible(true);
        });
    }

    public static List<MethodSpec> methodSpecs(TypeSpec.Builder builder) {
        return methodSpecs == null ? builder.build().methodSpecs() : (List<MethodSpec>) Try.orThrow(() -> methodSpecs.get(builder));
    }

    public static List<FieldSpec> fieldSpecs(TypeSpec.Builder builder) {
        return fieldSpecs == null ? builder.build().fieldSpecs() : (List<FieldSpec>) Try.orThrow(() -> fieldSpecs.get(builder));
    }
}
