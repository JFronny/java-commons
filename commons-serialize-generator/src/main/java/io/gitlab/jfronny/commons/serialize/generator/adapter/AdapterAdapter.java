package io.gitlab.jfronny.commons.serialize.generator.adapter;

import com.palantir.javapoet.FieldSpec;
import com.palantir.javapoet.ParameterizedTypeName;
import com.palantir.javapoet.TypeName;
import com.palantir.javapoet.TypeVariableName;
import io.gitlab.jfronny.commons.serialize.generator.Cl;
import io.gitlab.jfronny.commons.serialize.generator.core.TypeHelper;

import javax.lang.model.type.TypeMirror;
import java.lang.reflect.ParameterizedType;
import java.util.Iterator;
import java.util.List;

import static io.gitlab.jfronny.commons.serialize.generator.adapter.impl.PoetUtils.fieldSpecs;

public abstract class AdapterAdapter<T extends AdapterAdapter<T>.Hydrated> extends Adapter<T> {
    public abstract class Hydrated extends Adapter<T>.Hydrated {
        @Override
        public void generateWrite(Runnable writeGet) {
            Object adapter = getAdapter();
            code.add("$" + (adapter instanceof String ? "L" : "T") + ".serialize(", adapter);
            writeGet.run();
            code.add(", writer);\n");
        }

        @Override
        public void generateRead() {
            Object adapter = getAdapter();
            code.add("$" + (adapter instanceof String ? "L" : "T") + ".deserialize(reader)", adapter);
        }

        private Object getAdapter() {
            for (FieldSpec spec : fieldSpecs(klazz)) {
                if (spec.name().equals(adapterName)) return adapterName;
            }
            return createAdapter(adapterName);
        }

        protected abstract Object createAdapter(String name);

        protected void appendFieldTypeToken(boolean allowClassType) {
            TypeName typeName = TypeName.get(type);

            if (TypeHelper.isComplexType(type, typeUtils)) {
                TypeName typeTokenType = ParameterizedTypeName.get(Cl.TYPE_TOKEN, typeName);
                List<? extends TypeMirror> typeParams = TypeHelper.getGenericTypes(type);
                if (typeParams.isEmpty()) {
                    code.add("new $T() {}", typeTokenType);
                } else {
                    code.add("($T) $T.getParameterized($T.class, ", typeTokenType, Cl.TYPE_TOKEN, typeUtils.erasure(type));
                    for (Iterator<? extends TypeMirror> iterator = typeParams.iterator(); iterator.hasNext(); ) {
                        TypeMirror typeParam = iterator.next();
                        int typeIndex = typeVariables.indexOf(TypeVariableName.get(typeParam.toString()));
                        code.add("(($T)typeToken.getType()).getActualTypeArguments()[$L]", ParameterizedType.class, typeIndex);
                        if (iterator.hasNext()) {
                            code.add(", ");
                        }
                    }
                    code.add(")");
                }
            } else if (TypeHelper.isGenericType(type)) {
                TypeName typeTokenType = ParameterizedTypeName.get(Cl.TYPE_TOKEN, typeName);
                int typeIndex = typeVariables.indexOf(TypeVariableName.get(type.toString()));
                code.add("($T) $T.get((($T)typeToken.getType()).getActualTypeArguments()[$L])",
                        typeTokenType, Cl.TYPE_TOKEN, ParameterizedType.class, typeIndex);
            } else {
                if (allowClassType) {
                    code.add("$T.class", typeName);
                } else {
                    code.add("$T.get($T.class)", Cl.TYPE_TOKEN, typeName);
                }
            }
        }
    }
}
