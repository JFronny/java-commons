package io.gitlab.jfronny.commons.serialize.generator;

import com.palantir.javapoet.ClassName;

public class Cl {
    // Core
    public static final ClassName GSON_ELEMENT = ClassName.get("io.gitlab.jfronny.commons.serialize.emulated", "DataElement");
    public static final ClassName SERIALIZE_WRITER = ClassName.get("io.gitlab.jfronny.commons.serialize", "SerializeWriter");
    public static final ClassName SERIALIZE_READER = ClassName.get("io.gitlab.jfronny.commons.serialize", "SerializeReader");
    public static final ClassName EMULATED_READER = ClassName.get("io.gitlab.jfronny.commons.serialize.emulated", "EmulatedReader");
    public static final ClassName EMULATED_WRITER = ClassName.get("io.gitlab.jfronny.commons.serialize.emulated", "EmulatedWriter");
    public static final ClassName GSON_TOKEN = ClassName.get("io.gitlab.jfronny.commons.serialize", "Token");
    public static final ClassName SERIALIZED_NAME = ClassName.get("io.gitlab.jfronny.commons.serialize.annotations", "SerializedName");
    public static final ClassName IGNORE = ClassName.get("io.gitlab.jfronny.commons.serialize.annotations", "Ignore");
    public static final ClassName MALFORMED_DATA_EXCEPTION = ClassName.get("io.gitlab.jfronny.commons.serialize", "MalformedDataException");
    public static final ClassName TRANSPORT = ClassName.get("io.gitlab.jfronny.commons.serialize", "Transport");

    // Databind Interop
    public static final ClassName TYPE_ADAPTER = ClassName.get("io.gitlab.jfronny.commons.serialize.databind.api", "TypeAdapter");
    public static final ClassName TYPE_TOKEN = ClassName.get("io.gitlab.jfronny.commons.serialize.databind.api", "TypeToken");
    public static final ClassName SERIALIZER_FOR = ClassName.get("io.gitlab.jfronny.commons.serialize.databind.api", "SerializerFor");
    public static final ClassName OBJECT_MAPPER = ClassName.get("io.gitlab.jfronny.commons.serialize.databind", "ObjectMapper");

    // Custom Utils
    public static final ClassName GISO8601UTILS = ClassName.get("io.gitlab.jfronny.commons.serialize", "ISO8601Utils");
    public static final ClassName GCOMMENT = ClassName.get("io.gitlab.jfronny.commons.serialize.generator.annotations", "GComment");
    public static final ClassName GWITH = ClassName.get("io.gitlab.jfronny.commons.serialize.generator.annotations", "GWith");

    // Manifold Interop
    public static final ClassName MANIFOLD_EXTENSION = ClassName.get("manifold.ext.rt.api", "Extension");
    public static final ClassName MANIFOLD_THIS = ClassName.get("manifold.ext.rt.api", "This");
}
