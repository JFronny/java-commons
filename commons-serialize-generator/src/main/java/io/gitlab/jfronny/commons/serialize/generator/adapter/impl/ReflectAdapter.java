package io.gitlab.jfronny.commons.serialize.generator.adapter.impl;

import com.palantir.javapoet.CodeBlock;
import com.palantir.javapoet.FieldSpec;
import com.palantir.javapoet.ParameterizedTypeName;
import com.palantir.javapoet.TypeName;
import io.gitlab.jfronny.commons.serialize.generator.Cl;
import io.gitlab.jfronny.commons.serialize.generator.adapter.AdapterAdapter;

import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;

public class ReflectAdapter extends AdapterAdapter<ReflectAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends AdapterAdapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return !options.containsKey("serializeProcessorNoReflect");
        }

        @Override
        protected String createAdapter(String typeAdapterName) {
            message.printMessage(Diagnostic.Kind.WARNING, "Falling back to adapter detection for unsupported type " + type, sourceElement);
            TypeName typeAdapterType = ParameterizedTypeName.get(Cl.TYPE_ADAPTER, typeName);
            CodeBlock.Builder block = CodeBlock.builder();
            block.add("$T.CURRENT.get().getAdapter(", Cl.OBJECT_MAPPER);
            appendFieldTypeToken(true);
            block.add(")");
            klazz.addField(
                    FieldSpec.builder(typeAdapterType, typeAdapterName)
                            .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                            .initializer(block.build())
                            .build()
            );
            return typeAdapterName;
        }
    }
}
