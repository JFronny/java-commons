package io.gitlab.jfronny.commons.serialize.generator.adapter.impl;

import com.palantir.javapoet.ClassName;
import com.palantir.javapoet.CodeBlock;
import com.palantir.javapoet.FieldSpec;
import com.palantir.javapoet.TypeName;
import io.gitlab.jfronny.commons.serialize.generator.AdapterRef;
import io.gitlab.jfronny.commons.serialize.generator.adapter.AdapterAdapter;
import io.gitlab.jfronny.commons.tuple.Tuple;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.Set;

public class InferredAdapter extends AdapterAdapter<InferredAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends AdapterAdapter<Hydrated>.Hydrated {
        private TypeElement typeAdapterClass;

        @Override
        public boolean applies() {
            return typeAdapterClass != null;
        }

        @Override
        protected void afterHydrate() {
            var tmp = findTypeAdapterClass(type, refs);
            // Ignore nullSafe for now
            if (tmp != null) {
                this.typeAdapterClass = tmp.left();
            }
        }

        @Override
        protected String createAdapter(String typeAdapterName) {
            CodeBlock.Builder block = CodeBlock.builder();
            block.add("new $T()", typeAdapterClass);
            klazz.addField(
                    FieldSpec.builder(ClassName.get(typeAdapterClass), typeAdapterName)
                            .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                            .initializer(block.build())
                            .build()
            );
            return typeAdapterName;
        }

        private static Tuple<TypeElement, Boolean> findTypeAdapterClass(TypeMirror type, Set<AdapterRef> refs) {
            TypeName tn = TypeName.get(type);
            for (AdapterRef ref : refs) {
                for (TypeMirror target : ref.targets()) {
                    if (tn.equals(TypeName.get(target))) {
                        return Tuple.of(ref.in(), ref.nullSafe());
                    }
                }
            }
            return null;
        }
    }
}
