package io.gitlab.jfronny.commons.serialize.generator.adapter;

import com.palantir.javapoet.CodeBlock;
import com.palantir.javapoet.TypeName;
import com.palantir.javapoet.TypeSpec;
import com.palantir.javapoet.TypeVariableName;
import io.gitlab.jfronny.commons.serialize.generator.AdapterRef;
import io.gitlab.jfronny.commons.serialize.generator.SerializableClass;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class Adapter<T extends Adapter<T>.Hydrated> {
    protected Messager message;
    protected Types typeUtils;
    protected Map<String, String> options;

    public abstract T instantiate();

    public void init(ProcessingEnvironment env) {
        this.message = env.getMessager();
        this.typeUtils = env.getTypeUtils();
        this.options = env.getOptions();
    }

    public final T hydrate(TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> other, Set<AdapterRef> refs, TypeMirror type, String propName, List<? extends AnnotationMirror> annotations, Element sourceElement, TypeVariableName exceptionType, TypeVariableName readerType, TypeVariableName writerType) {
        T instance = instantiate();
        instance.klazz = klazz;
        instance.typeVariables = typeVariables;
        instance.other = other;
        instance.refs = refs;
        instance.type = type;
        instance.code = code;
        instance.unboxedType = instance.unbox(type);
        instance.name = propName;
        instance.argName = "_" + propName;
        instance.adapterName = "adapter_" + propName;
        instance.typeName = TypeName.get(type).box();
        instance.annotations = annotations;
        instance.sourceElement = sourceElement;
        instance.exceptionType = exceptionType;
        instance.readerType = readerType;
        instance.writerType = writerType;
        instance.afterHydrate();
        return instance;
    }

    public abstract class Hydrated {
        protected TypeSpec.Builder klazz;
        protected List<TypeVariableName> typeVariables;
        protected Set<SerializableClass> other;
        protected Set<AdapterRef> refs;
        protected TypeMirror type;
        protected TypeMirror unboxedType;
        protected CodeBlock.Builder code;
        protected String name;
        protected String argName;
        protected String adapterName;
        protected TypeName typeName;
        protected List<? extends AnnotationMirror> annotations;
        protected Element sourceElement;
        protected TypeVariableName exceptionType;
        protected TypeVariableName readerType;
        protected TypeVariableName writerType;

        public abstract boolean applies();
        public abstract void generateWrite(Runnable writeGet);
        public abstract void generateRead();
        protected void afterHydrate() {}

        protected void generateRead(CodeBlock.Builder code, TypeMirror type, String name, List<? extends AnnotationMirror> annotations) {
            Adapters.generateRead(klazz, code, typeVariables, other, refs, type, name, annotations, sourceElement, exceptionType, readerType, writerType, message);
        }

        protected void generateWrite(CodeBlock.Builder code, TypeMirror type, String name, List<? extends AnnotationMirror> annotations, Runnable writeGet) {
            Adapters.generateWrite(klazz, code, typeVariables, other, refs, type, name, annotations, sourceElement, exceptionType, readerType, writerType, message, writeGet);
        }

        protected TypeMirror unbox(TypeMirror type) {
            try {
                return typeUtils.unboxedType(type);
            } catch (IllegalArgumentException e) {
                return type;
            }
        }
    }
}
