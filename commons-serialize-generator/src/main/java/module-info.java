module io.gitlab.jfronny.commons.serialize.generator {
    requires com.palantir.javapoet;
    requires java.compiler;
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.serialize.generator.core;
    requires io.gitlab.jfronny.commons.serialize.generator.annotations;
    requires io.gitlab.jfronny.commons.serialize.databind.api;
    provides javax.annotation.processing.Processor with io.gitlab.jfronny.commons.serialize.generator.SerializeGeneratorProcessor;
}