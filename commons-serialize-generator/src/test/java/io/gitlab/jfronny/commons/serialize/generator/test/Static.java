package io.gitlab.jfronny.commons.serialize.generator.test;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable(isStatic = true, configure = Main.Configuration.class)
public class Static {
    public boolean nonStatic;
    public static boolean joe;

    public static void setNine(boolean nine) {
        joe = !nine;
    }

    public static String getSerde() {
        return "serde? " + joe;
    }
}
