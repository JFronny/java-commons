package io.gitlab.jfronny.commons.serialize.generator.test;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;

@SerializerFor(targets = Main.CustomValue.class)
public class ExampleAdapter2 extends TypeAdapter<Main.CustomValue> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Main.CustomValue value, Writer writer) throws TEx, MalformedDataException {
        writer.value(value.nein);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Main.CustomValue deserialize(Reader reader) throws TEx, MalformedDataException {
        Main.CustomValue cv = new Main.CustomValue();
        cv.nein = reader.nextString();
        return cv;
    }
}
