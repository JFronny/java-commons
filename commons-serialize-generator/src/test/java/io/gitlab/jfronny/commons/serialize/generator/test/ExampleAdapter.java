package io.gitlab.jfronny.commons.serialize.generator.test;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;

public class ExampleAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(boolean bool, Writer writer) throws TEx {
        writer.value(!bool);
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> boolean deserialize(Reader reader) throws TEx {
        return !reader.nextBoolean();
    }
}
