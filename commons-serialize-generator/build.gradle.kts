import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    implementation(projects.commonsSerializeGeneratorCore)
    implementation(projects.commonsSerializeGeneratorAnnotations)
    implementation(projects.commonsSerializeDatabindApi)
    implementation(libs.java.poet)

    testAnnotationProcessor(sourceSets.main.get().output)
    configurations.testAnnotationProcessor.get().extendsFrom(configurations.implementation.get())
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-serialize-generator"

            from(components["java"])
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize-generator-core/$version/raw", projects.commonsSerializeGeneratorCore)
}
