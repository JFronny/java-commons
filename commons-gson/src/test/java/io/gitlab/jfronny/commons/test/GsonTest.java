package io.gitlab.jfronny.commons.test;

import io.gitlab.jfronny.commons.ComparableVersion;
import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.Serializer;
import io.gitlab.jfronny.commons.serialize.annotations.Ignore;
import io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonHolders;
import io.gitlab.jfronny.gson.reflect.TypeToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GsonTest {
    @BeforeAll
    static void setup() {
        GsonHolders.registerSerializer();
    }

    @Test
    void gsonIgnore() throws IOException {
        assertEquals("null", Serializer.getInstance().serialize(new ExampleTypeHidden()));
        assertEquals("{\"shouldShow\":\"Yes\"}", Serializer.getInstance().serialize(new TransitiveHiddenType()));
        assertEquals("{\"id\":\"Yes\",\"shown\":{\"shouldShow\":\"Yes\"}}", Serializer.getInstance().serialize(new ExampleType()));
    }

    @Test
    void comparableVersionAdapter() throws IOException {
        assertEquals("\"1.0.0\"", Serializer.getInstance().serialize(new ComparableVersion("1.0.0")));
        assertDoesNotThrow(() -> assertEquals(new ComparableVersion("1.0.0"), Serializer.getInstance().deserialize("\"1.0.0\"", ComparableVersion.class)));
    }

    @Test
    void gsonString2ObjectMap() throws IOException {
        String2ObjectMap<SimpleRecord> map = new String2ObjectMap<>();
        map.put("Some String", new SimpleRecord("Aaee", 12));
        map.put("Some", new SimpleRecord("Aeio", 4));
        map.put("Some Test", new SimpleRecord("U", 64));
        final String serialized = "{\"Some\":{\"someValue\":\"Aeio\",\"yes\":4},\"Some String\":{\"someValue\":\"Aaee\",\"yes\":12},\"Some Test\":{\"someValue\":\"U\",\"yes\":64}}";
        assertEquals(serialized, Serializer.getInstance().serialize(map));
        assertEquals(map, Serializer.getInstance().deserialize(serialized, new TypeToken<String2ObjectMap<SimpleRecord>>() {}.getType()));
    }

    public static final class SimpleRecord {
        public final String someValue;
        public final int yes;

        public SimpleRecord(String someValue, int yes) {
            this.someValue = someValue;
            this.yes = yes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SimpleRecord that)) return false;
            return yes == that.yes && Objects.equals(someValue, that.someValue);
        }

        @Override
        public int hashCode() {
            return Objects.hash(someValue, yes);
        }
    }

    public static class ExampleType {
        String id = "Yes";
        @Ignore
        String other = "No";
        transient Integer one = 1;
        ExampleTypeHidden type = new ExampleTypeHidden();
        TransitiveHiddenType shown = new TransitiveHiddenType();
    }

    @Ignore
    public static class ExampleTypeHidden {
        String someMetadata = "No";
    }

    public static class TransitiveHiddenType extends ExampleTypeHidden {
        String shouldShow = "Yes";
    }
}
