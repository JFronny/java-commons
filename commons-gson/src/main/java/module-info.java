module io.gitlab.jfronny.commons.serialize.gson {
    uses io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonTransformer;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.serialize;
    requires transitive io.gitlab.jfronny.gson;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.serialize.gson.api.v2;
    provides io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonTransformer with
            io.gitlab.jfronny.commons.serialize.gson.impl.DefaultTransformer;
}