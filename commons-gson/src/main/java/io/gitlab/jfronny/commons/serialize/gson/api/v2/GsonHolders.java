package io.gitlab.jfronny.commons.serialize.gson.api.v2;

import io.gitlab.jfronny.commons.ref.WeakSet;
import io.gitlab.jfronny.commons.Serializer;
import io.gitlab.jfronny.commons.serialize.gson.impl.GsonHolderSerializer;
import org.jetbrains.annotations.ApiStatus;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Stream;

public class GsonHolders {
    @ApiStatus.Internal
    static GsonTransformer PAST_MODIFICATIONS = GsonTransformer.IDENTITY;
    @ApiStatus.Internal
    static final WeakSet<GsonHolder> KNOWN_INSTANCES = new WeakSet<>();

    public static final GsonHolder API = new GsonHolder();
    public static final GsonHolder CONFIG = new GsonHolder().apply(b -> b
            .setOmitQuotes()
            .serializeSpecialFloatingPointValues()
            .serializeNulls()
            .setPrettyPrinting()
    );

    public static void applyTransform(GsonTransformer transformer) {
        synchronized (KNOWN_INSTANCES) {
            PAST_MODIFICATIONS = GsonTransformer.concat(List.of(PAST_MODIFICATIONS, transformer));
            for (GsonHolder holder : KNOWN_INSTANCES) holder.apply(transformer);
        }
    }

    public static void applyTransforms(List<GsonTransformer> transformers) {
        synchronized (KNOWN_INSTANCES) {
            PAST_MODIFICATIONS = GsonTransformer.concat(Stream.concat(
                    Stream.of(PAST_MODIFICATIONS),
                    transformers.stream()
            ).toList());
            for (GsonHolder holder : KNOWN_INSTANCES)
                for (GsonTransformer transformer : transformers)
                    holder.apply(transformer);
        }
    }

    static {
        applyTransforms(ServiceLoader.load(GsonTransformer.class)
                .stream()
                .map(ServiceLoader.Provider::get)
                .toList());
    }

    public static void registerSerializer() {
        Serializer.setInstance(new GsonHolderSerializer(API));
    }
}
