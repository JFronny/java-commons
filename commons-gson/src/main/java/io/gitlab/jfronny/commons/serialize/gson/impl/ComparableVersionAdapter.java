package io.gitlab.jfronny.commons.serialize.gson.impl;

import io.gitlab.jfronny.commons.ComparableVersion;
import io.gitlab.jfronny.gson.*;

import java.lang.reflect.Type;

/**
 * A json serializer and deserializer for {@code ComparableVersion}
 * The json representation is a string
 */
public class ComparableVersionAdapter implements JsonDeserializer<ComparableVersion>, JsonSerializer<ComparableVersion> {
    @Override
    public ComparableVersion deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonPrimitive() && json.getAsJsonPrimitive().isString())
            return new ComparableVersion(json.getAsString());
        else throw new JsonParseException("Expected Version to be a string");
    }

    @Override
    public JsonElement serialize(ComparableVersion src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
    }
}
