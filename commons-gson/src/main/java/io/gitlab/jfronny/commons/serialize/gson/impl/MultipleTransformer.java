package io.gitlab.jfronny.commons.serialize.gson.impl;

import io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonTransformer;
import io.gitlab.jfronny.gson.GsonBuilder;

import java.util.List;

public record MultipleTransformer(List<GsonTransformer> transformers) implements GsonTransformer {
    @Override
    public void apply(GsonBuilder builder) {
        for (GsonTransformer transformer : transformers) {
            transformer.apply(builder);
        }
    }
}
