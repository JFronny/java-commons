package io.gitlab.jfronny.commons.serialize.gson.api.v2;

import io.gitlab.jfronny.gson.*;

import java.lang.reflect.Type;

/**
 * Holds a common instance of the Gson object.
 * Supports registering type adapters/etc. as needed
 */
public class GsonHolder {
    private final GsonBuilder builder = new GsonBuilder();

    private boolean clean = false;
    private Gson gson;

    public GsonHolder() {
        synchronized (GsonHolders.KNOWN_INSTANCES) {
            GsonHolders.KNOWN_INSTANCES.add(this);
            GsonHolders.PAST_MODIFICATIONS.apply(builder);
        }
    }

    /**
     * Get the current gson instance or build it if needed
     *
     * @return The Gson instance
     */
    public Gson getGson() {
        if (!clean) {
            gson = builder.create();
            clean = true;
        }
        return gson;
    }

    /**
     * Register a type adapter and mark the gson instance as unclean
     *
     * @param type        The type for which to register the adapter
     * @param typeAdapter The adapter type
     */
    public GsonHolder registerTypeAdapter(Type type, Object typeAdapter) {
        builder.registerTypeAdapter(type, typeAdapter);
        clean = false;
        return this;
    }

    /**
     * Register a type adapter factory and mark the gson instance as unclean
     *
     * @param factory The factory to register
     */
    public GsonHolder registerTypeAdapterFactory(TypeAdapterFactory factory) {
        builder.registerTypeAdapterFactory(factory);
        clean = false;
        return this;
    }

    /**
     * Run a function on the builder for modifying it and mark the gson instance as unclean
     *
     * @param transformer The transformer to apply
     */
    public GsonHolder apply(GsonTransformer transformer) {
        transformer.apply(builder);
        clean = false;
        return this;
    }
}
