package io.gitlab.jfronny.commons.serialize.gson.impl;

import io.gitlab.jfronny.commons.ComparableVersion;
import io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonTransformer;
import io.gitlab.jfronny.gson.GsonBuilder;

import java.lang.reflect.Modifier;

public class DefaultTransformer implements GsonTransformer {
    @Override
    public void apply(GsonBuilder builder) {
        builder.registerTypeAdapter(ComparableVersion.class, new ComparableVersionAdapter())
                .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.PRIVATE)
                .setExclusionStrategies(new GsonIgnoreExclusionStrategy())
                .setLenient();
    }
}
