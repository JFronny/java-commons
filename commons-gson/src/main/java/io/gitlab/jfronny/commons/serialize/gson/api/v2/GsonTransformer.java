package io.gitlab.jfronny.commons.serialize.gson.api.v2;

import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.serialize.gson.impl.MultipleTransformer;
import io.gitlab.jfronny.gson.GsonBuilder;
import io.gitlab.jfronny.gson.TypeAdapterFactory;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Represents a transformation for {@code GsonBuilder} objects handled by {@code GsonHolder}.
 * Transformers may be applied to individual holders, manually to all holders via {@code GsonHolders.applyTransforms}
 * or automatically through the service loader mechanism.
 */
public interface GsonTransformer {
    GsonTransformer IDENTITY = R::nop;

    static GsonTransformer byConsumer(Consumer<GsonBuilder> source) {
        return source::accept;
    }

    static GsonTransformer byTypeAdapter(Type type, Object typeAdapter) {
        return b -> b.registerTypeAdapter(type, typeAdapter);
    }

    static GsonTransformer byTypeAdapterFactory(TypeAdapterFactory factory) {
        return b -> b.registerTypeAdapterFactory(factory);
    }

    static GsonTransformer concat(List<GsonTransformer> transformers) {
        transformers = transformers.stream()
                .flatMap(t -> t instanceof MultipleTransformer mt
                        ? mt.transformers().stream()
                        : Stream.of(t))
                .filter(s -> s != IDENTITY)
                .toList();
        return switch (transformers.size()) {
            case 0 -> IDENTITY;
            case 1 -> transformers.get(0);
            default -> new MultipleTransformer(transformers);
        };
    }

    void apply(GsonBuilder builder);
}
