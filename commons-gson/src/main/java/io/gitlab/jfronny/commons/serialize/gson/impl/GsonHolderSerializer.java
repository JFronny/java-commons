package io.gitlab.jfronny.commons.serialize.gson.impl;

import io.gitlab.jfronny.commons.Serializer;
import io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonHolder;
import io.gitlab.jfronny.gson.*;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;

public record GsonHolderSerializer(GsonHolder holder) implements Serializer {
    @Override
    public String serialize(Object object) throws IOException {
        try {
            return getGson().toJson(object);
        } catch (JsonIOException | JsonSyntaxException e) {
            throw generateException(e);
        }
    }

    @Override
    public void serialize(Object object, Appendable writer) throws IOException {
        try {
            getGson().toJson(object, writer);
        } catch (Exception e) {
            throw generateException(e);
        }
    }

    @Override
    public <T> T deserialize(Reader source, Class<T> typeOfT) throws IOException {
        try {
            return getGson().fromJson(source, typeOfT);
        } catch (Exception e) {
            throw generateException(e);
        }
    }

    @Override
    public <T> T deserialize(Reader source, Type typeOfT) throws IOException {
        if (typeOfT instanceof Class<?> k) {
            //noinspection unchecked
            return (T) deserialize(source, k);
        }
        try {
            return getGson().fromJson(source, typeOfT);
        } catch (Exception e) {
            throw generateException(e);
        }
    }

    @Override
    public <T> T deserialize(String source, Class<T> typeOfT) throws IOException {
        try {
            return getGson().fromJson(source, typeOfT);
        } catch (Exception e) {
            throw generateException(e);
        }
    }

    @Override
    public <T> T deserialize(String source, Type typeOfT) throws IOException {
        if (typeOfT instanceof Class<?> k) {
            //noinspection unchecked
            return (T) deserialize(source, k);
        }
        try {
            return getGson().fromJson(source, typeOfT);
        } catch (Exception e) {
            throw generateException(e);
        }
    }

    private IOException generateException(Exception e) {
        if (e instanceof IOException io) return io;
        Throwable cause = e.getCause();
        String message = e.getMessage();
        if (cause == null) return new SerializeException(message);
        boolean lackingMessage = message == null || message.equals(cause.toString());
        if (e instanceof JsonIOException) {
            if (lackingMessage) {
                if (cause instanceof IOException io) return io;
                return new SerializeException(cause);
            }
            return new SerializeException(message, cause);
        }
        if (lackingMessage) return new SerializeException(cause);
        return new SerializeException(message, cause);
    }

    private Gson getGson() {
        return holder.getGson();
    }

    @Override
    public String getFormatMime() {
        return "application/json";
    }
}
