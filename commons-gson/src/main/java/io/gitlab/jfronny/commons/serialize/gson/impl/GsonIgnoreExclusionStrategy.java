package io.gitlab.jfronny.commons.serialize.gson.impl;

import io.gitlab.jfronny.commons.serialize.annotations.Ignore;
import io.gitlab.jfronny.gson.*;

/**
 * An exclusion strategy that ignores fields with the GsonIgnore attribute
 */
public class GsonIgnoreExclusionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return clazz.isAnnotationPresent(Ignore.class);
    }

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(Ignore.class) != null || shouldSkipClass(f.getDeclaringClass());
    }
}
