import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    api(projects.muscriptAst)
    api(projects.muscriptDataDynamic)
    api(projects.muscriptDataAdditional)

    testImplementation(libs.junit.jupiter.api)
    testImplementation(projects.muscriptParser)
    testImplementation(projects.muscriptSerialize)
    testRuntimeOnly(libs.junit.jupiter.engine)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "muscript-runtime"

            from(components["java"])
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/muscript-ast/$version/raw", projects.muscriptAst)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/muscript-data-dynamic/$version/raw", projects.muscriptDataDynamic)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/muscript-data-additional/$version/raw", projects.muscriptDataAdditional)
}
