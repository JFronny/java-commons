# μScript Standard Library

The purpose of this document is to be a guide to the standard library provided by μScript.
It is not intended to explain the syntax of the language or provide information on its history, implementation, or how to embed it in programs.

For information on that, look at the [language guide](src/test/resources/example.md) and the [developer info](README.md).

## Types
| name     | description                                                                                         |
|----------|-----------------------------------------------------------------------------------------------------|
| number   | Equivalent to a java double, the only number type in muScript                                       |
| bool     | A boolean. Either true or false                                                                     |
| null     | Equivalent to null in java or Unit in kotlin                                                        |
| string   | A sequence of characters                                                                            |
| list     | An ordered sequence of items of any type                                                            |
| object   | A mapping between keys (string) and values (any)                                                    |
| callable | A value that can be invoked via `()`                                                                |
| date     | Represents a calendar date. Can be represented as string, number (days since 01-01-1970) and object |
| time     | Represents a time of day. Can be represented as a string, number (second of day) and object         |
| enum     | Details below                                                                                       |
| try      | Details below                                                                                       |

### Enum
A map between names and values of any type. Can also be represented as a list of names.
One entry may be selected, its index will be accessible as the number representation and its name as the string representation of the enum.

### Try
An `object` returned by the `try` function. Contains the following entries:
- `result`: The result of executing the closure, the result of `catch` if it failed and `catch` was called, or `null`
- `catch(handler: {string -> any})`: If the execution of the closure failed,
  `handler` will be executed with the error message and its result will be provided as `result`.
  No more than one `catch` may exist per `try`.

## Functions
The standard library also provides a number of default utility functions.
Below, these are listed alongside their arguments.
The syntax used for signatures in this list is not used elsewhere and not part of the language.

### Signature syntax
- If a parameter is suffixed with `?`, it can be omitted. If omission is equivalent to a default value, it will be listed with the default behind an equals sign
- If a parameter is suffixed with `...`, it will be variadic (support 0 or more arguments)
- If multiple types are possible, they are listed as `T1 / T2`, where T1 is preferred over T2
- If an object supports multiple representations, it will be denoted as `T1 & T2`
- Callables are represented like closures but with types instead of names

### Supported functions
| signature                                                                             | description                                                                                                                                                                       |
|---------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `round(value: number, decimalPlaces?: number = 0): number`                            | Rounds `value` while retaining `decimalPlaces` places after the comma                                                                                                             |
| `floor(value: number): number`                                                        | Returns the largest integer lower than or equal to `value`                                                                                                                        |
| `ceil(value: number): number`                                                         | Returns the smallest integer larger than or equal to `value`                                                                                                                      |
| `abs(value: number): number`                                                          | Returns the absolute value of `value` (always positive or zero)                                                                                                                   |
| `sin(radians: number): number`                                                        | Returns the sine of `radians`                                                                                                                                                     |
| `cos(radians: number): number`                                                        | Returns the cosine of `radians`                                                                                                                                                   |
| `tan(radians: number): number`                                                        | Returns the tangent of `radians`                                                                                                                                                  |
| `asin(value: number): number`                                                         | Returns the arcsine of `value`                                                                                                                                                    |
| `acos(value: number): number`                                                         | Returns the arccosine of `value`                                                                                                                                                  |
| `atan(value: number): number`                                                         | Returns the arctangent of `value`                                                                                                                                                 |
| `atan2(y: number, x: number): number`                                                 | Returns the arctangent of `y / x`                                                                                                                                                 |
| `pow(base: number, exponent: number): number`                                         | Returns `base` to the power of `exponent`                                                                                                                                         |
| `sqrt(value: number): number`                                                         | Returns the square root of `value`                                                                                                                                                |
| `cbrt(value: number): number`                                                         | Returns the cube root of `value`                                                                                                                                                  |
| `log(value: number): number`                                                          | Returns the natural logarithm of `value`                                                                                                                                          |
| `log10(value: number): number`                                                        | Returns the base 10 logarithm of `value`                                                                                                                                          |
| `log2(value: number): number`                                                         | Returns the base 2 logarithm of `value`                                                                                                                                           |
| `exp(value: number): number`                                                          | Returns `e` to the power of `value`                                                                                                                                               |
| `random(min?: number = 0, max?: number = 1): number`                                  | Returns a random number between `min` and `max`. If `min` is provided, `max` cannot be omitted.                                                                                   |
| `toUpper(value: string): string`                                                      | Converts all characters in `value` to upper case                                                                                                                                  |
| `toLower(value: string): string`                                                      | Converts all characters in `value` to lower case                                                                                                                                  |
| `contains(in: list / object / string, find: any): bool`                               | Checks whether `find` is an element of the list, a key of the object or a substring of the string provided as `in`                                                                |
| `replace(source: string, target: string, replacement: string): string`                | Replaces all instances of `target` in `source` with `replacement` and returns the result                                                                                          |
| `listOf(elements: any...): list`                                                      | Creates a list with the provided elements                                                                                                                                         |
| `len(toCheck: string / object / list): number`                                        | Gets the amount of characters in the string or entries in the object or list provided as `toCheck`                                                                                |
| `isEmpty(toCheck: object / list): bool`                                               | Checks whether the object or list provided as `toCheck` contain any entries                                                                                                       |
| `concat(lists: list...): list`                                                        | Returns a list containing all elements of all provided lists in their original order                                                                                              |
| `filter(in: list, with: {any -> bool}): list`                                         | Returns a list of all elements `el` of `in` for which `with(el)` returned true                                                                                                    |
| `allMatch(in: list, with: {any -> bool}): bool`                                       | Whether `with(el)` is true for all elements `el` of `in`                                                                                                                          |
| `anyMatch(in: list, with: {any -> bool}): bool`                                       | Whether `with(el)` is true for any element `el` of `in`                                                                                                                           |
| `map(in: list, with: {any -> any}): list`                                             | Returns a list of the results of applying `with` to all elements of `in`                                                                                                          |
| `flatMap(in: list, with: {any -> list}): list`                                        | Returns the concatenated list of the results of applying `with` to all elements of `in`                                                                                           |
| `fold(in: list, identity: any, operator: {any, any -> any}): any`                     | Runs `operator(previous, current)` for every element in `in` where `previous` is the previous result of `operator`, starting with `identity` and `current` is the current element |
| `forEach(in: list, with: {any -> any}): any`                                          | Runs `with` for each element in `in`, returning the last result or `null` for empty lists                                                                                         |
| `toObject(from: list, keyMapper: {any -> string}, valueMapper: {any -> any}): object` | Creates an object mapping the results of `keyMapper` to the results of `valueMapper` for every element in `from`                                                                  |
| `callableObject(source: object, action: callable): object & callable`                 | Returns an object equivalent to `source` that can be called, resulting in the invocation of `action`                                                                              |
| `enum(values: object, selected?: string): enum`                                       | Creates an enum with the values provided as `values`, optionally marking the one with the key `selected` as such                                                                  |
| `keys(ob: object): list`                                                              | Returns the list of names of entries of the object `ob`                                                                                                                           |
| `values(ob: object): list`                                                            | Returns the list of values of entries of the object `ob`                                                                                                                          |
| `try(closure: {any... -> any}, args: any...): try`                                    | Attempts to execute the closure with the provided args and returns a `try`. For details on what to do with that, look above.                                                      |

Other project-specific functions (like [μScript-gson](../muscript-json/README.md)) will likely also be available to your scripts.
Please consult the documentation for the project using μScript for more information on what is available or use `this::keys()` to view everything.