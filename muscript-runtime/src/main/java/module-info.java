module io.gitlab.jfronny.commons.muscript.runtime {
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.ast;
    requires io.gitlab.jfronny.commons.muscript.core;
    requires io.gitlab.jfronny.commons.muscript.data;
    requires io.gitlab.jfronny.commons.muscript.data.additional;
    exports io.gitlab.jfronny.muscript.runtime;
    exports io.gitlab.jfronny.muscript.runtime.context;
    provides io.gitlab.jfronny.muscript.data.additional.context.IExprBinder with io.gitlab.jfronny.muscript.runtime.impl.ExprBinderImpl;
}