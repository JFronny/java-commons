package io.gitlab.jfronny.muscript.runtime.context;

import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleNumberExpr;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;

import static io.gitlab.jfronny.muscript.runtime.Runtime.coerce;

public interface CustomNumberExpr extends ExtensibleNumberExpr {
    @Override
    default double evaluate(IScope scope) {
        return evaluate(coerce(this, scope));
    }

    double evaluate(Scope scope);
}
