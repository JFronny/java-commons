package io.gitlab.jfronny.muscript.runtime.context;

import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleBoolExpr;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;

import static io.gitlab.jfronny.muscript.runtime.Runtime.coerce;

public interface CustomBoolExpr extends ExtensibleBoolExpr {
    @Override
    default boolean evaluate(IScope scope) {
        return evaluate(coerce(this, scope));
    }

    boolean evaluate(Scope scope);
}
