package io.gitlab.jfronny.muscript.runtime;

import io.gitlab.jfronny.muscript.ast.*;
import io.gitlab.jfronny.muscript.ast.bool.*;
import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.ast.dynamic.*;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleBoolExpr;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleDynamicExpr;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleNumberExpr;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleStringExpr;
import io.gitlab.jfronny.muscript.ast.number.*;
import io.gitlab.jfronny.muscript.ast.string.*;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.IDynamic;
import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.core.StackFrame;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.IntentionalException;
import io.gitlab.jfronny.muscript.data.additional.libs.SignatureDesyncException;
import io.gitlab.jfronny.muscript.data.dynamic.*;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
import io.gitlab.jfronny.muscript.data.dynamic.type.DTypeCallable;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asNumber;
import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asString;
import static io.gitlab.jfronny.muscript.data.additional.DFinal.of;
import static io.gitlab.jfronny.muscript.runtime.Except.locationalException;

public class Runtime {
    public static Dynamic run(Script script, Scope scope) {
        return evaluate(script.content(), scope);
    }

    public static Dynamic evaluate(Expr expr, Scope scope) {
        Objects.requireNonNull(expr);
        return switch (expr) {
            case StringExpr e -> of(evaluate(e, scope));
            case NumberExpr e -> of(evaluate(e, scope));
            case BoolExpr e -> of(evaluate(e, scope));
            case DynamicExpr e -> evaluate(e, scope);
            case NullLiteral e -> new DNull();
        };
    }

    public static String evaluate(StringExpr expr, Scope scope) {
        Objects.requireNonNull(expr);
        try {
            return switch (expr) {
                case StringLiteral(var location, var value) -> value;
                case ExtensibleStringExpr e -> e.evaluate(scope);
                case StringUnpack(var inner) -> {
                    try {
                        Dynamic iv = evaluate(inner, scope);
                        if (Dynamic.isNull(iv)) throw locationalException(expr, "Cannot unpack null");
                        yield iv.asString().getValue();
                    } catch (DynamicTypeConversionException e) {
                        throw locationalException(e, expr);
                    }
                }
                case StringCoerce(var inner) -> DynamicSerializer.INSTANCE.serialize(evaluate(inner, scope));
                case StringAssign(var location, var variable, var value) -> {
                    var data = evaluate(value, scope);
                    scope.set(variable, data);
                    yield data;
                }
                case StringConditional(var location, var condition, var ifTrue, var ifFalse) -> evaluate(condition, scope) ? evaluate(ifTrue, scope) : evaluate(ifFalse, scope);
                case Concatenate(var location, var left, var right) -> evaluate(left, scope) + evaluate(right, scope);
                case CharAt(var location, var left, var index) -> {
                    var l = evaluate(left, scope);
                    var i = (int) evaluate(index, scope);
                    if (i < 0 || i >= l.length()) throw locationalException(expr, "Index " + i + " is out of range for string with length " + l.length());
                    yield String.valueOf(l.charAt(i));
                }
            };
        } catch (DynamicTypeConversionException e) {
            throw locationalException(e, expr);
        } catch (NullPointerException e) {
            throw locationalException(expr, e);
        }
    }

    public static double evaluate(NumberExpr expr, Scope scope) {
        Objects.requireNonNull(expr);
        try {
            return switch (expr) {
                case NumberLiteral(var location, var value) -> value;
                case ExtensibleNumberExpr e -> e.evaluate(scope);
                case NumberUnpack(var inner) -> {
                    try {
                        Dynamic iv = evaluate(inner, scope);
                        if (Dynamic.isNull(iv)) throw locationalException(expr, "Cannot unpack null");
                        yield iv.asNumber().getValue();
                    } catch (DynamicTypeConversionException e) {
                        throw locationalException(e, expr);
                    }
                }
                case NumberAssign(var location, var variable, var value) -> {
                    var data = evaluate(value, scope);
                    scope.set(variable, data);
                    yield data;
                }
                case NumberConditional(var location, var condition, var ifTrue, var ifFalse) -> evaluate(condition, scope) ? evaluate(ifTrue, scope) : evaluate(ifFalse, scope);
                case Add(var location, var augend, var addend) -> evaluate(augend, scope) + evaluate(addend, scope);
                case Subtract(var location, var minuend, var subtrahend) -> evaluate(minuend, scope) - evaluate(subtrahend, scope);
                case Negate(var location, var inner) -> -evaluate(inner, scope);
                case Multiply(var location, var multiplier, var multiplicand) -> evaluate(multiplier, scope) * evaluate(multiplicand, scope);
                case Divide(var location, var dividend, var divisor) -> evaluate(dividend, scope) / evaluate(divisor, scope);
                case Modulo(var location, var dividend, var divisor) -> evaluate(dividend, scope) % evaluate(divisor, scope);
                case Power(var location, var base, var exponent) -> Math.pow(evaluate(base, scope), evaluate(exponent, scope));
            };
        } catch (DynamicTypeConversionException e) {
            throw locationalException(e, expr);
        } catch (NullPointerException | ArithmeticException e) {
            throw locationalException(expr, e);
        }
    }

    public static boolean evaluate(BoolExpr expr, Scope scope) {
        Objects.requireNonNull(expr);
        try {
            return switch (expr) {
                case BoolLiteral(var location, var value) -> value;
                case ExtensibleBoolExpr e -> e.evaluate(scope);
                case BoolUnpack(var inner) -> {
                    try {
                        Dynamic iv = evaluate(inner, scope);
                        if (Dynamic.isNull(iv)) throw locationalException(expr, "Cannot unpack null");
                        yield iv.asBool().getValue();
                    } catch (DynamicTypeConversionException e) {
                        throw locationalException(e, expr);
                    }
                }
                case BoolAssign(var location, var variable, var value) -> {
                    var data = evaluate(value, scope);
                    scope.set(variable, data);
                    yield data;
                }
                case BoolConditional(var location, var condition, var ifTrue, var ifFalse) -> evaluate(condition, scope) ? evaluate(ifTrue, scope) : evaluate(ifFalse, scope);
                case And(var location, var left, var right) -> evaluate(left, scope) && evaluate(right, scope);
                case Or(var location, var left, var right) -> evaluate(left, scope) || evaluate(right, scope);
                case Not(var location, var inner) -> !evaluate(inner, scope);
                case Equals(var location, var left, var right) -> Objects.equals(evaluate(left, scope), evaluate(right, scope));
                case GreaterThan(var location, var left, var right) -> evaluate(left, scope) > evaluate(right, scope);
            };
        } catch (DynamicTypeConversionException e) {
            throw locationalException(e, expr);
        } catch (NullPointerException e) {
            throw locationalException(expr, e);
        }
    }

    public static Dynamic evaluate(DynamicExpr expr, Scope scope) {
        Objects.requireNonNull(expr);
        try {
            return switch (expr) {
                case DynamicLiteral(var location, var value) -> coerce(expr, value);
                case ExtensibleDynamicExpr e -> coerce(e, e.evaluate(scope));
                case DynamicCoerce(StringExpr inner) -> of(evaluate(inner, scope));
                case DynamicCoerce(NumberExpr inner) -> of(evaluate(inner, scope));
                case DynamicCoerce(BoolExpr inner) -> of(evaluate(inner, scope));
                case DynamicCoerce(DynamicExpr inner) -> evaluate(inner, scope);
                case DynamicCoerce(NullLiteral inner) -> new DNull();
                case DynamicAssign(var location, var variable, var value) -> {
                    var data = evaluate(value, scope);
                    scope.set(variable, data.isCallable() ? data.asCallable().named(variable) : data);
                    yield data;
                }
                case DynamicConditional(var location, var condition, var ifTrue, var ifFalse) -> evaluate(condition, scope) ? evaluate(ifTrue, scope) : evaluate(ifFalse, scope);
                case This e -> scope;
                case Variable(var location, var name) -> {
                    if (scope.has(name)) yield scope.get(name);
                    else throw locationalException(expr, "Variable " + name + " not found");
                }
                case Get(var location, var leftExpr, var name) -> {
                    var left = evaluate(leftExpr, scope);
                    if (Dynamic.isNull(left)) throw locationalException(expr, "Could not get \"" + evaluate(name, scope) + "\" because left is null");
                    if (!left.isObject()) throw locationalException(expr, "Cannot get from non-object");
                    var o = left.asObject();
                    var n = evaluate(asString(name), scope);
                    if (!o.has(n)) throw locationalException(expr, "Object does not contain \"" + n + "\"");
                    yield o.get(n);
                }
                case At(var location, var leftExpr, var index) -> {
                    var left = evaluate(leftExpr, scope);
                    if (Dynamic.isNull(left)) throw locationalException(expr, "Could not get \"" + evaluate(index, scope) + "\" because left is null");
                    if (!left.isList()) {
                        if (left.isObject()) {
                            // optimizer was too eager, handle like Get
                            var o = left.asObject();
                            var n = evaluate(asString(index), scope);
                            if (!o.has(n)) throw locationalException(expr, "Object does not contain \"" + n + "\"");
                            yield o.get(n);
                        }
                        if (left.isString()) {
                            // this is a CharAt that could not be identified cleanly
                            var l = left.asString().getValue();
                            var i = (int) evaluate(index, scope);
                            if (i < 0 || i >= l.length()) throw locationalException(expr, "Index " + i + " is out of range for string with length " + l.length());
                            yield of(String.valueOf(l.charAt(i)));
                        }
                        throw locationalException(expr, "Get/At operator only supports lists, objects and strings");
                    }
                    var l = left.asList();
                    int idx = (int) evaluate(index, scope);
                    if (idx < 0 || idx >= l.size()) throw locationalException(expr, "Index " + idx + " is out of range for list with size " + l.size());
                    yield l.get(idx);
                }
                case GetOrAt(var location, var leftExpr, var nameOrIndex) -> {
                    var left = evaluate(leftExpr, scope);
                    var right = evaluate(nameOrIndex, scope);
                    if (Dynamic.isNull(left)) throw locationalException(expr, "Could not get \"" + evaluate(nameOrIndex, scope) + "\" because left is null");
                    if (left.isList() && right.isNumber()) {
                        var l = left.asList();
                        int idx = (int) evaluate(asNumber(nameOrIndex), scope);
                        if (idx < 0 || idx >= l.size()) throw locationalException(expr, "Index " + idx + " is out of range for list with size " + l.size());
                        yield l.get(idx);
                    } else if (left.isObject()) {
                        var o = left.asObject();
                        var n = right.asString().getValue();
                        if (!o.has(n)) throw locationalException(expr, "Object does not contain \"" + n + "\"");
                        yield o.get(n);
                    } else if (left.isString() && right.isNumber()) {
                        var l = left.asString().getValue();
                        var i = (int) evaluate(asNumber(nameOrIndex), scope);
                        if (i < 0 || i >= l.length()) throw locationalException(expr, "Index " + i + " is out of range for string with length " + l.length());
                        yield of(String.valueOf(l.charAt(i)));
                    } else throw new DynamicTypeConversionException("object, list or string", left);
                }
                case Bind(var location, var callable, var parameter) -> {
                    var param = evaluate(parameter, scope);
                    var clb = evaluate(callable, scope).asCallable();
                    yield of("<bind>", args -> {
                        var argsWithParameter = new LinkedList<Dynamic>(args.getValue());
                        argsWithParameter.addFirst(param);
                        return clb.call(of(argsWithParameter));
                    }, () -> expr);
                }
                case Call e -> {
                    DCallable dc;
                    DList arg;
                    try {
                        Dynamic lv = evaluate(e.callable(), scope);
                        if (Dynamic.isNull(lv)) throw locationalException(expr, "Cannot invoke null");
                        dc = lv.asCallable();
                        arg = of(e.arguments().stream().flatMap(a -> evaluate(a, scope)).toArray(Dynamic[]::new));
                    } catch (RuntimeException ex) {
                        throw locationalException(expr, "Could not perform call successfully", ex);
                    }
                    try {
                        yield dc.call(arg);
                    } catch (LocationalException le) {
                        throw le.appendStack(new StackFrame.Raw(expr.location().file(), dc.getName(), e.callable().location().chStart()));
                    } catch (IntentionalException | SignatureDesyncException | SignatureMismatchException ex) {
                        throw new LocationalException(expr.location(), ex.getMessage(), ex);
                    } catch (RuntimeException ex) {
                        throw locationalException(expr, ex.getMessage(), ex);
                    }
                }
                case Closure(var location, var boundArgs, var variadic, var steps, var finish) -> of(getSignature(boundArgs, variadic), null, args -> {
                    int ac = args.size();
                    int ae = boundArgs.size();
                    if (variadic) ae--;
                    if (ac < ae) throw locationalException(expr, "Invoked with too few arguments (expected " + ae + " but got " + ac + ")");
                    if (!variadic && ac > ae) throw locationalException(expr, "Invoked with too many arguments (expected " + ae + " but got " + ac + ")");
                    var fork = scope.fork();
                    for (int i = 0; i < ae; i++) fork.set(boundArgs.get(i), args.get(i));
                    if (variadic) {
                        fork.set(boundArgs.getLast(), IntStream.range(ae, ac).mapToObj(args::get).toList());
                    }
                    for (var step : steps) {
                        evaluate(step, fork);
                    }
                    return evaluate(finish, fork);
                }, () -> expr);
                case ExprGroup(var location, var steps, var finish, var packedArgs, var shouldFork) -> {
                    var fork = shouldFork ? scope.fork() : scope;
                    if (shouldFork && packedArgs instanceof ExprGroup.PackedArgs(var sourceArgs, var boundArgs, var variadic)) {
                        var from = new LinkedList<Dynamic>();
                        for (var arg : sourceArgs) {
                            var data = evaluate(arg.value(), scope);
                            if (arg.variadic()) from.addAll(data.asList().getValue());
                            else from.add(data);
                        }
                        int ac = from.size();
                        int ae = boundArgs.size();
                        if (variadic) ae--;
                        if (ac < ae) throw locationalException(expr, "Invoked with too few arguments (expected " + ae + " but got " + ac + ")");
                        if (!variadic && ac > ae) throw locationalException(expr, "Invoked with too many arguments (expected " + ae + " but got " + ac + ")");
                        for (int i = 0; i < ae; i++) fork.set(boundArgs.get(i), from.get(i));
                        if (variadic) {
                            fork.set(boundArgs.getLast(), IntStream.range(ae, ac).mapToObj(from::get).toList());
                        }
                    }
                    for (var step : steps) {
                        evaluate(step, fork);
                    }
                    yield evaluate(finish, fork);
                }
                case ListLiteral(var location, var elements) -> of(elements.stream().map(e -> evaluate(e, scope)).toList());
                case ObjectLiteral(var location, var content) -> {
                    var result = new LinkedHashMap<String, Dynamic>();
                    content.forEach((k, v) -> result.put(k, evaluate(v, scope)));
                    yield of(result);
                }
            };
        } catch (DynamicTypeConversionException e) {
            throw locationalException(e, expr);
        } catch (NullPointerException e) {
            throw locationalException(expr, e);
        }
    }

    private static DTypeCallable getSignature(List<String> boundArgs, boolean variadic) {
        var args = new LinkedList<DTypeCallable.Arg>();
        for (int i = 0; i < boundArgs.size(); i++) {
            args.add(new DTypeCallable.Arg(boundArgs.get(i), null, variadic && (i == boundArgs.size() - 1)));
        }
        return new DTypeCallable(args, null);
    }

    private static Stream<? extends Dynamic> evaluate(Call.Argument argument, Scope scope) {
        return argument.variadic()
                ? evaluate(argument.value(), scope).asList().getValue().stream()
                : Stream.of(evaluate(argument.value(), scope));
    }

    public static Dynamic coerce(DynamicExpr expr, IDynamic value) {
        return switch (value) {
            case null -> null;
            case Dynamic d -> d;
            default -> throw locationalException(expr, "Unsupported implementation of Dynamic");
        };
    }

    public static Scope coerce(Expr expr, IScope scope) {
        return switch (scope) {
            case null -> null;
            case Scope s -> s;
            default -> throw locationalException(expr, "Unsupported implementation of Scope");
        };
    }

    public static DCallable bind(Script script, Scope scope) {
        return of("<root>", args -> {
            scope.set("args", args);
            return run(script, scope);
        }, script::content);
    }
}
