package io.gitlab.jfronny.muscript.runtime.impl;

import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.data.additional.context.IExprBinder;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.dynamic.DCallable;
import io.gitlab.jfronny.muscript.runtime.Runtime;

public class ExprBinderImpl implements IExprBinder {
    @Override
    public DCallable bind(Script script, Scope scope) {
        return Runtime.bind(script, scope);
    }
}
