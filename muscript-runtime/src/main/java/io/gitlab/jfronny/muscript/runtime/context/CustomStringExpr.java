package io.gitlab.jfronny.muscript.runtime.context;

import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleStringExpr;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;

import static io.gitlab.jfronny.muscript.runtime.Runtime.coerce;

public interface CustomStringExpr extends ExtensibleStringExpr {
    @Override
    default String evaluate(IScope scope) {
        return evaluate(coerce(this, scope));
    }

    String evaluate(Scope scope);
}
