package io.gitlab.jfronny.muscript.runtime.context;

import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleDynamicExpr;
import io.gitlab.jfronny.muscript.core.IDynamic;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;

import static io.gitlab.jfronny.muscript.runtime.Runtime.coerce;

public interface CustomDynamicExpr extends ExtensibleDynamicExpr {
    @Override
    default IDynamic evaluate(IScope scope) {
        return evaluate(coerce(this, scope));
    }

    Dynamic evaluate(Scope scope);
}
