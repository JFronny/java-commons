package io.gitlab.jfronny.muscript.runtime;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.data.dynamic.DynamicTypeConversionException;
import org.jetbrains.annotations.Nullable;

public class Except {
    private static String formatMessage(Expr source, @Nullable String message) {
        String msg = "Could not evaluate " + source.getClass().getSimpleName();
        if (message != null) {
            msg += ": " + message;
        }
        return msg;
    }

    public static LocationalException locationalException(Expr source) {
        return new LocationalException(source.location(), formatMessage(source, null));
    }

    public static LocationalException locationalException(Expr source, String message) {
        return new LocationalException(source.location(), formatMessage(source, message));
    }

    public static LocationalException locationalException(Expr source, String message, Throwable cause) {
        return new LocationalException(source.location(), formatMessage(source, message), cause);
    }

    public static LocationalException locationalException(Expr source, Throwable cause) {
        return new LocationalException(source.location(), formatMessage(source, null), cause);
    }

    public static LocationalException locationalException(DynamicTypeConversionException e, Expr source) {
        return locationalException(source, formatMessage(source, e.getMessage()), e);
    }

    public static LocationalException locationalException(DynamicTypeConversionException e, CodeLocation location) {
        return new LocationalException(location, e.getMessage(), e);
    }
}
