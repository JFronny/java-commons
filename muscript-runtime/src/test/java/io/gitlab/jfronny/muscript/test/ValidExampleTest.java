package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asString;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ValidExampleTest {
    @Test
    void assertValidExample() throws IOException {
        final String source;
        try (InputStream is = Objects.requireNonNull(ValidExampleTest.class.getClassLoader().getResourceAsStream("example.md"))) {
            source = new String(is.readAllBytes());
        }
        String[] split = source.lines().toArray(String[]::new);
        List<Executable> testCases = new LinkedList<>();
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            if (s.equals("```mu")) {
                StringBuilder blockBuilder = new StringBuilder();
                while (!split[++i].equals("```")) {
                    blockBuilder.append('\n').append(split[i]);
                }
                assertEquals("```", split[i++]);
                assertEquals("Result:", split[i++]);
                assertEquals("```", split[i]);
                StringBuilder resultBuilder = new StringBuilder();
                while (!split[++i].equals("```")) {
                    resultBuilder.append('\n').append(split[i]);
                }
                i++;
                final String block = blockBuilder.substring(1);
                final String expectedResult = resultBuilder.substring(1);
                testCases.add(new TestCase(block, expectedResult));
            }
        }

        assertAll(testCases);
    }

    record TestCase(String source, String expectedResult) implements Executable {
        @Override
        public void execute() {
            String result = null;
            try {
                result = evaluate(asString(parseScript(source).content()), StandardLib.createScope(MuScriptVersion.DEFAULT));
            } catch (Throwable t) {
                assertEquals(expectedResult, StringFormatter.toString(t, e ->
                        e instanceof LocationalException le
                                ? le.asPrintable().toString()
                                : e.toString()
                ));
            }
            if (result != null) assertEquals(expectedResult, result);
        }
    }
}
