package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.parser.StarScriptIngester;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StarScriptIngesterTest {
    @Test
    void ingestionTest() {
        assertEquals("'Hello ' || (name) || '!'", StarScriptIngester.starScriptToMu("Hello {name}!"));
        assertEquals("('Only \"Content')", StarScriptIngester.starScriptToMu("{'Only \"Content'}"));
        assertEquals("(a + c / 75) || ' equals ' || (b)", StarScriptIngester.starScriptToMu("{a + c / 75} equals {b}"));
    }
}
