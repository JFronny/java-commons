package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.IntentionalException;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StackTraceTest {
    final String source = """
                someInner = { ->
                  throw()
                }
                
                someOuter = { ->
                  someInner()
                }
                
                someOuter()
                """;

    final Scope scope = StandardLib.createScope(MuScriptVersion.DEFAULT)
            .set("throw", args -> {
                throw new IntentionalException("Expected Exception");
            });

    @Test
    void stackTrace() {
        assertEquals("""
                Error at '(' (character 8): Expected Exception
                     1 |   throw()
                                ^-- Here
                  at someInner (call: line 5)
                  at someOuter (call: line 8)""",
                assertThrows(LocationalException.class, () -> run(parseScript(source), scope))
                        .asPrintable().toString());
    }

    @Test
    void stackTrace2() {
        assertEquals("""
                Error at '(' (character 8): Expected Exception
                     1 |   throw()
                                ^-- Here
                  at someInner (call: line 5 in some/file.mu)
                  at someOuter (call: line 8 in some/file.mu)""",
                assertThrows(LocationalException.class, () -> run(parseScript(source, "some/file.mu"), scope))
                        .asPrintable().toString());
    }
}
