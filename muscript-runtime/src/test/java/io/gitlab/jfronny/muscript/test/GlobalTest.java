package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.core.LocationalException;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.makeArgs;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GlobalTest {
    @Test
    void notAll() {
        assertEquals(12, run(parseScript("""
                v = 12
                {->v = 5}()
                v
                """), makeArgs()).asNumber().getValue());
    }

    @Test
    void global() {
        assertEquals(5, run(parseScript("""
                $v = 12
                {->$v = 5}()
                $v
                """), makeArgs()).asNumber().getValue());
    }

    @Test
    void innermost() {
        assertThrows(LocationalException.class,
                () -> run(parseScript("""
                        {->$v = 12}()
                        $v
                        """), makeArgs()).asNumber().getValue(),
                "This object doesn't contain '$v'");
    }
}
