package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.bool;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.number;
import static org.junit.jupiter.api.Assertions.*;

class BooleanTest {
    @Test
    void simpleLogic() {
        assertTrue(bool("false | true"));
        assertTrue(bool("false != true"));
        assertTrue(bool("!false != false"));
        assertFalse(bool("!false & false"));
        assertTrue(bool("!false & true"));
        assertTrue(bool("true == true"));
        assertTrue(bool("false != true"));
    }

    @Test
    void conditional() {
        assertEquals(3, number("true ? 3 : 4"));
        assertEquals(4, number("false ? 3 : 4"));
    }
}
