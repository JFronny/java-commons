package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.core.SourceFS;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.parser.Parser;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiScriptTest {
    final SourceFS FS = new SourceFS.Immutable(Map.of(
            "main.mu", """
                    #include import1.mu
                    func(4)""",
            "import1.mu", """
                    #include import2.mu
                    func = {c->fn2(c-1, c)}""",
            "import2.mu", """
                    #include import3.mu
                    fn = {a->a}""",
            "import3.mu", """
                    #include import2.mu
                    fn = {a->a*2}
                    fn2 = {a, b -> fn(a)+b}"""
    ));

    final Scope scope = StandardLib.createScope(MuScriptVersion.DEFAULT)
            .set("throw", args -> {
                throw new IllegalArgumentException("No");
            });

    @Test
    void simpleMultifile() {
        assertEquals(7, run(Parser.parseMultiScript(MuScriptVersion.DEFAULT, "main.mu", FS), scope).asNumber().getValue());
    }
}
