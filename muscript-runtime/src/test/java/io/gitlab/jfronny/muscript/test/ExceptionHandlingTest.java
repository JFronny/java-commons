package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.core.LocationalException;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExceptionHandlingTest {
    @Test
    void catchStdlib() {
        assertEquals("Signature mismatch for isEmpty: expected <(collection: string | [any] | {any}) -> bool> but got <() -> any>", assertThrows(LocationalException.class, () -> string("isEmpty()")).getMessage());
        assertEquals("Signature mismatch for isEmpty: expected <(collection: string | [any] | {any}) -> bool> but got <() -> any>", string("try({->isEmpty()}).catch({e->e.message}).result"));
    }

    @Test
    void fail() {
        assertEquals("Failed", assertThrows(LocationalException.class, () -> string("fail()")).getMessage());
        assertEquals("Joe", assertThrows(LocationalException.class, () -> string("fail('Joe')")).getMessage());
    }

    @Test
    void catchFail() {
        assertEquals("Cought Joe", string("try({->fail('Joe')}).catch({e->'Cought ' || e.message}).result"));
    }

    @Test
    void catchInner() {
        assertEquals("Got Signature mismatch for isEmpty: expected <(collection: string | [any] | {any}) -> bool> but got <() -> any>", assertThrows(LocationalException.class, () -> run(parseScript("""
                inner = {-> isEmpty()}
                outer = {-> inner()}
                outer2 = {-> try({a->a()}, outer).catch({e -> fail('Got ' || e.message)}).result}
                outer2()
                """), makeArgs())).getMessage());
    }
}
