package io.gitlab.jfronny.muscript.test.util;

import io.gitlab.jfronny.muscript.data.additional.context.Scope;

public class UnforkableScope extends Scope {
    @Override
    public Scope fork() {
        return this;
    }
}
