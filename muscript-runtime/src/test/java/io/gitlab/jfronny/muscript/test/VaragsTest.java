package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.number;
import static org.junit.jupiter.api.Assertions.assertEquals;

class VaragsTest {
    @Test
    void basicListOf() {
        assertEquals(2, number("listOf(1, 2, 3, 4)[2] - 1"));
    }

    @Test
    void variadicFunction() {
        assertEquals(2, number("{joe...->joe[2] - 1}(1, 2, 3, 4)"));
    }

    @Test
    void variadicParameter() {
        assertEquals(2, number("{n...->n[2] - 1}(listOf(1, 2, 3, 4)...)"));
    }

    @Test
    void variadicFootgun() {
        assertEquals(2, number("{a, b, c, d -> c - 1}(listOf(1, 2, 3, 4)...)"));
    }
}
