package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.bool;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ListTest {
    @Test
    void listAccess() {
        assertTrue(bool("list[0]"));
        assertTrue(bool("list[1] < 3"));
        assertTrue(bool("list[2] || list[0] == '3true'"));
    }

    @Test
    void contains() {
        assertFalse(bool("numbers::contains(',')"));
        assertFalse(bool("numbers::contains('1,')"));
        assertFalse(bool("numbers::contains('1')"));
        assertTrue(bool("numbers::contains(1)"));
    }
}
