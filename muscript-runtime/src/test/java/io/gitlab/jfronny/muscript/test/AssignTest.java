package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.ast.dynamic.DynamicAssign;
import io.gitlab.jfronny.muscript.ast.string.StringLiteral;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.serialize.Decompiler;
import io.gitlab.jfronny.muscript.test.util.UnforkableScope;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asDynamic;
import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asString;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.number;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AssignTest {
    @Test
    void testAssignSimple() {
        String source = "someval = 'test'";
        StringExpr expr = asString(parse(source));
        assertEquals(asString(new DynamicAssign(new CodeLocation(0, 6, source, null), "someval", asDynamic(new StringLiteral(new CodeLocation(10, 15, source, null), "test")))), expr);
        assertEquals("someval = 'test'", Decompiler.decompile(expr));
        Scope scope = new UnforkableScope();
        assertEquals("test", evaluate(expr, scope));
        assertEquals("test", scope.getValue().get("someval").asString().getValue());
    }

    @Test
    void testAssignInner() {
        assertEquals(2, number("{->some = other = 2; other}()"));
        assertEquals(2, number("{->some = other = 2; some}()"));
        assertEquals(2, number("{->some = 2 + 4 * (other = 4 / 2); other}()"));
    }
}
