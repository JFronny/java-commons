package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.bool;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.number;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NumberTest {
    @Test
    void simpleMath() {
        assertEquals(12, number("7 + 5"));
        assertEquals(12, number("14 - 2"));
        assertEquals(12, number("4* 3"));
        assertEquals(12, number("24 /2"));
        assertEquals(12, number("92 % 20"));
        assertEquals(12, number("2^2*3"));
        assertEquals(-12, number("-12"));
        assertEquals(12, number("-1 * -12 + 2 + (-2)"));
    }

    @Test
    void compare() {
        assertTrue(bool("12 < 10 * 2"));
        assertTrue(bool("12 > 14 / 2"));
        assertTrue(bool("12 == 10 + 2"));
        assertTrue(bool("12 >= 10 + 2"));
        assertTrue(bool("10 <= 10 + 2"));
        assertTrue(bool("12 != 10 * 2"));
    }

    @Test
    void orderOfOperations() {
        assertEquals(12, number("2 + 5 * 2"));
        assertEquals(12, number("3*2^2"));
    }
}
