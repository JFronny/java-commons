package io.gitlab.jfronny.muscript.test.util;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.impl.ObjectGraphPrinter;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.parser.Parser;

import java.util.Map;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.*;
import static io.gitlab.jfronny.muscript.data.additional.DFinal.of;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;

public class MuTestUtil {
    public static double number(String source) {
        return evaluate(asNumber(parse(source)), makeArgs());
    }

    public static boolean bool(String source) {
        Expr tree = parse(source);
        try {
            return evaluate(asBool(tree), makeArgs());
        } catch (RuntimeException e) {
            try {
                System.out.println("Caught error with tree:\n" + ObjectGraphPrinter.printGraph(tree));
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
            throw e;
        }
    }

    public static String string(String source) {
        return evaluate(asString(parse(source)), makeArgs());
    }

    public static Expr parse(String source) {
        return Parser.parse(MuScriptVersion.DEFAULT, source);
    }

    public static Script parseScript(String source) {
        return Parser.parseScript(MuScriptVersion.DEFAULT, source);
    }

    public static Script parseScript(String source, String file) {
        return Parser.parseScript(MuScriptVersion.DEFAULT, source, file);
    }

    public static Scope makeArgs() {
        return StandardLib.createScope(MuScriptVersion.DEFAULT)
                .set("boolean", true)
                .set("number", 15)
                .set("string", "Value")
                .set("object", Map.of(
                        "subvalue", of(1024),
                        "subfunc", of(v -> of(v.get(1).asNumber().getValue() * v.size()), "object.subfunc"),
                        "1", of("One")
                ))
                .set("object2", Map.of(
                        "valuename", of("subvalue"),
                        "sub", of(Map.of(
                                "val", of(10)
                        )),
                        "stringfunc", of(v -> of(v.get(0).asString().getValue()), "object2.stringfunc")
                ))
                .set("list", of(of(true), of(2), of("3")))
                .set("numbers", of(of(1), of(2), of(3), of(4), of(5), of(6), of(7), of(8), of(9), of(10)))
                .set("function", v -> of(Math.pow(v.get(0).asNumber().getValue(), v.get(1).asNumber().getValue())))
                .set("repeatArgs", v -> makeArgs());
    }
}
