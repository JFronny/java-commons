package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asNumber;
import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asString;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.makeArgs;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BindTest {
    @Test
    void simpleBind() {
        assertEquals(30, run(parseScript( """
                fn = {n, b -> n*b*2}
                5::fn(3)
                """), makeArgs()).asNumber().getValue());
    }

    @Test
    void lists() {
        assertEquals("[1, 9, 25, 49, 81]", evaluate(asString(parseScript("""
                numbers::map({n->n::function(2)})::filter({n->n%2!=0})
                """).content()), makeArgs()));
    }

    @Test
    void complexBind() {
        // fn = {b -> 2 * b}
        assertEquals(6, evaluate(asNumber(parseScript("""
                fn = 2::({a, b -> a * b})
                fn(3)
                """).content()), makeArgs()));
        // {a, b, c -> a(b(c))}(fn, {a -> a + 5}, 3)
        assertEquals(16, evaluate(asNumber(parseScript("""
                fn = 2::({a, b -> a * b})
                fn::({a, b, c -> a(b(c))})({a -> a + 5}, 3)
                """).content()), makeArgs()));
    }
}
