package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ObjectTest {
    @Test
    void valueAccess() {
        assertTrue(bool("boolean"));
        assertTrue(bool("object.subvalue > 1000"));
        assertTrue(bool("object.subfunc(2, 4, 8) == 12"));
    }

    @Test
    void arrayAccess() {
        assertTrue(bool("object[1] == 'One'"));
        assertTrue(bool("object['subvalue'] == 1024"));
        assertTrue(bool("object[object2.valuename] == 1024"));
        assertTrue(bool("object2['sub'].val == 10"));
        assertTrue(bool("object2.sub['val'] == 10"));
    }

    @Test
    void objectLiteral() {
        assertEquals(12, run(parseScript("""
                ob = {}
                ob = {test = 2, test2 = 3}
                t = ob.test
                ob = {test2 = 3, test = 2}
                t = t * ob.test
                ob = {test = 3}
                t * ob.test
                """), makeArgs()).asNumber().getValue());
    }
}
