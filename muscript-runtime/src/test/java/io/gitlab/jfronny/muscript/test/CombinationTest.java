package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.bool;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.string;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CombinationTest {
    @Test
    void simpleTest() {
        assertTrue(bool("1 + 5 < 12 & (true != false)"));
        assertEquals("59", string("15 / 3 || 2 + 7"));
        assertEquals("7yes5", string("2 + 5 || (true ? 'yes' : 'no') || 15 / 3"));
    }
}
