package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.serialize.Decompiler;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DecompileTest {
    private static final String script = """
            clientVersion = challenge { ->
              mod('better-whitelist').version;
            };
            assert(mod('better-whitelist').version == clientVersion, 'You have the wrong mod version');
            assert(challenge({ arg ->
              allMatch(arg) { v ->
                anyMatch(mods) { m ->
                  v.name == m.name & v.version == m.version;
                };
              };
            }, map(filter(mods) { v ->
              v.environment != 'server';
            }) { v ->
              {
                name = v.name,
                version = v.version
              };
            }));
            """;

    @Test
    void testDecompile() {
        assertEquals(script, Decompiler.decompile(parseScript(script)));
    }
}
