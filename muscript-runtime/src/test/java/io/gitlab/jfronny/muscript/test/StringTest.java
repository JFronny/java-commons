package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.test.util.MuTestUtil;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asString;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

class StringTest {
    @Test
    void operators() {
        assertEquals("Hello, world!", string("'Hello, ' || 'world!'"));
        assertEquals("Yes 15 hello", string("'Yes ' || 16 - 1 || ' hello'"));
        assertEquals("Value", string("string"));
        assertTrue(bool("string == 'Value'"));
        assertFalse(bool("string == 'Something else'"));
    }

    @Test
    void concatComparison() {
        assertEquals("Hellotrue", string("'Hello' || -12 < 5"));
        assertEquals("trueHello", string("-12 < 5 || 'Hello'"));
    }

    @Test
    void testCharAt() {
        assertEquals("A", string("'ABC'[0]"));
        assertEquals("B", string("'ABC'[1]"));
        assertEquals("C", string("'ABC'[2]"));
        assertThrows(LocationalException.class, () -> string("'ABC'[3]"));
        assertEquals("h", evaluate(asString(parseScript("x={a -> 'hel' || a || 'lo'}; x('A')[0]").content()), makeArgs()));
    }
}
