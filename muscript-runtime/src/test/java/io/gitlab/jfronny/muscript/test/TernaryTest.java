package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.makeArgs;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TernaryTest {
    @Test
    void testRootTernary() {
        assertEquals(2, run(parseScript("""
                value = 0;
                value == 1 ? joe() : value = 1;
                value == 1 ? value = 2 : joe();
                value == 1 ? value = 3 : value;
                value;
                """), makeArgs()).asNumber().getValue());
    }
}
