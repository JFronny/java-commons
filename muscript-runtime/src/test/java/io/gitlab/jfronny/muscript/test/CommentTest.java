package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.makeArgs;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parseScript;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CommentTest {
    @Test
    void singleLine() {
        assertEquals(2, run(parseScript("""
                n = 5
                n = 2
                // n = 3
                n
                """), makeArgs()).asNumber().getValue());
    }

    @Test
    void multiLine() {
        assertEquals(2, run(parseScript("""
                n = 2
                /* n = 3 */
                /*
                  n = 3
                  n = 4
                  n = 5
                */
                n
                """), makeArgs()).asNumber().getValue());
    }

    @Test
    void commentWithStuff() {
        assertDoesNotThrow(() -> run(parseScript("""
                ob = {
                  k1 = 'Yes',
                  `1` = "One"
                }
                """), makeArgs()));
    }
}
