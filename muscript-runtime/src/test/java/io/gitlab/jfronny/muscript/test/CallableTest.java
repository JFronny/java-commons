package io.gitlab.jfronny.muscript.test;

import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.runtime.Runtime.run;
import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CallableTest {
    @Test
    void basicFunctionTest() {
        assertEquals(3, number("object.subfunc(0, 1, 2)"));
        assertEquals(18, number("object.subfunc(0, object.subfunc(1, 2, 3), 4)"));
        assertTrue(bool("repeatArgs().repeatArgs().boolean"));
        assertEquals(32, number("function(object.subfunc(0, 1), 5)"));
        assertEquals("<=1.16.5\"", string("object2.stringfunc('<=1.16.5\"')"));
        assertEquals("minecraft", string("object2.stringfunc('minecraft', '<=1.16.5')"));
    }

    @Test
    void postfixWithCallTest() {
        assertEquals("2", run(parseScript("""
                fnc = {a, b -> b(a)}
                fnc(1, {a -> a + 1})
                """), makeArgs()).asString().getValue());
        assertEquals("2", run(parseScript("""
                fnc = {a, b -> b(a)}
                fnc(1) {a -> a + 1}
                """), makeArgs()).asString().getValue());
    }

    @Test
    void postfixImplicitCallTest() {
        assertEquals("[2, 4]", run(parseScript("""
                listOf(1, 2, 3, 4)::filter({ a -> a % 2 == 0 })
                """), makeArgs()).asString().getValue());
        assertEquals("[2, 4]", run(parseScript("""
                listOf(1, 2, 3, 4)::filter { a -> a % 2 == 0 }
                """), makeArgs()).asString().getValue());
    }

    @Test
    void postfixImplicitCallMultipleTest() {
        assertEquals(4, number("{ f -> f() } { -> { g -> g(2) } } { n -> 2 * n }"));
    }
}
