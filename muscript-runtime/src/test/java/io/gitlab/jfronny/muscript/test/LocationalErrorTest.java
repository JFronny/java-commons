package io.gitlab.jfronny.muscript.test;

import io.gitlab.jfronny.muscript.parser.Parser;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.test.util.MuTestUtil.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LocationalErrorTest {
    @Test
    void invalidCode() {
        assertEquals("""
                        Error at 't' (character 6): Expected number expression but got boolean
                             1 | 15 + true
                                      ^--^-- Here""",
                assertThrows(Parser.ParseException.class, () -> parse("15 + true")).error.toString());
    }

    @Test
    void invalidCode2() {
        assertEquals("""
                        Error at ''' (character 6): Expected number expression but got string
                             1 | 15 + 'yes'
                                      ^---^-- Here""",
                assertThrows(Parser.ParseException.class, () -> parse("15 + 'yes'")).error.toString());
    }
}
