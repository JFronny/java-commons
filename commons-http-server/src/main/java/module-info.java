module io.gitlab.jfronny.commons.http.server {
    exports io.gitlab.jfronny.commons.http.server;
    exports io.gitlab.jfronny.commons.http.server.api;
    exports io.gitlab.jfronny.commons.http.server.io;
    exports io.gitlab.jfronny.commons.http.server.util;
}