package io.gitlab.jfronny.commons.http.server.util;

import io.gitlab.jfronny.commons.http.server.JLHTTPServer;
import io.gitlab.jfronny.commons.http.server.api.ContextHandler;

import java.io.File;
import java.io.IOException;

/**
 * The {@code FileContextHandler} services a context by mapping it
 * to a file or folder (recursively) on disk.
 */
public class FileContextHandler implements ContextHandler {

    protected final File base;

    public FileContextHandler(File dir) throws IOException {
        this.base = dir.getCanonicalFile();
    }

    public int serve(JLHTTPServer.Request req, JLHTTPServer.Response resp) throws IOException {
        return JLHTTPServer.serveFile(base, req.getContext().getPath(), req, resp);
    }
}
