import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-http-server"

            from(components["java"])
        }
    }
}
