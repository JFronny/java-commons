package io.gitlab.jfronny.muscript.parser.lexer;

import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import org.jetbrains.annotations.Nullable;

public interface Lexer {
    CodeLocation location();
    MuScriptVersion version();
    boolean wasNewlinePassed();
    Token getPrevious();
    Token advance();

    @Nullable String getSource();
    @Nullable String getFile();

    record Token(String lexeme, io.gitlab.jfronny.muscript.parser.lexer.Token token, int start, int current, char ch, CodeLocation location) {
        @Override
        public String toString() {
            return token + " '" + lexeme + "'";
        }
    }
}
