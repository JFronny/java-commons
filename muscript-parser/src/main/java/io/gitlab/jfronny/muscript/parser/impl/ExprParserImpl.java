package io.gitlab.jfronny.muscript.parser.impl;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.IExprParser;
import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.core.SourceFS;
import io.gitlab.jfronny.muscript.parser.Parser;

public class ExprParserImpl implements IExprParser {
    @Override
    public Expr parse(MuScriptVersion version, String expr) {
        return Parser.parse(version, expr);
    }

    @Override
    public Script parseMultiScript(MuScriptVersion version, String path, SourceFS filesystem) {
        return Parser.parseMultiScript(version, path, filesystem);
    }
}
