package io.gitlab.jfronny.muscript.parser;

import io.gitlab.jfronny.muscript.core.MuUtil;

public class StarScriptIngester {
    private static final System.Logger LOGGER = System.getLogger("MuScript");

    /**
     * Naive conversion of starscript syntax to muscript
     */
    public static String starScriptToMu(String source) {
        StringBuilder result = new StringBuilder();
        StringBuilder currbuf = new StringBuilder();
        State state = State.Surrounding;
        for (char c : source.toCharArray()) {
            switch (state) {
                case Surrounding -> {
                    if (c == '{') {
                        if (!currbuf.isEmpty()) {
                            if (!result.isEmpty()) result.append(" || ");
                            result.append(MuUtil.enquote(currbuf.toString()));
                        }
                        currbuf = new StringBuilder();
                        state = State.UnquotedInner;
                    } else currbuf.append(c);
                }
                case UnquotedInner -> {
                    switch (c) {
                        case '}' -> {
                            if (!currbuf.isEmpty()) {
                                if (!result.isEmpty()) result.append(" || ");
                                result.append("(").append(currbuf).append(")");
                            }
                            currbuf = new StringBuilder();
                            state = State.Surrounding;
                        }
                        case '\'' -> {
                            currbuf.append(c);
                            state = State.SingleQuoteInner;
                        }
                        case '"' -> {
                            currbuf.append(c);
                            state = State.DoubleQuoteInner;
                        }
                        default -> currbuf.append(c);
                    }
                }
                case SingleQuoteInner -> {
                    currbuf.append(c);
                    if (c == '\'') state = State.UnquotedInner;
                }
                case DoubleQuoteInner -> {
                    currbuf.append(c);
                    if (c == '"') state = State.UnquotedInner;
                }
            }
        }
        if (!currbuf.isEmpty() && !result.isEmpty()) result.append(" || ");
        switch (state) {
            case Surrounding -> {
                if (!currbuf.isEmpty()) result.append(MuUtil.enquote(currbuf.toString()));
            }
            case UnquotedInner -> {
                LOGGER.log(System.Logger.Level.WARNING, "Starscript code segment improperly closed, closing automatically");
                if (!currbuf.isEmpty()) result.append("(").append(currbuf).append(")");
            }
            case SingleQuoteInner -> {
                LOGGER.log(System.Logger.Level.WARNING, "Quote in starscript swallows ending, completing with closing quote");
                if (!currbuf.isEmpty()) result.append("(").append(currbuf).append("')");
            }
            case DoubleQuoteInner -> {
                LOGGER.log(System.Logger.Level.WARNING, "Quote in starscript swallows ending, completing with closing quote");
                if (!currbuf.isEmpty()) result.append("(").append(currbuf).append("\")");
            }
        }
        return result.toString();
    }

    enum State {
        Surrounding, UnquotedInner, SingleQuoteInner, DoubleQuoteInner
    }
}
