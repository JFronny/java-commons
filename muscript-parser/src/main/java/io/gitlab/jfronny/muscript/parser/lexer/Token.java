package io.gitlab.jfronny.muscript.parser.lexer;

public enum Token {
    String, Identifier, Number,

    Null,
    True, False,
    And, Or,

    Assign,
    EqualEqual, BangEqual,

    Concat,

    Greater, GreaterEqual,
    Less, LessEqual,

    Plus, Minus,
    Star, Slash, Percentage, UpArrow,
    Bang,

    Dot, Ellipsis, Comma,
    QuestionMark, Colon,
    LeftParen, RightParen,
    LeftBracket, RightBracket,

    LeftBrace, RightBrace,
    Arrow,

    Semicolon,

    DoubleColon,

    Error, EOF
}
