package io.gitlab.jfronny.muscript.parser;

import io.gitlab.jfronny.muscript.core.MuScriptVersion;

import java.util.Objects;

public abstract class VersionedComponent {
    protected final MuScriptVersion version;

    public VersionedComponent(MuScriptVersion version) {
        this.version = Objects.requireNonNull(version);
    }

    public MuScriptVersion getComponentVersion() {
        return version;
    }
}
