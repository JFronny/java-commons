package io.gitlab.jfronny.muscript.parser;

public enum Type {
    String, Number, Boolean, Dynamic
}
