package io.gitlab.jfronny.muscript.parser.lexer;

import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import org.jetbrains.annotations.Nullable;

/**
 * Wraps the old Lexer implementation in the new Lexer interface for compatibility
 */
public class LegacyLexer implements Lexer {
    private final LexerImpl backend;
    private Token previous = null;

    public LegacyLexer(MuScriptVersion version, String source) {
        this(new LexerImpl(version, source));
    }

    public LegacyLexer(MuScriptVersion version, String source, String file) {
        this(new LexerImpl(version, source, file));
    }

    public LegacyLexer(LexerImpl backend) {
        this.backend = backend;
    }

    @Override
    public CodeLocation location() {
        return backend.location();
    }

    @Override
    public MuScriptVersion version() {
        return backend.getComponentVersion();
    }

    @Override
    public boolean wasNewlinePassed() {
        return backend.passedNewline;
    }

    @Override
    public Token getPrevious() {
        return previous;
    }

    @Override
    public Token advance() {
        backend.next();
        return previous = new Token(
                backend.lexeme,
                backend.token,
                backend.start,
                backend.current,
                backend.ch,
                new CodeLocation(backend.start, backend.current - 1, backend.source, backend.file)
        );
    }

    @Override
    public @Nullable String getSource() {
        return backend.source;
    }

    @Override
    public @Nullable String getFile() {
        return backend.file;
    }
}
