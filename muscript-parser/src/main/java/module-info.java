module io.gitlab.jfronny.commons.muscript.parser {
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.ast;
    requires io.gitlab.jfronny.commons.muscript.core;
    exports io.gitlab.jfronny.muscript.parser;
    exports io.gitlab.jfronny.muscript.parser.lexer;
    provides io.gitlab.jfronny.muscript.ast.context.IExprParser with io.gitlab.jfronny.muscript.parser.impl.ExprParserImpl;
}