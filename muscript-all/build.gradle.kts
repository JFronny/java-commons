import io.gitlab.jfronny.scripts.*
import org.gradle.api.internal.catalog.DelegatingProjectDependency

plugins {
    commons.library
    alias(libs.plugins.shadow)
}

dependencies {
    fun depend(dep: DelegatingProjectDependency) {
        shadow(dep) { isTransitive = false }
        // If we ever want code in muscript-all, we'd need to add compile-time dependencies here as well
    }
    depend(projects.muscriptCore)
    depend(projects.muscriptAst)
    depend(projects.muscriptDataDynamic)
    depend(projects.muscriptDataAdditional)
    depend(projects.muscriptParser)
    depend(projects.muscriptSerialize)
    depend(projects.muscriptOptimizer)
    depend(projects.muscriptRuntime)
}

tasks.shadowJar {
    mergeServiceFiles()
    archiveClassifier = ""
    configurations.clear()
    from(project.configurations.shadow, tasks.jar)
}

tasks.jar {
    archiveClassifier = "dev"
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "muscript-all"

            from(components["java"])
            setArtifacts(listOf(tasks.shadowJar, tasks.sourcesJar))
        }
    }
}

// Otherwise we can't easily overwrite the artifacts to publish while keeping dependency metadata
tasks.withType<GenerateModuleMetadata> {
    enabled = false
}
