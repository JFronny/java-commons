# JfCommons

Common code for my java projects. Uses my common [build scripts](https://git.frohnmeyer-wds.de/Johannes/Scripts).

## Modules

- commons: Common java code without a major theme or external dependencies
- commons-io: Utility classes for caching, hashing, reading json, opening archives, etc.
- commons-http-client: A simple http client
- commons-http-server: A simple http server based on JLHTTP
- commons-flow: A simple framework wrapping Continuation to support using iterators and pattern matching instead of visitors. Also provides support for virtual threads with custom schedulers including a default priority-based scheduler
- commons-flow-backend-unsafe: A backend for `commons-flow` using `commons-unsafe` to access JVM internals

### Serialization
- commons-serialize: Provides the framework for serialization and deserialization. Describes `SerializeWriter`, `SerializeReader`, `Transport` and related classes.
- commons-serialize-dsl: Provides a Kotlin DSL for serialization and deserialization wrapping `commons-serialize`
- commons-serialize-json: Provides a json implementation for `commons-serialize`
- commons-serialize-xml: Provides an xml implementation for `commons-serialize`
- commons-serialize-databind: Provides data binding inspired by Gson for `commons-serialize`
- commons-serialize-databind-api: Provides a thin API around `commons-serialize-databind` for use in other libraries that don't want to depend on `commons-serialize-databind` directly
- commons-serialize-databind-sql: Provides TypeAdapters for SQL types for `commons-serialize-databind` (separate module because of the dependency on `java.sql`)
- commons-serialize-generator-core: Provides a common core for annotation processors that generate serializers and deserializers. Not directly dependent on commons-serialize, as it is also used in libjf-config.
- commons-serialize-generator-annotations: Provides the annotations used to configure `commons-serialize-generator`
- commons-serialize-generator: Provides an annotation processor for generating serializers and deserializers for `commons-serialize`

### muScript
(muscript is a simple scripting language, see [here](./muscript-runtime/README.md) for more information
- muscript-all: Non-JPMS module shadowing the parser, serializer, optimizer, runtime, and data modules.
- muscript-core: Core classes for muscript. Required by all other muscript modules.
- muscript-ast: Implements the AST for muscript as algebraic data types. Contains no logic.
- muscript-data-dynamic: Implements the dynamic data model for muscript.
- muscript-data-additional: Implements additional data types for muscript, including the [standard library](./muscript-runtime/StandardLib.md).
- muscript-parser: Implements the parser for muscript. Originally based on StarScript, it also provides conversion from code targeting that.
- muscript-serialize: Implements a Decompiler for muscript. It is called this because it can be used to serialize Dynamic data.
- muscript-optimizer: Implements a simple optimizer for muscript. Entirely optional.
- muscript-runtime: Implements a runtime for muscript, based on muscript-data-dynamic for runtime data representation but with optimizations for typed expressions.
- [muscript-json](./muscript-json/README.md): Glue code for manipulating json with muscript

### Interop
- commons-logger: A simple logging framework for System.Logger supporting hot reloading and slightly improved APIs
- commons-manifold: Some common code using the features of manifold-ext. Mainly extension classes.
- commons-gson: Provides some utility classes around my fork of gson and an implementation of the `Serializer` interface
- commons-unsafe: Provides wrappers around sun.misc.Unsafe and the LambdaMetaFactory
- commons-bom: A maven BOM to make using multiple modules easier
- commons-catalog: A gradle version catalog to make using multiple modules easier
- commons-flow-asm: A wrapper around ASM using `commons-flow` to make generating bytecode easier
- slf4j-over-jpl: A simple SLF4J implementation redirecting all logs to a `System.Logger`