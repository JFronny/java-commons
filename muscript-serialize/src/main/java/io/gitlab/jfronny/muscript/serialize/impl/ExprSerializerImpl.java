package io.gitlab.jfronny.muscript.serialize.impl;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.IExprSerializer;
import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.serialize.Decompiler;

public class ExprSerializerImpl implements IExprSerializer {
    @Override
    public String serialize(Expr expr) {
        return Decompiler.decompile(expr);
    }

    @Override
    public String serialize(Script script) {
        return Decompiler.decompile(script);
    }
}
