package io.gitlab.jfronny.muscript.serialize.impl;

import io.gitlab.jfronny.muscript.core.ExprWriter;
import io.gitlab.jfronny.muscript.data.additional.DataExprMapper;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer;
import io.gitlab.jfronny.muscript.serialize.Decompiler;

import java.io.IOException;
import java.util.Objects;

public class DynamicSerializerImpl implements DynamicSerializer {
    @Override
    public void serialize(ExprWriter writer, Dynamic value) throws IOException {
        Decompiler.decompile(writer, Objects.requireNonNull(DataExprMapper.map(value)));
    }
}
