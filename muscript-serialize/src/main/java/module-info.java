module io.gitlab.jfronny.commons.muscript.serialize {
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.ast;
    requires io.gitlab.jfronny.commons.muscript.data;
    requires io.gitlab.jfronny.commons.muscript.core;
    requires io.gitlab.jfronny.commons.muscript.data.additional;
    exports io.gitlab.jfronny.muscript.serialize;
    provides io.gitlab.jfronny.muscript.ast.context.IExprSerializer with io.gitlab.jfronny.muscript.serialize.impl.ExprSerializerImpl;
    provides io.gitlab.jfronny.muscript.data.dynamic.context.DynamicSerializer with io.gitlab.jfronny.muscript.serialize.impl.DynamicSerializerImpl;
}