package io.gitlab.jfronny.commons.serialize.databind.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a class with this to specify a custom adapter for serialization and deserialization of its instances.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SerializeWithAdapter {
    /**
     * Either a {@link TypeAdapter} or {@code TypeAdapterFactory} (in commons-serialize-databind).
     */
    Class<?> adapter();

    /**
     * Whether the adapter referenced by {@link #adapter()} should be made {@linkplain
     * TypeAdapter#nullSafe() null-safe}.
     *
     * <p>If {@code true} (the default), it will be made null-safe and Gson will handle {@code null}
     * Java objects on serialization and JSON {@code null} on deserialization without calling the
     * adapter. If {@code false}, the adapter will have to handle the {@code null} values.
     */
    boolean nullSafe() default true;
}
