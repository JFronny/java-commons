package io.gitlab.jfronny.commons.serialize.databind.api.impl;

import io.gitlab.jfronny.commons.data.LinkedTreeMap;
import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class ConstructorConstructor {
    public static <T> ThrowingSupplier<T, MalformedDataException> get(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();

        // Ensure we don't accidentally attempt to use internal constructors when we know a proper constructor exists
        ThrowingSupplier<T, MalformedDataException> specialConstructor = newSpecialCollectionConstructor(type, rawType);
        if (specialConstructor != null) {
            return specialConstructor;
        }

        ThrowingSupplier<T, MalformedDataException> defaultConstructor = newDefaultConstructor(rawType);
        if (defaultConstructor != null) {
            return defaultConstructor;
        }

        ThrowingSupplier<T, MalformedDataException> defaultImplementation = newDefaultImplementationConstructor(type, rawType);
        if (defaultImplementation != null) {
            return defaultImplementation;
        }

        return Modifier.isInterface(rawType.getModifiers()) || Modifier.isAbstract(rawType.getModifiers())
                ? fail("Cannot instantiate abstract class or interface: " + rawType)
                : fail("Class lacks viable constructor: " + rawType);
    }

    private static <T> ThrowingSupplier<T, MalformedDataException> newSpecialCollectionConstructor(Type type, Class<? super T> rawType) {
        if (EnumSet.class.isAssignableFrom(rawType)) {
            return () -> {
                if (type instanceof ParameterizedType) {
                    Type elementType = ((ParameterizedType) type).getActualTypeArguments()[0];
                    if (elementType instanceof Class) {
                        @SuppressWarnings({"unchecked", "rawtypes"})
                        T set = (T) EnumSet.noneOf((Class) elementType);
                        return set;
                    } else {
                        throw new MalformedDataException("Invalid EnumSet type: " + type);
                    }
                } else {
                    throw new MalformedDataException("Invalid EnumSet type: " + type);
                }
            };
        } else if (rawType == EnumMap.class) { // no subtypes!
            return () -> {
                if (type instanceof ParameterizedType) {
                    Type elementType = ((ParameterizedType) type).getActualTypeArguments()[0];
                    if (elementType instanceof Class) {
                        @SuppressWarnings({"unchecked", "rawtypes"})
                        T map = (T) new EnumMap((Class) elementType);
                        return map;
                    } else {
                        throw new MalformedDataException("Invalid EnumMap type: " + type);
                    }
                } else {
                    throw new MalformedDataException("Invalid EnumMap type: " + type);
                }
            };
        }

        return null;
    }

    private static <T> ThrowingSupplier<T, MalformedDataException> newDefaultConstructor(Class<? super T> rawType) {
        if (Modifier.isAbstract(rawType.getModifiers())) return null;

        Constructor<? super T> constructor;
        try {
            constructor = rawType.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            return null;
        }

        if (!Modifier.isPublic(constructor.getModifiers())) {
            return fail("Unable to invoke no-args constructor of " + rawType + ": no public no-args constructor");
        }

        return () -> {
            try {
                @SuppressWarnings("unchecked")
                T newInstance = (T) constructor.newInstance();
                return newInstance;
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new MalformedDataException("Failed to invoke no-args constructor for " + rawType, e);
            }
        };
    }

    private static <T> ThrowingSupplier<T, MalformedDataException> newDefaultImplementationConstructor(Type type, Class<? super T> rawType) {
        if (Collection.class.isAssignableFrom(rawType)) {
            if (SortedSet.class.isAssignableFrom(rawType)) return coerce(TreeSet::new, rawType);
            else if (Set.class.isAssignableFrom(rawType)) return coerce(LinkedHashSet::new, rawType);
            else if (Queue.class.isAssignableFrom(rawType)) return coerce(ArrayDeque::new, rawType);
            else return coerce(ArrayList::new, rawType);
        }

        if (Map.class.isAssignableFrom(rawType)) {
            if (ConcurrentNavigableMap.class.isAssignableFrom(rawType)) return coerce(ConcurrentSkipListMap::new, rawType);
            else if (ConcurrentMap.class.isAssignableFrom(rawType)) return coerce(ConcurrentHashMap::new, rawType);
            else if (SortedMap.class.isAssignableFrom(rawType)) return coerce(TreeMap::new, rawType);
            else if (type instanceof ParameterizedType
                    && !String.class.isAssignableFrom(
                    TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType()))
                return coerce(LinkedHashMap::new, rawType);
            else return coerce(LinkedTreeMap::new, rawType);
        }

        return null;
    }

    private static <T> ThrowingSupplier<T, MalformedDataException> coerce(ThrowingSupplier<?, MalformedDataException> supplier, Class<? super T> rawType) {
        return supplier.cast(rawType);
    }

    private static <T> ThrowingSupplier<T, MalformedDataException> fail(String message) {
        return () -> { throw new MalformedDataException(message); };
    }
}
