package io.gitlab.jfronny.commons.serialize.databind.api;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;

/**
 * A type adapter for serializing and deserializing objects of a specific type.
 *
 * @param <T> The type of the object to serialize/deserialize
 */
public abstract class TypeAdapter<T> {
    public abstract <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(T value, Writer writer) throws TEx, MalformedDataException;
    public abstract <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> T deserialize(Reader reader) throws TEx, MalformedDataException;

    public final TypeAdapter<T> nullSafe() {
        return new TypeAdapter<>() {
            @Override
            public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(T value, Writer writer) throws TEx, MalformedDataException {
                if (value == null) {
                    writer.nullValue();
                } else {
                    TypeAdapter.this.serialize(value, writer);
                }
            }

            @Override
            public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> T deserialize(Reader reader) throws TEx, MalformedDataException {
                if (reader.peek() == null) {
                    reader.nextNull();
                    return null;
                } else {
                    return TypeAdapter.this.deserialize(reader);
                }
            }
        };
    }

    public final TypeAdapter<T> viewWrapped() {
        return new TypeAdapter<>() {
            @Override
            public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(T value, Writer writer) throws TEx, MalformedDataException {
                try (var view = writer.createView()) {
                    TypeAdapter.this.serialize(value, view);
                }
            }

            @Override
            public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> T deserialize(Reader reader) throws TEx, MalformedDataException {
                try (var view = reader.createView()) {
                    return TypeAdapter.this.deserialize(view);
                }
            }
        };
    }
}
