package io.gitlab.jfronny.commons.serialize.databind.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate a TypeAdapter with this to specify the type it handles.
 * Required for the ServiceLoader-based registration of TypeAdapters.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SerializerFor {
    /**
     * The type this {@link TypeAdapter} handles.
     */
    Class<?>[] targets();

    /**
     * Whether this adapter should be made {@linkplain TypeAdapter#nullSafe() null-safe}.
     *
     * <p>If {@code true} (the default), it will be made null-safe and Gson will handle {@code null}
     * Java objects on serialization and JSON {@code null} on deserialization without calling the
     * adapter. If {@code false}, the adapter will have to handle the {@code null} values.
     */
    boolean nullSafe() default true;

    /**
     * Whether this adapter should also be used for all subtypes of the target type.
     */
    boolean hierarchical() default false;
}
