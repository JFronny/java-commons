import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
    jf.manifold
}

dependencies {
    implementation(projects.commons)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-manifold"

            from(components["java"])
        }
    }
}

tasks.jar {
    manifest {
        attributes(mapOf("Contains-Sources" to "java,class"))
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
}
