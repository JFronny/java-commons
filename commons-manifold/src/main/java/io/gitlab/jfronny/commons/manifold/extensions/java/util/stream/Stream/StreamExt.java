package io.gitlab.jfronny.commons.manifold.extensions.java.util.stream.Stream;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Extension
public class StreamExt {
    public static <T> Set<T> toSet(@This Stream<T> thiz) {
        return thiz.collect(LinkedHashSet::new, Set::add, Set::addAll);
    }

    public static <T, K, V> Map<K, V> toMap(@This Stream<T> thiz, Function<? super T, K> keyMapper, Function<? super T, V> valueMapper) {
        return thiz.collect(Collectors.toMap(keyMapper, valueMapper));
    }

    public static <T, K> Map<K, T> toMap(@This Stream<T> thiz, Function<? super T, K> keyMapper) {
        return thiz.toMap(keyMapper, Function.identity());
    }

    public static <T, V> Map<V, List<T>> groupingBy(@This Stream<T> thiz, Function<? super T, V> valueMapper) {
        return thiz.collect(Collectors.groupingBy(valueMapper));
    }

    public static String join(@This Stream<String> thiz) {
        return thiz.collect(Collectors.joining());
    }

    public static String join(@This Stream<String> thiz, String delimiter) {
        return thiz.collect(Collectors.joining(delimiter));
    }

    public static String join(@This Stream<String> thiz, char delimiter) {
        return thiz.join("" + delimiter);
    }

    public static <T> Stream<T> concat(@This Stream<T> thiz, Stream<T> other) {
        return Stream.concat(thiz, other);
    }
}
