package io.gitlab.jfronny.commons.manifold.extensions.org.w3c.dom.NodeList;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Iterator;
import java.util.NoSuchElementException;

@Extension
public abstract class NodeListExt implements Iterable<Node> {
    public static Iterator<Node> iterator(@This NodeList thiz) {
        return new Iterator<>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                while (index < thiz.length && thiz[index].isWhitespace()) {
                    index++;
                }
                return index < thiz.length;
            }

            @Override
            public Node next() {
                if (!hasNext()) throw new NoSuchElementException();
                return thiz[index++];
            }
        };
    }

    public static Node get(@This NodeList thiz, int index) {
        return thiz.item(index);
    }
}
