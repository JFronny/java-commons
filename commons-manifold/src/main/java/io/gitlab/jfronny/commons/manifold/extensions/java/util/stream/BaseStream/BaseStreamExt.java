package io.gitlab.jfronny.commons.manifold.extensions.java.util.stream.BaseStream;

import manifold.ext.rt.api.Extension;

@Extension
public abstract class BaseStreamExt<T> implements Iterable<T> {
}
