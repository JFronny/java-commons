package io.gitlab.jfronny.commons.manifold.extensions.java.lang.Iterable;

import manifold.ext.rt.api.*;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Extension
@Structural
public class IterableExt {
    public static <T> Stream<T> stream(@This Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
