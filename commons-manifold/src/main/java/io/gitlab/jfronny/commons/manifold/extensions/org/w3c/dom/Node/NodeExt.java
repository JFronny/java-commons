package io.gitlab.jfronny.commons.manifold.extensions.org.w3c.dom.Node;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;
import org.w3c.dom.Node;

@Extension
public class NodeExt {
    public static boolean isWhitespace(@This Node thiz) {
        if (thiz.nodeType == Node.TEXT_NODE && thiz.textContent.isBlank()) return true;
        if (thiz.nodeType == Node.COMMENT_NODE) return true;
        return false;
    }
}
