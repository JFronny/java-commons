package io.gitlab.jfronny.commons.manifold.extensions.java.util.Map;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.Iterator;
import java.util.Map;

@Extension
public abstract class MapExt<K, V> implements Iterable<Map.Entry<K, V>> {
    public static <K, V> Iterator<Map.Entry<K, V>> iterator(@This Map<K, V> thiz) {
        return thiz.entrySet().iterator();
    }

    public static <K, V> V set(@This Map<K, V> thiz, K key, V value) {
        return thiz.put(key, value);
    }
}
