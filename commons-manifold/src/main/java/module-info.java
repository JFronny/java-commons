module io.gitlab.jfronny.commons.manifold {
    requires java.xml;
    requires manifold.ext.rt;
    exports io.gitlab.jfronny.commons.manifold.extensions.java.lang.Iterable;
    exports io.gitlab.jfronny.commons.manifold.extensions.java.util.Map;
    exports io.gitlab.jfronny.commons.manifold.extensions.java.util.stream.BaseStream;
    exports io.gitlab.jfronny.commons.manifold.extensions.java.util.stream.Stream;
    exports io.gitlab.jfronny.commons.manifold.extensions.org.w3c.dom.Node;
    exports io.gitlab.jfronny.commons.manifold.extensions.org.w3c.dom.NodeList;
}