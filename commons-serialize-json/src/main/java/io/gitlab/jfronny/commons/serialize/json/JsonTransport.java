package io.gitlab.jfronny.commons.serialize.json;

import io.gitlab.jfronny.commons.serialize.Transport;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class JsonTransport implements Transport<IOException, JsonReader, JsonWriter> {
    @Override
    public JsonReader createReader(Reader source) {
        return new JsonReader(source);
    }

    @Override
    public JsonWriter createWriter(Writer target) throws IOException {
        return new JsonWriter(target);
    }

    @Override
    public String getFormatMime() {
        return "application/json";
    }
}
