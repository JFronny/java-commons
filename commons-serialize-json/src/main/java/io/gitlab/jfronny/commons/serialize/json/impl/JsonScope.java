package io.gitlab.jfronny.commons.serialize.json.impl;

public class JsonScope {
    /** An array with no elements requires no separator before the next element. */
    public static final int EMPTY_ARRAY = 1;

    /** An array with at least one value requires a separator before the next element. */
    public static final int NONEMPTY_ARRAY = 2;

    /** An object with no name/value pairs requires no separator before the next element. */
    public static final int EMPTY_OBJECT = 3;

    /** An object whose most recent element is a key. The next element must be a value. */
    public static final int DANGLING_NAME = 4;

    /** An object with at least one name/value pair requires a separator before the next element. */
    public static final int NONEMPTY_OBJECT = 5;

    /** No top-level value has been started yet. */
    public static final int EMPTY_DOCUMENT = 6;

    /** A top-level value has already been started. */
    public static final int NONEMPTY_DOCUMENT = 7;

    /** A document that's been closed and cannot be accessed. */
    public static final int CLOSED = 8;
}
