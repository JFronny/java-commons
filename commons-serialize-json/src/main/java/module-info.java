module io.gitlab.jfronny.commons.serialize.json {
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.serialize;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.serialize.json;
}