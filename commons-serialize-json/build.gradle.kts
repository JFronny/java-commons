import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    api(projects.commonsSerialize)

    testImplementation(libs.junit.jupiter.api)
    testImplementation(libs.google.truth)
    testRuntimeOnly(libs.junit.jupiter.engine)
    testRuntimeOnly(libs.junit.vintage)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-serialize-json"

            from(components["java"])
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize/$version/raw", projects.commonsSerialize)
}
