package some.other.location;

import java.util.List;
import java.util.Objects;

class ToConstruct {
    private final int argCount;
    private final String arg1;
    private final List<String> arg2;

    private ToConstruct() {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.C0;
        this.argCount = 1;
        this.arg1 = null;
        this.arg2 = null;
    }

    private ToConstruct(String arg1) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.C1;
        this.argCount = 2;
        this.arg1 = arg1;
        this.arg2 = null;
    }

    private ToConstruct(String arg1, List<String> arg2) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.C2;
        this.argCount = 3;
        this.arg1 = arg1;
        this.arg2 = arg2;
    }

    static Object create() {
        return new ToConstruct();
    }

    static Object create(String arg1) {
        return new ToConstruct(arg1);
    }

    static Object create(String arg1, List<String> arg2) {
        return new ToConstruct(arg1, arg2);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ToConstruct other
                && argCount == other.argCount
                && Objects.equals(arg1, other.arg1)
                && Objects.equals(arg2, other.arg2);
    }

    private static void procedure() {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.P0;
    }

    private void instanceProcedure() {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.P1;
    }

    private static void procedure(String arg1) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.P1;
    }

    private void instanceProcedure(String arg1) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.P2;
    }

    private static void procedure(String arg1, List<String> arg2) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.P2;
    }

    private static String function() {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.F0;
        return InstanceConstruct.lastProc.toString();
    }

    private String instanceFunction() {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.F1;
        return InstanceConstruct.lastProc.toString();
    }

    private static String function(String arg1) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.F1;
        return InstanceConstruct.lastProc.toString();
    }

    private String instanceFunction(String arg1) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.F2;
        return InstanceConstruct.lastProc.toString();
    }

    private static String function(String arg1, List<String> arg2) {
        InstanceConstruct.lastProc = InstanceConstruct.LastProc.F2;
        return InstanceConstruct.lastProc.toString();
    }
}
