package some.other.location;

import java.util.List;

public class InstanceConstruct {
    static LastProc lastProc;

    public static LastProc getLastProc() {
        LastProc tmp = lastProc;
        lastProc = null;
        return tmp;
    }

    public static Object construct() {
        return ToConstruct.create();
    }

    public static Object construct(String arg1) {
        return ToConstruct.create(arg1);
    }

    public static Object construct(String arg1, List<String> arg2) {
        return ToConstruct.create(arg1, arg2);
    }

    public enum LastProc {
        C0, C1, C2, P0, P1, P2, F0, F1, F2
    }
}
