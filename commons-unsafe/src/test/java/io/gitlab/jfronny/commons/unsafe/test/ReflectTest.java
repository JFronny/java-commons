package io.gitlab.jfronny.commons.unsafe.test;

import io.gitlab.jfronny.commons.unsafe.reflect.Reflect;
import org.junit.jupiter.api.Test;
import some.other.location.InstanceConstruct;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReflectTest {
    private static final String CLASS_NAME = "some.other.location.ToConstruct";

    @Test
    void noArgConstructor() {
        assertEquals(InstanceConstruct.construct(), assertDoesNotThrow(() -> Reflect.constructor(CLASS_NAME).get()));
        assertEquals(InstanceConstruct.LastProc.C0, InstanceConstruct.getLastProc());
    }

    @Test
    void oneArgConstructor() {
        String arg1 = "Some arg";
        assertEquals(InstanceConstruct.construct(arg1), assertDoesNotThrow(() -> Reflect.constructor(CLASS_NAME, String.class).apply(arg1)));
        assertEquals(InstanceConstruct.LastProc.C1, InstanceConstruct.getLastProc());
    }

    @Test
    void twoArgConstructor() {
        String arg1 = "Some other arg";
        List<String> arg2 = List.of("Hello", "World");
        assertEquals(InstanceConstruct.construct(arg1, arg2), assertDoesNotThrow(() -> Reflect.constructor(CLASS_NAME, String.class, List.class).apply(arg1, arg2)));
        assertEquals(InstanceConstruct.LastProc.C2, InstanceConstruct.getLastProc());
    }

    @Test
    void noArgProcedure() {
        assertDoesNotThrow(() -> Reflect.staticProcedure(CLASS_NAME, "procedure").run());
        assertEquals(InstanceConstruct.LastProc.P0, InstanceConstruct.getLastProc());
    }

    @Test
    void oneArgProcedure() {
        String arg1 = "Some arg";
        assertDoesNotThrow(() -> Reflect.staticProcedure(CLASS_NAME, "procedure", String.class).accept(arg1));
        assertEquals(InstanceConstruct.LastProc.P1, InstanceConstruct.getLastProc());
    }

    @Test
    void noArgInstanceProcedure() {
        assertDoesNotThrow(() -> Reflect.instanceProcedure(CLASS_NAME, "instanceProcedure").accept(InstanceConstruct.construct()));
        assertEquals(InstanceConstruct.LastProc.P1, InstanceConstruct.getLastProc());
    }

    @Test
    void twoArgProcedure() {
        String arg1 = "Some other arg";
        List<String> arg2 = List.of("Hello", "World");
        assertDoesNotThrow(() -> Reflect.staticProcedure(CLASS_NAME, "procedure", String.class, List.class).accept(arg1, arg2));
        assertEquals(InstanceConstruct.LastProc.P2, InstanceConstruct.getLastProc());
    }

    @Test
    void oneArgInstanceProcedure() {
        String arg1 = "Some arg";
        assertDoesNotThrow(() -> Reflect.instanceProcedure(CLASS_NAME, "instanceProcedure", String.class).accept(InstanceConstruct.construct(), arg1));
        assertEquals(InstanceConstruct.LastProc.P2, InstanceConstruct.getLastProc());
    }

    @Test
    void noArgFunction() {
        assertEquals(assertDoesNotThrow(() -> Reflect.staticFunction(CLASS_NAME, "function", String.class).get()), InstanceConstruct.getLastProc().toString());
    }

    @Test
    void oneArgFunction() {
        String arg1 = "Some arg";
        assertEquals(assertDoesNotThrow(() -> Reflect.staticFunction(CLASS_NAME, "function", String.class, String.class).apply(arg1)), InstanceConstruct.getLastProc().toString());
    }

    @Test
    void noArgInstanceFunction() {
        assertEquals(assertDoesNotThrow(() -> Reflect.instanceFunction(CLASS_NAME, "instanceFunction", String.class).apply(InstanceConstruct.construct())), InstanceConstruct.getLastProc().toString());
    }

    @Test
    void twoArgFunction() {
        String arg1 = "Some other arg";
        List<String> arg2 = List.of("Hello", "World");
        assertEquals(assertDoesNotThrow(() -> Reflect.staticFunction(CLASS_NAME, "function", String.class, String.class, List.class).apply(arg1, arg2)), InstanceConstruct.getLastProc().toString());
    }

    @Test
    void oneArgInstanceFunction() {
        String arg1 = "Some arg";
        assertEquals(assertDoesNotThrow(() -> Reflect.instanceFunction(CLASS_NAME, "instanceFunction", String.class, String.class).apply(InstanceConstruct.construct(), arg1)), InstanceConstruct.getLastProc().toString());
    }
}
