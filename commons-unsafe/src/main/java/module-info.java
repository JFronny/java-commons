module io.gitlab.jfronny.commons.unsafe {
    requires io.gitlab.jfronny.commons;
    requires jdk.unsupported;
    requires org.jetbrains.annotations;

    exports io.gitlab.jfronny.commons.unsafe;
    exports io.gitlab.jfronny.commons.unsafe.reflect;
}