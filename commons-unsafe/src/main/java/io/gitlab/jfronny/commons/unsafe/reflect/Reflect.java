package io.gitlab.jfronny.commons.unsafe.reflect;

import io.gitlab.jfronny.commons.StringFormatter;
import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.commons.unsafe.reflect.impl.CoreReflect;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.*;
import java.util.function.*;

/**
 * A class that provides a set of methods to create functional interfaces for reflective calls.
 */
@SuppressWarnings("unchecked")
public class Reflect {
    /**
     * Gets the method with the given name and parameter types from the given class.
     *
     * Emulates the access possible from within the class itself
     * (i.e. private methods and public methods of parents are included).
     *
     * @param clazz the class to get the method from
     * @param name the name of the method
     * @param parameterTypes the types of the parameters
     * @return the method with the given name and parameter types
     * @throws NoSuchMethodException if the method does not exist
     */
    public static Method getMethod(Class<?> clazz, String name, Class<?>... parameterTypes) throws NoSuchMethodException {
        Method method = getMethod(clazz, true, name, parameterTypes);
        if (method == null) throw new NoSuchMethodException(StringFormatter.methodToString(clazz, name, parameterTypes));
        return method;
    }

    private static @Nullable Method getMethod(Class<?> clazz, boolean withPrivate, String name, Class<?>... parameterTypes) {
        if (clazz == null) return null;
        outer: for (Method method : clazz.getDeclaredMethods()) {
            if (!method.getName().equals(name)) continue;
            if (method.getParameterCount() != parameterTypes.length) continue;
            for (int i = 0; i < parameterTypes.length; i++) {
                if (!method.getParameterTypes()[i].isAssignableFrom(parameterTypes[i])) continue outer;
            }
            if (!withPrivate && Modifier.isPrivate(method.getModifiers())) continue;
            return method;
        }
        for (Class<?> aClass : clazz.getInterfaces()) {
            Method method = getMethod(aClass, false, name, parameterTypes);
            if (method != null) return method;
        }
        return getMethod(clazz.getSuperclass(), false, name, parameterTypes);
    }

    /**
     * Gets all fields declared by the given class.
     * Does not apply filtering like {@link Class#getDeclaredFields()}.
     *
     * @param clazz the class to get the fields from
     * @return an array of all fields declared by the class
     */
    public static Field[] getDeclaredFields(Class<?> clazz) {
        return CoreReflect.getDeclaredFields(clazz);
    }

    /**
     * Gets the field with the given name from the given class.
     * Does not apply filtering like {@link Class#getDeclaredField(String)}.
     * Also does not apply caching, so use sparingly.
     *
     * @param clazz the class to get the field from
     * @param name the name of the field
     * @return the field with the given name
     * @throws NoSuchFieldException if the field does not exist
     */
    public static Field getDeclaredField(Class<?> clazz, String name) throws NoSuchFieldException {
        return CoreReflect.getDeclaredField(clazz, name);
    }

    /**
     * Creates a supplier that constructs the given class using the default, no-arg constructor.
     * @param toConstruct the class to construct
     * @param <TOut> the type of the class to construct
     * @return a supplier that constructs the class
     * @throws Throwable if an error occurs
     */
    public static <TOut> Supplier<TOut> constructor(Class<TOut> toConstruct) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(toConstruct);
        return LambdaFactory.supplier(lookup, lookup.unreflectConstructor(toConstruct.getDeclaredConstructor()), toConstruct);
    }

    /**
     * Creates a supplier that constructs the given class using the default, no-arg constructor.
     * @param targetClassName the name of the class to construct
     * @param <TOut> the type of the class to construct
     * @return a supplier that constructs the class
     * @throws Throwable if an error occurs
     */
    public static <TOut> Supplier<TOut> constructor(String targetClassName) throws Throwable {
        return (Supplier<TOut>) constructor(Class.forName(targetClassName));
    }

    /**
     * Creates a function that constructs the given class using a constructor with one parameter.
     * @param toConstruct the class to construct
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @param <TOut> the type of the class to construct
     * @return a function that constructs the class given a parameter
     * @throws Throwable if an error occurs
     */
    public static <TIn, TOut> Function<TIn, TOut> constructor(Class<TOut> toConstruct, Class<TIn> parameterType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(toConstruct);
        return LambdaFactory.function(lookup, lookup.unreflectConstructor(toConstruct.getDeclaredConstructor(parameterType)), toConstruct, mapLambdaParameter(parameterType));
    }

    /**
     * Creates a function that constructs the given class using a constructor with one parameter.
     * @param targetClassName the name of the class to construct
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @param <TOut> the type of the class to construct
     * @return a function that constructs the class given a parameter
     * @throws Throwable if an error occurs
     */
    public static <TIn, TOut> Function<TIn, TOut> constructor(String targetClassName, Class<TIn> parameterType) throws Throwable {
        return (Function<TIn, TOut>) constructor(Class.forName(targetClassName), parameterType);
    }

    /**
     * Creates a function that constructs the given class using a constructor with two parameters.
     * @param toConstruct the class to construct
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @param <TOut> the type of the class to construct
     * @return a function that constructs the class given two parameters
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2, TOut> BiFunction<TIn1, TIn2, TOut> constructor(Class<TOut> toConstruct, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(toConstruct);
        return LambdaFactory.function(lookup, lookup.unreflectConstructor(toConstruct.getDeclaredConstructor(parameterType1, parameterType2)), toConstruct, mapLambdaParameter(parameterType1), mapLambdaParameter(parameterType2));
    }

    /**
     * Creates a function that constructs the given class using a constructor with two parameters.
     * @param targetClassName the name of the class to construct
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @param <TOut> the type of the class to construct
     * @return a function that constructs the class given two parameters
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2, TOut> BiFunction<TIn1, TIn2, TOut> constructor(String targetClassName, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        return constructor((Class<TOut>) Class.forName(targetClassName), parameterType1, parameterType2);
    }

    /**
     * Creates a runnable that calls the given static method, which returns void and takes no arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @return a runnable that calls the method
     * @throws Throwable if an error occurs
     */
    public static Runnable staticProcedure(Class<?> targetClass, String name) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.runnable(lookup, lookup.unreflect(getMethod(targetClass, name)));
    }

    /**
     * Creates a runnable that calls the given static method, which returns void and takes no arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @return a runnable that calls the method
     * @throws Throwable if an error occurs
     */
    public static Runnable staticProcedure(String targetClassName, String name) throws Throwable {
        return staticProcedure(Class.forName(targetClassName), name);
    }

    /**
     * Creates a consumer that calls the given static method, which returns void and takes one argument.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn> Consumer<TIn> staticProcedure(Class<?> targetClass, String name, Class<TIn> parameterType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.consumer(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType)), mapLambdaParameter(parameterType));
    }

    /**
     * Creates a consumer that calls the given static method, which returns void and takes one argument.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn> Consumer<TIn> staticProcedure(String targetClassName, String name, Class<TIn> parameterType) throws Throwable {
        return staticProcedure(Class.forName(targetClassName), name, parameterType);
    }

    /**
     * Creates a consumer that calls the given static method, which returns void and takes two arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2> BiConsumer<TIn1, TIn2> staticProcedure(Class<?> targetClass, String name, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.consumer(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType1, parameterType2)), mapLambdaParameter(parameterType1), mapLambdaParameter(parameterType2));
    }

    /**
     * Creates a consumer that calls the given static method, which returns void and takes two arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2> BiConsumer<TIn1, TIn2> staticProcedure(String targetClassName, String name, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        return staticProcedure(Class.forName(targetClassName), name, parameterType1, parameterType2);
    }

    /**
     * Creates a supplier that calls the given static method, which returns a value and takes no arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param <TOut> the return type of the method
     * @return a supplier that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TOut> Supplier<TOut> staticFunction(Class<?> targetClass, String name, Class<TOut> returnType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.supplier(lookup, lookup.unreflect(getMethod(targetClass, name)), returnType);
    }

    /**
     * Creates a supplier that calls the given static method, which returns a value and takes no arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param <TOut> the return type of the method
     * @return a supplier that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TOut> Supplier<TOut> staticFunction(String targetClassName, String name, Class<TOut> returnType) throws Throwable {
        return staticFunction(Class.forName(targetClassName), name, returnType);
    }

    /**
     * Creates a function that calls the given static method, which returns a value and takes one argument.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn, TOut> Function<TIn, TOut> staticFunction(Class<?> targetClass, String name, Class<TOut> returnType, Class<TIn> parameterType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.function(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType)), returnType, mapLambdaParameter(parameterType));
    }

    /**
     * Creates a function that calls the given static method, which returns a value and takes one argument.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType the type of the parameter
     * @param <TIn> the type of the parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn, TOut> Function<TIn, TOut> staticFunction(String targetClassName, String name, Class<TOut> returnType, Class<TIn> parameterType) throws Throwable {
        return staticFunction(Class.forName(targetClassName), name, returnType, parameterType);
    }

    /**
     * Creates a function that calls the given static method, which returns a value and takes two arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2, TOut> BiFunction<TIn1, TIn2, TOut> staticFunction(Class<?> targetClass, String name, Class<TOut> returnType, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.function(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType1, parameterType2)), returnType, mapLambdaParameter(parameterType1), mapLambdaParameter(parameterType2));
    }

    /**
     * Creates a function that calls the given static method, which returns a value and takes two arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType1 the type of the first parameter
     * @param parameterType2 the type of the second parameter
     * @param <TIn1> the type of the first parameter
     * @param <TIn2> the type of the second parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TIn1, TIn2, TOut> BiFunction<TIn1, TIn2, TOut> staticFunction(String targetClassName, String name, Class<TOut> returnType, Class<TIn1> parameterType1, Class<TIn2> parameterType2) throws Throwable {
        return staticFunction(Class.forName(targetClassName), name, returnType, parameterType1, parameterType2);
    }

    /**
     * Creates a consumer that calls the given instance method, which returns void and takes no arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param <TTarget> the type of the class containing the method
     * @return a supplier that constructs the class
     * @throws Throwable if an error occurs
     */
    public static <TTarget> Consumer<TTarget> instanceProcedure(Class<TTarget> targetClass, String name) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.consumer(lookup, lookup.unreflect(getMethod(targetClass, name)), targetClass);
    }

    /**
     * Creates a consumer that calls the given instance method, which returns void and takes no arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param <TTarget> the type of the class containing the method
     * @return a supplier that constructs the class
     * @throws Throwable if an error occurs
     */
    public static <TTarget> Consumer<TTarget> instanceProcedure(String targetClassName, String name) throws Throwable {
        return (Consumer<TTarget>) instanceProcedure(Class.forName(targetClassName), name);
    }

    /**
     * Creates a consumer that calls the given instance method, which returns void and takes one argument.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param parameterType the type of the parameter
     * @param <TTarget> the type of the class containing the method
     * @param <TIn> the type of the parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TIn> BiConsumer<TTarget, TIn> instanceProcedure(Class<TTarget> targetClass, String name, Class<TIn> parameterType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.consumer(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType)), targetClass, mapLambdaParameter(parameterType));
    }

    /**
     * Creates a consumer that calls the given instance method, which returns void and takes one argument.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param parameterType the type of the parameter
     * @param <TTarget> the type of the class containing the method
     * @param <TIn> the type of the parameter
     * @return a consumer that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TIn> BiConsumer<TTarget, TIn> instanceProcedure(String targetClassName, String name, Class<TIn> parameterType) throws Throwable {
        return (BiConsumer<TTarget, TIn>) instanceProcedure(Class.forName(targetClassName), name, mapLambdaParameter(parameterType));
    }

    /**
     * Creates a function that calls the given instance method, which returns a value and takes no arguments.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param <TTarget> the type of the class containing the method
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TOut> Function<TTarget, TOut> instanceFunction(Class<TTarget> targetClass, String name, Class<TOut> returnType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.function(lookup, lookup.unreflect(getMethod(targetClass, name)), returnType, targetClass);
    }

    /**
     * Creates a function that calls the given instance method, which returns a value and takes no arguments.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param <TTarget> the type of the class containing the method
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TOut> Function<TTarget, TOut> instanceFunction(String targetClassName, String name, Class<TOut> returnType) throws Throwable {
        return (Function<TTarget, TOut>) instanceFunction(Class.forName(targetClassName), name, returnType);
    }

    /**
     * Creates a function that calls the given instance method, which returns a value and takes one argument.
     * @param targetClass the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType the type of the parameter
     * @param <TTarget> the type of the class containing the method
     * @param <TIn> the type of the parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TIn, TOut> BiFunction<TTarget, TIn, TOut> instanceFunction(Class<TTarget> targetClass, String name, Class<TOut> returnType, Class<TIn> parameterType) throws Throwable {
        MethodHandles.Lookup lookup = LambdaFactory.lookup(targetClass);
        return LambdaFactory.function(lookup, lookup.unreflect(getMethod(targetClass, name, parameterType)), returnType, targetClass, mapLambdaParameter(parameterType));
    }

    /**
     * Creates a function that calls the given instance method, which returns a value and takes one argument.
     * @param targetClassName the name of the class containing the method
     * @param name the name of the method
     * @param returnType the return type of the method
     * @param parameterType the type of the parameter
     * @param <TTarget> the type of the class containing the method
     * @param <TIn> the type of the parameter
     * @param <TOut> the return type of the method
     * @return a function that calls the method
     * @throws Throwable if an error occurs
     */
    public static <TTarget, TIn, TOut> BiFunction<TTarget, TIn, TOut> instanceFunction(String targetClassName, String name, Class<TOut> returnType, Class<TIn> parameterType) throws Throwable {
        return (BiFunction<TTarget, TIn, TOut>) instanceFunction(Class.forName(targetClassName), name, returnType, parameterType);
    }

    private static <T> Class<T> mapLambdaParameter(Class<T> klazz) {
        if (klazz == boolean.class) return (Class<T>) Boolean.class;
        if (klazz == byte.class) return (Class<T>) Byte.class;
        if (klazz == short.class) return (Class<T>) Short.class;
        if (klazz == char.class) return (Class<T>) Character.class;
        if (klazz == int.class) return (Class<T>) Integer.class;
        if (klazz == long.class) return (Class<T>) Long.class;
        if (klazz == float.class) return (Class<T>) Float.class;
        if (klazz == double.class) return (Class<T>) Double.class;
        return klazz;
    }

    private static final MethodHandle accessibleSetter = Try.orThrow(() -> CoreReflect.lookup(AccessibleObject.class).findSetter(AccessibleObject.class, "override", boolean.class));
    public static void setAccessible(Field field) throws Throwable {
        accessibleSetter.invoke(field, true);
    }
}
