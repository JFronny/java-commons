package io.gitlab.jfronny.commons.unsafe.reflect.impl;

import io.gitlab.jfronny.commons.throwable.Try;
import io.gitlab.jfronny.commons.unsafe.Unsafe;

import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.function.BiFunction;

public class CoreReflect {
    private static final MethodHandles.Lookup LOOKUP_ROOT;
    private static final BiFunction<Class<?>, Boolean, Field[]> getDeclaredFields;

    static {
        // Used to be just MethodHandles.lookup()
        // The offset used here was obtained in a debugger. It may not be correct for all JVMs or versions.
        // It cannot be obtained using Unsafe.objectFieldOffset at runtime because the field is hidden from reflection, and we need it to enable our reflection into hidden fields.
        LOOKUP_ROOT = MethodHandles.lookup(); // Create a lookup for the current class
        Class<?> lookupClass = MethodHandles.Lookup.class; // Get the lookup class
        //Field field = Try.orThrow(() -> lookupClass.getDeclaredField("lookupClass")); // Get the lookupClass field
        long offset = 16; // Unsafe.objectFieldOffset(field); // Get the offset of the lookupClass field
        Unsafe.putObject(LOOKUP_ROOT, offset, lookupClass); // Set the lookupClass field to the lookup class. We use MethodHandles.Lookup because we need it to set the allowedModes field to -1
        Try.orThrow(() -> {
            // Unsafe.putObject(lookup, Unsafe.objectFieldOffset(Reflect.getDeclaredField(lookupClass, "lookupClass")), lookupClass);
            MethodHandles.Lookup lookup = lookup(Class.class);
            //noinspection unchecked
            Unsafe.putInt(
                    LOOKUP_ROOT,
                    Unsafe.objectFieldOffset(Arrays.stream(((BiFunction<Class, Boolean, Field[]>) LambdaMetafactory.metafactory(
                                    lookup,
                                    "apply",
                                    MethodType.methodType(BiFunction.class),
                                    MethodType.methodType(Object.class, Object.class, Object.class),
                                    lookup.unreflect(Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class)),
                                    MethodType.methodType(Field[].class, Class.class, Boolean.class)
                            ).getTarget().invoke()).apply(lookupClass, false))
                            .filter(f -> f.getName().equals("allowedModes"))
                            .findFirst()
                            .orElseThrow()),
                    -1);
            Unsafe.putObject(LOOKUP_ROOT, offset, Object.class); // Configures this lookup to be identical to IMPL_LOOKUP
        });
        // Obtain the unfiltered getDeclaredFields method
        getDeclaredFields = Try.orThrow(() -> {
            MethodHandles.Lookup lookup = lookup(Class.class);
            //noinspection unchecked
            return (BiFunction<Class<?>, Boolean, Field[]>) LambdaMetafactory.metafactory(
                    lookup,
                    "apply",
                    MethodType.methodType(BiFunction.class),
                    MethodType.methodType(Object.class, Object.class, Object.class),
                    lookup.unreflect(Class.class.getDeclaredMethod("getDeclaredFields0", boolean.class)),
                    MethodType.methodType(Field[].class, Class.class, Boolean.class)
            ).getTarget().invoke();
        });
    }

    public static Field[] getDeclaredFields(Class<?> clazz) {
        return getDeclaredFields.apply(clazz, false);
    }

    public static Field getDeclaredField(Class<?> clazz, String name) throws NoSuchFieldException {
        return Arrays.stream(getDeclaredFields(clazz))
                .filter(f -> f.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new NoSuchFieldException(name));
    }

    public static MethodHandles.Lookup lookup(Class<?> klazz) throws IllegalAccessException {
        return MethodHandles.privateLookupIn(klazz, LOOKUP_ROOT);
    }
}
