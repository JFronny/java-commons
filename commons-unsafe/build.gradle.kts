plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-unsafe"

            from(components["java"])
        }
    }
}
