module io.gitlab.jfronny.commons.http.client {
    requires java.net.http;
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    exports io.gitlab.jfronny.commons.http.client;
}