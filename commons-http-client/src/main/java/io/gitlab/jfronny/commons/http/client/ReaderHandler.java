package io.gitlab.jfronny.commons.http.client;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;

public record ReaderHandler(HttpResponse.BodySubscriber<InputStream> delegate) implements HttpResponse.BodySubscriber<Reader> {
    public static HttpResponse.BodyHandler<Reader> of() {
        return responseInfo -> new ReaderHandler(HttpResponse.BodySubscribers.ofInputStream());
    }

    @Override
    public CompletionStage<Reader> getBody() {
        return this.delegate.getBody().thenApply(in -> in == null ? null : new InputStreamReader(in));
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.delegate.onSubscribe(subscription);
    }

    @Override
    public void onNext(List<ByteBuffer> item) {
        this.delegate.onNext(item);
    }

    @Override
    public void onError(Throwable throwable) {
        this.delegate.onError(throwable);
    }

    @Override
    public void onComplete() {
        this.delegate.onComplete();
    }
}
