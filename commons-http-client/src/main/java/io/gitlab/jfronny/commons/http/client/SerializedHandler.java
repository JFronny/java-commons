package io.gitlab.jfronny.commons.http.client;

import io.gitlab.jfronny.commons.data.Either;
import io.gitlab.jfronny.commons.Serializer;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;

public record SerializedHandler<T>(HttpResponse.BodySubscriber<Reader> delegate, Serializer serializer, Type type) implements HttpResponse.BodySubscriber<Either<T, IOException>> {
    public static <T> HttpResponse.BodyHandler<Either<T, IOException>> of(Serializer serializer, Type type) {
        return responseInfo -> new SerializedHandler<>(new ReaderHandler(HttpResponse.BodySubscribers.ofInputStream()), serializer, type);
    }

    @Override
    public CompletionStage<Either<T, IOException>> getBody() {
        return this.delegate.getBody().thenApply(in -> {
            try {
                return Either.left(in == null ? null : this.serializer.<T>deserialize(in, this.type));
            } catch (IOException e) {
                return Either.right(e);
            }
        });
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.delegate.onSubscribe(subscription);
    }

    @Override
    public void onNext(List<ByteBuffer> byteBuffers) {
        this.delegate.onNext(byteBuffers);
    }

    @Override
    public void onError(Throwable throwable) {
        this.delegate.onError(throwable);
    }

    @Override
    public void onComplete() {
        this.delegate.onComplete();
    }
}
