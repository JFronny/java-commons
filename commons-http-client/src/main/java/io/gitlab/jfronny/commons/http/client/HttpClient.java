package io.gitlab.jfronny.commons.http.client;

import java.net.*;
import java.time.Duration;
import java.util.*;

public class HttpClient {
    protected static String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36";
    protected static final String PROXY_AUTH;
    protected static final java.net.http.HttpClient CLIENT;
    protected static final java.net.http.HttpClient CLIENT11;

    static {
        // Enables HTTPS proxying
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");

        java.net.http.HttpClient.Builder clientBuilder = java.net.http.HttpClient.newBuilder().followRedirects(java.net.http.HttpClient.Redirect.ALWAYS);

        String host = System.getProperty("http.proxyHost");
        String port = System.getProperty("http.proxyPort");
        if (host != null && port != null) {
            if (port.matches("\\d+")) {
                clientBuilder.proxy(ProxySelector.of(new InetSocketAddress(host, Integer.parseInt(port))));
            } else {
                System.err.println("Could not parse proxy settings: Port is not a number");
            }
        }

        String name = System.getProperty("http.proxyUserName");
        String pass = System.getProperty("http.proxyUserPassword");
        if (name != null && pass != null) {
            PROXY_AUTH = "Basic " + new String(Base64.getEncoder().encode((name + ":" + pass).getBytes()));
        } else {
            PROXY_AUTH = null;
        }

        CLIENT = clientBuilder.build();
        CLIENT11 = clientBuilder.version(java.net.http.HttpClient.Version.HTTP_1_1).build();
    }

    public static void setUserAgent(String hostname) {
        if (hostname == null || hostname.isEmpty()) throw new IllegalArgumentException("Hostname cannot be empty");
        userAgent = hostname;
    }

    public static RequestBuilder get(String url) throws URISyntaxException {
        return new RequestBuilder(Method.GET, url);
    }

    public static RequestBuilder post(String url) throws URISyntaxException {
        return new RequestBuilder(Method.POST, url);
    }

    public static RequestBuilder put(String url) throws URISyntaxException {
        return new RequestBuilder(Method.PUT, url);
    }

    public static RequestBuilder patch(String url) throws URISyntaxException {
        return new RequestBuilder(Method.PATCH, url);
    }

    public static RequestBuilder delete(String url) throws URISyntaxException {
        return new RequestBuilder(Method.DELETE, url);
    }

    private static final long CHECK_INTERVAL = 10000;
    private static boolean wasOnline = false;
    private static long lastCheck = 0;

    public static void scheduleOnlineCheck() {
        Thread.startVirtualThread(() -> {
            try {
                while (true) {
                    try {
                        long toWait = CHECK_INTERVAL - (System.currentTimeMillis() - lastCheck);
                        Thread.sleep(Math.max(50, toWait));
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    checkOnline();
                }
            } catch (Throwable t) {
                System.getLogger("commons-http-client").log(System.Logger.Level.ERROR, "Something went wrong in the scheduled online check. Disabling until restart", t);
            }
        });
    }

    public static boolean wasOnline() {
        return wasOnline;
    }

    public static boolean checkOnline() {
        try {
            get("https://one.one.one.one").timeout(Duration.ofSeconds(5)).send();
            wasOnline = true;
        } catch (Throwable e) {
            wasOnline = false;
        }
        lastCheck = System.currentTimeMillis();
        return wasOnline;
    }
}
