package io.gitlab.jfronny.commons.http.client;

enum Method {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}
