package io.gitlab.jfronny.commons.http.client;

enum ResponseHandlingMode {
    IGNORE_ALL, HANDLE_REDIRECTS, HANDLE_ALL
}
