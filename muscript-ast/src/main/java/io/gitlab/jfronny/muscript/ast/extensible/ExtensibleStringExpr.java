package io.gitlab.jfronny.muscript.ast.extensible;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.ast.context.IScope;

import java.util.stream.Stream;

public non-sealed interface ExtensibleStringExpr extends StringExpr, ExtensibleExpr {
    @Override
    default StringExpr optimize() {
        return this;
    }

    @Override
    default Stream<Expr> extractSideEffects() {
        return Stream.of(this);
    }

    String evaluate(IScope scope);
}
