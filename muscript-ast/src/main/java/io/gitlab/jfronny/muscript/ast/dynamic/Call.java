package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

import java.util.List;

public record Call(CodeLocation location, DynamicExpr callable, List<Argument> arguments) implements DynamicExpr {
    @Override
    public Order order() {
        return Order.Call;
    }

    public record Argument(DynamicExpr value, boolean variadic) {
    }
}
