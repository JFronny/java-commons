package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record DynamicCoerce(Expr inner) implements DynamicExpr {
    @Override
    public CodeLocation location() {
        return inner.location();
    }

    @Override
    public Order order() {
        return inner.order();
    }
}
