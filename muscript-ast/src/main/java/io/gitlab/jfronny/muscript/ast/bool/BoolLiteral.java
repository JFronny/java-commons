package io.gitlab.jfronny.muscript.ast.bool;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record BoolLiteral(CodeLocation location, boolean value) implements BoolExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
