package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record StringAssign(CodeLocation location, String variable, StringExpr value) implements StringExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
