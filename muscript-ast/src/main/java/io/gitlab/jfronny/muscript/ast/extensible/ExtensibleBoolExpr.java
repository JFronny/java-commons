package io.gitlab.jfronny.muscript.ast.extensible;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.IScope;

import java.util.stream.Stream;

public non-sealed interface ExtensibleBoolExpr extends BoolExpr, ExtensibleExpr {
    @Override
    default BoolExpr optimize() {
        return this;
    }

    @Override
    default Stream<Expr> extractSideEffects() {
        return Stream.of(this);
    }

    boolean evaluate(IScope scope);
}
