package io.gitlab.jfronny.muscript.ast.bool;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record BoolAssign(CodeLocation location, String variable, BoolExpr value) implements BoolExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
