package io.gitlab.jfronny.muscript.ast.context;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.core.SourceFS;

import java.util.ServiceLoader;

public interface IExprParser {
    IExprParser INSTANCE = ServiceLoader.load(IExprParser.class)
            .findFirst()
            .orElseGet(() -> new IExprParser() {
                @Override
                public Expr parse(MuScriptVersion ver, String expr) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public Script parseMultiScript(MuScriptVersion version, String path, SourceFS filesystem) {
                    throw new UnsupportedOperationException();
                }
            });

    default Expr parse(String expr) {
        return parse(MuScriptVersion.DEFAULT, expr);
    }
    Expr parse(MuScriptVersion version, String expr);
    Script parseMultiScript(MuScriptVersion version, String path, SourceFS filesystem);
}
