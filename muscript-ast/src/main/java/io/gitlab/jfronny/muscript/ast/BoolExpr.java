package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.ast.bool.*;
import io.gitlab.jfronny.muscript.ast.dynamic.Equals;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleBoolExpr;
import io.gitlab.jfronny.muscript.ast.number.GreaterThan;

public sealed interface BoolExpr extends Expr permits And, BoolAssign, BoolConditional, BoolLiteral, BoolUnpack, Not, Or, Equals, ExtensibleBoolExpr, GreaterThan {
}
