package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record Modulo(CodeLocation location, NumberExpr dividend, NumberExpr divisor) implements NumberExpr {
    @Override
    public Order order() {
        return Order.Factor;
    }
}
