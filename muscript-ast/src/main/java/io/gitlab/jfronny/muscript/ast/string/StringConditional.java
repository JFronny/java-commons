package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record StringConditional(CodeLocation location, BoolExpr condition, StringExpr ifTrue, StringExpr ifFalse) implements StringExpr {
    @Override
    public Order order() {
        return Order.Conditional;
    }
}
