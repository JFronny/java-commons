package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

import java.util.Map;

public record ObjectLiteral(CodeLocation location, Map<String, DynamicExpr> content) implements DynamicExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
