package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleNumberExpr;
import io.gitlab.jfronny.muscript.ast.number.*;

public sealed interface NumberExpr extends Expr permits ExtensibleNumberExpr, Add, Divide, Modulo, Multiply, Negate, NumberAssign, NumberConditional, NumberLiteral, NumberUnpack, Power, Subtract {
}
