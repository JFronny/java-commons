package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record This(CodeLocation location) implements DynamicExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
