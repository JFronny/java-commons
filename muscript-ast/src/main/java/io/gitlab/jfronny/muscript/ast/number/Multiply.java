package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record Multiply(CodeLocation location, NumberExpr multiplier, NumberExpr multiplicand) implements NumberExpr {
    @Override
    public Order order() {
        return Order.Factor;
    }
}
