package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record StringCoerce(Expr inner) implements StringExpr {
    @Override
    public CodeLocation location() {
        return inner.location();
    }

    @Override
    public Order order() {
        return inner.order();
    }
}
