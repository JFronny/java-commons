package io.gitlab.jfronny.muscript.ast.context;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.dynamic.ExprGroup;
import io.gitlab.jfronny.muscript.core.CodeLocation;

import java.util.List;
import java.util.stream.Stream;

public record Script(DynamicExpr content) {
    public Script(List<Expr> expressions) {
        this(new ExprGroup(null, expressions, null, true));
    }

    public Script(List<Expr> expressions, String fileName) {
        this(expressions, CodeLocation.NONE.withFile(fileName));
    }

    public Script(List<Expr> expressions, CodeLocation location) {
        this(new ExprGroup(location, expressions, null, true));
    }

    public Stream<Expr> stream() {
        return content instanceof ExprGroup group ? group.stream() : Stream.of(content);
    }

    public Script concat(Script other) {
        return new Script(Stream.concat(this.stream(), other.stream()).toList());
    }
}
