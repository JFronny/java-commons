package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

import java.util.List;

public record ListLiteral(CodeLocation location, List<DynamicExpr> elements) implements DynamicExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
