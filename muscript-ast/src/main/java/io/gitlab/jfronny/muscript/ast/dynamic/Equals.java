package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record Equals(CodeLocation location, Expr left, Expr right) implements BoolExpr {
    @Override
    public Order order() {
        return Order.Equality;
    }
}
