package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record DynamicConditional(CodeLocation location, BoolExpr condition, DynamicExpr ifTrue, DynamicExpr ifFalse) implements DynamicExpr {
    @Override
    public Order order() {
        return Order.Conditional;
    }
}
