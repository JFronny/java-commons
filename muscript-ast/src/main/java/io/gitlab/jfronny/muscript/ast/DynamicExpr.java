package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.ast.dynamic.*;
import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleDynamicExpr;

public sealed interface DynamicExpr extends Expr permits At, Bind, Call, Closure, DynamicAssign, DynamicCoerce, DynamicConditional, DynamicLiteral, ExprGroup, Get, GetOrAt, ListLiteral, ObjectLiteral, This, Variable, ExtensibleDynamicExpr {
}
