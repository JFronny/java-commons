package io.gitlab.jfronny.muscript.ast.context;

public class TypeMismatchException extends RuntimeException {
    public TypeMismatchException(String message) {
        super(message);
    }
}
