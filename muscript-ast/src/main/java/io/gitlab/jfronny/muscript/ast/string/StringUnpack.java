package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record StringUnpack(DynamicExpr inner) implements StringExpr {
    @Override
    public CodeLocation location() {
        return inner.location();
    }

    @Override
    public Order order() {
        return inner.order();
    }
}
