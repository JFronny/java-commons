package io.gitlab.jfronny.muscript.ast.extensible;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.core.ExprWriter;

import java.util.stream.Stream;

public sealed interface ExtensibleExpr permits ExtensibleBoolExpr, ExtensibleDynamicExpr, ExtensibleNumberExpr, ExtensibleStringExpr {
    void decompile(ExprWriter writer);
    Expr optimize();
    Stream<Expr> extractSideEffects();
}
