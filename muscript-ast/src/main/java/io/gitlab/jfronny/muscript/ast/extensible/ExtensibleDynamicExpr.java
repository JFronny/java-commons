package io.gitlab.jfronny.muscript.ast.extensible;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.IScope;
import io.gitlab.jfronny.muscript.core.IDynamic;

import java.util.stream.Stream;

public non-sealed interface ExtensibleDynamicExpr extends DynamicExpr, ExtensibleExpr {
    @Override
    default DynamicExpr optimize() {
        return this;
    }

    @Override
    default Stream<Expr> extractSideEffects() {
        return Stream.of(this);
    }

    IDynamic evaluate(IScope scope);
}
