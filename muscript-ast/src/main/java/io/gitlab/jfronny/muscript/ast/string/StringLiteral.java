package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record StringLiteral(CodeLocation location, String value) implements StringExpr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
