package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record NumberConditional(CodeLocation location, BoolExpr condition, NumberExpr ifTrue, NumberExpr ifFalse) implements NumberExpr {
    @Override
    public Order order() {
        return Order.Conditional;
    }
}
