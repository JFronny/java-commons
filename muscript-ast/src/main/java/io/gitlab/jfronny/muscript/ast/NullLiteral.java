package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record NullLiteral(CodeLocation location) implements Expr {
    @Override
    public Order order() {
        return Order.Primary;
    }
}
