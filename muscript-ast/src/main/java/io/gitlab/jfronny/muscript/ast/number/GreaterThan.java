package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record GreaterThan(CodeLocation location, NumberExpr left, NumberExpr right) implements BoolExpr {
    @Override
    public Order order() {
        return Order.Comparison;
    }
}
