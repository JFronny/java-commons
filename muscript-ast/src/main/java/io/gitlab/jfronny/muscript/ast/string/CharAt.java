package io.gitlab.jfronny.muscript.ast.string;

import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record CharAt(CodeLocation location, StringExpr left, NumberExpr index) implements StringExpr {
    @Override
    public Order order() {
        return Order.Call;
    }
}
