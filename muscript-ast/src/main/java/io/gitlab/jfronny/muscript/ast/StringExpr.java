package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.ast.extensible.ExtensibleStringExpr;
import io.gitlab.jfronny.muscript.ast.string.*;

public sealed interface StringExpr extends Expr permits ExtensibleStringExpr, CharAt, Concatenate, StringAssign, StringCoerce, StringConditional, StringLiteral, StringUnpack {
}
