package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.context.ExprUtils;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

import java.util.List;
import java.util.stream.Stream;

public record Closure(CodeLocation location, List<String> boundArgs, boolean variadic, List<Expr> steps, DynamicExpr finish) implements DynamicExpr {
    public Closure(CodeLocation location, List<String> boundArgs, boolean variadic, List<Expr> expressions) {
        //TODO use static methods before constructor once available
        this(location, boundArgs, variadic, expressions.subList(0, expressions.size() - 1), ExprUtils.asDynamic(expressions.getLast()));
    }

    public Stream<Expr> stream() {
        return Stream.concat(steps.stream(), Stream.of(finish));
    }

    @Override
    public Order order() {
        return Order.Primary;
    }
}
