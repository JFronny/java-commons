package io.gitlab.jfronny.muscript.ast;

import io.gitlab.jfronny.muscript.ast.bool.BoolLiteral;
import io.gitlab.jfronny.muscript.ast.number.NumberLiteral;
import io.gitlab.jfronny.muscript.ast.string.StringLiteral;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public sealed interface Expr permits BoolExpr, DynamicExpr, NullLiteral, NumberExpr, StringExpr {
    CodeLocation location();
    Order order();

    public static BoolExpr literal(boolean bool) {
        return literal(null, bool);
    }

    public static StringExpr literal(String string) {
        return literal(null, string);
    }

    public static NumberExpr literal(double number) {
        return literal(null, number);
    }

    public static NullLiteral literalNull() {
        return literalNull(null);
    }

    public static BoolExpr literal(CodeLocation location, boolean bool) {
        return new BoolLiteral(location, bool);
    }

    public static StringExpr literal(CodeLocation location, String string) {
        return new StringLiteral(location, string);
    }

    public static NumberExpr literal(CodeLocation location, double number) {
        return new NumberLiteral(location, number);
    }

    public static NullLiteral literalNull(CodeLocation location) {
        return new NullLiteral(location);
    }
}
