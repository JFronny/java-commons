package io.gitlab.jfronny.muscript.ast.extensible;

import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.ast.context.IScope;

import java.util.stream.Stream;

public non-sealed interface ExtensibleNumberExpr extends NumberExpr, ExtensibleExpr {
    @Override
    default NumberExpr optimize() {
        return this;
    }

    @Override
    default Stream<Expr> extractSideEffects() {
        return Stream.of(this);
    }

    double evaluate(IScope scope);
}
