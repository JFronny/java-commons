package io.gitlab.jfronny.muscript.ast.bool;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record BoolUnpack(DynamicExpr inner) implements BoolExpr {
    @Override
    public CodeLocation location() {
        return inner.location();
    }

    @Override
    public Order order() {
        return inner.order();
    }
}
