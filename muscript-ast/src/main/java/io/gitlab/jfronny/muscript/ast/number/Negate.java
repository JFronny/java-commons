package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record Negate(CodeLocation location, NumberExpr inner) implements NumberExpr {
    @Override
    public Order order() {
        return Order.Unary;
    }
}
