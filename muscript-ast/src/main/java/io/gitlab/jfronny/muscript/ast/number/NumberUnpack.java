package io.gitlab.jfronny.muscript.ast.number;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.NumberExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record NumberUnpack(DynamicExpr inner) implements NumberExpr {
    @Override
    public CodeLocation location() {
        return inner.location();
    }

    @Override
    public Order order() {
        return inner.order();
    }
}
