package io.gitlab.jfronny.muscript.ast.bool;

import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;

public record BoolConditional(CodeLocation location, BoolExpr condition, BoolExpr ifTrue, BoolExpr ifFalse) implements BoolExpr {
    @Override
    public Order order() {
        return Order.Conditional;
    }
}
