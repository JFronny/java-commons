package io.gitlab.jfronny.muscript.ast.dynamic;

import io.gitlab.jfronny.muscript.ast.DynamicExpr;
import io.gitlab.jfronny.muscript.ast.Expr;
import io.gitlab.jfronny.muscript.ast.NullLiteral;
import io.gitlab.jfronny.muscript.ast.context.ExprUtils;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.Order;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Stream;

public record ExprGroup(CodeLocation location, List<Expr> steps, DynamicExpr finish, @Nullable PackedArgs packedArgs, boolean fork) implements DynamicExpr {
    public ExprGroup(CodeLocation location, List<Expr> expressions, @Nullable PackedArgs packedArgs, boolean fork) {
        this(location, expressions.subList(0, expressions.size() - 1), ExprUtils.asDynamic(expressions.getLast()), packedArgs, fork);
    }

    public static Expr of(CodeLocation location, List<Expr> expressions) {
        return of(location, expressions, true);
    }

    public static Expr of(CodeLocation location, List<Expr> expressions, boolean fork) {
        if (expressions.isEmpty()) return new NullLiteral(location);
        if (!fork && expressions.size() == 1) return expressions.getFirst();
        return new ExprGroup(location, expressions, null, fork);
    }

    public Stream<Expr> stream() {
        return Stream.concat(steps.stream(), Stream.of(finish));
    }

    @Override
    public Order order() {
        return Order.Primary;
    }

    public record PackedArgs(List<Call.Argument> from, List<String> to, boolean variadic) {
    }
}
