package io.gitlab.jfronny.muscript.ast.context;

import io.gitlab.jfronny.muscript.ast.Expr;
import org.jetbrains.annotations.Nullable;

import java.util.ServiceLoader;

public interface IExprSerializer {
    IExprSerializer INSTANCE = ServiceLoader.load(IExprSerializer.class)
            .findFirst()
            .orElseGet(() -> new IExprSerializer() {
                @Override
                public @Nullable String serialize(Expr expr) {
                    return null;
                }

                @Override
                public @Nullable String serialize(Script script) {
                    return null;
                }
            });

    @Nullable String serialize(Expr expr);
    @Nullable String serialize(Script script);
}
