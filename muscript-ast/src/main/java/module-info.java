module io.gitlab.jfronny.commons.muscript.ast {
    uses io.gitlab.jfronny.muscript.ast.context.IExprParser;
    uses io.gitlab.jfronny.muscript.ast.context.IExprSerializer;
    requires io.gitlab.jfronny.commons;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons.muscript.core;
    exports io.gitlab.jfronny.muscript.ast;
    exports io.gitlab.jfronny.muscript.ast.extensible;
    exports io.gitlab.jfronny.muscript.ast.context;
    exports io.gitlab.jfronny.muscript.ast.bool;
    exports io.gitlab.jfronny.muscript.ast.number;
    exports io.gitlab.jfronny.muscript.ast.string;
    exports io.gitlab.jfronny.muscript.ast.dynamic;
}