plugins {
    commons.library
}

dependencies {
    api(projects.commons)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-serialize"

            from(components["java"])
        }
    }
}
