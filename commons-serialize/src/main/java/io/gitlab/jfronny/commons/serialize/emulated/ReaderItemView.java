package io.gitlab.jfronny.commons.serialize.emulated;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.Token;

public class ReaderItemView<TEx extends Exception> extends SerializeReader.Delegating<TEx, ReaderItemView<TEx>> {
    private int depth = 0;
    private boolean began = false;

    public ReaderItemView(SerializeReader<TEx, ?> delegate) {
        super(delegate);
    }

    private void validateLegalBegin() throws TEx {
        if (depth -1 >= nestingLimit) {
            throw createException("Nesting limit " + nestingLimit + " reached" + locationString());
        }

        if (!began) began = true;
        else if (depth == 0) throw createException("View already exhausted");
    }

    private void validateLegalEnd() throws TEx {
        if (!began) throw createException("View not yet begun");
        else if (depth == 0) throw createException("View already exhausted");
    }

    @Override
    public ReaderItemView<TEx> beginArray() throws TEx {
        validateLegalBegin();
        delegate.beginArray();
        began = true;
        depth++;
        return this;
    }

    @Override
    public ReaderItemView<TEx> endArray() throws TEx {
        validateLegalEnd();
        delegate.endArray();
        depth--;
        return this;
    }

    @Override
    public ReaderItemView<TEx> beginObject() throws TEx {
        validateLegalBegin();
        delegate.beginObject();
        began = true;
        depth++;
        return this;
    }

    @Override
    public ReaderItemView<TEx> endObject() throws TEx {
        validateLegalEnd();
        delegate.endObject();
        depth--;
        return this;
    }

    @Override
    public boolean hasNext() throws TEx {
        return (!began || depth > 0) && delegate.hasNext();
    }

    @Override
    public Token peek() throws TEx {
        return (began && depth == 0) ? Token.END_DOCUMENT : delegate.peek();
    }

    @Override
    public String nextName() throws TEx {
        validateLegalBegin();
        return delegate.nextName();
    }

    @Override
    public String nextString() throws TEx {
        validateLegalBegin();
        String s = delegate.nextString();
        began = true;
        return s;
    }

    @Override
    public boolean nextBoolean() throws TEx {
        validateLegalBegin();
        boolean b = delegate.nextBoolean();
        began = true;
        return b;
    }

    @Override
    public void nextNull() throws TEx {
        validateLegalBegin();
        delegate.nextNull();
        began = true;
    }

    @Override
    public double nextDouble() throws TEx {
        validateLegalBegin();
        double d = delegate.nextDouble();
        began = true;
        return d;
    }

    @Override
    public long nextLong() throws TEx {
        validateLegalBegin();
        long l = delegate.nextLong();
        began = true;
        return l;
    }

    @Override
    public int nextInt() throws TEx {
        validateLegalBegin();
        int i = delegate.nextInt();
        began = true;
        return i;
    }

    @Override
    public Number nextNumber() throws TEx {
        validateLegalBegin();
        Number n = delegate.nextNumber();
        began = true;
        return n;
    }

    @Override
    public void skipValue() throws TEx {
        validateLegalBegin();
        delegate.skipValue();
        began = true;
    }

    @Override
    public void close() throws TEx {
        if (!began) {
            skipValue();
            return;
        }
        while (depth > 0) {
            depth = switch (delegate.peek()) {
                case BEGIN_ARRAY, BEGIN_OBJECT, NULL, BOOLEAN, NUMBER, STRING, NAME -> {
                    delegate.skipValue();
                    yield depth;
                }
                case END_ARRAY -> {
                    delegate.endArray();
                    yield depth - 1;
                }
                case END_OBJECT -> {
                    delegate.endObject();
                    yield depth - 1;
                }
                case END_DOCUMENT -> 0;
            };
        }
    }
}
