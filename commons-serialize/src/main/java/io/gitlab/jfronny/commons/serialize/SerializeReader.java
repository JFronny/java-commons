package io.gitlab.jfronny.commons.serialize;

import io.gitlab.jfronny.commons.SamWithReceiver;
import io.gitlab.jfronny.commons.serialize.emulated.ReaderItemView;
import io.gitlab.jfronny.commons.throwable.Unchecked;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A reader for serialized data.
 * Although it is marked as {@link Iterable}, you should make sure iteration is only done once, since it <b>consumes tokens</b>!
 *
 * @param <TEx> the exception type
 * @param <T>   the reader type
 */
public abstract class SerializeReader<TEx extends Exception, T extends SerializeReader<TEx, T>> implements AutoCloseable, Iterable<RToken> {
    protected boolean lenient = false;
    protected boolean serializeSpecialFloatingPointValues = false;
    protected int nestingLimit = 255;

    public boolean isLenient() {
        return lenient;
    }

    public T setLenient(boolean lenient) {
        this.lenient = lenient;
        if (lenient) return setSerializeSpecialFloatingPointValues(true);
        return (T) this;
    }

    public boolean isSerializeSpecialFloatingPointValues() {
        return serializeSpecialFloatingPointValues;
    }

    public T setSerializeSpecialFloatingPointValues(boolean serializeSpecialFloatingPointValues) {
        this.serializeSpecialFloatingPointValues = serializeSpecialFloatingPointValues;
        return (T) this;
    }

    public int getNestingLimit() {
        return nestingLimit;
    }

    public T setNestingLimit(int nestingLimit) {
        if (nestingLimit < 0) throw new IllegalArgumentException("Invalid nesting limit: " + nestingLimit);
        this.nestingLimit = nestingLimit;
        return (T) this;
    }

    public abstract T beginArray() throws TEx;
    public abstract T endArray() throws TEx;
    public <R> R array(SerializeReaderFunction<TEx, T, R> consumer) throws TEx {
        beginArray();
        var result = consumer.accept((T) this);
        endArray();
        return result;
    }
    public <R> List<R> arrayByElements(SerializeReaderFunction<TEx, T, R> consumer) throws TEx {
        List<R> result = new ArrayList<>();
        beginArray();
        while (hasNext()) result.add(consumer.accept((T) this));
        endArray();
        return result;
    }
    public abstract T beginObject() throws TEx;
    public abstract T endObject() throws TEx;
    public <R> R object(SerializeReaderFunction<TEx, T, R> consumer) throws TEx {
        beginObject();
        var result = consumer.accept((T) this);
        endObject();
        return result;
    }

    public abstract boolean hasNext() throws TEx;
    public abstract Token peek() throws TEx;

    public abstract String nextName() throws TEx;
    public abstract String nextString() throws TEx;
    public abstract boolean nextBoolean() throws TEx;
    public abstract void nextNull() throws TEx;
    public double nextDouble() throws TEx {
        return nextNumber().doubleValue();
    }
    public long nextLong() throws TEx {
        return nextNumber().longValue();
    }
    public int nextInt() throws TEx {
        return nextNumber().intValue();
    }
    public abstract Number nextNumber() throws TEx;
    public abstract void skipValue() throws TEx;

    public abstract String getPath();
    public abstract String getPreviousPath();

    @Override
    public abstract void close() throws TEx;

    /**
     * Creates a view of this reader that can be used to read the next item and no more.
     * Prevents methods outside your control from advancing the reader past the item.
     * Ensure that the view is closed after use.
     *
     * @return a view of this reader that can be used to read the item
     */
    public SerializeReader<TEx, ?> createView() {
        return new ReaderItemView<>(this);
    }

    /**
     * Returns the next token alongside its value as an {@link RToken} and advances the reader.
     *
     * @return the next token
     * @throws TEx if an error occurs
     */
    public RToken next() throws TEx {
        return switch (peek()) {
            case BEGIN_ARRAY -> {
                beginArray();
                yield RToken.Simple.BEGIN_ARRAY;
            }
            case END_ARRAY -> {
                endArray();
                yield RToken.Simple.END_ARRAY;
            }
            case BEGIN_OBJECT -> {
                beginObject();
                yield RToken.Simple.BEGIN_OBJECT;
            }
            case END_OBJECT -> {
                endObject();
                yield RToken.Simple.END_OBJECT;
            }
            case NAME -> new RToken.Name(nextName());
            case STRING -> new RToken.String(nextString());
            case NUMBER -> new RToken.Number(nextNumber());
            case BOOLEAN -> new RToken.Boolean(nextBoolean());
            case NULL -> {
                nextNull();
                yield RToken.Simple.NULL;
            }
            case END_DOCUMENT -> RToken.Simple.END_DOCUMENT;
        };
    }

    /**
     * Returns a {@link Stream} of the tokens within the current object or array.
     * The stream consumes tokens and advances the reader, so it is not possible to iterate over the tokens more than once.
     * Additionally, although it is not marked as such, iteration may throw a TEx.
     * The caller is responsible for making sure it is caught and not propagated.
     *
     * @return a stream of the tokens
     */
    public @NotNull Stream<RToken> stream() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator(), Spliterator.NONNULL | Spliterator.ORDERED), false);
    }

    /**
     * Returns an {@link Iterator} over the tokens within the current object or array.
     * The iterator consumes tokens and advances the reader, so it is not possible to iterate over the tokens more than once.
     * Additionally, although it is not marked as such, iteration may throw a TEx.
     * The caller is responsible for making sure it is caught and not propagated.
     *
     * @return an iterator over the tokens
     */
    @Override
    public @NotNull TokenIterator iterator() {
        return new TokenIterator();
    }

    /**
     * An iterator over the tokens within the current object or array.
     * The iterator consumes tokens and advances the reader, so it is not possible to iterate over the tokens more than once.
     * Additionally, although it is not marked as such in code, iteration may throw a TEx.
     * <p>
     * Note: this iterator tracks the depth of the tree it traversed,
     * so consuming BEGIN_OBJECT or BEGIN_ARRAY tokens while it is in use
     * without consuming the corresponding END_OBJECT or END_ARRAY tokens before the next call to it is not supported.
     */
    public class TokenIterator implements Iterator<RToken> {
        int depth = 0;
        /**
         * {@inheritDoc}
         * throws {@link TEx} if an error occurs
         */
        @Override
        public boolean hasNext() {
            try {
                return depth > 0 || SerializeReader.this.hasNext();
            } catch (Exception e) {
                return Unchecked.sneakyThrow(e);
            }
        }

        /**
         * {@inheritDoc}
         * throws {@link TEx} if an error occurs
         */
        @Override
        public RToken next() {
            if (!hasNext()) throw new NoSuchElementException("No more tokens");
            Unchecked.reintroduce();
            try {
                RToken token = SerializeReader.this.next();
                if (token == RToken.Simple.BEGIN_ARRAY || token == RToken.Simple.BEGIN_OBJECT) {
                    depth++;
                } else if (token == RToken.Simple.END_ARRAY || token == RToken.Simple.END_OBJECT) {
                    depth--;
                }
                return token;
            } catch (Exception e) {
                return Unchecked.sneakyThrow(e);
            }
        }
    }

    /**
     * Copies the current element to the writer.
     *
     * @param writer the writer to copy to
     * @throws TEx if an error occurs
     */
    public <TEx2 extends Exception> void copyTo(SerializeWriter<TEx2, ?> writer) throws TEx, TEx2 {
        switch (peek()) {
            case BEGIN_ARRAY -> {
                this.beginArray();
                writer.beginArray();
                while (this.hasNext()) {
                    this.copyTo(writer);
                }
                this.endArray();
                writer.endArray();
            }
            case END_ARRAY -> throw new IllegalStateException("Cannot copy standalone END_ARRAY");
            case BEGIN_OBJECT -> {
                this.beginObject();
                writer.beginObject();
                while (this.hasNext()) {
                    writer.name(this.nextName());
                    this.copyTo(writer);
                }
                this.endObject();
                writer.endObject();
            }
            case END_OBJECT -> throw new IllegalStateException("Cannot copy standalone END_OBJECT");
            case NAME -> throw new IllegalStateException("Cannot copy standalone NAME");
            case STRING -> writer.value(this.nextString());
            case NUMBER -> writer.value(this.nextNumber());
            case BOOLEAN -> writer.value(this.nextBoolean());
            case NULL -> {
                this.nextNull();
                writer.nullValue();
            }
            case END_DOCUMENT -> throw new IllegalStateException("Cannot copy END_DOCUMENT");
        }
    }

    protected abstract TEx createException(String message);

    @Override
    public String toString() {
        return getClass().getSimpleName() + locationString();
    }

    /**
     * Used to provide toString and location information in error messages. Format should resemble that in {@code serialize-json}.
     * @return a string representation of the location of the reader
     */
    protected String locationString() {
        return " at path " + getPath();
    }

    @SamWithReceiver
    public interface SerializeReaderFunction<TEx extends Exception, T extends SerializeReader<TEx, T>, R> {
        R accept(T reader) throws TEx;
    }

    public static abstract class Delegating<TEx extends Exception, Reader extends Delegating<TEx, Reader>> extends SerializeReader<TEx, Reader> {
        protected final SerializeReader<TEx, ?> delegate;

        public Delegating(SerializeReader<TEx, ?> delegate) {
            this.delegate = Objects.requireNonNull(delegate);
        }

        @Override
        public boolean isLenient() {
            return this.lenient = delegate.isLenient();
        }

        @Override
        public Reader setLenient(boolean lenient) {
            delegate.setLenient(lenient);
            this.lenient = lenient;
            return (Reader) this;
        }

        @Override
        public boolean isSerializeSpecialFloatingPointValues() {
            return this.serializeSpecialFloatingPointValues = delegate.isSerializeSpecialFloatingPointValues();
        }

        @Override
        public Reader setSerializeSpecialFloatingPointValues(boolean serializeSpecialFloatingPointValues) {
            delegate.setSerializeSpecialFloatingPointValues(serializeSpecialFloatingPointValues);
            this.serializeSpecialFloatingPointValues = serializeSpecialFloatingPointValues;
            return (Reader) this;
        }

        @Override
        public int getNestingLimit() {
            return this.nestingLimit = delegate.getNestingLimit();
        }

        @Override
        public Reader setNestingLimit(int nestingLimit) {
            delegate.setNestingLimit(nestingLimit);
            this.nestingLimit = nestingLimit;
            return (Reader) this;
        }

        @Override
        public String getPath() {
            return delegate.getPath();
        }

        @Override
        public String getPreviousPath() {
            return delegate.getPreviousPath();
        }

        @Override
        protected String locationString() {
            return delegate.locationString();
        }

        @Override
        protected TEx createException(String message) {
            return delegate.createException(message);
        }

        @Override public abstract double nextDouble() throws TEx;
        @Override public abstract long nextLong() throws TEx;
        @Override public abstract int nextInt() throws TEx;
    }
}
