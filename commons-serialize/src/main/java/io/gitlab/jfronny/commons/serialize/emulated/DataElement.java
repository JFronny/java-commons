package io.gitlab.jfronny.commons.serialize.emulated;

import io.gitlab.jfronny.commons.data.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public sealed interface DataElement {
    record Null() implements DataElement {}
    sealed interface Primitive extends DataElement {
        java.lang.String asString();

        record Boolean(boolean value) implements Primitive {
            @Override
            public java.lang.String asString() {
                return value ? "true" : "false";
            }
        }
        record Number(java.lang.Number value) implements Primitive {
            public Number {
                Objects.requireNonNull(value);
            }

            @Override
            public java.lang.String asString() {
                return value.toString();
            }
        }
        record String(java.lang.String value) implements Primitive {
            @Override
            public java.lang.String asString() {
                return value;
            }
        }
    }
    record Object(Map<String, DataElement> members) implements DataElement {
        public Object() {
            this(new LinkedTreeMap<>(false));
        }
    }
    record Array(List<DataElement> elements) implements DataElement {
        public Array() {
            this(new ArrayList<>());
        }
    }
}
