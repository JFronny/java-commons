package io.gitlab.jfronny.commons.serialize;

import java.io.IOException;

public class MalformedDataException extends IOException {
    public MalformedDataException(String msg) {
        super(msg);
    }

    public MalformedDataException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public MalformedDataException(Throwable throwable) {
        super(throwable);
    }
}
