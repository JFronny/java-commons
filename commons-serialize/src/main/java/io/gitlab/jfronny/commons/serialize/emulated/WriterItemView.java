package io.gitlab.jfronny.commons.serialize.emulated;

import io.gitlab.jfronny.commons.serialize.SerializeWriter;

import java.util.Arrays;

public class WriterItemView<TEx extends Exception> extends SerializeWriter.Delegating<TEx, WriterItemView<TEx>> {
    private boolean[] stack = new boolean[4];
    private int depth = 0;
    private boolean began = false;

    public WriterItemView(SerializeWriter<TEx, ?> delegate) {
        super(delegate);
    }

    private void validateLegalBegin() throws TEx {
        if (!began) began = true;
        else if (depth == 0) throw createException("View already exhausted");
    }

    private void validateLegalEnd() throws TEx {
        if (!began) throw createException("View not yet begun");
        else if (depth == 0) throw createException("View already exhausted");
    }

    private void push(boolean value) {
        if (depth == stack.length) {
            stack = Arrays.copyOf(stack, stack.length * 2);
        }
        stack[depth++] = value;
    }

    @Override
    public WriterItemView<TEx> beginArray() throws TEx {
        validateLegalBegin();
        delegate.beginArray();
        began = true;
        push(true);
        return this;
    }

    @Override
    public WriterItemView<TEx> endArray() throws TEx {
        validateLegalEnd();
        delegate.endArray();
        depth--;
        return this;
    }

    @Override
    public WriterItemView<TEx> beginObject() throws TEx {
        validateLegalBegin();
        delegate.beginObject();
        began = true;
        push(false);
        return this;
    }

    @Override
    public WriterItemView<TEx> endObject() throws TEx {
        validateLegalEnd();
        delegate.endObject();
        depth--;
        return this;
    }

    @Override
    public WriterItemView<TEx> comment(String comment) throws TEx {
        validateLegalBegin();
        delegate.comment(comment);
        return this;
    }

    @Override
    public WriterItemView<TEx> name(String name) throws TEx {
        validateLegalBegin();
        delegate.name(name);
        return this;
    }

    @Override
    public WriterItemView<TEx> value(String value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> literalValue(String value) throws TEx {
        validateLegalBegin();
        delegate.literalValue(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> nullValue() throws TEx {
        validateLegalBegin();
        delegate.nullValue();
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> value(boolean value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> value(float value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> value(double value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> value(long value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public WriterItemView<TEx> value(Number value) throws TEx {
        validateLegalBegin();
        delegate.value(value);
        began = true;
        return this;
    }

    @Override
    public void close() throws TEx {
        if (!began) {
            delegate.nullValue();
            return;
        }
        while (depth > 0) {
            if (stack[--depth]) delegate.endArray();
            else delegate.endObject();
        }
    }
}
