package io.gitlab.jfronny.commons.serialize.emulated;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;

import java.util.Map;

public class DataElementSerializer {
    public static <TEx extends Exception, T extends SerializeWriter<TEx, ?>> void serialize(DataElement element, T out) throws TEx {
        switch (element) {
            case DataElement.Array(var elements) -> out.array(b -> {
                for (DataElement e : elements) {
                    serialize(e, b);
                }
                return b;
            });
            case DataElement.Null n -> out.nullValue();
            case DataElement.Object(var members) -> out.object(b -> {
                for (Map.Entry<String, DataElement> e : members.entrySet()) {
                    b.name(e.getKey());
                    serialize(e.getValue(), b);
                }
                return b;
            });
            case DataElement.Primitive p -> {
                switch (p) {
                    case DataElement.Primitive.Boolean(var value) -> out.value(value);
                    case DataElement.Primitive.Number(var value) -> out.value(value);
                    case DataElement.Primitive.String(var value) -> out.value(value);
                }
            }
        }
    }

    public static <TEx extends Exception, T extends SerializeReader<TEx, ?>> DataElement deserialize(T in) throws TEx {
        return switch (in.peek()) {
            case STRING -> new DataElement.Primitive.String(in.nextString());
            case NUMBER -> new DataElement.Primitive.Number(in.nextNumber());
            case BOOLEAN -> new DataElement.Primitive.Boolean(in.nextBoolean());
            case NULL -> {
                in.nextNull();
                yield new DataElement.Null();
            }
            case BEGIN_ARRAY -> in.array(b -> {
                DataElement.Array array = new DataElement.Array();
                while (b.hasNext()) {
                    array.elements().add(deserialize(b));
                }
                return array;
            });
            case BEGIN_OBJECT -> in.object(b -> {
                DataElement.Object object = new DataElement.Object();
                while (b.hasNext()) {
                    object.members().put(b.nextName(), deserialize(b));
                }
                return object;
            });
            case END_ARRAY, END_OBJECT, END_DOCUMENT, NAME -> throw new IllegalStateException();
        };
    }
}
