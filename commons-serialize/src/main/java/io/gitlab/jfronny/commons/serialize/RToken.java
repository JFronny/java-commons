package io.gitlab.jfronny.commons.serialize;

public sealed interface RToken {
    enum Simple implements RToken {
        BEGIN_ARRAY, END_ARRAY, BEGIN_OBJECT, END_OBJECT, NULL, END_DOCUMENT
    }

    record Name(java.lang.String name) implements RToken {}
    record String(java.lang.String value) implements RToken {}
    record Number(java.lang.Number value) implements RToken {}
    record Boolean(boolean value) implements RToken {}
}
