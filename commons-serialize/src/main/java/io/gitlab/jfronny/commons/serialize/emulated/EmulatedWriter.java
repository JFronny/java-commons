package io.gitlab.jfronny.commons.serialize.emulated;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmulatedWriter extends SerializeWriter<RuntimeException, EmulatedWriter> implements Closeable {
    /** Added to the top of the stack when this writer is closed to cause following ops to fail. */
    private static final DataElement.Primitive.String SENTINEL_CLOSED = new DataElement.Primitive.String("closed");

    /** The JsonElements and JsonArrays under modification, outermost to innermost. */
    private final List<DataElement> stack = new ArrayList<>();

    /** The name for the next JSON object value. If non-null, the top of the stack is a JsonObject. */
    private String pendingName;

    /** the JSON element constructed by this writer. */
    private DataElement product = new DataElement.Null();

    public DataElement get() {
        if (!stack.isEmpty()) {
            throw new IllegalStateException("Expected one JSON element but was " + stack);
        }
        return product;
    }

    private void put(DataElement value) {
        if (pendingName != null) {
            if (!(value instanceof DataElement.Null) || serializeNulls) {
                ((DataElement.Object) stack.getLast()).members().put(pendingName, value);
            }
            pendingName = null;
        } else if (stack.isEmpty()) {
            product = value;
        } else {
            DataElement element = stack.getLast();
            if (element instanceof DataElement.Array array) {
                array.elements().add(value);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    @Override
    public EmulatedWriter beginArray() throws RuntimeException {
        DataElement.Array array = new DataElement.Array();
        put(array);
        stack.add(array);
        return this;
    }

    @Override
    public EmulatedWriter endArray() throws RuntimeException {
        if (stack.isEmpty() || pendingName != null) {
            throw new IllegalStateException();
        }
        DataElement element = stack.getLast();
        if (element instanceof DataElement.Array) {
            stack.removeLast();
            return this;
        }
        throw new IllegalStateException();
    }

    @Override
    public EmulatedWriter beginObject() throws RuntimeException {
        DataElement.Object object = new DataElement.Object();
        put(object);
        stack.add(object);
        return this;
    }

    @Override
    public EmulatedWriter endObject() throws RuntimeException {
        if (stack.isEmpty() || pendingName != null) {
            throw new IllegalStateException();
        }
        DataElement element = stack.getLast();
        if (element instanceof DataElement.Object) {
            stack.removeLast();
            return this;
        }
        throw new IllegalStateException();
    }

    @Override
    public EmulatedWriter comment(String comment) throws RuntimeException {
        return this;
    }

    @Override
    public EmulatedWriter name(String name) throws RuntimeException {
        Objects.requireNonNull(name, "name == null");
        if (stack.isEmpty() || pendingName != null) {
            throw new IllegalStateException("Did not expect a name");
        }
        DataElement element = stack.getLast();
        if (element instanceof DataElement.Object) {
            pendingName = name;
            return this;
        }
        throw new IllegalStateException("Please begin an object before writing a name.");
    }

    @Override
    public EmulatedWriter value(String value) throws RuntimeException {
        if (value == null) {
            return nullValue();
        }
        put(new DataElement.Primitive.String(value));
        return this;
    }

    @Override
    public EmulatedWriter value(boolean value) throws RuntimeException {
        put(new DataElement.Primitive.Boolean(value));
        return this;
    }

    @Override
    public EmulatedWriter value(float value) throws RuntimeException {
        if (!serializeSpecialFloatingPointValues && (Float.isNaN(value) || Float.isInfinite(value))) {
            throw new IllegalArgumentException("NaN and infinities are not permitted in this writer: " + value);
        }
        put(new DataElement.Primitive.Number(value));
        return this;
    }

    @Override
    public EmulatedWriter value(double value) throws RuntimeException {
        if (!serializeSpecialFloatingPointValues && (Double.isNaN(value) || Double.isInfinite(value))) {
            throw new IllegalArgumentException("NaN and infinities are not permitted in this writer: " + value);
        }
        put(new DataElement.Primitive.Number(value));
        return this;
    }

    @Override
    public EmulatedWriter value(long value) throws RuntimeException {
        put(new DataElement.Primitive.Number(value));
        return this;
    }

    @Override
    public EmulatedWriter value(Number value) throws RuntimeException {
        if (value == null) {
            return nullValue();
        }

        if (!isLenient()) {
            double d = value.doubleValue();
            if (Double.isNaN(d) || Double.isInfinite(d)) {
                throw new IllegalArgumentException("NaN and infinities are not permitted in this writer: " + value);
            }
        }

        put(new DataElement.Primitive.Number(value));
        return this;
    }

    @Override
    public EmulatedWriter nullValue() throws RuntimeException {
        put(new DataElement.Null());
        return this;
    }

    @Override
    public EmulatedWriter literalValue(String value) throws RuntimeException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected RuntimeException createException(String message) {
        return new IllegalStateException(message);
    }

    @Override
    public void flush() throws IOException {}

    @Override
    public void close() {
        if (!stack.isEmpty()) {
            throw new IllegalStateException("Incomplete document");
        }
        stack.add(SENTINEL_CLOSED);
    }
}
