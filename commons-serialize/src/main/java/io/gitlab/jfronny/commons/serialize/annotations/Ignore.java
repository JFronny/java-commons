package io.gitlab.jfronny.commons.serialize.annotations;

import java.lang.annotation.*;

/**
 * Mark a class/field to be ignored by Gson.
 * May be used for metadata in serialized classes
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
public @interface Ignore {
}
