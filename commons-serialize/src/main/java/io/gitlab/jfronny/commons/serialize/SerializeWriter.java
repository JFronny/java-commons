package io.gitlab.jfronny.commons.serialize;

import io.gitlab.jfronny.commons.SamWithReceiver;
import io.gitlab.jfronny.commons.serialize.emulated.WriterItemView;

import java.io.Flushable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

public abstract class SerializeWriter<TEx extends Exception, T extends SerializeWriter<TEx, T>> implements AutoCloseable, Flushable {
    private static final Pattern VALID_JSON_NUMBER_PATTERN =
            Pattern.compile("-?(?:0|[1-9][0-9]*)(?:\\.[0-9]+)?(?:[eE][-+]?[0-9]+)?");

    protected boolean lenient = false;
    protected boolean serializeNulls = true;
    protected boolean serializeSpecialFloatingPointValues = false;

    public boolean isLenient() {
        return lenient;
    }

    public T setLenient(boolean lenient) {
        this.lenient = lenient;
        if (lenient) return setSerializeSpecialFloatingPointValues(true).setSerializeNulls(true);
        return (T) this;
    }
    
    public boolean isSerializeNulls() {
        return serializeNulls;
    }
    
    public T setSerializeNulls(boolean serializeNulls) {
        this.serializeNulls = serializeNulls;
        return (T) this;
    }

    public boolean isSerializeSpecialFloatingPointValues() {
        return serializeSpecialFloatingPointValues;
    }
    
    public T setSerializeSpecialFloatingPointValues(boolean serializeSpecialFloatingPointValues) {
        this.serializeSpecialFloatingPointValues = serializeSpecialFloatingPointValues;
        return (T) this;
    }

    public abstract T beginArray() throws TEx;
    public abstract T endArray() throws TEx;
    public T array(SerializeWriterConsumer<TEx, T> consumer) throws TEx {
        return consumer.accept(this.beginArray()).endObject();
    }
    public abstract T beginObject() throws TEx;
    public abstract T endObject() throws TEx;
    public T object(SerializeWriterConsumer<TEx, T> consumer) throws TEx {
        return consumer.accept(this.beginObject()).endObject();
    }

    public abstract T comment(String comment) throws TEx;
    public abstract T name(String name) throws TEx;
    public T nullValue() throws TEx {
        if (serializeNulls) {
            return literalValue("null");
        } else {
            throw new IllegalArgumentException("Null values are not allowed");
        }
    }
    public abstract T value(String value) throws TEx;
    public T value(boolean value) throws TEx {
        return literalValue(value ? "true" : "false");
    }
    public T value(Boolean value) throws TEx {
        return value == null ? nullValue() : value(value.booleanValue());
    }
    public T value(float value) throws TEx {
        if (!serializeSpecialFloatingPointValues && (Float.isNaN(value) || Float.isInfinite(value)))
            throw new IllegalArgumentException("Numeric values must be finite, but was " + value);
        return literalValue(Float.toString(value));
    }
    public T value(Float value) throws TEx {
        return value == null ? nullValue() : value(value.floatValue());
    }
    public T value(double value) throws TEx {
        if (!serializeSpecialFloatingPointValues && (Double.isNaN(value) || Double.isInfinite(value)))
            throw new IllegalArgumentException("Numeric values must be finite, but was " + value);
        return literalValue(Double.toString(value));
    }
    public T value(Double value) throws TEx {
        return value == null ? nullValue() : value(value.doubleValue());
    }
    public T value(long value) throws TEx {
        return literalValue(Long.toString(value));
    }
    public T value(Long value) throws TEx {
        return value == null ? nullValue() : value(value.longValue());
    }
    public T value(Number value) throws TEx {
        if (value == null) return nullValue();
        String s = value.toString();
        Class<? extends Number> numberClass = value.getClass();

        if (!alwaysCreatesValidJsonNumber(numberClass)) {
            // Validate that string is valid before writing it directly to JSON output
            if (s.equals("-Infinity") || s.equals("Infinity") || s.equals("NaN")) {
                if (!serializeSpecialFloatingPointValues) {
                    throw new IllegalArgumentException("Numeric values must be finite, but was " + s);
                }
            } else if (numberClass != Float.class
                    && numberClass != Double.class
                    && !VALID_JSON_NUMBER_PATTERN.matcher(s).matches()) {
                throw new IllegalArgumentException(
                        "String created by " + numberClass + " is not a valid number: " + s);
            }
        }

        return literalValue(s);
    }
    
    private static boolean alwaysCreatesValidJsonNumber(Class<? extends Number> c) {
        // Does not include Float or Double because their value can be NaN or Infinity
        // Does not include LazilyParsedNumber because it could contain a malformed string
        return c == Integer.class
                || c == Long.class
                || c == Byte.class
                || c == Short.class
                || c == BigDecimal.class
                || c == BigInteger.class
                || c == AtomicInteger.class
                || c == AtomicLong.class;
    }

    /**
     * Writes a literal value to the output without quoting or escaping.
     * This may not be supported by all implementations, if not supported an {@link UnsupportedOperationException} will be thrown.
     * @param value the literal value to write
     * @return this writer
     */
    public abstract T literalValue(String value) throws TEx;

    /**
     * Creates a view of this write that can be used to write the next item and no more.
     * Prevents methods outside your control from advancing the writer past the item.
     * Ensure that the view is closed after use.
     *
     * @return a view of this write that can be used to write the item
     */
    public SerializeWriter<TEx, ?> createView() {
        return new WriterItemView<>(this);
    }

    protected abstract TEx createException(String message);

    @Override
    public abstract void close() throws TEx;

    @SamWithReceiver
    public interface SerializeWriterConsumer<TEx extends Exception, T extends SerializeWriter<TEx, T>> {
        T accept(T writer) throws TEx;
    }

    public static abstract class Delegating<TEx extends Exception, Writer extends Delegating<TEx, Writer>> extends SerializeWriter<TEx, Writer> {
        protected final SerializeWriter<TEx, ?> delegate;

        public Delegating(SerializeWriter<TEx, ?> delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean isLenient() {
            return this.lenient = delegate.isLenient();
        }

        @Override
        public Writer setLenient(boolean lenient) {
            this.lenient = lenient;
            delegate.setLenient(lenient);
            return (Writer) this;
        }

        @Override
        public boolean isSerializeNulls() {
            return this.serializeNulls = delegate.isSerializeNulls();
        }

        @Override
        public Writer setSerializeNulls(boolean serializeNulls) {
            this.serializeNulls = serializeNulls;
            delegate.setSerializeNulls(serializeNulls);
            return (Writer) this;
        }

        @Override
        public boolean isSerializeSpecialFloatingPointValues() {
            return this.serializeSpecialFloatingPointValues = delegate.isSerializeSpecialFloatingPointValues();
        }

        @Override
        public Writer setSerializeSpecialFloatingPointValues(boolean serializeSpecialFloatingPointValues) {
            this.serializeSpecialFloatingPointValues = serializeSpecialFloatingPointValues;
            delegate.setSerializeSpecialFloatingPointValues(serializeSpecialFloatingPointValues);
            return (Writer) this;
        }

        @Override
        protected TEx createException(String message) {
            return delegate.createException(message);
        }

        @Override
        public void flush() throws IOException {
            delegate.flush();
        }

        @Override public abstract Writer nullValue() throws TEx;
        @Override public abstract Writer value(boolean value) throws TEx;
        @Override public abstract Writer value(float value) throws TEx;
        @Override public abstract Writer value(double value) throws TEx;
        @Override public abstract Writer value(long value) throws TEx;
        @Override public abstract Writer value(Number value) throws TEx;
    }
}
