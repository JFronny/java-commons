module io.gitlab.jfronny.commons.serialize {
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.commons.serialize;
    exports io.gitlab.jfronny.commons.serialize.emulated;
    exports io.gitlab.jfronny.commons.serialize.annotations;
}