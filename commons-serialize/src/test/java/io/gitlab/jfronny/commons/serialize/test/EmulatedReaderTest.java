package io.gitlab.jfronny.commons.serialize.test;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.emulated.DataElement;
import io.gitlab.jfronny.commons.serialize.emulated.EmulatedReader;
import io.gitlab.jfronny.commons.serialize.emulated.EmulatedWriter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmulatedReaderTest {
    @Test
    public void testSimple() throws MalformedDataException {
        DataElement.Object del = new DataElement.Object();
        del.members().put("type", new DataElement.Primitive.String("boolean"));
        del.members().put("default", new DataElement.Primitive.Boolean(true));
        del.members().put("reloadType", new DataElement.Primitive.String("Simple"));
        EmulatedReader er = new EmulatedReader(del);
        assertEquals(Token.BEGIN_OBJECT, er.peek());
        er.beginObject();
        assertEquals(Token.NAME, er.peek());
        assertEquals("type", er.nextName());
        assertEquals(Token.STRING, er.peek());
        assertEquals("boolean", er.nextString());
        assertEquals(Token.NAME, er.peek());
        assertEquals("default", er.nextName());
        assertEquals(Token.BOOLEAN, er.peek());
        assertTrue(er.nextBoolean());
        assertEquals(Token.NAME, er.peek());
        assertEquals("reloadType", er.nextName());
        assertEquals(Token.STRING, er.peek());
        assertEquals("Simple", er.nextString());
        assertEquals(Token.END_OBJECT, er.peek());
        er.endObject();
        assertEquals(Token.END_DOCUMENT, er.peek());
    }

    @Test
    public void testSimpleWithoutPeek() throws MalformedDataException {
        DataElement.Object del = new DataElement.Object();
        del.members().put("type", new DataElement.Primitive.String("boolean"));
        del.members().put("default", new DataElement.Primitive.Boolean(true));
        del.members().put("reloadType", new DataElement.Primitive.String("Simple"));
        EmulatedReader er = new EmulatedReader(del);
        er.beginObject();
        assertEquals("type", er.nextName());
        assertEquals("boolean", er.nextString());
        assertEquals("default", er.nextName());
        assertTrue(er.nextBoolean());
        assertEquals("reloadType", er.nextName());
        assertEquals("Simple", er.nextString());
        er.endObject();
        assertEquals(Token.END_DOCUMENT, er.peek());
    }

    @Test
    public void testTransfer() throws MalformedDataException {
        DataElement.Object del = new DataElement.Object();
        del.members().put("type", new DataElement.Primitive.String("boolean"));
        del.members().put("default", new DataElement.Primitive.Boolean(true));
        del.members().put("reloadType", new DataElement.Primitive.String("Simple"));
        del.members().put("joe", new DataElement.Primitive.Number(12));
        EmulatedReader er = new EmulatedReader(del);
        EmulatedWriter ew = new EmulatedWriter();
        er.beginObject();
        ew.beginObject();
        String type = null;
        while (er.hasNext()) {
            String key;
            ew.name(key = er.nextName());
            if (key.equals("type")) ew.value(type = er.nextString());
            else er.copyTo(ew);
        }
        er.endObject();
        ew.endObject();
        assertEquals(del, ew.get());
        assertEquals("boolean", type);
    }
}
