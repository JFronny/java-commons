import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
    kotlin("jvm")
}

dependencies {
    api(projects.commonsSerialize)

    testImplementation(kotlin("test"))
    testImplementation(projects.commonsSerializeJson)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-serialize-dsl"

            from(components["java"])
        }
    }
}

tasks.compileKotlin {
    destinationDirectory = tasks.compileJava.get().destinationDirectory.asFile.orNull
}

tasks.javadoc {
    enabled = false
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize/$version/raw", projects.commonsSerialize)
}
