package io.gitlab.jfronny.commons.serialize.dsl

import io.gitlab.jfronny.commons.serialize.SerializeWriter
import java.lang.Exception

class ArrayScope<TEx : Exception, Writer: SerializeWriter<TEx, Writer>>(val writer: Writer) {
    fun jObject(content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) {
        writer.beginObject()
        ObjectScope(writer).content()
        writer.endObject()
    }

    fun jArray(content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) {
        writer.beginArray()
        content()
        writer.endArray()
    }

    fun jValue(value: String) {
        writer.value(value)
    }

    fun jValue(value: Boolean) {
        writer.value(value)
    }

    fun jValue(value: Float) {
        writer.value(value)
    }

    fun jValue(value: Double) {
        writer.value(value)
    }

    fun jValue(value: Long) {
        writer.value(value)
    }

    fun jNull() {
        writer.nullValue()
    }

    fun jComment(comment: String) {
        writer.comment(comment)
    }
}