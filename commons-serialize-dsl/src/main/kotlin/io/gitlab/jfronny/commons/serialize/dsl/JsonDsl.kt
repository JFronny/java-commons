package io.gitlab.jfronny.commons.serialize.dsl

import io.gitlab.jfronny.commons.serialize.SerializeReader
import io.gitlab.jfronny.commons.serialize.SerializeWriter
import io.gitlab.jfronny.commons.serialize.Transport
import io.gitlab.jfronny.commons.serialize.emulated.DataElement
import io.gitlab.jfronny.commons.serialize.emulated.EmulatedWriter
import java.io.OutputStream
import java.io.StringWriter
import java.io.Writer as IOWriter
import java.nio.file.Path
import kotlin.io.path.writer

@DslMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.TYPE)
annotation class JsonDsl

fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> Path.jObject(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) = openJson(transport) { jObject(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> Path.jArray(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) = openJson(transport) { jArray(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> IOWriter.jObject(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) = openJson(transport) { jObject(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> IOWriter.jArray(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) = openJson(transport) { jArray(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> OutputStream.jObject(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) = openJson(transport) { jObject(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> OutputStream.jArray(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) = openJson(transport) { jArray(content) }
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> jObjectString(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit): String = StringWriter().use {
    it.jObject(transport, content)
    it.toString()
}
fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> jArrayString(transport: Transport<TEx, Reader, Writer>, content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit): String = StringWriter().use {
    it.jArray(transport, content)
    it.toString()
}
fun jObjectTree(content: (@JsonDsl ObjectScope<RuntimeException, EmulatedWriter>).() -> Unit): DataElement.Object = EmulatedWriter().use {
    it.jObject(content)
    it.get()
} as DataElement.Object
fun jArrayTree(content: (@JsonDsl ArrayScope<RuntimeException, EmulatedWriter>).() -> Unit): DataElement.Array = EmulatedWriter().use {
    it.jArray(content)
    it.get()
} as DataElement.Array

fun <TEx : Exception, Writer : SerializeWriter<TEx, Writer>> Writer.jObject(content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) {
    beginObject()
    ObjectScope(this).content()
    endObject()
}

fun <TEx : Exception, Writer : SerializeWriter<TEx, Writer>> Writer.jArray(content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) {
    beginArray()
    ArrayScope(this).content()
    endArray()
}

private fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> Path.openJson(transport: Transport<TEx, Reader, Writer>, write: Writer.() -> Unit) = writer().use { it.openJson(transport, write) }
private fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> IOWriter.openJson(transport: Transport<TEx, Reader, Writer>, write: Writer.() -> Unit) = transport.createWriter(this).write()
private fun <TEx : Exception, Reader : SerializeReader<TEx, Reader>, Writer : SerializeWriter<TEx, Writer>> OutputStream.openJson(transport: Transport<TEx, Reader, Writer>, write: Writer.() -> Unit) = writer().use { it.openJson(transport, write) }
