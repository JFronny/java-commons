package io.gitlab.jfronny.commons.serialize.dsl

import io.gitlab.jfronny.commons.serialize.SerializeWriter

class ObjectScope<TEx : Exception, Writer : SerializeWriter<TEx, Writer>>(val writer: Writer) {
    fun jObject(name: String, content: (@JsonDsl ObjectScope<TEx, Writer>).() -> Unit) {
        writer.name(name)
        writer.beginObject()
        content()
        writer.endObject()
    }

    fun jArray(name: String, content: (@JsonDsl ArrayScope<TEx, Writer>).() -> Unit) {
        writer.name(name)
        writer.beginArray()
        ArrayScope(writer).content()
        writer.endArray()
    }

    operator fun String.invoke(value: String) = jValue(this, value)
    operator fun String.invoke(value: Boolean) = jValue(this, value)
    operator fun String.invoke(value: Float) = jValue(this, value)
    operator fun String.invoke(value: Double) = jValue(this, value)
    operator fun String.invoke(value: Long) = jValue(this, value)

    fun jValue(name: String, value: String) {
        writer.name(name)
        writer.value(value)
    }

    fun jValue(name: String, value: Boolean) {
        writer.name(name)
        writer.value(value)
    }

    fun jValue(name: String, value: Float) {
        writer.name(name)
        writer.value(value)
    }

    fun jValue(name: String, value: Double) {
        writer.name(name)
        writer.value(value)
    }

    fun jValue(name: String, value: Long) {
        writer.name(name)
        writer.value(value)
    }

    fun jNull(name: String) {
        writer.name(name)
        writer.nullValue()
    }

    fun jComment(comment: String) {
        writer.comment(comment)
    }
}