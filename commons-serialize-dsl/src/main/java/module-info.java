module io.gitlab.jfronny.commons.serialize.dsl {
    requires io.gitlab.jfronny.commons.serialize;
    requires kotlin.stdlib;
    exports io.gitlab.jfronny.commons.serialize.dsl;
}