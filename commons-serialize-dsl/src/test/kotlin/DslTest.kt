package io.gitlab.jfronny.commons.serialize.gson.dsl.test

import io.gitlab.jfronny.commons.serialize.dsl.jObjectString
import io.gitlab.jfronny.commons.serialize.json.JsonTransport
import kotlin.test.Test
import kotlin.test.assertEquals

class DslTest {
    @Test
    fun simpleTest() {
        assertEquals("""{"hi":[["A",12],{"somEVal":true}]}""", jObjectString(JsonTransport()) {
            jArray("hi") {
                jArray {
                    jValue("A")
                    jValue(12)
                }
                jObject {
                    "somEVal"(true)
                }
            }
        })
    }
}