package io.gitlab.jfronny.commons.io;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class HashUtils {
    public static String sha1(byte[] data) {
        Formatter formatter = new Formatter();
        try {
            for (byte b : MessageDigest.getInstance("SHA-1").digest(data))
                formatter.format("%02x", b);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not hash using SHA1", e);
        }
        return formatter.toString();
    }

    public static String sha256(byte[] data) {
        Formatter formatter = new Formatter();
        try {
            for (byte b : MessageDigest.getInstance("SHA-256").digest(data))
                formatter.format("%02x", b);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not hash using SHA256", e);
        }
        return formatter.toString();
    }

    public static String sha512(byte[] data) {
        Formatter formatter = new Formatter();
        try {
            for (byte b : MessageDigest.getInstance("SHA-512").digest(data))
                formatter.format("%02x", b);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not hash using SHA512", e);
        }
        return formatter.toString();
    }

    public static long murmur2(byte[] data) {
        final int m = 0x5bd1e995;
        final int r = 24;
        long k = 0x0L;
        int seed = 1;
        int shift = 0x0;

        long length = 0;
        char b;
        // get good bytes from file
        for (byte datum : data) {
            b = (char) datum;

            if (b == 0x9 || b == 0xa || b == 0xd || b == 0x20) {
                continue;
            }

            length += 1;
        }
        long h = (seed ^ length);

        for (byte datum : data) {
            b = (char) datum;

            if (b == 0x9 || b == 0xa || b == 0xd || b == 0x20) {
                continue;
            }

            if (b > 255) {
                while (b > 255) {
                    b -= 255;
                }
            }

            k = k | ((long) b << shift);

            shift = shift + 0x8;

            if (shift == 0x20) {
                h = 0x00000000FFFFFFFFL & h;

                k = k * m;
                k = 0x00000000FFFFFFFFL & k;

                k = k ^ (k >> r);
                k = 0x00000000FFFFFFFFL & k;

                k = k * m;
                k = 0x00000000FFFFFFFFL & k;

                h = h * m;
                h = 0x00000000FFFFFFFFL & h;

                h = h ^ k;
                h = 0x00000000FFFFFFFFL & h;

                k = 0x0;
                shift = 0x0;
            }
        }

        if (shift > 0) {
            h = h ^ k;
            h = 0x00000000FFFFFFFFL & h;

            h = h * m;
            h = 0x00000000FFFFFFFFL & h;
        }

        h = h ^ (h >> 13);
        h = 0x00000000FFFFFFFFL & h;

        h = h * m;
        h = 0x00000000FFFFFFFFL & h;

        h = h ^ (h >> 15);
        h = 0x00000000FFFFFFFFL & h;

        return h;
    }
}
