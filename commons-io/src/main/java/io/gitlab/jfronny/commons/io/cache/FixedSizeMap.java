package io.gitlab.jfronny.commons.io.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A linked map that discards earlier entries once a fixed size is reached
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class FixedSizeMap<K, V> extends LinkedHashMap<K, V> {
    private final int maxSize;

    public FixedSizeMap(int size) {
        super(size + 2, 1F);
        this.maxSize = size;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
