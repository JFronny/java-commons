package io.gitlab.jfronny.commons.io;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.util.*;

public class MultiAccessFileSystem implements Closeable {
    @Contract(value = "_ -> new", pure = true)
    public static @NotNull MultiAccessFileSystem wrap(FileSystem delegate) {
        return new MultiAccessFileSystem(delegate);
    }

    @Contract("_ -> new")
    public static @NotNull MultiAccessFileSystem create(URI uri) throws IOException {
        return wrap(FileSystems.getFileSystem(uri));
    }

    @Contract("_, _ -> new")
    public static @NotNull MultiAccessFileSystem create(URI uri, ClassLoader loader) throws IOException {
        return wrap(FileSystems.newFileSystem(uri, Map.of(), loader));
    }

    @Contract("_, _ -> new")
    public static @NotNull MultiAccessFileSystem create(URI uri, Map<String, ?> env) throws IOException {
        return wrap(FileSystems.newFileSystem(uri, env));
    }

    @Contract("_, _, _ -> new")
    public static @NotNull MultiAccessFileSystem create(URI uri, Map<String, ?> env, ClassLoader loader) throws IOException {
        return wrap(FileSystems.newFileSystem(uri, env, loader));
    }

    @Contract("_ -> new")
    public static @NotNull MultiAccessFileSystem create(Path path) throws IOException {
        return wrap(FileSystems.newFileSystem(path));
    }

    @Contract("_, _ -> new")
    public static @NotNull MultiAccessFileSystem create(Path path, ClassLoader loader) throws IOException {
        return wrap(FileSystems.newFileSystem(path, loader));
    }

    @Contract("_, _ -> new")
    public static @NotNull MultiAccessFileSystem create(Path path, Map<String, ?> env) throws IOException {
        return wrap(FileSystems.newFileSystem(path, env));
    }

    @Contract("_, _, _ -> new")
    public static @NotNull MultiAccessFileSystem create(Path path, Map<String, ?> env, ClassLoader loader) throws IOException {
        return wrap(FileSystems.newFileSystem(path, env, loader));
    }

    private final FileSystem delegate;
    private final List<Lens> lenses = new LinkedList<>();
    private boolean closed = false;

    private MultiAccessFileSystem(FileSystem delegate) {
        this.delegate = delegate;
    }

    @Override
    public synchronized void close() throws IOException {
        if (!lenses.isEmpty()) throw new IOException("Attempted to close multi-access file system with live lenses");
        closed = true;
        delegate.close();
    }

    public boolean isClosed() {
        return closed;
    }

    public synchronized FileSystem createLens() throws IOException {
        if (closed) throw new ClosedFileSystemException();
        Lens lens = new Lens();
        lenses.add(lens);
        return lens;
    }

    private class Lens extends FileSystem {
        @Override
        public FileSystemProvider provider() {
            return delegate.provider();
        }

        @Override
        public void close() throws IOException {
            lenses.remove(this);
            if (lenses.isEmpty()) MultiAccessFileSystem.this.close();
        }

        @Override
        public boolean isOpen() {
            return delegate.isOpen();
        }

        @Override
        public boolean isReadOnly() {
            return delegate.isReadOnly();
        }

        @Override
        public String getSeparator() {
            return delegate.getSeparator();
        }

        @Override
        public Iterable<Path> getRootDirectories() {
            return delegate.getRootDirectories();
        }

        @Override
        public Iterable<FileStore> getFileStores() {
            return delegate.getFileStores();
        }

        @Override
        public Set<String> supportedFileAttributeViews() {
            return delegate.supportedFileAttributeViews();
        }

        @Override
        public @NotNull Path getPath(String s, String... strings) {
            return delegate.getPath(s, strings);
        }

        @Override
        public PathMatcher getPathMatcher(String s) {
            return delegate.getPathMatcher(s);
        }

        @Override
        public UserPrincipalLookupService getUserPrincipalLookupService() {
            return delegate.getUserPrincipalLookupService();
        }

        @Override
        public WatchService newWatchService() throws IOException {
            return delegate.newWatchService();
        }
    }
}
