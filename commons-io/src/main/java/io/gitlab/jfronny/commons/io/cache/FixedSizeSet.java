package io.gitlab.jfronny.commons.io.cache;

import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * An ordered set that discards earlier entries once a fixed size limit is reached
 *
 * @param <E> the type of elements in this set
 */
public class FixedSizeSet<E> implements Set<E> {
    private final List<E> backing;
    private final int size;

    public FixedSizeSet(int size) {
        this.backing = new LinkedList<>();
        this.size = size;
    }

    @Override
    public int size() {
        return backing.size();
    }

    @Override
    public boolean isEmpty() {
        return backing.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return backing.contains(o);
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return backing.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return backing.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] ts) {
        return backing.toArray(ts);
    }

    @Override
    public boolean add(E e) {
        synchronized (backing) {
            if (backing.contains(e)) return false;
            if (size() >= size) backing.remove(0);
            return backing.add(e);
        }
    }

    @Override
    public boolean remove(Object o) {
        return backing.remove(o);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> collection) {
        for (Object o : Objects.requireNonNull(collection)) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> collection) {
        synchronized (backing) {
            boolean result = false;
            for (E e : collection) {
                result |= add(e);
            }
            return result;
        }
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> collection) {
        return backing.retainAll(collection);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> collection) {
        return backing.removeAll(collection);
    }

    @Override
    public void clear() {
        backing.clear();
    }

    @Override
    public Spliterator<E> spliterator() {
        return backing.spliterator();
    }

    @Override
    public String toString() {
        return backing.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Set s)) return false;
        if (s.size() != this.size()) return false;
        return this.containsAll(s);
    }

    @Override
    public int hashCode() {
        return backing.hashCode();
    }
}
