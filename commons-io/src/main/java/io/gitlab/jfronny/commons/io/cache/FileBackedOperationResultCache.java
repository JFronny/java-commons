package io.gitlab.jfronny.commons.io.cache;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ConcurrentHashMap;

public class FileBackedOperationResultCache {
    private final ConcurrentHashMap<String, Object> container = new ConcurrentHashMap<>();
    private final Path cacheDir;

    public FileBackedOperationResultCache(Path cacheDir) {
        this.cacheDir = cacheDir;
    }

    public void remove(String key) throws IOException {
        container.remove(key);
        Files.delete(cacheDir.resolve(key));
    }

    public void clear() throws IOException {
        container.clear();
        JFiles.clearDirectory(cacheDir);
    }

    public <T, TEx extends Throwable> T get(String key, ThrowingSupplier<T, TEx> builder, Type klazz) throws IOException, TEx {
        if (!container.containsKey(key)) {
            Path cd = cacheDir.resolve(key);
            if (Files.exists(cd)) {
                T value = JFiles.readObject(cd, klazz);
                JFiles.writeObject(cd, value);
                container.put(key, value);
            } else container.put(key, builder.get());
        }
        //noinspection unchecked
        return (T) container.get(key);
    }
}
