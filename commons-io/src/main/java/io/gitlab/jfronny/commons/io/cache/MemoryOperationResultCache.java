package io.gitlab.jfronny.commons.io.cache;

import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

import java.util.HashMap;
import java.util.Map;

/**
 * Stores results of operations by keys
 *
 * @param <K> The key of the operation
 * @param <V> The result of the operation
 */
public class MemoryOperationResultCache<K, V> {
    private final Map<K, V> container;

    /**
     * Create a cache with a fixed size. Once it is reached, old entries will be discarded
     *
     * @param size The size to allocate
     */
    public MemoryOperationResultCache(int size) {
        this.container = new FixedSizeMap<>(size);
    }

    /**
     * Create a cache with an unlimited size. Old entries will never be discarded. May fill up memory
     */
    public MemoryOperationResultCache() {
        this.container = new HashMap<>();
    }

    /**
     * Get or set a value by its key. If a builder for the key is cached, the cached value will be returned instead.
     *
     * @param key     The key to cache the value for
     * @param builder A function to build a value should it not exist in the cache
     * @param <TEx>   An exception the builder may throw
     * @return The result, either loaded from the cache or generated from the builder
     * @throws TEx The builder threw an exception
     */
    public synchronized <TEx extends Throwable> V get(K key, ThrowingSupplier<V, TEx> builder) throws TEx {
        if (!container.containsKey(key)) container.put(key, builder.get());
        return container.get(key);
    }
}
