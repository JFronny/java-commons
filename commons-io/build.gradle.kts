import io.gitlab.jfronny.scripts.*

plugins {
    commons.library
}

dependencies {
    implementation(projects.commons)
    implementation(projects.commonsSerialize)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "io.gitlab.jfronny"
            artifactId = "commons-io"

            from(components["java"])
        }
    }
}

tasks.javadoc {
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons/$version/raw", projects.commons)
    linksOffline("https://maven.frohnmeyer-wds.de/javadoc/artifacts/io/gitlab/jfronny/commons-serialize/$version/raw", projects.commonsSerialize)
}
